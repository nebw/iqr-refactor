export IQR_TEMP=iqr-devel
export ZIP_EXE=c:/PROGRA~2/GnuWin32/bin/zip.exe

export ARCHIVE=iqr-devel-2.5.zip

mkdir -p $IQR_TEMP/include/iqr/Common/Helper/
mkdir -p $IQR_TEMP/include/iqr/Common/Item/
mkdir -p $IQR_TEMP/doc/examples/Modules
mkdir -p $IQR_TEMP/doc/examples/Neurons

cp  ../src-qt4/Common/Helper/iqrUtils.h             $IQR_TEMP/include/iqr/Common/Helper/
cp  ../src-qt4/Common/Item/boolParameter.hpp        $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/colorParameter.hpp       $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/doubleParameter.hpp      $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/indirectPtr.hpp          $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/intParameter.hpp         $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/item.hpp                 $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/module.hpp               $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/moduleIcon.hpp           $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/namedPtr.hpp             $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/neuron.hpp               $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/optionsParameter.hpp     $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/parameter.hpp            $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/parameterList.hpp        $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/pathParameter.hpp        $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/pattern.hpp              $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/ringBuffer.hpp           $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/stateArray.hpp           $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/stateVariable.hpp        $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/stateVariableHolder.hpp  $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/stateVariableList.hpp    $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/stringParameter.hpp      $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/synapse.hpp              $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/threadModule.hpp         $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/xRef.hpp                 $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/xRefHolder.hpp           $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt4/Common/Item/xRefList.hpp             $IQR_TEMP/include/iqr/Common/Item/
cp  ../src-qt5/doc/iqrUserdefinedTypes.pdf                  $IQR_TEMP/doc/


cp  ../distro/resources/UserDefFramework/examples/Modules/* $IQR_TEMP/doc/examples/Modules
cp  ../distro/resources/UserDefFramework/examples/Neurons/* $IQR_TEMP/doc/examples/Neurons

#creating the archive
$ZIP_EXE -r $ARCHIVE $IQR_TEMP

# clean
rm -rf $IQR_TEMP

