IF(EXISTS "${PROJECT_SOURCE_DIR}/CMakeScripts/extractLinuxDistroInfo.sh") 
  EXEC_PROGRAM(bash
    ARGS "${PROJECT_SOURCE_DIR}/CMakeScripts/extractLinuxDistroInfo.sh"
    OUTPUT_VARIABLE LINUX_DISTRO
    )
  MESSAGE( STATUS "Linux Distribution: " ${LINUX_DISTRO} )
ELSE(EXISTS "${PROJECT_SOURCE_DIR}/CMakeScripts/extractLinuxDistroInfo.sh") 
  MESSAGE( STATUS "Skipping Linux Distribution Check" )
ENDIF(EXISTS "${PROJECT_SOURCE_DIR}/CMakeScripts/extractLinuxDistroInfo.sh") 