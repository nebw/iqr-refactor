#ifndef CLSMYENTITYRESOLVER_H
#define CLSMYENTITYRESOLVER_H

#include <QtXml>

#include "iqr421_System_050602040805040202_dtd.cpp"

namespace iqrcommon {

class MyResolver : public QXmlEntityResolver {
public:
  virtual QString errorString() const override { return QString(); }

  virtual bool resolveEntity(const QString &publicId, const QString &systemId,
                             QXmlInputSource *&ret) override {
    if (publicId == "iqrSystem.dtd" && systemId == "-//INI/iqr421") {
      ret = new QXmlInputSource();
      ret->setData(QByteArray(iqrSystemDTD_data));

      return true;
    }
    return false;
  }
} l;
}

#endif

/// Local Variables:
/// mode: c++
/// End:
