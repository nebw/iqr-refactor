#ifndef CLSMYERRORHANDLER_H
#define CLSMYERRORHANDLER_H

#include <QtXml>

#include <iostream>

using namespace std;

namespace iqrcommon {

class ClsMyErrorHandler : public ErrorHandler {
public:
  ClsMyErrorHandler() {}

  ~ClsMyErrorHandler() {}

  void warning(const SAXParseException &toCatch);
  void error(const SAXParseException &toCatch);
  void fatalError(const SAXParseException &toCatch);
  void resetErrors();
};
};

#endif

/// Local Variables:
/// mode: c++
/// End:
