file(GLOB src "*.cpp" "*.h" "*.hpp")

# TODO: doesn't compile and not in use -> remove?
list(REMOVE_ITEM src "${CMAKE_CURRENT_SOURCE_DIR}/ClsMyErrorHandler.cpp")

add_library(CommonParser STATIC ${src})
