/****************************************************************************
 **
 ** $Author: Mark Blanchard
 **
 *****************************************************************************/

#ifndef DOUBLEPARAMETER_HPP
#define DOUBLEPARAMETER_HPP

// #include <string>

#include <string>

#include "parameter.hpp"

namespace iqrcommon {

//     using std::string;

class ClsDoubleParameter : public ClsParameter {
public:
  ClsDoubleParameter(string _strName, string _strLabel,
                     string _strDescription = "", string _strCategory = "");
  ClsDoubleParameter(string _strName, string _strLabel, double _dValue,
                     double _dMinimum, double _dMaximum, int _iPrecision,
                     string _strDescription = "", string _strCategory = "");

  ClsDoubleParameter(const ClsDoubleParameter &_source);

  ~ClsDoubleParameter();

  ClsDoubleParameter &operator=(const ClsDoubleParameter &_source);

  double getValue() const { return dValue; }
  string getValueAsString() const override;
  void setValue(double _dValue);
  void setValueFromString(string _strValue) override;

  ClsDoubleParameter *makeCopy() const override;

  double getMinimum() const { return dMinimum; }
  double getMaximum() const { return dMaximum; }

  int getPrecision() const { return iPrecision; }
  void setPrecision(int _iPrecision) { iPrecision = _iPrecision; }

  string getMinSetter() const { return strMinSetter; }
  string getMaxSetter() const { return strMaxSetter; }
  void setMinSetter(ClsDoubleParameter *_pMinSetter);
  void setMaxSetter(ClsDoubleParameter *_pMaxSetter);

private:
  // Disable default constructor.
  ClsDoubleParameter();

  double DEFAULT_VALUE() const {
    return 0.0;
  };
  double DEFAULT_MINIMUM() const {
    return 0.0;
  };
  double DEFAULT_MAXIMUM() const {
    return 1.0;
  };

  // Precision specifies number of decimal places.
  static const int DEFAULT_PRECISION = 3;

  double dValue, dMinimum, dMaximum;
  int iPrecision;

  string strMinSetter, strMaxSetter;
};
};

#endif
