/****************************************************************************
 ** $Filename: threadModule.hpp
 **
 ** $Author: Ulysses Bernardet
 **
 *****************************************************************************/

#ifndef THREADMODULE_HPP
#define THREADMODULE_HPP

#include <pthread.h>
#include <QMutex>

#include <Common/Item/module.hpp>

using namespace std;

namespace iqrcommon {

class ClsThreadModule : public ClsModule {
public:
  ~ClsThreadModule() {
    cleanup();
  }

  void stop() override;
  void join();

  virtual void start() override;
  static void *run(void *arg);

  void setMutex(QMutex *_qmutexThread) { qmutexThread = _qmutexThread; }

protected:
  int flag;
  pthread_t tid;
  QMutex *qmutexThread;
};
}

#endif
