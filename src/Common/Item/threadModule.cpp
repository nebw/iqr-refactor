/****************************************************************************
 ** $Filename: threadModule.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 *****************************************************************************/

#include "threadModule.hpp"

using namespace std;

void *iqrcommon::ClsThreadModule::run(void *arg) {
  ClsThreadModule *th = (ClsThreadModule *)arg;
  th->flag = 1;

  while (th->flag) {
    th->update();
  }
  return nullptr;
}

void iqrcommon::ClsThreadModule::start() {
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  pthread_create(&tid, &attr, run, this);
}

void iqrcommon::ClsThreadModule::stop() {
  flag = 0;
#ifndef _WINDOWS // this check does not work under windows....
  if (tid) {
#endif
    pthread_join(tid, nullptr);
#ifndef _WINDOWS
  }
#endif
};

void iqrcommon::ClsThreadModule::join() {
#ifndef _WINDOWS // this check does not work under windows....
  if (tid) {
#endif
    pthread_join(tid, nullptr);
#ifndef _WINDOWS
  }
#endif
}
