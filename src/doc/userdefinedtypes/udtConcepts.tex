\chapter{Concepts}
\label{cha:concepts}


This document gives an overview on how to write your own neurons, synapses, and
modules.

The first part will explain the basic concepts, the second part will provide walk-through example
implementations, and the appendix lists the definition of the relevant member variables and
functions for the different types.

%\rework{Concepts that are shared by all types will be introduced next, those only applying to a
%certain type, as the type is explained.}

\hint \iqr{} does not make a distinction between types that are defined by the user, and those
that come with the installation; both are implemented in the same way. 

The base-classes for all three types, neurons, synapses, and modules, are derived from
\code{ClsItem} (\figref{ClsItem-inherit}). 

\begin{figure}[!h]
  \begin{center}
    \includegraphics*[width=\textwidth, clip=true]{ClsItem-inherit}
    \caption{Class diagram for types}
    \label{ClsItem-inherit}
  \end{center}
\end{figure}

% this actually just does not matter here...
The classes derived from \code{ClsItem} are in turn the parent classes for the specific
types; a specific neuron type will be derived from \code{ClsNeuron}, a specific synapse
type from \code{ClsSynapse}. In case of modules, a distinction is made between threaded
and non-threaded modules. Modules that are not threaded are derived from \code{ClsModule},
threaded ones from \code{ClsThreadModule}.

The inheritance schema defines the framework, in which 
\begin{itemize}
\item parameters are defined
\item data is represented and accessed
\item input, output, and feedback is added
\end{itemize}

\attention All types are defined in the namespace\index{namespace} \code{iqrcommon}.

\section{Object model}
\label{sec:object-model}
To write user-defined types, it is vital to understand the object model \iqr{} uses. 
Figure \ref{object-model} shows a simplified version of the class diagram for an \iqr{}
system. 

The lines connecting the objects represent the relation between the objects. 
The arrow heads and tails have a specific meaning:
 \includegraphics*[scale=.7, clip=true]{legend}
stands for a relation where \textbf{A} contains \textbf{B}. % i.e. \textbf{B} is part of \textbf{A}. 

\begin{figure}[!h]
  \begin{center}
    \includegraphics*[width=.8\textwidth, clip=true]{object-model}
    \caption{Simplified class diagram of an \protect\iqr{} system}
    \label{object-model}
  \end{center}
\end{figure}
%points:
%- single instance of neurons, synapses -> access to data structure 
%- (one module per process)

The multiplicity\index{multiplicity} relation between the objects is denoted by the numbers at the start and
the end of the line. E.g. a system can have $0$ or more processes ($0\ldots*$ near the arrow
head). A process in turn can only exist in one system ($1$ near the $\Diamond$).

On the phenomenological level, to a user, a group consists of a $n\ge1$ neuron(s), and a connection
of $n\ge1$ synapse(s). In terms of the implementation though, as can be seen in \figref{object-model}, 
%The non trivial point to make is that 
a group contains only \textbf{one instance} of a neuron object, and a connection only
\textbf{one instance} of a synapse object. This is independent of the size of the group or
the number of synapses in a connection.


\section{Data representation}
\label{sec:data-repr-access}

% StateArray
% delays 
% valarrays
% StateVariablePtr
% ClsStateVariable


In the concept of \iqr{}, neurons and synapses do have individual values for parameters
like the membrane potential or weight associated to them. In this
document, type-associated values, that change during the simulation, are referred to as
``states''\index{state}.

There are essentially two ways in which individual value association can be implemented: \bull{} multiple
instantiations of objects with local data storage
(\figref{Instance-States}a), or \bull{} single-instance with states for
individual ``objects'' in vector like structure (\figref{Instance-States}b). 

\begin{figure}[!h]
  \centering
  \subfigure[Multiple instantiations, local storage]{\parbox{.49\textwidth}{\centering\includegraphics*[scale=.5]{MultipleInstance-States}}}
  \subfigure[Single-instance, storage vector]{\parbox{.49\textwidth}{\centering\includegraphics*[scale=.5]{SingleInstance-States}}}
  \caption{}
  \label{Instance-States}
\end{figure}

For reasons of efficiency, \iqr{} uses the single-instance implementation (see also
\figref{object-model}). For authors of types, the drawback is a somewhat more demanding
handling of states and update functions. 
To compensate for this, great care was taken to provide an easy to use framework for writing types.


\subsection{The \code{StateArray}}
\label{sec:data-representation}
The data structure used to store states of neurons and synapses is the
\code{StateArray}. The structure of a \code{StateArray} is depicted in
\figref{StateArray_annotated}. It is used like a two-dimensional matrix, with the
first dimension being the time and the second dimension the index of the individual item (neuron or synapse).
Hence \code{StateArray}$[t][n]$ is the value of item $n$ at time $t$.

\begin{figure}[!h]
  \begin{center}
    \includegraphics*[scale=.8, clip=true]{StateArray_annotated}
    \caption{Concept of \code{StateArray}}
    \label{StateArray_annotated}
  \end{center}
\end{figure}

Internally  \code{StateArrays} make use of the \textbf{valarray}\index{valarray} class from the standard
C++ library. 

To extract a valarray containing the values for all the items at time $t-d$ use 
\begin{lstlisting}{numbers=none}
  std::valarray<double> va(n);
  va = StateArray[d];
\end{lstlisting}
The convention is that \code{StateArray[0]} is the current valarray, whereas
\code{StateArray[2]} denotes the valarray that is 2 simulation cycles back in time.

%\begin{lstlisting}{numbers=none}
%std::valarry<double> va1(3); std::valarry<double> va2(3); 
%\end{lstlisting}
%
%\begin{lstlisting}{numbers=none}
%va1 = 4 * va2;
%\end{lstlisting}
%is equivalent to 
%\begin{lstlisting}{numbers=none}
%va1[0] = 4 * va2[0];
%va1[1] = 4 * va2[1];
%va1[2] = 4 * va2[2];
%\end{lstlisting}

Valarrays provide a compact syntax for operations on each element in the vector, the
possibility to apply masks to select specific elements, and a couple of other useful
features. A good reference on the topic is \cite{Josuttis:STL:1999}.

\subsection{States in neurons and synapses}
\label{sec:stat-neur-synaps}
The subsequently discussed functions are the main functions used to deal with
states. Additional functions for the individual types are listed in the appendix.

%\item{what to do with this??}
%\begin{figure}[!h]
%  \begin{center}
%    \includegraphics*[scale=.7, clip=true]{neuron-synapse_states}
%    \caption{Simplified class diagram of an \iqr{} system}
%    \label{neuron-synapse_states}
%  \end{center}
%\end{figure}

Adding an internal state\index{internal state} to neurons and synapses is done via the
wrapper class\\
 \code{ClsStateVariable}\index{ClsStateVariable}:
\begin{lstlisting}
  ClsStateVariable *pStateVariable;
  pStateVariable = addStateVariable("st" /*identifier*/, 
                                    "A state variable" /*visible name*/);
\end{lstlisting}

To manipulate the state we first extract the \code{StateArray}, where after we can
address and change the state as described above:
\begin{lstlisting}
  StateArray &sa = pStateVariable->getStateArray();
  sa[0][1] = .5;
\end{lstlisting}

The output state is a special state for neurons and synapses. For most neurons the 
output state will be the activity. The neuronal output state is used as input to synapses
and modules. For synapses, the output state acts as an input to neurons. A neuron or synapse can only
have one output state. 

An output state is defined by means of the \code{addOutputState(...)} function:

\begin{lstlisting}
  ClsStateVariable *pActivity;       
  pActivity  = addOutputState("act" /*identifier*/, 
                              "Activity" /*name*/);
\end{lstlisting}

\hint States created in this framework are accessible in the GUI for graphing and saving.

The full description of these functions can be found in documentation for
\code{ClsNeuron} (\secref{classiqrcommon_1_1ClsNeuron}) and \code{ClsSynapse} (
\secref{classiqrcommon_1_1ClsSynapse}).


\subsubsection{State related functions in neurons}
\label{sec:state-relat-funct-neur}

The base-class for the neuron type automatically creates three input
states: the excitatory, inhibitory, and modulatory. Therefore, you do
not create any input state when implementing a neuron type. To access
the existing ones, use the following functions, which return a reference to a \code{StateArray}:
\begin{lstlisting}
  StateArray &excitation = getExcitatoryInput();
  StateArray &inhibition = getInhibitoryInput();
  StateArray &modulation = getModulatoryInput();
\end{lstlisting}
The user is free as to which of these functions to use.
  
\subsubsection{State related functions in synapses}
\label{sec:state-relat-funct-syn}

Synapses also must have access to the input state\index{input state}, which is actually the
output state of the \textbf{pre-synaptic} neuron. \attention The
implementation of the pre-synaptic neuron type thus defines the input
state of the synapse.

To access the input state the function \code{getInputState()} is employed, which returns
a pointer to a \code{ClsStateVariable}:
\begin{lstlisting}
  StateArray &synIn    = getInputState()->getStateArray();
\end{lstlisting}

To use feedback\index{feedback} from the \textbf{post-synaptic} neuron use the
\code{addFeedbackInput()} function:
\begin{lstlisting}
  ClsStateVariable *pApicalShunt;
  pApicalShunt = addFeedbackInput("apicalShunt" /*identifier*/, 
                                  "Apical dendrite shunt" /*description*/);
  StateArray &shunt    = pApicalShunt->getStateArray();
\end{lstlisting}

\subsection{Using history\index{history}}
\label{sec:using-history}
To be able to make use of previous states, i.e. to use the history of a state, you
explicitly have to declare a history length when you create the \code{StateArray} using
the \code{addStateVariable(...)}, \code{addOutputState(...)}, or
\code{addFeedbackInput(...)} functions.  The reference for the syntax is given in the
appendix (
neurons: 
\ref{classiqrcommon_1_1ClsNeuron_b1}, %
%\ref{classiqrcommon_1_1ClsNeuron_b0}, %
synapses: 
\ref{classiqrcommon_1_1ClsSynapse_b2}%
%\ref{classiqrcommon_1_1ClsSynapse_b1},% 
%\ref{classiqrcommon_1_1ClsSynapse_b0}%
)


\subsection{Modules and access to states}
\label{sec:modul-access-states}

%\issue{what to do with this??}
%\begin{figure}[!h]
%  \begin{center}
%    \includegraphics*[scale=.7, clip=true]{module_states}
%    \caption{Simplified class diagram of an \iqr{} system}
%    \label{module_states}
%  \end{center}
%\end{figure}

Unlike neurons and synapses, modules do not need to use internal states to represent
multiple elements. Modules require to read states from neurons, and feed data into states
of neurons. The functions provided for this purpose are using a naming schema that is
module centered: data that is read from the group is referred to as ``input from group'',
data fed into a group is pointed to as ``output to group''. The references for theses
states will be set in the module properties of the process (see \iqr{} Manual)

Defining the output to a group is done with the \code{addOutputToGroup(...)} function:
\begin{lstlisting}
  ClsStateVariable* outputStateVariable;
  outputStateVariable = addOutputToGroup("output0" /*identifier*/, 
                                         "Output 0 description" /*description*/);
  StateArray &clsStateArrayOut = outputStateVariable->getStateArray();
\end{lstlisting}

Specifying input from a group into a module employs a slightly different syntax using the\\
\code{StateVariablePtr} class:
\begin{lstlisting}
  StateVariablePtr* inputStateVariablePtr;
  inputStateVariablePtr = addInputFromGroup("input0" /*identifier*/, 
                                            "Input 0 description" /*description*/);
  StateArray &clsStateArrayInput = 
  inputStateVariablePtr->getTarget()->getStateArray();
\end{lstlisting}

Once the \code{StateArray} references are created, the data can be manipulated as
described above. 

\attention Please do not write into the input from group \code{StateArray}. The result
might be catastrophic.


\hint When adding output to groups, or input from groups, no size
constraint for the state array can be defined. It is therefore advisable to either write
the module in a way that it can cope with arbitrary sizes of state arrays, or to throw
a \code{ModuleError} (see \secref{sec:errors}) in the \code{init()} function if the
size criteria is not met.


\subsubsection{Access protection}
\label{sec:threaded-modules-1}
If a module is threaded, i.e. the access to the input and output states is not
synchronized with the rest of the simulation, the read and write operations need to be
protected by a mutex\index{mutex}. The \code{ClsThreadedModule} provides the
\code{qmutexThread} member class for this purpose. The procedure is to lock the mutex,
to perform any read and write operations, and then to unlock the mutex:

\begin{lstlisting}
  qmutexThread->lock(); 
  /*
    any operations that accesses the
    input or the output state
 */
  qmutexThread->unlock(); 
\end{lstlisting}

As the locked mutex is
impairing the main simulation loop, as few as possible operations should be performed
between locking and unlocking. 

\attention Failure to properly implement the locking, access, and unlocking schema will
eventually lead to a crash of \iqr{}\@.


\section{Defining parameters}
\label{sec:defining-parameters}
The functions inherited from \code{ClsItem} define the framework for adding parameters
to the type. Parameters defined within this framework are accessible through
the GUI and saved in the system file.  To this end, \iqr{} defines wrapper classes for
parameters of type \code{double}, \code{int}, \code{bool}, \code{string}, and
\code{options} (list of options).

\subsection*{Usage}
The best way to use these parameters, is to define a pointer to the desired type in the
header, and to instantiate the parameter-class in the constructor, using the\\
\code{add[Double,Int,Bool,String,Options]Parameter} functions. The value of the parameter
object can be retrieved at run-time by virtue of the \code{getValue()}
function. Examples for the usage are given in sections \ref{sec:neurons}, \ref{sec:synapses},
and \ref{sec:modules}. The
extensive list of these functions is provided in the documentation for the \code{ClsItem}
class in \secref{classiqrcommon_1_1ClsItem}.

%Pointers to parameter objects. The objects are managed by
%the base class, the pointers are only needed to provide
%rapid access to these objects during the performance
%critical update function.


% TO DO \section{Makefiles}
% TO DO \label{sec:makefiles}

\section{Where to store the types}
\label{sec:where-store}
The location where \iqr{} loads types from, is defined in the \iqr{} settings \menu{NeuronPath},
\menu{SynapsePath}, and \menu{ModulePath} (see \iqr{} Manual). 
Best practice is to enable\\
 \menu{Use user defined $nnn$} (where $nnn$ stands for Neuron,
Synapse, or Module), and to store the files in the
location indicated by the \menu{Path to user defined $nnn$}.
As the neurons, synapses, and modules are read from disk when \iqr{} starts up, any changes
to the type, while \iqr{} is running, has no effect; you will have to restart \iqr{} if you
make changes to the types.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "UserdefinedTypes"
%%% End: 
