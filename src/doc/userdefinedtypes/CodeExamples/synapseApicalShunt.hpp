#ifndef SYNAPSEAPICALSHUNT_HPP
#define SYNAPSEAPICALSHUNT_HPP

#include <Common/Item/synapse.hpp>

namespace iqrcommon {

    class ClsSynapseApicalShunt : public ClsSynapse
    {
    public:
	ClsSynapseApicalShunt();

	void update();

    private:
	/* Hide copy constructor. */
	ClsSynapseApicalShunt(const ClsSynapseApicalShunt&);

	/* Feedback input */
	ClsStateVariable *pApicalShunt;

	/* Pointer to output state. */
	ClsStateVariable *pPostsynapticPotential;
    };
}

#endif
