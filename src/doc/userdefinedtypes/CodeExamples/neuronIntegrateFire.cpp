#include "neuronIntegrateFire.hpp"

/* Interface for dynamic loading is built using a macro. */
MAKE_NEURON_DLL_INTERFACE(iqrcommon::ClsNeuronIntegrateFire, 
			  "Integrate & fire")

iqrcommon::ClsNeuronIntegrateFire::ClsNeuronIntegrateFire()
    : ClsNeuron(),
      pVmembrane(0),
      pActivity(0) {    

    pExcGain = addDoubleParameter("excGain", "Excitatory gain",
				  1.0, 0.0, 10.0, 4, 
				  "Gain of excitatory inputs.\n"
				  "The inputs are summed before\n"
				  "being multiplied by this gain.",
				  "Input");
    
    pInhGain = addDoubleParameter("inhGain", "Inhibitory gain",
				  1.0, 0.0, 10.0, 4, 
				  "Gain of inhibitory inputs.\n"
				  "The inputs are summed before\n"
				  "being multiplied by this gain.",
				  "Input");
    
    /* Membrane persistence. */
    pVmPrs = addDoubleParameter("vmPrs", "Membrane persistence",
				0.0, 0.0, 1.0, 4, 
				"Proportion of the membrane potential\n"
				"which remains after one time step\n"
			     "if no input arrives.",
				"Membrane");
    
    pProbability = addDoubleParameter("probability", "Probability",
				      0.0, 0.0, 1.0, 4,
				      "Probability of output occuring\n"
				      "during a single timestep.",
				      "Membrane");

    /* Add state variables. */
    pVmembrane = addStateVariable("vm", "Membrane potential");
    pActivity  = addOutputState("act", "Activity");
}

void
iqrcommon::ClsNeuronIntegrateFire::update() {
    StateArray &excitation = getExcitatoryInput();
    StateArray &inhibition = getInhibitoryInput();
    StateArray &vm         = pVmembrane->getStateArray();
    StateArray &activity   = pActivity->getStateArray();

    double excGain     = pExcGain->getValue();
    double inhGain     = pInhGain->getValue();
    double vmPrs       = pVmPrs->getValue();
    double probability = pProbability->getValue();

    /* Calculate membrane potential */
    vm[0] *= vmPrs;
    vm[0] += excitation[0] * excGain;
    vm[0] -= inhibition[0] * inhGain;
    
    activity.fillProbabilityMask(probability);
    /* All neurons at threshold or above produce a spike. */
    activity[0][vm[0] >= 1.0] = 1.0;
    activity[0][vm[0] <  1.0] = 0.0;
}
