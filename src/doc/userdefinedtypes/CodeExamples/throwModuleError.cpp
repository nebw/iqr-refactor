void 
iqrcommon::ClsModuleTest::init(){
    if(inputStateVariablePtr->getTarget()->getStateArray().getWidth()!=9 ){
	throw ModuleError(string("Module \"") + 
			  label() + 
			  "\": needs 9 cells for output");
    }

    if(outputStateVariable->getStateArray().getWidth()!=10){
	throw ModuleError(string("Module \"") + 
			  label() + 
			  "\": needs 10 cells for input");
    }
}
