#ifndef NEURONINTEGRATEFIRE_HPP
#define NEURONINTEGRATEFIRE_HPP

#include <Common/Item/neuron.hpp>

namespace iqrcommon {

    class ClsNeuronIntegrateFire : public ClsNeuron
    {
    public:	
	ClsNeuronIntegrateFire();

	void update();

    private:
	/* Hide copy constructor. */
	ClsNeuronIntegrateFire(const ClsNeuronIntegrateFire&);

        /* Pointers to parameter objects */
	ClsDoubleParameter *pVmPrs;
	ClsDoubleParameter *pProbability, *pThreshold;

	/* Pointers to state variables.	*/
	ClsStateVariable *pVmembrane, *pActivity;	
    };
}

#endif
