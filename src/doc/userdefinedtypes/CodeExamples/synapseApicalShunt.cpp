#include "synapseApicalShunt.hpp"

/* Interface for dynamic loading is built using a macro. */
MAKE_SYNAPSE_DLL_INTERFACE(iqrcommon::ClsSynapseApicalShunt, 
			  "Apical shunt")

iqrcommon::ClsSynapseApicalShunt::ClsSynapseApicalShunt()
    : ClsSynapse() {    

    /* Add state variables. */
    pPostsynapticPotential = addOutputState("psp", "Postsynaptic potential");

    /* Add feedback input */
    pApicalShunt = addFeedbackInput("apicalShunt", "Apical dendrite shunt");
}

void 
iqrcommon::ClsSynapseApicalShunt::update() {    
    StateArray &synIn    = getInputState()->getStateArray();
    StateArray &shunt    = pApicalShunt->getStateArray();
    StateArray &psp      = pPostsynapticPotential->getStateArray();

    psp[0] = synIn[0] * shunt[0];
}
