#include "moduleTest.hpp"

MAKE_MODULE_DLL_INTERFACE(iqrcommon::ClsModuleTest,
			  "test module 1")

iqrcommon::ClsModuleTest::ClsModuleTest() : 
    ClsModule() {
    pParam = addDoubleParameter("dummy Par0",
		       "short description",
		       0.0, 0.0,
		       1.0, 3,
		       "Longer description",
		       "Params");
   
    /* add input from group */
    inputStateVariablePtr = addInputFromGroup("_nameIFG0", "IFG 0");
    
    /* add output to group */
    outputStateVariable = addOutputToGroup("_nameOTG0", "OTG 0");
}


void 
iqrcommon::ClsModuleTest::init(){
    /* open any devices here */
};

void 
iqrcommon::ClsModuleTest::update(){
    /* input from group */
    StateArray &clsStateArrayInput = 
	inputStateVariablePtr->getTarget()->getStateArray();

    /* output to group */
    StateArray &clsStateArrayOut = outputStateVariable->getStateArray();

    for(unsigned int ii=0; ii<clsStateArrayOut.getWidth(); ii++){
	clsStateArrayOut[0][ii] = ii;
    }
};

void 
iqrcommon::ClsModuleTest::cleanup(){
    /* close any devices here */
};



