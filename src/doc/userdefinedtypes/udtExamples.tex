\chapter{Example implementations}
\label{cha:example-impl}

\section{Neurons}
\label{sec:neurons}

\subsection*{Header}
Let us first have a look at the header file for a specific neuron
type. As you can see in
\lstref{neuronIntegrateFire-hpp}, the only functions that must be reimplemented are the
constructor [11] and \code{update()} [13]. Hiding the copy-constructor [17] is an
optional safety measure. Lines [20-21] declare pointers to parameter objects. Line [24] 
declares the two  states of the neuron.  

\lstinputlisting[frame=single, caption={Neuron header example},label=neuronIntegrateFire-hpp, numbers=left]{CodeExamples/neuronIntegrateFire.hpp}

\subsection*{Source}
Next we'll walk through the implementation of the neuron, which is shown in
\lstref{neuronIntegrateFire-cpp}. Line [4-5] defines the precompile statement that \iqr{}
uses to identify the type of neuron (see \secref{neuron_8hpp_a0}). In the constructor we
reset the two \code{StateVariables} [9,10]. On line [12-38] we instantiate the parameter
objects (see \secref{classiqrcommon_1_1ClsItem}), and at the end of the constructor we
instantiate one internal state [41] with \code{addStateVariable(...)}, and the output
state [42] with \code{addOutputState(...)}. \attention As for all types, the constructor
is only called once, when \iqr{} starts, or the type is changed. The constructor is
\textbf{not} called before each start of the simulation.

The other function being implemented is \code{update()} [46].
Firstly, we get a reference to the \code{StateArray} for the excitatory and inhibitory inputs
[47-48] (see \secref{sec:state-relat-funct-neur}).

For clarity, we create a local reference to the state arrays
[49-50]. Thus, the state array pointers need only be dereferenced
once, which enhances performance.

For ease of use the parameter values can be extracted from parameter objects [52-55]. On
line [58-60] we update the internal state, and the output state [64-65].
The calculation of the output state may seem strange, but becomes
clearer when taking into account that \code{StateArray[0]} returns a \code{valarray}. The operation performed
here is referred to as ``subset masking'' \cite{Josuttis:STL:1999}.

%For details on the syntax refer to \secref{sec:data-repr-access}.

\lstinputlisting[frame=single, caption={Neuron code example}, label=neuronIntegrateFire-cpp, numbers=left]{CodeExamples/neuronIntegrateFire.cpp}

\clearpage
\section{Synapses}
\label{sec:synapses}

\subsection*{Header}
The header file for the synapse shown in \lstref{synapseApicalShunt-hpp} is very similar
to the one for the neuron. The major difference lies in the definition
of a state variable that will be
used for feedback input [20].

%\attention The constructor is only called once, when \iqr{} starts.

\lstinputlisting[frame=single, caption={Synapse header example}, label=synapseApicalShunt-hpp, numbers=left]{CodeExamples/synapseApicalShunt.hpp}

\subsection*{Source}
The source code for the synapse is shown in \lstref{synapseApicalShunt-cpp}. The
precompile statement [4-5] at the beginning of the file identifies the synapse type. In
the constructor [7] we define the output state for the synapse
[10]. In deviation to neurons, a definition of a feedback input [14] using
\code{addFeedbackInput(...)} is introduced. The remains of the synapse code are essentially the same
as for the neuron explained above.

\lstinputlisting[frame=single, caption={Synapse code example}, label=synapseApicalShunt-cpp, numbers=left]{CodeExamples/synapseApicalShunt.cpp}

\clearpage
\section{Modules}
\label{sec:modules}

\subsection*{Header}
Listing \ref{moduleTest-hpp} shows the header file for a module. As for the neurons and
the synapses, the constructor for the module is only called once during start-up of
\iqr{}\@, or if the module type is changed. The constructor is \textbf{not} called before each
start of the simulation. During the simulation \iqr{} will call the \code{update()} function of the
module at every simulation cycle. 

During the process of starting the simulation, \code{init()} is called, at the end of
the simulation \code{cleanup()}. Any opening of files and devices should therefore be put
in \code{init()}, and not in the constructor. It is crucial to the working of the
module, that \code{cleanup()} resets the module to a state in which \code{init()}
can be called safely again.  \code{cleanup()} must hence close any files and devices
that were opened in \code{init()}.  

Modules can receive information from group output state. This is achieved with a\\
\code{StateVariablePtr} as defined on line [21].
\lstinputlisting[numbers=left, frame=single, caption={Module header example}, label=moduleTest-hpp, numbers=left]{CodeExamples/moduleTest.hpp}

\subsection*{Source}
In the implementation of the module (\lstref{moduleTest-cpp}) we first define the
precompile statement [3-4] to identify the module vis-\`a-vis \iqr{}\@.  As seen
previously, a parameter is added [8-13] in the constructor.  Using the
function \code{addInputFromGroup(...)}, which returns a pointer to a\\
\code{StateVariablePtr}, we define one input from a group, and via
\code{addOutputToGroup(...)} one output to a group.

In the \code{update()} function starting on line [28], we access the input state array with\\
\code{getTarget()->getStateArray()} [31], and the output with \code{getStateArray()} [35].



\lstinputlisting[numbers=left, frame=single, caption={Module code example}, label=moduleTest-cpp, numbers=left]{CodeExamples/moduleTest.cpp}

\subsection{Threaded modules}

\label{sec:threaded-modules}
Threaded modules are derived from the \code{ClsThreadModule} class, as shown in
the code fragment in \lstref{moduleTTest-hpp}.
\lstinputlisting[frame=single, caption={Threaded module header fragment}, label=moduleTTest-hpp, numbers=left]{CodeExamples/moduleTTest.hpp}

The main difference in comparison with a non-threaded module is the protection of the
access to the input and output data structures by means of a mutex as shown in
\lstref{moduleTTest-cpp}. On line [3] we lock the mutex, then access the data structure,
and unlock the mutex, when done [8]

\lstinputlisting[frame=single, caption={Threaded module \code{update()} function}, label=moduleTTest-cpp, numbers=left]{CodeExamples/moduleTTest.cpp}

\subsection{Module errors}
\label{sec:errors}
To have a standardized way of coping with errors occurring in modules the
\code{ModuleError} class is used. Listing \ref{throwModuleError-cpp} shows a possible
application of the error class for checking the size of the input and output states.

\lstinputlisting[numbers=left, frame=single, caption={Throwing a \code{ModuleError} code example}, label=throwModuleError-cpp, numbers=left]{CodeExamples/throwModuleError.cpp}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "UserdefinedTypes"
%%% End: 
