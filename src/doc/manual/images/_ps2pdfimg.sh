#!/bin/bash

for file in `ls *.ps`; 
  do 
  target=`echo  $file | sed "s/\.ps/\.pdf/"`;

  if test -r $target
      then
      echo "   $target already exists; skipping";
  else
      echo "*converting $file to $target";
      ps2epsi $file .temp.epsi;
      epstopdf .temp.epsi --outfile=$target;
#      echo $file $taget;
  fi
done
