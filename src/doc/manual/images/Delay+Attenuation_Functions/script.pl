#!/usr/bin/perl
use FileHandle;
GPL_PIPE -> autoflush();


open (GPL_PIPE,'| gnuplot');



#set term postscript


print GPL_PIPE "set grid\n";
print GPL_PIPE "set xrange [0:10]\n";
print GPL_PIPE "set key bottom right spacing 3 box. \n";
print GPL_PIPE "set xlabel \"Distance\"\n";






print GPL_PIPE "set term postscript  enhanced  color  lw 5. 20\n";


$bDelayFunction = 0;


$gaussian = -1;
$linear = -1;
$block = -1;

$type = "";
if($bDelayFunction){
    print GPL_PIPE "set yrange [-.1:5.1]\n";
    $fMax = 5.;
    $type = "Delay";
    print "$type\n";
} else {
    print GPL_PIPE "set yrange [-.02:1.02]\n";
    $fMax = 1.;
    $type = "Attenutation";
    print "$type\n";
}

$fMin = 0.;

print GPL_PIPE "fMax = $fMax\n";
print GPL_PIPE "fMin = $fMinin.\n";

print GPL_PIPE "set ylabel \"$type\"\n";


if($gaussian){
#min, max, sigma
    $func = "Gaussian";
    $outfile = $type . "Function" . $func . ".ps";
    print GPL_PIPE "set output \"$outfile\"\n";    
    print GPL_PIPE "set title \"$type function $func, min=$fMin, max=$fMax\"\n";
    print "$outfile\n";
    $fSigma = 0.5;
    $counter= 0;
    print GPL_PIPE "fun(x) = (exp(-((x*x)/(2*fSigma*fSigma)))) * (fMax-fMin) + fMin\n";
    $plotcmd = "plot 1/0 title \"\"";
    while($counter<5){
	print GPL_PIPE "fSigma = $fSigma\n";
	if($bDelayFunction){
	    $plotcmd = $plotcmd . ", fMax - int((exp(-((x*x)/(2*$fSigma*$fSigma)))) * (fMax-fMin) + fMin) lw 2 title \"sigma = $fSigma\"";
	    $fSigma *= 1.8;
	} else {
	    $plotcmd = $plotcmd . ", fMax - (exp(-((x*x)/(2*$fSigma*$fSigma)))) * (fMax-fMin) + fMin lw 2 title \"sigma = $fSigma\"";
	    $fSigma *= 1.6;
	}
	$counter++;
	$fSigma = (int($fSigma*10.)/10.);
    }

    print $plotcmd . "\n";
    print GPL_PIPE $plotcmd . "\n";
}

if($linear){
#min, max
    $func = "Linear";
    $outfile = $type . "Function" . $func . ".ps";
    print GPL_PIPE "set output \"$outfile\"\n";    
    print GPL_PIPE "set title \"$type function $func, min=$fMin, max=$fMax\"\n";

    $fDistMax=1;
    $counter= 0;
    print GPL_PIPE "fSlope = 0\n";
    print GPL_PIPE "fun(x) = fSlope * x + fMin\n";

    $plotcmd = "plot 1/0 title \"\"";
    while($counter<5){
	print GPL_PIPE "fDistMax = $fDistMax\n";
	print GPL_PIPE "fSlope = (fMax - fMin) / fDistMax\n";
	if($bDelayFunction){
	    $plotcmd = $plotcmd . ", int(((fMax - fMin) / $fDistMax )  * x + fMin) lw 2 title \"dist_{max} = $fDistMax\"";
	    $fDistMax += 1.5;
	} else {
	    $plotcmd = $plotcmd . ", ((fMax - fMin) / $fDistMax )  * x + fMin lw 2 title \"dist_{max} = $fDistMax\"";
	    $fDistMax += 2;
	}
	$counter++;
    }
    print $plotcmd . "\n";
    print GPL_PIPE $plotcmd . "\n";
}




if($block){
#min, max, width
    $func = "Block";
    $outfile = $type . "Function" . $func . ".ps";
    $iWidth = 4;
    print GPL_PIPE "set output \"$outfile\"\n";    
    print GPL_PIPE "set title \"$type function $func, min=$fMin, max=$fMax, width=$iWidth\"\n";
    print GPL_PIPE "iWidth = $iWidth\n"; 
    $plotcmd = "plot 1/0 title \"\"";

    if($bDelayFunction){
	$plotcmd = $plotcmd . ",(x<=(iWidth /2.)?fMin:fMax) lw 2 title \"width = $iWidth\"";
    } else {
	$plotcmd = $plotcmd . ",(x<=(iWidth /2.)?fMin:fMax) lw 2 title \"width = $iWidth\"";
    }
    print $plotcmd . "\n";
    print GPL_PIPE $plotcmd . "\n";
}



#sleep 100




