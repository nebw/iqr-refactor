#!/bin/bash

for file in `ls *.svg`; 
  do 
  target=`echo  $file | sed "s/\.svg/\.eps/"`;

  if test -r $target
      then
      echo "   $target already exists; skipping";
  else
      echo "*converting $file to $target";
      convert $file $target;
  fi
done


