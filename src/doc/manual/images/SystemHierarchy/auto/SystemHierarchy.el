(TeX-add-style-hook "SystemHierarchy"
 (function
  (lambda ()
    (TeX-run-style-hooks
     "treelist"
     "caption"
     "hang"
     "small"
     "pifont"
     "hhline"
     "tabularx"
     "graphicx"
     "a4"
     "babel"
     "german"
     "english"
     "german"
     "fontenc"
     "T1"
     "latex2e"
     "art12"
     "article"
     "12pt"
     "a5paper"))))

