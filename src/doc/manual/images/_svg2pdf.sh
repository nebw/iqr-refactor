#!/bin/bash

for file in `ls *.svg`; 
  do 
  target=`echo  $file | sed "s/\.svg/\.pdf/"`;

  if test -r $target
      then
      echo "   $target already exists; skipping";
  else
      echo "*converting $file to $target";
      convert -density 144 $file $target;
#      java -jar /usr/share/java/batik-1.5.1/batik-rasterizer.jar -m application/pdf $file;
  fi
done


