## Copyright (C) 2000 John W. Eaton
##
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to the Free
## Software Foundation, 59 Temple Place - Suite 330, Boston, MA
## 02111-1307, USA.

## usage: motormap (x, y, u, v)
## usage: motormap (u, v)
##
## Plot the (u,v) components of a vector field in a (x,y) meshgrid.
##
## You can try:
##
##   a = b = 1:10;
##   [x,y] = meshgrid(a,b);
##   u = rand (10,10);
##   v = rand (10,10);
##   motormap(x,y,u,v)
##
## See also: plot, semilogx, semilogy, loglog, polar, mesh, contour,
##           bar, stairs, gplot, gsplot, replot, xlabel, ylabel, title

## Author: Roberto A. F. Almeida <roberto@calvin.ocfis.furg.br>
## 2001-02-25 Paul Kienzle
##     * change parameter order to correspond to matlab
##     * allow vector x,y,  defaulting to 1:n, 1:m for mxn u
##     * vectorize

## TODO: use gnuplot 'vector' style instead of setting arrows one at a time
function motormap (x, y, left, right)

  if (nargin != 2 && nargin != 4)
    usage ("motormap (x, y, left, right)");
  endif

  if (nargin == 2)
    left = x; right = y; x = 1:columns(left); y= 1:rows(left);
  endif

  if (any(size(left)!=size(right)))
    error ("motormap: left, right must have the same shape.");
  endif

  if is_vector(x) && is_vector(y)
    [nr, nc] = size(left);
    if (length(x) != nc || length(y) != nr)
      error ("motormap: x, y vectors must have correct length");
    endif
    [x,y] = meshgrid(x,y);
  endif

  if (any (size(x) != size(left) | size(y) != size(left)))
    error ("motormap: x, y must have the same shape as left, right");
  endif


  left *= -.9;
  right *= -.9;

  ## Calculating the grid limits.
  
  minx = min (min (x));
  maxx = max (max (x));
  miny = min (min (y));
  maxy = max (max (y));
  
  max_arrow = max ( [max(max (left)) , max(max (right)) , ...
		     abs(min (min (left))) , abs(min (min (right)))] );
  border = max_arrow * 1.2;
  smallborder = .1;

#  limits = [ minx - border, maxx + border, maxy + border, miny - border ];
  limits = [ minx-smallborder, maxx + 1+smallborder,  maxy + 1 + smallborder, miny - smallborder ];


  axis (limits);
  
  
  ## Ok, now plot the arrows.
  offset = .1;
  originoffset =.5;
  
  gset size square
  gset noarrow;
  gplot 0 title "";
  gset xtics 1
  gset ytics 1
  gset grid
  gset format x ""
  gset format y ""
  gset tmargin .8
  gset bmargin .8
  gset lmargin 1
  gset rmargin 1

#  command = sprintf ("gset arrow from %f,%f to %f,%f\n", [ x(:)'; y(:)'; x(:)'+left(:)'; y(:)'+right(:)' ]);
#  command = sprintf ("gset arrow from %f,%f to %f,%f\n", [ x(:)'; y(:)'; x(:)'; y(:)'+right(:)' ]);
#  command = sprintf ("gset arrow from %f,%f to %f,%f\n", [ x(:)'+offset; y(:)'; x(:)'+left(:)'; y(:)'+right(:)' ]);
#  command = sprintf ("gset arrow from %f,%f to %f,%f lt 1\n", \
#		     [ x(:)'+offset+originoffset; y(:)'; x(:)'+offset+originoffset'; y(:)'+right(:)' ]);

  command = sprintf ("gset arrow from %f,%f to %f,%f lt 1 lw 2\n", \
		     [ x(:)'-offset+originoffset; \
		      y(:)'+.5 - left(:)'/2; \
		      x(:)'-offset+originoffset'; \
		      y(:)'+.5 + left(:)'/2; ]);


#command
  eval ( [ "if 1\n", command, "\nendif" ] );


#  command = sprintf ("gset arrow from %f,%f to %f,%f lt 2\n", \
#		     [ x(:)'-offset+originoffset; y(:)'; x(:)'-offset+originoffset'; y(:)'+left(:)' ]);
  command = sprintf ("gset arrow from %f,%f to %f,%f lt 3 lw 2\n", \
		     [ x(:)'+offset+originoffset; \
		      y(:)'+.5 - right(:)'/2; \
		      x(:)'+offset+originoffset'; \
		      y(:)'+.5 + right(:)'/2; ]);

#command
  eval ( [ "if 1\n", command, "\nendif" ] );

  
replot;
      
endfunction
