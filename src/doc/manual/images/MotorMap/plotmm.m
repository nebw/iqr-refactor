function plotmm ()


gset output "DiscreteMotorMap.svg"
#gset terminal post eps  enhanced  color solid 'Helvetica' 14
gset term svg size 600 600

width = 9;
a = b = 0:width-1;
[x,y] = meshgrid(a,b);
#u = rand (10,10);
right = sin(x);
#v = rand (10,10);
left = cos(x);

left = x/(width-1) - y/(width-1);
right =  (width-1-y)/(width-1) - x/(width-1);


motormap(x,y,left,right)


#replot

endfunction


