\section{Neuron types}
\label{sec:appendix-neurontypes}

\def\VmPrs{VmPrs}%Membrane persistence}
\def\InhGain{InhGain}%Inhibitory gain}
\def\ExcGain{ExcGain}%Excitatory gain}
\def\Prob{Prob}%Probability}
\def\ThSet{ThSet}%Threshold potential}
\def\VmMin{VmMin}%Minimum potential}
\def\VmMax{VmMax}%Maximum potential}
\def\VmReset{VmReset}%Membrane potential reset}


This section of the Appendix gives an overview of the neuron types most frequently used in
\iqr{}. 


%Cells in \iqr{} feature two states: membrane potential (Vm) and
%activity (A).

%The four predefined cell types in \iqr{} are: RandomSpike; LinTh;
%Integ-Fire; Sigmoid.


\paragraph{Input types}
\label{sec:input-types}
Depending on the connection type, neurons in the post-synaptic group will receive either
 \bull{} excitatory (excIn) \bull{} inhibitory (inhIn)
or \bull{} modulatory (modIn) input.

\attention Not all types of neurons make use of all three types of inputs.



\subsection{Random spike}
\label{sec:ClsNeuronRandomSpike}
A random spike cell produces random spiking activity with a
user-defined spiking probability.  Unlike the other cell types, it
receives no input and has no membrane potential.  The output of a
random spike cell $i$ at time $t+1$, $a_i(t+1)$, is given by
\begin{equation}
  \label{eq:randspk-act}
  a_i(t+1)=\left\{
    \begin{array}{ll}
      \mbox{SpikeAmpl} & \mbox{with probability \Prob}\\
      0            & \mbox{otherwise}
    \end{array} \right.
\end{equation}
%where $\mbox{Spike Amplitude}$ sets the size of the output spikes, $\mbox{Rand}
%\in \{0,1\}$ is a random number and $\mbox{\Prob}$ gives the incidence probability, i.e. the probability
%of a spike to occur.

\textbf{Parameters}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Probability (Prob)}\\
& description   &  Probability of a spike occurring during a single time step\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Spike amplitude (SpikeAmpl)}\\
& description   &  Amplitude of each spike\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}


\textbf{States}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name        & \textbf{Activity (act)}\\
& description & \\\\
\end{tabularx}


\subsection{Linear threshold}
\label{sec:ClsNeuronLinearThreshold}

Graded potential cells are modeled using linear threshold cells.  The
membrane potential of a linear threshold cell $i$ at time $t+1$,
$v_i(t+1)$, is given by
%\begin{equation}
\begin{eqnarray}
  \label{eq:linth-vm}
  v_i(t+1) = \mbox{\VmPrs}_i v_i(t)\nonumber\\
           &+& \mbox{\ExcGain}_i \sum_{j=1}^{m} w_{ij} a_j(t-\delta_{ij})\nonumber\\
           &-& \mbox{\InhGain}_i \sum_{k=1}^{n} w_{ik} a_k(t-\delta_{ik})
         \end{eqnarray}
%       \end{equation}
where $\mbox{\VmPrs}_i \in \{0,1\}$ is the persistence of the membrane
potential, $\mbox{\ExcGain}_i$ and $\mbox{\InhGain}_i$ are the gains of
the excitatory (excIn) and inhibitory (inhIn) inputs respectively, $m$ is the number
of excitatory inputs, $n$ is the number of inhibitory inputs, $w_{ij}$
and $w_{ik}$ are the strengths of the synaptic connections between
cells $i$ and $j$ and $i$ and $k$ respectively, $a_j$ and $a_k$ are
the output activities of cells $j$ and $k$ respectively, and
$\delta_{ij} \geq 0$ and $\delta_{ik} \geq 0$ are the delays along the
connections between cells $i$ and $j$ and cells $i$ and $k$
respectively.  The values of $w$ and $\delta$ are set by the synapse
type and are described in Appendix Synapse types \vpageref{sec:appendix-synapsetypes}.



The output activity of cell $i$ at time $t+1$, $a_i(t+1)$, is given by
\begin{equation}
  \label{eq:linth-act}
  a_i(t+1) = \left\{
    \begin{array}{ll}
      v_i(t+1) & \mbox{with probability Prob for $v_i(t+1) \geq$ \ThSet}\\
      0      & \mbox{otherwise}
    \end{array} \right.
\end{equation}
where $\mbox{\ThSet}$ is the membrane potential threshold, $\mbox{Rand}
\in \{0,1\}$ is a random number and $\mbox{\Prob}$ is the probability
of activity.


\textbf{Parameters}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Excitatory gain (ExcGain)}\\
& description   &  Gain of excitatory inputs. The inputs are summed before being multiplied by this gain.\\
& range   &  0.0 -  10.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Inhibitory gain (InhGain)}\\
& description   &  Gain of inhibitory inputs. The inputs are summed before being multiplied by this gain.\\
& range   &  0.0 -  10.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Membrane persistence (VmPrs)}\\
& description   &  Proportion of the membrane potential remaining after one time step if no input arrives.\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Clip potential (Clip)}\\
& description   &  %Restrict the range of the membrane potential\\
Limits the membrane potential to values between \VmMax{} and \VmMin.
Parameters: maximum potential, \VmMax; minimum potential, \VmMin\\
& options & true, false\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Minimum potential (VmMin)}\\
& description   &  Minimum value of the membrane potential\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Maximum potential (VmMax)}\\
& description   &  Maximum value of the membrane potential\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Probability  (Prob)}\\
& description   &  Probability of output occurring during a single time step\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Threshold potential (ThSet)}\\
& description   &  Membrane potential threshold for output activity\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}


\textbf{States}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name        & \textbf{Membrane potential (vm)}\\
%& description & \\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name        & \textbf{Activity (act)}\\
%& description & \\\\
\end{tabularx}



% \subsubsection*{Active dendrite}
% Simulates the effect of active membrane on the electrotonic length of
% the dendrite.  Parameters: electrotonic length, EtlSet; increment
% after spike, EtlInc; persistence, EtlPrs.
% 
% \subsubsection*{Adapting threshold}
% After the cell goes active, the threshold is incremented until the cell
% inactivates.  Threshold decays back to normal level at a rate controlled
% by the membrane persistence.  Parameters: increment, ThInc; persistence, ThPrs.
% 



\subsection{Integrate \& fire}
\label{sec:ClsNeuronIntegrateFire}

Spiking cells are modeled with an integrate-and-fire cell model.  The
membrane potential is calculated using equation~(\ref{eq:linth-vm}).
The output activity of an integrate-and-fire cell at time $t+1$,
$a_i(t+1)$ is given by
\begin{equation}
  \label{eq:i&f-act}
  a_i(t+1)=\left\{
    \begin{array}{ll}
      \mbox{SpikeAmpl} & \mbox{with probability Prob for $v_i(t+1) \geq$ \ThSet}\\
      0 & \mbox{otherwise}
    \end{array} \right.
\end{equation}
where $\mbox{SpikeAmpl}$ is the height of the output spikes,
$\mbox{\ThSet}$ is the membrane potential threshold, $\mbox{Rand} \in
\{0,1\}$ is a random number and $\mbox{\Prob}$ is the probability of
activity.

After cell $i$ produces a spike, the membrane potential is
hyperpolarized such that
\begin{equation}
  \label{eq:i&f-reset}
  v_i^{'}(t+1) = v_i(t+1) - \mbox{\VmReset}
\end{equation}
where $v_i^{'}(t+1)$ is the membrane potential after hyperpolarization
and $\mbox{\VmReset}$ is the amplitude of the hyperpolarization.


\textbf{Parameters}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Clip potential (Clip)}\\
& description   &  %Restrict the range of the membrane potential\\
Limits the membrane potential to values between \VmMax{} and \VmMin.
Parameters: maximum potential, \VmMax; minimum potential, \VmMin\\
& options & true, false\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Excitatory gain (ExcGain)}\\
& description   &  Gain of excitatory inputs. The inputs are summed before being multiplied by this gain.\\
& range   &  0.0 -  10.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Inhibitory gain (InhGain)}\\
& description   &  Gain of inhibitory inputs. The inputs are summed before being multiplied by this gain.\\
& range   &  0.0 -  10.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Membrane persistence (VmPrs)}\\
& description   &  Proportion of the membrane potential remaining after one time step if no input arrives.\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Minimum potential (VmMin)}\\
& description   &  Minimum value of the membrane potential\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Maximum potential  (VmMax)}\\
& description   &  Maximum value of the membrane potential\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Probability  (Prob)}\\
& description   &  Probability of output occurring during a single time step\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Threshold potential (ThSet)}\\
& description   &  Membrane potential threshold for output of a spike\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Spike amplitude (SpikeAmpl)}\\
& description   &  Amplitude of output spikes\\
& range   &  1.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Membrane potential reset (VmReset)}\\
& description   &  Membrane potential reduction after a spike\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}


\textbf{States}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name        & \textbf{Activity}\\
& direction   & output\\
%& description & \\\\
\end{tabularx}


% \subsubsection*{Active dendrite}
% Simulates the effect of active membrane on the electrotonic length of
% the dendrite.  Parameters: electrotonic length, EtlSet; increment
% after spike, EtlInc; persistence, EtlPrs.
% 
% \subsubsection*{Adapting threshold}
% After the cell goes active, the threshold is incremented until the cell
% inactivates.  Threshold decays back to a normal level at a rate controlled
% by persistence.  Parameters: increment, ThInc; persistence, ThPrs.
                  

\subsection{Sigmoid}
\label{sec:ClsNeuronSigmoid}

The \iqr{} sigmoid cell type is based on the perception cell model
often used in neural networks.  The membrane potential of a sigmoid
cell $i$ at time $t+1$, $v_i(t+1)$, is given by
equation~(\ref{eq:linth-vm}).  The output activity, $a_i(t+1)$ is
given by
\begin{equation}
  \label{eq:sigmoid-act}
  a_i(t+1) = 0.5*(1 + \tanh(2*\mbox{Slope}*(v_i(t+1)-\mbox{\ThSet})))
\end{equation}
where $Slope$ is the slope and $ThSet$ is the midpoint of the sigmoid
function respectively.


\textbf{Parameters}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Clip potential (Clip)}\\
& description   &  %Restrict the range of the membrane potential\\
Limits the membrane potential to values between \VmMax{} and \VmMin.
Parameters: maximum potential, \VmMax; minimum potential, \VmMin\\
& options & true, false\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Excitatory gain (ExcGain)}\\
& description   &  Gain of excitatory inputs. The inputs are summed before being multiplied by this gain.\\
& range   &  0.0 -  10.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Inhibitory gain (InhGain)}\\
& description   &  Gain of inhibitory inputs. The inputs are summed before being multiplied by this gain.\\
& range   &  0.0 -  10.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Membrane persistence (VmPrs)}\\
& description   &  Proportion of the membrane potential remaining after one time step if no input arrives.\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Minimum potential (VmMin)}\\
& description   &  Minimum value of the membrane potential\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Maximum potential (VmMax)}\\
& description   &  Maximum value of the membrane potential\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Midpoint}\\
& description   &  Midpoint of the sigmoid\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name   & \textbf{Slope}\\
& description   &  Slope of the sigmoid\\
& range   &  0.0 -  1.0\\\\
\end{tabularx}


\textbf{States}\\
\begin{tabularx}{\linewidth}{p{1em}lX}
& name        & \textbf{Activity (act)}\\
%& description & \\\\
\end{tabularx}

\begin{tabularx}{\linewidth}{p{1em}lX}
& name        & \textbf{Membrane potential (vm)}\\
%& description & \\\\
\end{tabularx}




%\subsection{Pyramidal with apical shunt}
%\label{sec:ClsNeuronPyramidApicalShunt}
%\input{Elements/autogenTypeDoc/neuronPyramidApicalShunt}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../iqrUserManual"
%%% TeX-command-default: "LaTeX PDF"
%%% End: 
