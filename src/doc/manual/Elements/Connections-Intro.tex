%%MARGOTIZED 20040614

\section{Connections}
\label{sec:connections-concepts}

%multiplicity:
%The level of the process, i.e. where the connections if drawn in the GUI, is shown in
%\figref{fig:Connection-LOD}a. 
%which group to which group
%The next level (\figref{fig:Connection-LOD}b) specifies which target neurons are receiving
%input. This is the level at which the dendrites are specified. \marked{\ra pattern}.
%The connection will be fully specified at the level of the synapses, as shown in
%\figref{fig:Connection-LOD}c. \marked{arborization}

Groups and connections are described on two different levels.
At the process level, groups and connections are created, and are symbolized as rectangles
and lines with arrow heads respectively (see \figref{fig:Connection-LOD}(a)).

From figure \vref{fig:Group-vs-Connection} we know, that groups are an abstraction of an
assembly of neurons, and connections are abstractions of an assembly of axon-synapse-dendrite
nexuses. 

In terms of the structure the description at the process level is therefore
underspecified. Regarding the group we neither know how many neurons it comprises of, nor what
the topology -- the arrangement of the neurons -- is. Regarding the connection we don't know
from which source neuron information is transmitted to which target neuron. 
The definition is only complete after specifying these parameters.
Figure \figref{fig:Connection-LOD}(b) shows one possible complete definition of the group
and the connection.

\begin{figure}[!h]
  \begin{maxipage}
    \centering
    \subfigure[]{\parbox[b]{.45\linewidth}{\centering\includegraphics*[scale=1.]{Connection-Process-Level}}}
    \subfigure[]{\parbox[b]{.45\linewidth}{\centering\includegraphics*[scale=1]{Connection-Synapse-Level}}}
  \end{maxipage}
  \caption{Levels of description for connections}
  \label{fig:Connection-LOD}
\end{figure}

%as a group is an assembly of neurons, a connection is an assembly of axon, synapses, and
%    dendrites
%-> synapse/dendrite parameters
%-> definition of the connectivity structure


In the framework of \iqr{}, the following assumptions concerning connections are made:\\
\bull{} there is no delay in axons, %maybe there will be some later...,
\bull{} the computation takes place in synapses,
\bull{} the transmission delay is dependent on the length of the dendrite,
\bull{} any back-propagating signals are limited to the dendrite.

In principle, the complete definition of a connection is twofold, in that it comprises of
the definition of the update function of the synapse, and the definition of the
connectivity. In this context, the term connectivity refers to the 
spacial layout of the axons, synapses and dendrites.

But update function and connectivity are not as easily separable, as the delays in the
dendrites are derived from the connectivity.


\paragraph{Multiplicity}
\label{sec:multiplicity}
%more multiplicity
As mentioned above, a single connection is an assembly of axon-synapse-dendrite nexuses.
A single nexus in turn can connect several pre-synaptic neurons to \textbf{one}
post-synaptic cell, or feed information from \textbf{one} pre-synaptic into multiple
post-synaptic neurons.

A nexus can hence comprise of several axons, synapses, and dendrites. In figure
\vref{fig:Connection-Dendrite-Distance} two possible cases of many-to-one, and one-to-many
are diagrammed. The first case (a) can be referred to as ``receptive field'' (RF), the
second one as ``projective field'' (PF). 

\begin{figure}[!h]
  \begin{maxipage}
    \centering
    \subfigure[Receptive field (RF)]{\parbox[b]{.47\linewidth}{\centering\includegraphics*[scale=1.5]{Connection-Dendrite-Distance-RF}}}
    \subfigure[Projective field (PF)]{\parbox[b]{.47\linewidth}{\centering\includegraphics*[scale=1.5]{Connection-Dendrite-Distance-PF}}}
  \end{maxipage}
  \caption{The axon-synapse-dendrite nexus and distance}
\label{fig:Connection-Dendrite-Distance}
\end{figure}

For reasons of ease of understanding the connectivity depicted in
\figref{fig:Connection-Dendrite-Distance} is only one-dimensional, i.e. the pre-synaptic
neurons are arranged in one row. This is not the standard case; most of the times a nexus
will be defined in two dimensions as shown in \figref{fig:Connection-Metaphor}.

\begin{figure}[!h]
  \centering
  \includegraphics*[width=.7\textwidth]{Connection-Metaphor}
  \caption{Two dimensional nexus}
  \label{fig:Connection-Metaphor}
\end{figure}


\paragraph{Delays}
\label{sec:delays}

The layout shown in \figref{fig:Connection-Dendrite-Distance} is directly derived from the
above listed assumptions that delays are properties of dendrites. 

The basis of the computation of the delay is the distance, as given by the eccentricity
of a neuron.

In the case of a receptive field, the eccentricity is defined by the position of the
sending cell relative to the position of the one receiving cell. In
\figref{fig:Connection-Dendrite-Distance}a this definition of the distance, and the
resulting values are depicted. 

In a projective field, the eccentricity is defined with respect of the position of the
multiple post-synaptic cells relative to the one pre-synaptic neuron
(\figref{fig:Connection-Dendrite-Distance}(b)).



\subsubsection*{Patterns and arborizations}
\label{sec:patt-arbor}
The next step is to understand how the connectivity can be specified in \iqr{}\@. 

Defining the connectivity comprises of two steps: 
\begin{enumerate}
\item define the \menu{pattern}
\item define the \menu{arborization}
\end{enumerate}

%\clearpage
%\subsection*{Specifying the connectivity}
%This section explains how to define the connectivity at the level of neurons and
%synapses, i.e. how to define which post-synaptic neuron receives information from which
%pre-synaptic neuron.


\paragraph{Pattern}
\label{sec:pattern}

The \iname{Pattern} defines \textbf{pairs of points} in the lattice of the
pre- and post-synaptic group. 
These points are \textbf{not} neurons, but (x,y) coordinates.
In \figref{fig:nexus-pattern-arb} the points in the pre-synaptic group are indicated by
the arrow labeled ``projected point''. 
For sake of ease of illustration we'll use a one dimensional layout again. The groups
are defined with size $10x1$ for source and $2x1$ for target.

The two pairs of points are: \\
pair 1: $(3,1)_{pre}$, $(1,1)_{post}$\\
pair 2: $(8.5,1)_{pre}$, $(2,1)_{post}$\\
Note that in the second pair the point in the pre-synaptic group is not the location of a
neuron.

%In the simplest case of groups with one neuron each, the pair of points might be
%$(1,1)_{pre}$, $(1,1)_{post}$. For two groups with $2*2$ neurons each, the pair could be
%$(1.5,1.5)_{pre}$, $(1.5,1.5)_{post}$, i.e. the points are at coordinates devoid of a neuron.

%Each connection consists of a list of pairs of points. 
\iqr{} provides several ways to specify the list of pairs. These different methods are
referred to as pattern types. The meaning of the pattern types and how to define them is
explained in detail below.


\begin{figure}[!h]
  \centering
  \includegraphics*[width=1\textwidth]{nexus-pattern-arb}
  \caption{Nexuses, pattern, and arborization}
  \label{fig:nexus-pattern-arb}
\end{figure}

\paragraph{Arborization}
\label{sec:arborization}

As previously mentioned, the pattern does not relate to neurons directly, but to coordinates in the
lattice of the groups. It is the arborization that defines the real neurons that send and
receive input. This can be imagined as the arrangement of the dendrites of the
post-synaptic neurons, as depicted in \figref{fig:nexus-pattern-arb}. The arborization is
applied to every pair of point defined by the pattern. Whether the arborization is applied
to the pre- or the post-synaptic group is defined by the direction parameter. If applied
to the source group, the effect is a fan-in receptive field. A fan-out, projective field,
is the result of applying the arborization to the target group (see also
\figref{fig:Connection-Dendrite-Distance}).

\begin{wrapfigure}{l}[\lwrap]{115pt}
  \includegraphics*[width=110pt]{Single-Arborization_annotated}
  \caption{Rectangular arborization}
  \label{Single-Arborization_annotated}
\end{wrapfigure}

\iqr{} provides a set of shapes of arborizations. Figure \vref{Single-Arborization_annotated} shows
an example of a rectangular arborization of a width and height of 2. The various types of
arborizations are described in detail below.

\paragraph{Combining pattern and arborization}
The combination of pattern and arborization is illustrated in \figref{fig:Arb+Pattern}.
The source group is on the left side, the target group on the right side. In the lower
part you find the pattern denoted by a gray arrow. The pattern consists of four pairs of
points. On the upper left tier, the arborization is applied to the points defined by
the pattern. For each cell that lies within the arborization, one \textbf{synapse} is
created. In the case presented in \figref{fig:Arb+Pattern} there are 16 synapses created.

For each synapse the \textbf{distance} with respect to the pattern point is calculated. This
distance serves as basis to calculate the \textbf{delay}. Several functions are available,
for details see the \iname{DelayFunction} section further below.

\begin{figure}[!h]
  \begin{maxipage}
  \centering
  \includegraphics[scale=.9]{Arb+Pattern}
  \caption{Combining pattern and arborization}
  \label{fig:Arb+Pattern}
\end{maxipage}
\end{figure}







%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../iqrUserManual"
%%% TeX-command-default: "LaTeX PDF"
%%% End: 
