%gui elements
\section{The User-interface}
\label{sec:user-interface}

\subsubsection*{\index{Diagram editing pane}Diagram editing pane}
\label{sec:GUI-diagram-pane}
The main \textbf{diagram editing pane} (\figref{fig:Main-UI_annotated}\dep{}) serves to add processes, groups
and connections to the model. The definition of a new process automatically adds a new tab
and therein a new diagram. To switch between diagram panes use the \textbf{tab-bar}
(\figref{fig:Main-UI_annotated}\tabbar). The left-most tab always presents the system-level.

\begin{figure}[!h]
  \begin{maxipage}
  \begin{center}
 \includegraphics*[width=.8\textwidth{}]{Main-UI_annotated.png}
    \caption{\protect\iqr{} graphical user-interface}
    \label{fig:Main-UI_annotated}
  \end{center}
\end{maxipage}
\end{figure}

On the diagram editing pane,
a gray square \includegraphics[scale=.3, viewport = 0 15 60 25]{CanvasProcess.png} represents a process,
a white square \includegraphics[scale=.3, viewport = 0 15 60 25]{CanvasGroup.png} a group, and
a line with an arrow head 
\includegraphics[scale=.3, viewport = 0 -10 60 20]{CanvasConnection.png} a connection.

A single process or group is selected by clicking on its icon in the diagram editing
pane. To select multiple processes or groups, hold down the control-key while clicking on the
icon.

\subsubsection*{Diagram editing toolbar}
\label{sec:diagr-edit-toolb}

The functionality of the diagram editing toolbar
(\figref{fig:Main-UI_annotated}\deptoolbar) is as follows:

\begin{tabularx}{\linewidth}{cX}
\includegraphics[scale=1., viewport = 0 6 45 20]{Toolbar-Zooming.png} & 
Zoom in and out of the diagram\\\\
\includegraphics[scale=1., viewport = 0 6 22 20]{Toolbar-New-Process.png} &
Add a new process to the system level\\\\
\includegraphics[scale=1., viewport = 0 6 22 20]{Toolbar-New-Group.png} &
Add a new group to the current process\\\\
\includegraphics[scale=1., viewport = 0 6 67 20]{Toolbar-New-Connections.png} &
Add a new connection between groups: excitatory (red), modulatory (green), inhibitory (blue)\\
\end{tabularx}

A more detailed description on how to use the diagram editing toolbar will be given when
outlining the editing of the system.

\subsubsection*{Splitting the diagram pane}
\begin{tabularx}{\linewidth}{cX} 
\includegraphics[scale=1., viewport = 0 6 72 20]{Toolbar-Splitting.png} &
Split the diagram editing pane into two separate views by using the
\textbf{splitter} (\figref{fig:Main-UI_annotated}\splitter{}). From left to right: split
vertically, horizontally, revert to single window view.\\
\end{tabularx}

\subsubsection*{Navigating the diagram}
To navigate on a large diagram that does not fit within the diagram editing pane, the
\textbf{\index{panner}panner} (\figref{fig:Main-UI_annotated}\panner{}) can be used to change to the visible section of
the pane.



\subsubsection*{Browser}
\label{sec:GUI-browser}
On the left side of the screen (\figref{fig:Main-UI_annotated}\treeview{}) a tree-view
of the model, the \textbf{\index{browser}browser}, can be found. It provides direct access to the
elements of the system. The top node of the tree represents the system level (see
\ref{fig:iqrSystemStructure}), the second level node reflects the processes and the third
level node points to the groups.  By double-clicking on the system or process node you can
open the corresponding diagram in the diagram editing pane. Right-clicking on any node
brings up the context-menu.


\subsubsection*{Copy and Paste elements}
You can copy and paste a process, one or more groups, and a connection to the clipboard.
To copy an object, right-click on it and select \menu{Copy ...} from the context-menu.

To paste the object, select \menu{Edit}\ra{}\menu{Paste ...}  from the main menu.
Processes can only be pasted at the system level, groups and connections only at the
process level.


\subsubsection*{Print and save the diagram}
You can export the diagram as PNG or SVG image. To do so, use the menu
 \menu{Diagram}\ra{}\menu{Save}. The graphics format will be determined by the extension
 of the filename. 
\hint If you choose SVG as the export format, the diagram will be split into several files
 (due to the SVG specification). It is therefore advisable to save the diagram to a
 separate folder. 

To print the diagram use the menu \menu{Diagram}\ra{}\menu{Print}.


\subsection{iqr Settings}
\label{sec:iqr-settings}
Via the menu \menu{Edit}\ra{}\menu{Settings} you can change the settings for
\iqr{}\@. 

\begin{itemize}
\item [\menu{General}]
\begin{itemize}
\item \menu{Auto Save} Change the interval at which \iqr{} writes backup files. The
  original files are not overwritten, but auto save will create a new backup file, the
  name of which is based on the name of the currently open system file, with the extension
  \texttt{,autosave} appended. 
  
\attention  The range is from $-1$ to $60$ minutes, where a value of $-1$ disables auto save.
\item \menu{Font Name} Set the font name for the diagrams. The changes will only be effective
  at the next start of \iqr{}\@.
\item \menu{Font Size} Set the font size for the diagrams. The changes  will only be effective
  at the next start of \iqr{}\@.
\end{itemize} 
\item [\menu{NeuronPath}]
\item [\menu{SynapsePath}]
\item [\menu{ModulePath}]
\item[] The options for NeuronPath, SynapsePath, ModulePath refer to the location where
  the specific files can be found. As they follow the same logic the description below
  applies to all three.
  \begin{itemize}
  \item \menu{Use local ...} Specifies whether \iqr{} loads types from the folder
  specified by \menu{Path to local} below.
\item \menu{Use user defined ...} Specifies whether \iqr{} loads types from the folder
  specified by \menu{Path to user defined} below.
  \item \menu{Path to standard ...} The folder into which the types were stored at
  installation time. 
  \item \menu{Path to local ...} The folder where system-wide non-standard types are stored.
  \item \menu{Path to user defined ...} The folder into which the user stores her/his own
  types.% (see  \secref{sec:appendix-userdeftypes}).
 \end{itemize}
\end{itemize}




\section{Creating a new system}
\label{sec:creating-new-system}
A new system is created by selecting \includegraphics[scale=1., viewport = 0 2 14 14]{filenew_14x14.png}
from the main toolbar (\figref{fig:Main-UI_annotated}\maintoolbar) or via the menu
\menu{File}\ra{}\menu{New System}.
Creating a new system will close any open systems.

\section{System properties}
\label{sec:system-properties}

To change the properties of the system right-click on the system name in the browser
(top-most node), and select \menu{Properties} from the context-menu, or via the menu
\menu{File}\ra{System Properties}.

\begin{wrapfigure}{r}{150pt}
    \includegraphics*[scale=.6]{System-Properties.png}
    \caption{System properties dialog}
    \label{fig:System-Properties}
\end{wrapfigure}

Using the system properties dialog (see \figref{fig:System-Properties}), you can change
the name of the system, the author, add a date, and notes (\menu{Hostname} has no meaning
in the present release). 

The most important property is\\
 \menu{Cycles Per Second}, with which you can define the speed
at which the simulation is updated. 

A value of $0$ means, the simulation is running as fast as possible, values larger
than zero, denotes how many updates per cycle are executed.  The value entered reflects ``best
effort'' in two ways. On the one hand, the simulation cannot run faster than a certain maximum speed,
as given by the complexity of the system, and the speed of the computer. On the other
hand, slight variations in the length of the individual update cycles can occur.


\section{Opening an existing system}
\label{sec:opening-an-existing}
An existing system is opened by selecting the button 
\includegraphics[scale=1., viewport = 0 2 16 14]{fileopen_16x14.png} from the main toolbar
(\figref{fig:Main-UI_annotated}\maintoolbar) or via the menu \menu{File}\ra{}\menu{Open}.
If you open an existing system, the current system will be closed.

\section{Saving the system}
\label{sec:saving-system}
To save the system, press the \includegraphics[scale=1., viewport = 0 2 14 14]{filesave_14x14.png} button in the main
toolbar (\figref{fig:Main-UI_annotated}\maintoolbar) or via menu select
\menu{File}\ra\menu{Save System}.
To save a system under a new name, select menu \menu{File}\ra{}\menu{Save System As}. 


\begin{wrapfigure}{r}{130pt}
  \includegraphics*[scale=.5]{Warning-Save-invalid.png}
  \caption{Warning dialog for saving invalid systems}
  \label{fig:Warning-Save-invalid}
\end{wrapfigure}

If your system contains inconsistencies, e.g. connections that have no source and/or target
defined, a dialog as shown in \figref{fig:Warning-Save-invalid} will show up.


\bigskip
\attention Please take this warning seriously. The system will be saved to disk, but you should fix the
problems mentioned in the warning dialog or you might not be able to re-open the system.





%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../iqrUserManual"
%%% TeX-command-default: "LaTeX PDF"
%%% End: 






