\section{Visualizing states and collecting data}
\label{sec:visualizing}

This section introduces the facilities \iqr{} provides to visualize and save the states of
elements of the model. \iname{Time plots} and \iname{Group plots} are used to visualize
the states of neurons, \iname{Connection plots} serve to visualize the connectivity and
the states of synapses of a connection. The \iname{Data Sampler} allows to save data from
the model to a file.


\begin{wrapfigure}[18]{r}{165pt}
%\begin{figure}[!h]
%  \begin{center}
 \includegraphics*[scale=.7]{StatePanel_annotated.png}
    \caption{}
    \label{fig:StatePanel_annotated}
%  \end{center}
%\end{figure}
\end{wrapfigure}

\subsection{\iname{State Panel}}
\label{sec:state-panel}

\iqr{} plots and the 
%\iname{Space plots}, \iname{Time plots} and 
\iname{Data Sampler}
(\secref{sec:datasampler}) share part of their handling. Therefore, we will first have a
look at some common features.

On the right side of the plots and the \iname{Data Sampler}, you find a \iname{State
  Panel}\index{State Panel} which looks similar to \figref{fig:StatePanel_annotated}.
\Pisymbol{pzd}{192} shows the name of the object from which the data originates. Neurons
and synapses have a number of internal states that vary from type to type. In the frame
entitled \menu{states} you can select which state \Pisymbol{pzd}{193} should be plotted or
  saved.
Squares in front of the names indicate that several states can be selected
simultaneously, circles mean that only one single state can be selected.

\attention In a newly created plot or \iname{Data Sampler}, the check-box \menu{live data}
\Pisymbol{pzd}{194} will not be checked, which means that no data from this \iname{State
  Panel} is plotted or saved. To activate the \iname{State Panel}, check the tic by
clicking the check-box.

To hide the panel with the states of the neuron or synapse, drag the bar
\Pisymbol{pzd}{195} to the right side.


\subsection{Drag \& drop}
\label{sec:drag--drop}

The visualizing and data collecting facilities of \iqr{} make extensive use of drag \&
drop. 

You can drag groups from the \iname{Browser} (\treeview{} \figref{fig:Main-UI_annotated},
\secref{sec:user-interface}), the symbol in the top-left corner of \iname{State Panels}
(\figref{fig:DragHandle_annotated}\Pisymbol{pzd}{192}), or regions from \iname{Space
  plots}.

 \begin{wrapfigure}[6]{l}[\lwrap]{65pt}
   \includegraphics*[scale=.6]{DragHandle_annotated.png}
   \caption{}%\protect\iqr{} Space plot}
   \label{fig:DragHandle_annotated}
 \end{wrapfigure}

To \textbf{drag}, click on any of the above mentioned items, and move the mouse while keeping the
button pressed. The cursor will change to \includegraphics[scale=1., viewport = 0 2 17
23]{drag_cursor.png}. Move the mouse over the target and \textbf{drop} by releasing the
mouse button. 



The table below summarized what can be dragged where.  

\begin{tabular}[!h]{lll|l}
origin   &  &target &what\\\hline
\iname{Browser}      &\ra{}&\iname{Time plot}, \iname{Group plot}, \iname{Data Sampler}  &Group\\
\iname{Time plot}    &\ra{}&\iname{Group plot}, \iname{Data Sampler}                     &Group\\
\iname{Data Sampler} &\ra{}&\iname{Time plot}, \iname{Group plot}                        &Group\\
\iname{Group plot}   &\ra{}&\iname{Time plot}, \iname{Data Sampler}                      &Group/Region\\
\end{tabular}




\subsection{\iname{Space plots}}\index{Space plot}
\label{sec:space-plots}
A \iname{Space plot} as shown in \figref{fig:GroupPlot_annotated} displays the state of
each cell in a group in the plot area \Pisymbol{pzd}{192}. 

To create a new \iname{Space plot}, right click on a group in the diagram editing pane or the
browser and select \menu{Space plot} from the context-menu.

The value for the selected state (see above) is color coded, the range indicated in the
color bar \Pisymbol{pzd}{193}. A Space plot solely plots one state of one group.

\begin{figure}[!h]
  \begin{center}
    \includegraphics*[scale=.6]{GroupPlot_annotated.png}
    \caption{\protect\iqr{} \iname{Space plot}}
    \label{fig:GroupPlot_annotated}
  \end{center}
\end{figure}

To change the mode of \textbf{scaling} for the color bar, right-click on the axis \Pisymbol{pzd}{194}
to the right size. If \menu{expand only} is ticked, the scale will only increase; if \menu{auto
  scale} is selected, you can manually enter the value range.

You can \textbf{zoom} into the plot by first clicking 
\includegraphics[scale=1., viewport = 0 2 16 13]{zoom_plot.png}  
 (\figref{fig:GroupPlot_annotated}\Pisymbol{pzd}{195}), and selecting the region of interest by moving the
cursor while keeping the mouse button pressed. To return to full-view, click into the
plot area.


%\paragraph{Drag \& drop} 
You can select a region of cells in the \iname{Space plot} by clicking in the plot area
({\figref{fig:GroupPlot_annotated}\Pisymbol{pzd}{192}) and moving the mouse while holding
  the left button pressed. This region of cells can now be dragged and dropped onto a
  \iname{Time plot}, \iname{Space plot}, or \iname{Data Sampler}.

To change the group associated to the \iname{Space plot}, drop a group 
onto the plot area or the \iname{State Panel}. Dropping a region of a group has the same
effect as dropping the entire group, as \iname{Space plot} always plots the entire group. 





\subsection{\iname{Time plots}}\index{Time plot}
\label{sec:timeplots}
A \iname{Time plot} (\figref{fig:TimePlot_annotated}) serves to plot the states of neurons against
time. 

To create a new \iname{Time plot}, right click on a group in the diagram editing pane or the
browser and select \menu{Time plot} from the context-menu.

\iname{Time plots} display the \textbf{average} value of the states of an entire group or
region of a group. A \iname{Time plot} can plot several states, and states from different
groups at once. Each state is plotted as a separate trace.

To add a new group to the \iname{Time plot} use drag \& drop as described in
\secref{sec:drag--drop}, or drag and drop a region from a \iname{Space plot} onto the
plot area or one of the \iname{State Panels}. If dropped onto the plot area, the group or
region will be added, if dropped onto a \iname{State Panel}, you have the choice to
replace the \iname{State Panel} under the drop point, or add the group/region to the plot.

To remove a group from the \iname{Time plot} close the \iname{State Panel} by clicking 
\includegraphics[scale=1., viewport = 0 2 16 13]{Toolbar-close_small.png} in the top-right corner.

The \iname{State Panels} for \iname{Time plots} are showing which region of a group is
plotted by means of a checker board
(\figref{fig:TimePlot_annotated}\Pisymbol{pzd}{193}). Depending on the selection, the
entire group or the region will be hight-lighting in red.


\begin{figure}[!h]
  \begin{center}
    \includegraphics*[scale=.6]{TimePlot_annotated.png}
%    \fbox{\includegraphics*[scale=.6, trim= 122 317 1055 602]{TimePlot_annotated.pdf}}
    \caption{\protect\iqr{} \iname{Time plot}}
    \label{fig:TimePlot_annotated}
  \end{center}
\end{figure}


You can \textbf{zoom} into the plot by first clicking \includegraphics[scale=1., viewport
= 0 2 16 13]{zoom_plot.png} (\figref{fig:GroupPlot_annotated}\Pisymbol{pzd}{195}), and
selecting the region of interest by moving the cursor while keeping the mouse button
pressed. To return to full-view, click into the plot area.

To change the scaling of the y-axis, use the context-menu of the axis \Pisymbol{pzd}{194}
(for detail see above, \iname{Space plots}).



\subsection{\iname{Connection plots}}\index{Connection plot}
\label{sec:connection-plots}
\iname{Connection plots} (\figref{fig:ConnectionDiagram_annotated}) are used to visualize the static
and dynamic properties of connections and synapses. The source group \Pisymbol{pzd}{192} is on the left, the
target group \Pisymbol{pzd}{193} on the right side.

To create a new \iname{Connection plot}, right click on a connection in the diagram editing
pane, and select \menu{Connection plot} from the context-menu.


The \iname{State Panel} \Pisymbol{pzd}{194} for \iname{Connection plots} is slightly different than
for other plots. You can select to display the static properties of a connection, i.e. the \menu{Distance}
or \menu{Delay}, or the internal states by selecting \menu{Synapse}, and one of the states
in the list.

To visualize the synapse value for the corresponding neuron, click on the cell in the source or the target group. 

\attention \iname{Connection plots} do not support drag \& drop.

\begin{figure}[!h]
  \begin{center}
    \includegraphics*[scale=.6]{ConnectionDiagram_annotated.png}
    \caption{\protect\iqr{} \iname{Connection plot}}
    \label{fig:ConnectionDiagram_annotated}
  \end{center}
\end{figure}



\subsection{\iname{Data Sampler}}\index{Data Sampler}
\label{sec:datasampler}
The \iname{Data Sampler}  (\figref{fig:DataSampler}) is used to save internal states of the model. To open the
\iname{Data Sampler} use the menu \menu{Data}\ra{}\menu{Data Sampler}. 

A newly created \iname{Data Sampler} is not bound to a group. To add groups or regions
from groups use drag \& drop as described earlier.

\begin{figure}[!h]
  \begin{center}
 \includegraphics*[scale=.6]{DataSampler.png}
    \caption{\iname{Data Sampler}}
    \label{fig:DataSampler}
  \end{center}
\end{figure}

Like for the \iname{Time plots}, the \iname{State Panel} for the \iname{Data Sampler}
indicates which region of the group is selected. Moreover you will find an \menu{average}
check-box which allows you to save the average of the entire group or region of cells
instead of the individual values.

The \iname{Data Sampler} has the following options:
\begin{itemize}
\item [\menu{Sampling}] Defines at what frequency data is saved.
  \begin{itemize}
  \item \menu{Every x Cycle} Specifies how often data is saved in relation to the updates
    of the model, e.g. a value of $1$ means data is saved at every update cycle of the
    model, a value of $10$ that the data is saved only every $10^{th}$ cycle.
  \end{itemize}
\item [\menu{Acquisition}] These options define for how long data is saved.
  \begin{itemize}
  \item \menu{Continuous} Data is saved until you click on \menu{Stop}.
  \item \menu{Steps} Data is saved for as many update cycles as you define.
  \end{itemize}
\item [\menu{Target}] 
  \begin{itemize}
  \item \menu{Save to:} Name of the file where the data is saved.
  \item \menu{Overwrite} Overwrite the file at every start of sampling.
  \item \menu{Append} Append data to the file at every start of sampling.
  \item \menu{Sequence} Create a new file at every start of sampling. The name of the new
    file is composed of the base name from\\ \menu{Save to:} with a counter appended.
  \end{itemize}
\item [\menu{Misc}]
 \begin{itemize}
   \item \menu{auto start/stop} Automatically start and stop sampling when the simulation
   is started and stopped.
\end{itemize}
\end{itemize}



If the \menu{Sampling}, \menu{Acquisition}, and \menu{Target} options are set, and one or
more groups/regions of cells are added, you can start sampling data. To do this, click on
the
\menu{Sample} button. While data is being sampled you will see the lights next to the
\menu{Sample} button blinking. To stop sampling, click on the \menu{Stop} button.


\attention You will only be able to start sampling data if you specified the
\menu{Sampling}, \menu{Acquisition}, and \menu{Target} options, and one or more groups
were added.



\subsection{Saving and loading configurations}
\label{sec:saving-load-conf}
You can save the arrangement and the properties of the current set of plots and
\iname{Data Sampler} (\secref{sec:datasampler}). To do so, \bull{} go to \menu{Data}\ra{}\menu{Save
  Configuration} \bull{} select a location and file name. 

To read an existing configuration file, select\newline \menu{Data}\ra{}\menu{Open
  Configuration}.

\attention If groups or connections were deleted, or sizes of groups changed between
saving and loading the configuration, the effect of loading a configuration is undefined.







%\begin{figure}[!h]
%  \begin{center}
%    \fbox{\includegraphics*[scale=.6, trim= 40 370 360 300]{test}}
%    \caption{\protect\iqr{} Space plot}
%    \label{fig:GroupPlot_annotated}
%  \end{center}
%\end{figure}
%







%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "../iqrUserManual"
%%% TeX-command-default: "LaTeX PDF"
%%% End: 
