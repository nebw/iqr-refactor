#Neurons
@neurons = `ls ~/Code/iqr421_Redesign/Common/Types/Neurons/*.cpp`;
foreach $n (@neurons){
    $n =~s/\n//;
    $_ = $n;
    m/(.*)(\/)(.*)(\..*)/;
    $target = $3 . ".tex";
    $cmd = "~/bin/params2tex.pl $n > $target";
#    print $cmd . "\n";
    system ($cmd);
}

#Synapses
@synapses = `ls ~/Code/iqr421_Redesign/Common/Types/Synapses/*.cpp`;
foreach $n (@synapses){
    $n =~s/\n//;
    $_ = $n;
    m/(.*)(\/)(.*)(\..*)/;
    $target = $3 . ".tex";
    $cmd = "~/bin/params2tex.pl $n > $target";
#    print $cmd . "\n";
    system ($cmd);
}

#Modules
@modules = (
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Common/ClsPanTilt.hpp",	
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Video/bttvVideo/moduleBttvVideo.cpp",
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Video/USBVideo/moduleUSBVideo.cpp", 
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Video/USBVideoPT/moduleUSBVideoPT.cpp", 
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Video/BlimpUSB/moduleBlimpUSB.cpp",
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Robots/Blimp/moduleBlimp.cpp",
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Robots/LegoMindStorm/moduleLegoMindStorm.cpp",
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Robots/KRobots/ClsBaseKRobot.cpp",
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Robots/KRobots/Khepera/moduleKhepera.cpp",
	    "~/Code/iqr421_Redesign/Common/Types/Modules/Robots/KRobots/KoalaW/moduleKoalaW.cpp");

foreach $n (@modules){
    $n =~s/\n//;
    $_ = $n;
    m/(.*)(\/)(.*)(\..*)/;
    $target = $3 . ".tex";
    $cmd = "~/bin/params2tex.pl $n > $target";
#    print $cmd . "\n";
    system ($cmd);
}




