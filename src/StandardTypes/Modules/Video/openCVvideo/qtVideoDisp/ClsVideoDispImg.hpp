#ifndef VIDEODISP_H
#define VIDEODISP_H

#include <ctype.h>
#include <cstdlib>
#include <math.h>
#include <iostream>
#include <vector>
#include <valarray>

//--#include <QApplication>
#include <QPainter>
#include <QWidget>
#include <QPushButton>
#include <QEvent>
#include <QPixmap>
#include <QEvent>
#include <QPaintEvent>
#include <QImage>

using namespace std;

class QCEventData : public QEvent {
public:
  QCEventData(QImage *_qimage) : QEvent((QEvent::Type)65434), qimage(_qimage) {}
  QImage *getData() { return qimage; }

private:
  QImage *qimage;
};

class ClsVideoDisp : public QWidget {
  Q_OBJECT
public:
  ClsVideoDisp(const char *caption, int w, int h);

public slots:

private:
  void customEvent(QEvent *e);

  void paintEvent(QPaintEvent *);

  int iImgWidth, iImgHeight;
  QImage *qimage;
};

#endif
