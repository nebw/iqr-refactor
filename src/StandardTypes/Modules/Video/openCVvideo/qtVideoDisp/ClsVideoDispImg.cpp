#include "ClsVideoDispImg.hpp"

ClsVideoDisp::ClsVideoDisp(const char *caption, int w, int h) {
  //    cout << "ClsVideoDisp::ClsVideoDisp( const char *caption)" << endl;

  setWindowTitle(caption);
  setFixedSize(w, h);
  qimage = new QImage(width(), height(), QImage::Format_RGB888);
  qimage->setColorCount(32);
}

void ClsVideoDisp::paintEvent(QPaintEvent * /* event */) {
  //    cout << "ClsVideoDisp::paintEvent( QPaintEvent* event)" << endl;
  QPainter p(this);
  p.drawImage(0, 0, qimage->scaled(width(), height()));
};

void ClsVideoDisp::customEvent(QEvent *e) {
  //    cout << "ClsVideoDisp::customEvent( QCustomEvent* e )" << endl;
  if (dynamic_cast<QCEventData *>(e)) {
    qimage = dynamic_cast<QCEventData *>(e)->getData();
    repaint();
  }
}
