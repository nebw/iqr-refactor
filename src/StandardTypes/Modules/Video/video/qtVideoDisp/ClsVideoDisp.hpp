#ifndef VIDEODISP_H
#define VIDEODISP_H

#include <QApplication>
#include <QPainter>
#include <QWidget>
#include <QEvent>
#include <QPixmap>
#include <QPaintEvent>
#include <QImage>

using namespace std;

class QCEventData : public QEvent {
public:
  QCEventData(uchar *_frame) : QEvent((QEvent::Type)65433), frame(_frame) {}
  uchar *getData() { return frame; }

private:
  uchar *frame;
};

class ClsVideoDisp : public QWidget {
  Q_OBJECT
public:
  ClsVideoDisp(const char *caption, int w, int h);

public slots:

private:
  void customEvent(QEvent *e);

  void paintEvent(QPaintEvent *);

  int iImgWidth, iImgHeight;
  QPixmap *_buffer;
  QImage *qimage;
};

#endif
