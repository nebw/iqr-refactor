set(IntegrateFireSrc neuronIntegrateFire.cpp)
set(LinearThresholdSrc neuronLinearThreshold.cpp)
set(PyramidApicalShuntSrc neuronPyramidApicalShunt.cpp)
set(RandomSpikeSrc neuronRandomSpike.cpp)
set(SigmoidSrc neuronSigmoid.cpp)

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/neurons")

add_library(neuronIntegrateFire SHARED ${IntegrateFireSrc})
set_target_properties(neuronIntegrateFire PROPERTIES PREFIX "")

add_library(neuronLinearThreshold SHARED ${LinearThresholdSrc})
set_target_properties(neuronLinearThreshold PROPERTIES PREFIX "")

add_library(neuronPyramidApicalShunt SHARED ${PyramidApicalShuntSrc})
set_target_properties(neuronPyramidApicalShunt PROPERTIES PREFIX "")

add_library(neuronRandomSpike SHARED ${RandomSpikeSrc})
set_target_properties(neuronRandomSpike PROPERTIES PREFIX "")

add_library(neuronSigmoid SHARED ${SigmoidSrc})
set_target_properties(neuronSigmoid PROPERTIES PREFIX "")
