/****************************************************************************
 ** $Filename: ClsQlstItemConnectionHeader.h
 ** $Header$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate$
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSQLSTITEMCONNECTIONHEADER_H
#define CLSQLSTITEMCONNECTIONHEADER_H /*+ To stop multiple inclusions. +*/

#include <string>
#include <list>

#include <QTreeWidget>
#include <QWidget>
#include <QFont>
#include <QPainter>

#include <ClsQBaseItem.h>

#include <ClsSysFileNode.h>
#include <ClsBaseConnection.h>

namespace iqrprc {
class ClsParameter;
}

class ClsQlstItemConnectionHeader : public QWidget, public ClsQBaseItem {

public:
  ClsQlstItemConnectionHeader(ClsQBaseItem *_parent)
      : ClsQBaseItem(_parent, "", -1) {
    setItemName("Connections");
  };
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd /home/ulysses/Code/iqr421_Redesign/FE/IQR/ && make -k
///"
//// End:
