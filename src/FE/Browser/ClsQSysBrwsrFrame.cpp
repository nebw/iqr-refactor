#include "ClsQSysBrwsrFrame.h"

#include <qboxlayout.h>
#include <qicon.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpixmap.h>
#include <qpushbutton.h>
#include <qsplitter.h>

#include "ClsQSystemBrowser.h"

namespace {
#include "clear_14x11.xpm"
}

ClsQSysBrwsrFrame::ClsQSysBrwsrFrame(QSplitter *_qsplitter,
                                     ClsQSystemBrowser *_browser)
    : QFrame(_qsplitter), browser(_browser) {
  auto qlyt = new QVBoxLayout(this);
  qlyt->addWidget(browser);

  auto qlytFilter = new QHBoxLayout();
  auto lbl = new QLabel();
  lbl->setText("Filter");
  qlytFilter->addWidget(lbl);
  pqledValueWidget = new QLineEdit();
  qlytFilter->addWidget(pqledValueWidget);
  qpbClear = new QPushButton(QIcon(QPixmap(clear)), "");
  qpbClear->setFlat(true);
  qlytFilter->addWidget(qpbClear);

  qlyt->addLayout(qlytFilter);

  connect(pqledValueWidget, SIGNAL(returnPressed()), this,
          SLOT(filterEdited()));
  connect(qpbClear, SIGNAL(pressed()), this, SLOT(clearFilter()));
}

void ClsQSysBrwsrFrame::filterEdited() {
  browser->setFilter(pqledValueWidget->text());
}

void ClsQSysBrwsrFrame::clearFilter() {
  pqledValueWidget->setText("");
  browser->removeFilter();
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
