/****************************************************************************
 ** $Filename: ClsQSystemBrowser.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Mon Sep 10 10:53:54 2001
 ** $Date: 2002/12/08 17:33:45 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSQSYSBROWSER_H
#define CLSQSYSBROWSER_H /*+ To stop multiple inclusions. +*/

#include <qnamespace.h>
#include <qobjectdefs.h>
#include <qstring.h>
#include <qtreewidget.h>
#include <string>

#include "ClsQlstItemConnectionHeader.h"
#include "neuronManager.hpp"

class ClsQBaseItem;
class ClsQlstItemSystem;
class QContextMenuEvent;
class QMouseEvent;
class QPoint;
class QWidget;

using namespace std;

class ClsQSystemBrowser : public QTreeWidget {
  Q_OBJECT

public:
  ClsQSystemBrowser(QWidget *parent);
  ClsQBaseItem *getItem(string strID);

  void closeSystem();
  int getWidth();
  void setFilter(QString qstrFilter);
  void removeFilter();

signals:
  void sigDataDisplay(int, string, string, string);
  void sysbrowserRightClick(int, string, QPoint);
  void sigShowBlockDiagram(string);

public
slots:
  void slotSystemChanged();
  void slotItemAdded(int iType, string strID);
  void slotItemDeleted(int iType, string strID);
  void slotItemChanged(int iType, string strID);
  void slotItemUnDeleted(int iType, string strID);
  void slotDiagItemActivated(int iType, string strID);

private
slots:
  void contextMenuEvent(QContextMenuEvent *e) override;
  void startDrag(Qt::DropActions) override;

protected:
  void mouseDoubleClickEvent(QMouseEvent *e) override;

  void setRoot(string strID, string strName);
  void addProcess(string strProcessID, string strProcessName);
  void addGroup(string strProcessID, string strGroupID, string strGroupName);
  void addConnection(string strConnectionID, string strConnectionName,
                     int iConnectionType);

  ClsQlstItemSystem *qlstvitemRootElement;
  ClsQlstItemConnectionHeader *qlstvitemConnections;

  QWidget *parent;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../.. && make -k "
//// End:
