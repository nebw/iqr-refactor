#ifndef CLSQSYSBRWSRFRAME_H
#define CLSQSYSBRWSRFRAME_H

#include <qframe.h>
#include <qobjectdefs.h>

class QLineEdit;
class QPushButton;
class QSplitter;

using namespace std;

class ClsQSystemBrowser;

class ClsQSysBrwsrFrame : public QFrame {
  Q_OBJECT
public:
  ClsQSysBrwsrFrame(QSplitter *_qsplitter, ClsQSystemBrowser *_browser);

private
slots:
  void filterEdited();
  void clearFilter();

private:
  ClsQSystemBrowser *browser;
  QLineEdit *pqledValueWidget;
  QPushButton *qpbClear;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
