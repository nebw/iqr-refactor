/****************************************************************************
 ** $Filename: ClsQSystemBrowser.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Thu Sep  6 23:54:24 2001
 ** $Date: 2002/12/08 17:33:42 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <ClsFESystemManager.h>
#include <qabstractitemview.h>
#include <qdrag.h>
#include <qevent.h>
#include <qheaderview.h>
#include <qmimedata.h>
#include <qstringlist.h>
#include <qtreewidgetitemiterator.h>
#include <map>
#include <utility>

#include "ClsDragDropDeEncoder.h"
#include "ClsFEConnection.h"
#include "ClsFEGroup.h"
#include "ClsFEProcess.h"
#include "ClsQBaseItem.h"
#include "ClsQSystemBrowser.h"
#include "ClsQlstItemConnection.h"
#include "ClsQlstItemGroup.h"
#include "ClsQlstItemProcess.h"
#include "ClsQlstItemSystem.h"

class QWidget;

//#define DEBUG_CLSQSYSBROWSER

ClsQSystemBrowser::ClsQSystemBrowser(QWidget *_parent) : QTreeWidget(_parent) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::ClsQSystemBrowser( QWidget *_parent): "
          "QTreeWidget(_parent, system browser)" << endl;
#endif
  parent = _parent;

  sortItems(0, Qt::AscendingOrder);
  setSortingEnabled(true);

  qlstvitemRootElement = nullptr;
  qlstvitemConnections = nullptr;

  setRootIsDecorated(true);
  setDragEnabled(true);

  setSelectionMode(QAbstractItemView::SingleSelection);
  setAllColumnsShowFocus(true);

  setColumnCount(2);
  QStringList qstrlstHeaders;
  qstrlstHeaders << "Name"
                 << "ID";
  setHeaderLabels(qstrlstHeaders);

  header()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
};

void ClsQSystemBrowser::closeSystem() {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::closeSystem()" << endl;
#endif

  QTreeWidget::clear();

  qlstvitemRootElement = nullptr;
}

void ClsQSystemBrowser::setRoot(string strID, string strName) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::setRoot(string _strID, string strName)" << endl;
#endif
  qlstvitemRootElement = new ClsQlstItemSystem(this, strID);
  qlstvitemRootElement->setItemName(strName.c_str());
  qlstvitemRootElement->setExpanded(true);

  qlstvitemConnections = new ClsQlstItemConnectionHeader(qlstvitemRootElement);
  qlstvitemConnections->setExpanded(false);
};

void ClsQSystemBrowser::addProcess(string strProcessID, string strProcessName) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::addProcess(string strProcessID, string "
          "strProcessName)" << endl;
#endif
  ClsQlstItemProcess *qlstvitemProcess =
      new ClsQlstItemProcess(qlstvitemRootElement, strProcessID);
  qlstvitemProcess->setItemName(strProcessName.c_str());
  qlstvitemProcess->setExpanded(false);
  string strColor =
      ClsFESystemManager::Instance()->getFEProcess(strProcessID)->getColor();
  qlstvitemProcess->setColor(strColor);
};

void ClsQSystemBrowser::addGroup(string strProcessID, string strGroupID,
                                 string strGroupName) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::addGroup(string strProcessID, string strGroupID, "
          "string strGroupName,....)" << endl;
#endif
  QTreeWidgetItemIterator it(this);
  while (*it) {
    string strItemID = (*it)->text(1).toStdString();
    if (!strItemID.compare(strProcessID)) {

      if (dynamic_cast<ClsQBaseItem *>(*it)) {
        ClsQBaseItem *clsQBaseItemProcess = dynamic_cast<ClsQBaseItem *>(*it);
        if (clsQBaseItemProcess != nullptr) {
          ClsQlstItemGroup *qlstvitemGroup =
              new ClsQlstItemGroup(clsQBaseItemProcess, strGroupID);
          qlstvitemGroup->setItemName(strGroupName.c_str());
          string strColor = ClsFESystemManager::Instance()
                                ->getFEGroup(strGroupID)
                                ->getColor();
          qlstvitemGroup->setColor(strColor);
        }
      }
      break;
    }
    ++it;
  }
};

void ClsQSystemBrowser::addConnection(string strConnectionID,
                                      string strConnectionName,
                                      int iConnectionType) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::addConnection(string strConnectionID, string "
          "strConnectionName)" << endl;
#endif
  ClsQlstItemConnection *qlstvitemConnection = new ClsQlstItemConnection(
      qlstvitemConnections, strConnectionID, iConnectionType);
  qlstvitemConnection->setItemName(strConnectionName.c_str());
  qlstvitemConnection->setExpanded(false);
};

ClsQBaseItem *ClsQSystemBrowser::getItem(string _strID) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQBaseItem* ClsQSystemBrowser::getItem(string _strID)" << endl;
#endif

  QTreeWidgetItemIterator it(this);
  while (*it) {
    ClsQBaseItem *clsQBaseItem = dynamic_cast<ClsQBaseItem *>(*it);
    string strID = clsQBaseItem->getID();
    if (!strID.compare(_strID)) {
      return clsQBaseItem;
    }
    ++it;
  }
  return nullptr;
};

void ClsQSystemBrowser::startDrag(Qt::DropActions) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::startDrag()" << endl;
#endif

  auto drag = new QDrag(this);
  auto mimeData = new QMimeData;

  if (dynamic_cast<ClsQlstItemGroup *>(currentItem())) {
    string strParamList = "";
    string strRange = "";
    string strMessage = "";
    strMessage = ClsDragDropDeEncoder::encode(
        ClsFESystemManager::ITEM_GROUP,
        dynamic_cast<ClsQlstItemGroup *>(currentItem())->getID(), strParamList,
        strRange);
    mimeData->setData("text/iqr-plot", strMessage.c_str());

    drag->setMimeData(mimeData);
    drag->exec();
  } else if (dynamic_cast<ClsQlstItemConnection *>(currentItem())) {
    string strMessage = "";
    strMessage = ClsDragDropDeEncoder::encode(
        ClsFESystemManager::ITEM_CONNECTION,
        dynamic_cast<ClsQlstItemConnection *>(currentItem())->getID(), "", "");
    mimeData->setData("text/iqr-plot", strMessage.c_str());

    drag->setMimeData(mimeData);
    drag->exec();
  }
};

void ClsQSystemBrowser::slotDiagItemActivated(int iType, string strID) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::slotItemActivated(int iType, string strID)"
       << endl;
  cout << "iType: " << iType << endl;
#endif

  if (iType == ClsFESystemManager::ITEM_SYSTEM) {
    setCurrentItem(qlstvitemRootElement);
  } else if (iType == ClsFESystemManager::ITEM_PROCESS) {
    QTreeWidgetItemIterator it(this);
    while (*it) {
      if (dynamic_cast<ClsQlstItemProcess *>((*it))) {
        string strProcessID =
            dynamic_cast<ClsQlstItemProcess *>((*it))->getProcessID();
        if (!strProcessID.compare(strID)) {
          setCurrentItem(*it);
          return;
        }
      }
      ++it;
    }
  }
}

void ClsQSystemBrowser::slotSystemChanged() {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::SystemChanged()" << endl;
#endif

  string strSystemID = ClsFESystemManager::Instance()->getSystemID();
  string strSystemName = ClsFESystemManager::Instance()->getSystemName();

  setRoot(strSystemID, strSystemName);

  map<string, ClsFEProcess *> mapProcesses =
      ClsFESystemManager::Instance()->getMapFEProcesses();
  map<string, ClsFEProcess *>::iterator mapIteratorProcesses;
  for (mapIteratorProcesses = mapProcesses.begin();
       mapIteratorProcesses != mapProcesses.end(); ++mapIteratorProcesses) {
    string strProcessID = mapIteratorProcesses->second->getProcessID();
    string strProcessName = mapIteratorProcesses->second->getProcessName();
    addProcess(strProcessID, strProcessName);
  }

  map<string, ClsFEGroup *> mapGroups =
      ClsFESystemManager::Instance()->getMapFEGroups();
  map<string, ClsFEGroup *>::iterator mapIteratorGroups;
  for (mapIteratorGroups = mapGroups.begin();
       mapIteratorGroups != mapGroups.end(); ++mapIteratorGroups) {
    string strProcessID = mapIteratorGroups->second->getProcessID();
    string strGroupID = mapIteratorGroups->second->getGroupID();
    string strGroupName = mapIteratorGroups->second->getGroupName();
    addGroup(strProcessID, strGroupID, strGroupName);
  }

  map<string, ClsFEConnection *> mapConnections =
      ClsFESystemManager::Instance()->getMapFEConnections();
  map<string, ClsFEConnection *>::iterator mapIteratorConnections;
  for (mapIteratorConnections = mapConnections.begin();
       mapIteratorConnections != mapConnections.end();
       ++mapIteratorConnections) {
    string strConnectionID = mapIteratorConnections->second->getConnectionID();
    string strConnectionName =
        mapIteratorConnections->second->getConnectionName();
    int iConnectionType =
        mapIteratorConnections->second->getConnectionTypeAsInt();
    addConnection(strConnectionID, strConnectionName, iConnectionType);
  }

  sortItems(0, Qt::AscendingOrder);
};

void ClsQSystemBrowser::slotItemChanged(int iType, string strID) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::slotItemChanged(int iType, string strID )"
       << endl;
#endif
  /* to do:
     for all items:
     - check name

     for groups:
     - check if parent changed
  */

  ClsQBaseItem *clsQBaseItem = getItem(strID);
  if (clsQBaseItem != nullptr) {
    string strName = "";
    string strColor = "";
    if (iType == ClsFESystemManager::ITEM_SYSTEM) {
      strName = ClsFESystemManager::Instance()->getSystemName();
      if (strName.length() > 0) {
        clsQBaseItem->setItemName(strName);
      }
    } else if (iType == ClsFESystemManager::ITEM_PROCESS) {
      strName = ClsFESystemManager::Instance()->getProcessName(strID);
      strColor =
          ClsFESystemManager::Instance()->getFEProcess(strID)->getColor();
      if (strName.length() > 0) {
        clsQBaseItem->setItemName(strName);
        clsQBaseItem->setColor(strColor);
      }

    } else if (iType == ClsFESystemManager::ITEM_GROUP) {
      strName = ClsFESystemManager::Instance()->getGroupName(strID);
      strColor = ClsFESystemManager::Instance()->getFEGroup(strID)->getColor();

      /* check if the parent process of the group has changed */
      string strNewProcessID =
          ClsFESystemManager::Instance()->getGroupProcessID(strID);
      string strOriginalProcessID =
          dynamic_cast<ClsQlstItemGroup *>(clsQBaseItem)->getProcessID();

      /* if it changed, the delete the group, and recreate */
      if (strName.length() > 0 && strNewProcessID.length() > 0) {
        if (!strOriginalProcessID.compare(strNewProcessID)) {
          slotItemDeleted(iType, strID);
          addGroup(strNewProcessID, strID, strName);
        } else {
          clsQBaseItem->setItemName(strName);
          clsQBaseItem->setColor(strColor);
        }
      }
    } else if (iType == ClsFESystemManager::ITEM_CONNECTION) {
      strName = ClsFESystemManager::Instance()->getConnectionName(strID);
      int iConnectionType =
          ClsFESystemManager::Instance()->getConnectionTypeAsInt(strID);
      if (strName.length() > 0) {
        clsQBaseItem->setItemName(strName);
        dynamic_cast<ClsQlstItemConnection *>(clsQBaseItem)
            ->setConnectionType(iConnectionType);
      }
    }
  }

  sortItems(0, Qt::AscendingOrder);
};

void ClsQSystemBrowser::slotItemAdded(int iType, string strID) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::slotItemAdded(int iType, string strID )" << endl;
#endif

  if (iType == ClsFESystemManager::ITEM_PROCESS) {
    string strName = ClsFESystemManager::Instance()->getProcessName(strID);
    addProcess(strID, strName);
  } else if (iType == ClsFESystemManager::ITEM_GROUP) {
    string strName = ClsFESystemManager::Instance()->getGroupName(strID);
    string strProcessID =
        ClsFESystemManager::Instance()->getGroupProcessID(strID);
    addGroup(strProcessID, strID, strName);
  } else if (iType == ClsFESystemManager::ITEM_CONNECTION) {
    string strName = ClsFESystemManager::Instance()->getConnectionName(strID);
    int iConnectionType =
        ClsFESystemManager::Instance()->getConnectionTypeAsInt(strID);
    addConnection(strID, strName, iConnectionType);
  }

  sortItems(0, Qt::AscendingOrder);
}

void ClsQSystemBrowser::slotItemDeleted(int /* iType */, string strID) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::slotItemDeleted(int iType, string strID )"
       << endl;
#endif
  QTreeWidgetItem *qListViewItem = nullptr;
  QTreeWidgetItemIterator it(this);
  while (*it) {
    string strItemID = (*it)->text(1).toStdString();
    if (!strItemID.compare(strID)) {
      qListViewItem = (*it);
      break;
    }
    ++it;
  }

  if (qListViewItem != nullptr) {
    delete qListViewItem;
  }
};

void ClsQSystemBrowser::slotItemUnDeleted(int iType, string strID) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::NOT IMPLEMENTED:";
  cout << "ClsQSystemBrowser::slotItemUnDeleted(int iType, string strID )"
       << endl;
#endif
  iType = 0;
  strID = "";
  sortItems(0, Qt::AscendingOrder);
};

void ClsQSystemBrowser::mouseDoubleClickEvent(QMouseEvent *e) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::contentsMouseDoubleClickEvent ( QMouseEvent * e )"
       << endl;
#endif

  QTreeWidgetItem *item = itemAt(e->pos());
  if (dynamic_cast<ClsQlstItemProcess *>(item) ||
      dynamic_cast<ClsQlstItemSystem *>(item)) {
    string strID = ((ClsQBaseItem *)(item))->getID();
    emit sigShowBlockDiagram(strID);
  }
};

void ClsQSystemBrowser::contextMenuEvent(QContextMenuEvent *event) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::contextMenuEvent ( QContextMenuEvent * e )"
       << endl;
#endif

  QTreeWidgetItem *item = itemAt(event->pos());
  if (item) {
    string strID = ((ClsQBaseItem *)(item))->getID();

    if (dynamic_cast<ClsQlstItemSystem *>(item)) {
      emit sysbrowserRightClick(ClsFESystemManager::ITEM_SYSTEM, strID,
                                event->globalPos());
    } else if (dynamic_cast<ClsQlstItemProcess *>(item)) {
      emit sysbrowserRightClick(ClsFESystemManager::ITEM_PROCESS, strID,
                                event->globalPos());
    } else if (dynamic_cast<ClsQlstItemGroup *>(item)) {
      emit sysbrowserRightClick(ClsFESystemManager::ITEM_GROUP, strID,
                                event->globalPos());
    } else if (dynamic_cast<ClsQlstItemConnection *>(item)) {
      emit sysbrowserRightClick(ClsFESystemManager::ITEM_CONNECTION, strID,
                                event->globalPos());
    }
  }
}

int ClsQSystemBrowser::getWidth() {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::getWidth ()" << endl;
#endif
  return header()->sectionSizeHint(0);
}

void ClsQSystemBrowser::setFilter(QString qstrFilter) {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::setFilter(QString qstrFilter)" << endl;
#endif

  QTreeWidgetItemIterator it(this);
  while (*it) {
    if (dynamic_cast<ClsQlstItemGroup *>((*it)) ||
        dynamic_cast<ClsQlstItemConnection *>((*it))) {
      QString qstrItemText = (*it)->text(0);
      if (!qstrItemText.contains(qstrFilter, Qt::CaseSensitive)) {
        (*it)->setHidden(true);
      } else {
        (*it)->setHidden(false);
      }
    }
    ++it;
  }
}

void ClsQSystemBrowser::removeFilter() {
#ifdef DEBUG_CLSQSYSBROWSER
  cout << "ClsQSystemBrowser::removeFilter()" << endl;
#endif

  QTreeWidgetItemIterator it(this);
  while (*it) {
    (*it)->setHidden(false);
    ++it;
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../.. && make -k "
//// End:
