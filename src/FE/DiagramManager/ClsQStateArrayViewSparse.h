/****************************************************************************
 ** $Filename: ClsQStateArrayViewSparse.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jul  4 19:14:00 2003
 **
 *****************************************************************************/

#ifndef CLSQSTATEARRAYVIEWSPARSE_H
#define CLSQSTATEARRAYVIEWSPARSE_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <iostream>
#include <list>
#include <utility>
#include <valarray>
#include <vector>

#include "ClsBaseQStateArrayView.h"

class ClsBaseTopology;
class ClsRegion;
class QPixmap;
class QPoint;
class QWidget;

using namespace std;

class ClsQStateArrayViewSparse : public ClsBaseQStateArrayView {
  Q_OBJECT
public:
  ClsQStateArrayViewSparse(QWidget *parent = nullptr,
                           ClsBaseTopology *_clsBaseTopologyGroup = nullptr,
                           const char *name = nullptr,
                           unsigned int _iSelectionMode = 0);

  void setValue(double fValue, int i, int j) override;
  void addValue(double fValue, int i, int j) override;
  void setValue(double fValue, list<pair<int, int> > lst) override;
  void setValue(vector<pair<int, double> > lstIndexDist) override;
  void addValue(vector<pair<int, double> > lstIndexDist) override;
  void setValue(vector<pair<int, int> > lstIndexDelay) override;
  void setValue(valarray<size_t> vaIndices, valarray<double> vaData) override;

  void setRegion(double fValue, int iXStart, int iYStart, int iWidth,
                 int iHeight) override;
  void setRegion(double fValue, ClsRegion _clsRegion) override;

  list<pair<int, int> > getSelected() override;

public
slots:
  void clear() override;

protected:
  void mouseHandleEllipse(const QPoint & /* pos */) {};

  void drawCheckerBoard(QPixmap &qpm, int iOffsetX, int iOffsetY) override;

private:
  list<pair<int, int> > lstPoints;

  bool isValidCell(int iX, int iY);

  int pos2index2(int x);
  int index2pos2(int i);
};

#endif /* CLSQSTATEARRAYVIEWSPARSE_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
