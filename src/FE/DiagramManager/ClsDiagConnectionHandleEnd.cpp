#include <algorithm>
#include <string>

using namespace std;

#include <ConnEndBlue.xpm>
#include <ConnEndGreen.xpm>
#include <ConnEndRed.xpm>
#include <qdrag.h>
#include <qgraphicsscene.h>
#include <qgraphicssceneevent.h>
#include <qline.h>
#include <qmimedata.h>
#include <qnamespace.h>
#include <qpoint.h>
#include <qstring.h>
#include <qwidget.h>

#include "ClsDiagBaseConnection.h"
#include "ClsDiagConnection.h"
#include "ClsDiagConnectionHandleEnd.h"
#include "ClsDiagItemAP.h"
#include "ClsInfoConnection.h"
#include "ClsScene.h"

//#define DEBUG_CLSCANVASHANDLEEND

ClsDiagConnectionHandleEnd::ClsDiagConnectionHandleEnd(
    ClsDiagBaseConnection *_parentConnection, string _strID, int _iOrientation)
    : parentConnection(_parentConnection), strID(std::move(_strID)),
      iOrientation(_iOrientation) {

  setFlag(QGraphicsItem::ItemIgnoresParentOpacity, true);
  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemIsSelectable, false);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
  setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);

  qpmExc = QPixmap(ConnEndRed);
  qpmMod = QPixmap(ConnEndGreen);
  qpmInh = QPixmap(ConnEndBlue);
  setPixmap(qpmExc);

  qrectMyBR = boundingRect();
  setOffset(-0.5 * QPointF(qrectMyBR.width(), 0));

  if (scene() != nullptr) {
    if (dynamic_cast<ClsScene *>(scene())->getCanvasType() ==
        ClsScene::CANVAS_PROCESS) {
      setCursor(Qt::OpenHandCursor);
    } else {
      setCursor(Qt::ArrowCursor);
    }
  }
};

void ClsDiagConnectionHandleEnd::setOrientation(int _iOrientation) {
  iOrientation = _iOrientation;

  resetTransform();
  switch (iOrientation) {
  case ClsDiagConnection::WEST_EAST:
    setRotation(90);
    break;
  case ClsDiagConnection::EAST_WEST:
    setRotation(270);
    break;
  case ClsDiagConnection::NORTH_SOUTH:
    setRotation(180);
    break;
  case ClsDiagConnection::SOUTH_NORTH:
    setRotation(0);
    break;
  default:
    break;
  };
}

int ClsDiagConnectionHandleEnd::type() const { return handleendRTTI; }

string ClsDiagConnectionHandleEnd::getConnectionID() {
  if (dynamic_cast<ClsDiagConnection *>(parentItem())) {
    return dynamic_cast<ClsDiagConnection *>(parentItem())->getConnectionID();
  }
  return nullptr;
};

int ClsDiagConnectionHandleEnd::getCanvasConnectionType() {
  parentConnection->getCanvasConnectionType();
  return -1;
};

void ClsDiagConnectionHandleEnd::setTargetID(string _strTargetID) {
  strTargetID = _strTargetID;
  parentConnection->setTargetID(_strTargetID);
}

void ClsDiagConnectionHandleEnd::setConnectionType(int iConnectionType) {

  if (iConnectionType == iqrcommon::ClsInfoConnection::CONN_EXCITATORY) {
    setPixmap(qpmExc);
  } else if (iConnectionType == iqrcommon::ClsInfoConnection::CONN_INHIBITORY) {
    setPixmap(qpmInh);
  } else if (iConnectionType == iqrcommon::ClsInfoConnection::CONN_MODULATORY) {
    setPixmap(qpmMod);
  }
};

string ClsDiagConnectionHandleEnd::getAPID() {
  if (dynamic_cast<ClsDiagItemAP *>(parentItem())) {
    return dynamic_cast<ClsDiagItemAP *>(parentItem())->getParentID();
  }
  return "";
}

int ClsDiagConnectionHandleEnd::getAPNumber() {
  if (dynamic_cast<ClsDiagItemAP *>(parentItem())) {
    return dynamic_cast<ClsDiagItemAP *>(parentItem())->getNumber();
  }
  return -1;
}

void ClsDiagConnectionHandleEnd::refresh() {
#ifdef DEBUG_CLSCANVASHANDLEEND
  cout << "ClsDiagConnectionHandleEnd::refresh()" << endl;
#endif

  qrectParentBR = parentItem()->boundingRect();

  setPos(qrectParentBR.center());
  if (dynamic_cast<ClsDiagItemAP *>(parentItem())) {
    iOrientation =
        dynamic_cast<ClsDiagItemAP *>(parentItem())->getOrientation();
    setOrientation(iOrientation);
  }
  QPointF qpHere = parentItem()->scenePos() + qrectParentBR.center();

  QGraphicsLineItem *qlinePre = parentConnection->getLastSegment();
  if (qlinePre != nullptr) {

    QPointF pStart = qlinePre->line().p1();
    QPointF pEnd;

    if (iOrientation == ClsDiagConnection::WEST_EAST) {
      pEnd = QPoint(qpHere.x() - qrectMyBR.height() + 2,
                    qpHere.y() - 1); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::EAST_WEST) {
      pEnd = QPoint(qpHere.x() + qrectMyBR.height() - 2,
                    qpHere.y() - 1); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::NORTH_SOUTH) {
      pEnd = QPoint(qpHere.x(), qpHere.y() - qrectMyBR.height() + 2);
    } else if (iOrientation == ClsDiagConnection::SOUTH_NORTH) {
      pEnd = QPoint(qpHere.x() - 0.5, qpHere.y() + qrectMyBR.height() - 2);
    }

    QLineF qline(pStart, pEnd);
    qlinePre->setLine(qline);
  }
  qlinePre->setZValue(0);
  setZValue(10);
}

QVariant ClsDiagConnectionHandleEnd::itemChange(GraphicsItemChange change,
                                                const QVariant &value) {
#ifdef DEBUG_CLSCANVASHANDLEEND
  cout << "ClsDiagConnectionHandleEnd::itemChange ( GraphicsItemChange change, "
          "const QVariant & value )";
  cout << ", change: " << change << endl;
#endif

  if (change == QGraphicsItem::ItemScenePositionHasChanged && scene()) {
/* we need this for when the handle is move within a selection */
#ifdef DEBUG_CLSCANVASHANDLEEND
    cout << "QGraphicsItem::ItemScenePositionHasChanged" << endl;
#endif
    moveChildren();
  } else if (change == QGraphicsItem::ItemParentHasChanged && scene()) {
/* this is used when the handle is dropped onto a new group connector */
#ifdef DEBUG_CLSCANVASHANDLEEND
    cout << "QGraphicsItem::ItemParentHasChanged" << endl;
#endif
    if (parentItem() != nullptr) {
      refresh();
    } else {
      setTargetID("");
      QGraphicsLineItem *qlinePre = parentConnection->getLastSegment();
      if (qlinePre != nullptr) {
        QPointF p = qlinePre->line().p2();
        setPos(p);
      }
    }

    if (scene() != nullptr) {
      if (dynamic_cast<ClsScene *>(scene())->getCanvasType() ==
          ClsScene::CANVAS_PROCESS) {
        setCursor(Qt::OpenHandCursor);
      } else {
        setCursor(Qt::ArrowCursor);
      }
    }
  }
  return QGraphicsItem::itemChange(change, value);
}

void
ClsDiagConnectionHandleEnd::mousePressEvent(QGraphicsSceneMouseEvent *event) {
#ifdef DEBUG_CLSCANVASHANDLEEND
  cout << "ClsDiagConnectionHandleEnd::mousePressEvent ( "
          "QGraphicsSceneMouseEvent * event)" << endl;
#endif

  if (dynamic_cast<ClsScene *>(scene())->getCanvasType() ==
      ClsScene::CANVAS_PROCESS) {
    auto data = new QMimeData;
    QString qstr = QString("HandleEnd ") + strID.c_str() + QString(" ") +
                   parentConnection->connTypeAsString().c_str();
    data->setText(qstr);
    auto drag = new QDrag(event->widget());
    drag->setMimeData(data);
    drag->exec();
  } else {
    event->ignore();
  }
}

void ClsDiagConnectionHandleEnd::moveChildren() {
#ifdef DEBUG_CLSCANVASHANDLEEND
  cout << "ClsDiagConnectionHandleEnd::moveChildrenBy ( qreal dx, qreal dy )"
       << endl;
#endif

  QPointF qpHere = parentItem()->scenePos() + qrectParentBR.center();

  QGraphicsLineItem *qlinePre = parentConnection->getLastSegment();
  if (qlinePre != nullptr) {

    QPointF pStart = qlinePre->line().p1();
    QPointF pEnd;
    if (iOrientation == ClsDiagConnection::WEST_EAST) {
      pEnd = QPoint(qpHere.x() - qrectMyBR.height() + 2,
                    qpHere.y() - 1); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::EAST_WEST) {
      pEnd = QPoint(qpHere.x() + qrectMyBR.height() - 2,
                    qpHere.y() - 1); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::NORTH_SOUTH) {
      pEnd = QPoint(qpHere.x(), qpHere.y() - qrectMyBR.height() + 2);
    } else if (iOrientation == ClsDiagConnection::SOUTH_NORTH) {
      pEnd = QPoint(qpHere.x() - 0.5, qpHere.y() + qrectMyBR.height() - 2);
    }

    QLineF qline(pStart, pEnd);
    qlinePre->setLine(qline);
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
