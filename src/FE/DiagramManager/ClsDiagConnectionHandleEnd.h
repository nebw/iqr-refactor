#ifndef CLSCANVASHANDLEEND_H
#define CLSCANVASHANDLEEND_H

#include <qgraphicsitem.h>
#include <qpixmap.h>
#include <qrect.h>
#include <qvariant.h>
#include <string>

class QGraphicsSceneMouseEvent;

using namespace std;

static const int handleendRTTI = 984379;

class ClsDiagBaseConnection;

class ClsDiagConnectionHandleEnd : public QGraphicsPixmapItem {
public:
  ClsDiagConnectionHandleEnd(ClsDiagBaseConnection *_parentConnection,
                             string _strID, int iOrientation);

  int type() const override;

  void setOrientation(int iOrientation);
  int getOrientation() { return iOrientation; }

  void setTargetID(string _strTargetID);
  string getTargetID() {
    return strTargetID;
  };

  string getID() { return strID; }
  string getConnectionID();
  int getCanvasConnectionType();

  void setConnectionType(int _iConnectionType);

  void refresh();

  string getAPID();

  QVariant itemChange(GraphicsItemChange change,
                      const QVariant &value) override;

  void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
  void moveChildren();

  int getAPNumber();

private:
  QPixmap qpmExc, qpmInh, qpmMod;

  ClsDiagBaseConnection *parentConnection;

  string strID;
  int iOrientation;
  string strTargetID;
  QRectF qrectParentBR;
  QRectF qrectMyBR;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
