/****************************************************************************
 ** $Filename: ClsQStateArrayViewRect.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Tue Sep  9 17:19:01 2003
 **
 *****************************************************************************/

#include <ext/alloc_traits.h>
#include <qbrush.h>
#include <qcoreevent.h>
#include <qdrawutil.h>
#include <qevent.h>
#include <qframe.h>
#include <qnamespace.h>
#include <qpainter.h>
#include <qpalette.h>
#include <qpen.h>
#include <qpixmap.h>
#include <qtooltip.h>
#include <qwidget.h>
#include <string>

#include "ClsBaseTopology.h"
#include "ClsQStateArrayViewRect.h"
#include "iqrUtils.h"
#include "parameter.hpp"
#include "parameterList.hpp"
#include "tagLibrary.hpp"

//#define DEBUG_CLSQSTATEARRAYVIEWRECT

ClsQStateArrayViewRect::ClsQStateArrayViewRect(
    QWidget *parent, ClsBaseTopology *_clsBaseTopologyGroup,
    unsigned int _iSelectionMode)
    : ClsBaseQStateArrayView(parent, _clsBaseTopologyGroup, _iSelectionMode) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect( QWidget *parent, ClsBaseTopology "
          "*_clsBaseTopologyGroup,  unsigned int _iSelectionMode ) " << endl;
#endif

  iCellSize = 20;
  iNrCellsX = 10;
  iNrCellsY = 10;

  ParameterList parameterList = _clsBaseTopologyGroup->getListParameters();

  while (parameterList.size()) {
    string strParamName = parameterList.front()->getName();
    string strParamValue = parameterList.front()->getValueAsString();
    if (!strParamName.compare(ClsTagLibrary::TopologyWidthTag())) {
      iNrCellsX = iqrUtils::string2int(strParamValue);
    } else if (!strParamName.compare(ClsTagLibrary::TopologyHeightTag())) {
      iNrCellsY = iqrUtils::string2int(strParamValue);
    }
    parameterList.pop_front();
  }

  iCellSize = iSizeMax / (iNrCellsX > iNrCellsY ? iNrCellsX : iNrCellsY);
  if (iCellSize <= 1) {
    iCellSize = 2;
  }

  vCells.resize(iNrCellsX + 1);
  for (int ii = 0; ii < (iNrCellsX + 1); ii++) {
    vCells[ii].resize(iNrCellsY + 1);
  }

  this->setFrameStyle(QFrame::WinPanel | QFrame::Raised);

  setMinimumSize(iNrCellsX * iCellSize + 2 * BORDER,
                 iNrCellsY * iCellSize + 2 * BORDER);
  setMaximumSize(iNrCellsX * iCellSize + 2 * BORDER,
                 iNrCellsY * iCellSize + 2 * BORDER);

  clear();
  createNullPixmap();
}

void ClsQStateArrayViewRect::clear() {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::clear()" << endl;
#endif

  for (int i = 0; i <= iNrCellsX; i++) {
    for (int j = 0; j <= iNrCellsY; j++) {
      vCells[i][j] = 0.;
    }
  }

  pmGridValues = pmGridEmpty;

  repaint();
}

list<pair<int, int> > ClsQStateArrayViewRect::getSelected() {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::getSelected()" << endl;
#endif

  list<pair<int, int> > lst;
  for (int i = 1; i <= iNrCellsX; i++) {
    for (int j = 1; j <= iNrCellsY; j++) {
      double fValue = vCells[i][j];
      if (fValue > 0) {
        pair<int, int> pairTemp(i, j);
        lst.push_back(pairTemp);
      }
    }
  }
  return lst;
};

void ClsQStateArrayViewRect::setValue(double fValue, int i, int j) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::setValue( double fValue, int i, int j )"
       << endl;
#endif

  if (i > 0 && j > 0 && i <= iNrCellsX && j <= iNrCellsY) {
    vCells[i][j] = fValue;
    QBrush qb(Qt::yellow);
    qb.setColor(getColor(fValue));

    QPainter paintPM(&pmGridValues);
    qDrawPlainRect(&paintPM, groupCoord2pixelCoord(i) - BORDER,
                   groupCoord2pixelCoord(j) - BORDER, iCellSize - 1,
                   iCellSize - 1, Qt::black, 0, &qb);
    paintPM.end();

    repaint();
  }
}

void ClsQStateArrayViewRect::addValue(double fValue, int i, int j) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::addValue( double fValue, int i, int j )"
       << endl;
#endif

  if (i > 0 && j > 0 && i <= iNrCellsX && j <= iNrCellsY) {
    vCells[i][j] += fValue;
    QBrush qb(Qt::yellow);
    QPainter paintPM(&pmGridValues);
    qb.setColor(getColor(fValue));
    qDrawPlainRect(&paintPM, groupCoord2pixelCoord(i) - BORDER,
                   groupCoord2pixelCoord(j) - BORDER, iCellSize - 1,
                   iCellSize - 1, Qt::black, 0, &qb);
    paintPM.end();
  }
}

void ClsQStateArrayViewRect::setValue(double fValue,
                                      list<pair<int, int> > lst) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::setValue(double fValue, list<pair<int, int> "
          "> lst)" << endl;
#endif

  list<pair<int, int> >::iterator it;
  for (it = lst.begin(); it != lst.end(); ++it) {
    int i, j;
    i = (*it).first;
    j = (*it).second;
    setValue(fValue, i, j);
  }
}

void ClsQStateArrayViewRect::setValue(vector<pair<int, double> > lstIndexDist) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::setValue(vector<pair<int, double> > "
          "lstIndexDist )" << endl;
#endif
  vector<pair<int, double> >::iterator it;
  for (it = lstIndexDist.begin(); it != lstIndexDist.end(); ++it) {
    int iIndex = (*it).first;
    double fValue = (*it).second;
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    setValue(fValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewRect::addValue(vector<pair<int, double> > lstIndexDist) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::addValue(vector<pair<int, double> > "
          "lstIndexDist )" << endl;
#endif
  vector<pair<int, double> >::iterator it;
  for (it = lstIndexDist.begin(); it != lstIndexDist.end(); ++it) {
    int iIndex = (*it).first;
    double fValue = (*it).second;
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    addValue(fValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewRect::setValue(vector<pair<int, int> > lstIndexDelay) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::setValue(vector<pair<int, int> > "
          "lstIndexDelay)" << endl;
#endif
  vector<pair<int, int> >::iterator it;
  for (it = lstIndexDelay.begin(); it != lstIndexDelay.end(); ++it) {
    int iIndex = (*it).first;
    int iValue = (*it).second;
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    setValue((double)iValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewRect::setValue(valarray<size_t> vaIndices,
                                      valarray<double> vaData) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::setValue(valarray<size_t> vaIndices, "
          "valarray<double> vaData)" << endl;
#endif
  for (unsigned int ii = 0; ii < vaIndices.size(); ii++) {
    int iIndex = vaIndices[ii];
    double fValue = vaData[ii];
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    setValue(fValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewRect::mouseHandleEllipse(const QPointF &_pos) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::mouseHandleEllipse( const QPointF &_pos )"
       << endl;
#endif

  int iX = pixelCoord2groupCoord(_pos.x());
  int iY = pixelCoord2groupCoord(_pos.y());

  int iWidth = 9;
  int iHeight = 5;
  bool bUseFloat = false;

  list<pair<int, int> > lst;

  QPainter paint(this);
  QPen qp(Qt::yellow);
  paint.setPen(qp);
  if (bUseFloat) {
    lst = clsBaseTopologyGroup->getPoints4Ellipse(
        (double)iX + .5, (double)iY + .5, (double)iWidth, (double)iHeight);

    setValue(.5, lst);

    paint.drawRect(groupCoord2pixelCoord(iX) + iCellSize / 2,
                   groupCoord2pixelCoord(iY) + iCellSize / 2, iCellSize,
                   iCellSize);

    paint.drawEllipse(groupCoord2pixelCoord(iX) + iCellSize -
                          groupCoord2pixelCoord(iWidth) / 2,
                      groupCoord2pixelCoord(iY) + iCellSize -
                          groupCoord2pixelCoord(iHeight) / 2,
                      groupCoord2pixelCoord(iWidth),
                      groupCoord2pixelCoord(iHeight));
  } else {
    lst = clsBaseTopologyGroup->getPoints4Ellipse(
        (double)iX, (double)iY, (double)iWidth, (double)iHeight);
    setValue(fFixedValue, lst);
    paint.drawRect(groupCoord2pixelCoord(iX), groupCoord2pixelCoord(iY),
                   iCellSize, iCellSize);

    paint.drawEllipse(groupCoord2pixelCoord(iX) + iCellSize / 2 -
                          groupCoord2pixelCoord(iWidth) / 2,
                      groupCoord2pixelCoord(iY) + iCellSize / 2 -
                          groupCoord2pixelCoord(iHeight) / 2,
                      groupCoord2pixelCoord(iWidth),
                      groupCoord2pixelCoord(iHeight));
  }
}

void ClsQStateArrayViewRect::drawCheckerBoard(QPixmap &qpm, int iOffsetX,
                                              int iOffsetY) {
#ifdef DEBUG_CLSQSTATEARRAYVIEWRECT
  cout << "ClsQStateArrayViewRect::drawCheckerBoard(QPixmap &qpm, int "
          "iOffsetX, int iOffsetY)" << endl;
#endif

  QPainter p(&qpm);

  QPen qp;
  QBrush qb(Qt::SolidPattern);

  p.fillRect(0, 0, qpm.width(), qpm.height(),
             palette().color(QPalette::Window));
  p.setPen(QPen(Qt::NoPen));

  for (int i = 1; i <= iNrCellsX; i++) {
    for (int j = 1; j <= iNrCellsY; j++) {
      double fValue = vCells[i][j];
      qb.setColor(getColor(fValue));
      p.setBrush(qb);
      p.drawRect(groupCoord2pixelCoord(i) + iOffsetX,
                 groupCoord2pixelCoord(j) + iOffsetY, iCellSize - 1,
                 iCellSize - 1);
    }
  }
}

bool ClsQStateArrayViewRect::event(QEvent *event) {

  if (event->type() == QEvent::ToolTip) {
    QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
    int iNewIndex = pos2index(pixelCoord2groupCoord(helpEvent->x()),
                              pixelCoord2groupCoord(helpEvent->y()));
    if (iNewIndex != iLastToolTipIndex) {
      QToolTip::showText(helpEvent->globalPos(),
                         iqrUtils::int2string(iNewIndex).c_str());
      iLastToolTipIndex = iNewIndex;
    }
    return true;
  }
  return QWidget::event(event);
}

int ClsQStateArrayViewRect::pixelCoord2groupCoord(int _x) {
  return (_x - BORDER) / iCellSize + 1;
}

int ClsQStateArrayViewRect::groupCoord2pixelCoord(int i) {
  return (i - 1) * iCellSize + BORDER;
}

int ClsQStateArrayViewRect::pos2index(int x, int y) {
  return (y - 1) * iNrCellsX + x;
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
