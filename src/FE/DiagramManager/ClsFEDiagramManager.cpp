#include <ClsFESystemManager.h>
#include <qmessagebox.h>
#include <qwidget.h>

#include "ClsFEDiagramManager.h"
#include "ClsQGroupStateManip.h"
#include "diagramTypes.h"

//#define DEBUG_CLSFEDIAGRAMMANAGER

ClsFEDiagramManager *ClsFEDiagramManager::_instanceDiagramManager = nullptr;

void ClsFEDiagramManager::initializeDiagramManager(QWidget *_toplevel) {
  _instanceDiagramManager = new ClsFEDiagramManager(_toplevel);
}

ClsFEDiagramManager *ClsFEDiagramManager::Instance() {
  return _instanceDiagramManager;
}

ClsFEDiagramManager::ClsFEDiagramManager(QWidget *_toplevel)
    : toplevel(_toplevel) {};

void ClsFEDiagramManager::DiagramShow(int iClientType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::DiagramShow(int iClientType , string strID)"
       << endl;
#endif
  if (iClientType == diagramTypes::DIAGRAM_BLOCK) {
    ClsBlockDiagram::Instance()->showProcessTab(strID);
  }
};

void ClsFEDiagramManager::slotBlockDiagramShow(string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotBlockDiagramShow(string strID)" << endl;
#endif
  ClsBlockDiagram::Instance()->showProcessTab(strID);
};

void ClsFEDiagramManager::slotSystemChanged() {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotSystemChanged()" << endl;
#endif

  ClsBlockDiagram::Instance()->slotSystemChanged();
}

void ClsFEDiagramManager::closeSystem() {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::closeSystem()" << endl;
#endif
  ClsBlockDiagram::Instance()->cleanup();
}

void ClsFEDiagramManager::slotItemChanged(int iType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotItemChanged(int iType, string strID "
          ")::strID: " << strID << endl;
  cout << "iType: " << iType << endl;
#endif
  ClsBlockDiagram::Instance()->slotItemChanged(iType, strID);

  if (iType == ClsFESystemManager::ITEM_GROUP) {
    QMap<QString, QWidget *>::iterator it;
    for (it = qmapDiagrams.begin(); it != qmapDiagrams.end(); ++it) {
      if (dynamic_cast<ClsQGroupStateManip *>(it.value())) {
        (dynamic_cast<ClsQGroupStateManip *>(it.value()))->slotGroupChanged();
      }
    }
  } else if (iType == ClsFESystemManager::ITEM_CONNECTION) {
    QMap<QString, QWidget *>::iterator it;
    for (it = qmapDiagrams.begin(); it != qmapDiagrams.end(); ++it) {
    }
  }
}

void ClsFEDiagramManager::slotItemDeleted(int iType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotItemDeleted(int iType, string strID )"
       << endl;
#endif

  ClsBlockDiagram::Instance()->slotItemDeleted(iType, strID);

  if (iType == ClsFESystemManager::ITEM_GROUP) {
    if (qmapDiagrams.find(QString(strID.c_str())) != qmapDiagrams.end()) {
      qmapDiagrams.find(QString(strID.c_str())).value()->close();
    }

    /* find the connection diagrams that use the group */
    QMap<QString, QWidget *>::iterator it;
    for (it = qmapDiagrams.begin(); it != qmapDiagrams.end(); ++it) {
    }

  } else if (iType == ClsFESystemManager::ITEM_CONNECTION) {
    if (qmapDiagrams.find(QString(strID.c_str())) != qmapDiagrams.end()) {
      qmapDiagrams.find(QString(strID.c_str())).value()->close();
    }
  }
}

void ClsFEDiagramManager::slotItemUnDeleted(int iType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotItemUnDeleted(int iType, string strID )"
       << endl;
#endif
  ClsBlockDiagram::Instance()->slotItemUnDeleted(iType, strID);
}

void ClsFEDiagramManager::DiagramCreate(int iClientType) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::DiagramCreate(int iClientType)" << endl;
#endif

  if (ClsBlockDiagram::Instance() != nullptr) {
    delete ClsBlockDiagram::Instance();
  }

  ClsBlockDiagram::initializeBlockDiagram(toplevel);
  ClsBlockDiagram::Instance()->show();

  connect(ClsBlockDiagram::Instance(),
          SIGNAL(sigDiagItemActivated(int, string)), this,
          SIGNAL(sigDiagItemActivated(int, string)));
  connect(ClsBlockDiagram::Instance(),
          SIGNAL(sigDiagViewActivated(int, string)), this,
          SIGNAL(sigDiagViewActivated(int, string)));
};

void ClsFEDiagramManager::DiagramCreate(int iClientType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::DiagramCreate(int iClientType , string strID)"
       << endl;
#endif

  if (iClientType == diagramTypes::DIAGRAM_CONNECTION) {
    if (qmapDiagrams.find(QString(strID.c_str())) != qmapDiagrams.end()) {
      qmapDiagrams.find(QString(strID.c_str())).value()->raise();
    } else {
      string strSourceID =
          ClsFESystemManager::Instance()->getConnectionSourceID(strID);
      string strTargetID =
          ClsFESystemManager::Instance()->getConnectionTargetID(strID);

      if (strSourceID.length() <= 0 && strTargetID.length() <= 0) {
        QMessageBox::information(nullptr, "IQR",
                                 "Cannot create diagram.\n"
                                 "Source and Target Groups do not exist.");
      } else if (strSourceID.length() <= 0) {
        QMessageBox::information(nullptr, "IQR",
                                 "Cannot create diagram.\n"
                                 "Source Group does not exist.");
      } else if (strTargetID.length() <= 0) {
        QMessageBox::information(nullptr, "IQR",
                                 "Cannot create diagram.\n"
                                 "Target Group does not exist.");
      }
    }
  } else if (iClientType == diagramTypes::DIAGRAM_GROUP_MANIP) {
    if (qmapDiagrams.find(QString(strID.c_str())) != qmapDiagrams.end()) {
      qmapDiagrams.find(QString(strID.c_str())).value()->raise();
    } else {
      ClsQGroupStateManip *clsQGroupStateManip =
          new ClsQGroupStateManip("", strID);
      clsQGroupStateManip->show();
      QString qstr(strID.c_str());
      qmapDiagrams[qstr] = (QWidget *)clsQGroupStateManip;
      connect(clsQGroupStateManip, SIGNAL(sigDiagramClosed(string)), this,
              SLOT(slotDiagramClosed(string)));
    }
  }
};

void ClsFEDiagramManager::saveConfig() {};

void ClsFEDiagramManager::applyConfig() {};

ClsInfoDiagramIcon ClsFEDiagramManager::getDiagramIcon(string strID) {
  return ClsBlockDiagram::Instance()->getDiagramIcon(strID);
}

ClsInfoDiagramLine ClsFEDiagramManager::getDiagramLine(string strID) {
  return ClsBlockDiagram::Instance()->getDiagramLine(strID);
};

void ClsFEDiagramManager::printBlockDiagram() {
  ClsBlockDiagram::Instance()->print();
};

void ClsFEDiagramManager::saveBlockDiagram() {
  ClsBlockDiagram::Instance()->save();
};

void ClsFEDiagramManager::slotItemAdded(int iType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotItemAdded(int iType, string strID )"
       << endl;
#endif
  ClsBlockDiagram::Instance()->slotItemAdded(iType, strID); //###
}

void ClsFEDiagramManager::slotItemDuplicated(int iType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotItemDuplicated(int iType, string strID )"
       << endl;
#endif
  ClsBlockDiagram::Instance()->slotItemDuplicated(iType, strID); //###
}

void ClsFEDiagramManager::slotDiagramClosed(string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotDiagramClosed(string)" << endl;
#endif

  if (qmapDiagrams.find(strID.c_str()) != qmapDiagrams.end()) {
    qmapDiagrams.erase(qmapDiagrams.find(strID.c_str()));
  }
}

void ClsFEDiagramManager::slotSimulationRunning(bool b) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::slotSimulationRunning(bool)" << endl;
#endif

  ClsBlockDiagram::Instance()->disableToolbarButtons(b);
}

void ClsFEDiagramManager::markItem(int iType, string strID) {
#ifdef DEBUG_CLSFEDIAGRAMMANAGER
  cout << "ClsFEDiagramManager::markItem(int iType, string strID)" << endl;
#endif
  ClsBlockDiagram::Instance()->markItem(iType, strID);
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
