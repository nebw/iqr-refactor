#ifndef CLSPANNERSCENEVIEW_H
#define CLSPANNERSCENEVIEW_H

#include <qgraphicsview.h>
#include <qobjectdefs.h>
#include <qrect.h>

class QMouseEvent;
class QPainter;
class QWidget;

using namespace std;

class ClsPannerSceneView : public QGraphicsView {
  Q_OBJECT

public:
  ClsPannerSceneView(QWidget *parent, QGraphicsView *_viewReference);

signals:
  void sigScrolledToSceneCenterPos(int, int);

private:
  void mouseMoveEvent(QMouseEvent *e) override;
  void drawForeground(QPainter *painter, const QRectF &rect) override;

  QGraphicsView *viewReference;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
