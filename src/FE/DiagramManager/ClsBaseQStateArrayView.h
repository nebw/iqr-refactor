/****************************************************************************
 ** $Filename: ClsBaseQStateArrayView.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Thu Jul  3 22:43:17 2003
 **
 *****************************************************************************/

#ifndef CLSBASEQSTATEARRAYVIEW_H
#define CLSBASEQSTATEARRAYVIEW_H /*+ To stop multiple inclusions. +*/

#include <math.h>
#include <qcolor.h>
#include <qframe.h>
#include <qobjectdefs.h>
#include <qpixmap.h>
#include <qpoint.h>
#include <iostream>
#include <list>
#include <utility>
#include <valarray>
#include <vector>

#include "ClsSubPopulations.h"

class ClsBaseTopology;
class QMouseEvent;
class QPaintEvent;
class QWidget;

using namespace std;

class ClsBaseQStateArrayView : public QFrame {
  Q_OBJECT
public:
  ClsBaseQStateArrayView(QWidget *parent = nullptr,
                         ClsBaseTopology *_clsBaseTopologyGroup = nullptr,
                         unsigned int _iSelectionMode = 0);

  virtual void setValue(double /* fValue */, int /* i */, int /* j */) = 0;
  virtual void addValue(double /* fValue */, int /* i */, int /* j */) = 0;

  virtual void setValue(double /* fValue */,
                        list<pair<int, int> > /* lst */) = 0;

  virtual void setValue(vector<pair<int, double> > /* lstIndexDist */) = 0;
  virtual void addValue(vector<pair<int, double> > lstIndexDist) = 0;

  virtual void setValue(vector<pair<int, int> > /* lstIndexDelay */) = 0;

  virtual void setValue(valarray<size_t>, valarray<double>) = 0;

  virtual void setRegion(double fValue, int iXStart, int iYStart, int iWidth,
                         int iHeight);
  virtual void setRegion(double fValue, ClsRegion _clsRegion);

  virtual void markCell(int iColor, int iX, int iY);
  virtual void markCell(int iColor, list<int> lstIndex);

  virtual list<pair<int, int> > getSelected() {
    list<pair<int, int> > l;
    return l;
  };

  vector<vector<double> > getMatrix() {
    return vCells;
  };

  void setMatrix(vector<vector<double> > _v) {
    vCells = _v;
    drawCheckerBoard(pmGridValues, -BORDER, -BORDER);
    repaint();
  }

  const ClsRegion getRegion();

  QPixmap getValuePixmap(int iImgWidth, int iImgHeight);
  QPixmap getGradientPixmap(int iImgWidth, int iImgHeight,
                            int iColorMode = ClsBaseQStateArrayView::BLUE2RED);

  enum SELECTIONMODE {
    LIST = 0,
    REGION,
    SINGLE
  };

  enum COLORMODE {
    GRAY,
    BLUE2RED,
    HSV
  };

  void setColorMode(int i) {
    if (i == ClsBaseQStateArrayView::GRAY ||
        i == ClsBaseQStateArrayView::BLUE2RED ||
        i == ClsBaseQStateArrayView::HSV) {
      iColorMode = i;
    }
  }

  void setMinValue(double f) {
    fValMin = f;
    fScaleFactor = 255. / fabs(fValMin - fValMax);
    fFixedValue = (fValMin + fValMax) / 2.; // new
  };

  void setMaxValue(double f) {
    fValMax = f;
    fScaleFactor = 255. / fabs(fValMin - fValMax);
    fFixedValue = (fValMin + fValMax) / 2.; // new
  };

signals:
  void cellClicked(int, int);

public
slots:
  virtual void clear() {};
  void setValue(double f) {
    fFixedValue = f;
  };

protected:
  QColor getColor(double fValue);

  enum {
    MAXSIZE = 50,
    MINSIZE = 10,
    BORDER = 5
  };

  virtual void paintEvent(QPaintEvent *) override;
  virtual void mouseHandle(const QPointF &pos);
  virtual void mouseMoveEvent(QMouseEvent *) override;
  virtual void mousePressEvent(QMouseEvent *) override;
  virtual void mouseHandleRegion(const QPointF &pos);
  virtual void mouseHandleEllipse(const QPointF &pos) = 0;
  virtual void mouseReleaseEvent(QMouseEvent *) override;

  virtual void createNullPixmap();

  virtual void drawCheckerBoard(QPixmap &qpm, int iOffsetX, int iOffsetY) = 0;

  virtual int pixelCoord2groupCoord(int x) = 0;
  virtual int groupCoord2pixelCoord(int i) = 0;

  static const int iSizeMax = 200;
  ClsBaseTopology *clsBaseTopologyGroup;
  unsigned int iSelectionMode;

  int iCellSize;
  int iNrCellsX, iNrCellsY;
  double fFixedValue, fValMax, fValMin, fScaleFactor;
  vector<vector<double> > vCells;

  int iXPosStart, iYPosStart;
  QPixmap pmGridEmpty;
  QPixmap pmGridValues;

  ClsRegion clsRegion;

  int iColorMode;
};

#endif /* CLSBASEQSTATEARRAYVIEW_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
