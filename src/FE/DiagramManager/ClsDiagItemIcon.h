/****************************************************************************
 ** $Filename: ClsDiagItemIcon.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Wed Nov 19 10:18:09 2003
 **
 *****************************************************************************/

#ifndef CLSCANVASNODEICON_H
#define CLSCANVASNODEICON_H /*+ To stop multiple inclusions. +*/

#include <iostream>

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QImage>
#include <QPainter>
#include <QSize>

#include "moduleIcon.hpp"

namespace {
#include <group_module_in_14x14.xpm>
#include <group_module_out_14x14.xpm>
#include <group_synapse_in_14x14.xpm>
#include <process_link_23x29.xpm>
}

static const int nodegroupIconRTTI = 984385;

class ClsDiagItemIcon : public QGraphicsRectItem {

public:
  enum ICON_TYPE {
    ICON_GROUP_MODULE_IN,
    ICON_GROUP_MODULE_OUT,
    ICON_GROUP_SYNAPSE_IN,
    ICON_PROCESS_LINK
  };

  ClsDiagItemIcon(QGraphicsItem *_parent) : QGraphicsRectItem(_parent) {
    image = nullptr;
    imageDisabled = nullptr;
    bEnabled = true;
  }

  ClsDiagItemIcon(QGraphicsItem *_parent, int iIconType)
      : QGraphicsRectItem(_parent) {
    /* pcData = NULL; */
    bEnabled = true;
    image = nullptr;
    imageDisabled = nullptr;
    if (iIconType == ICON_GROUP_MODULE_IN) {
      image = new QImage(group_module_in);
    } else if (iIconType == ICON_GROUP_MODULE_OUT) {
      image = new QImage(group_module_out);
    } else if (iIconType == ICON_GROUP_SYNAPSE_IN) {
      image = new QImage(group_synapse_in);
    } else if (iIconType == ICON_PROCESS_LINK) {
      image = new QImage(process_link);
    }
    if (image != nullptr) {
      setRect(0, 0, image->width(), image->height());
      qsizeIcon.setWidth(image->width());
      qsizeIcon.setHeight(image->height());
    }
  }

  ClsDiagItemIcon(QGraphicsItem *_parent, moduleIcon mi)
      : QGraphicsRectItem(_parent) {
    /// cout << "ClsDiagItemIcon( QCanvas * _parent, moduleIcon mi)" << endl;
    image = nullptr;
    imageDisabled = nullptr;
    if (mi.data != nullptr) {

      image = new QImage();
      image->loadFromData(mi.data, mi.size, nullptr);
      setRect(0, 0, image->width(), image->height());

      qsizeIcon.setWidth(image->width());
      qsizeIcon.setHeight(image->height());
      imageDisabled = new QImage(*image);
      image->detach();
      imageDisabled->invertPixels(QImage::InvertRgb);
    }
  }

  ~ClsDiagItemIcon() {
    if (image != nullptr) {
      delete image;
      image = nullptr;
    }

    if (imageDisabled != nullptr) {
      delete imageDisabled;
      imageDisabled = nullptr;
    }
  }

  void selectIcon(int iIconType) {
    if (image != nullptr) {
      delete image;
      image = nullptr;
    }
    if (iIconType == ICON_GROUP_MODULE_IN) {
      image = new QImage(group_module_in);
    } else if (iIconType == ICON_GROUP_MODULE_OUT) {
      image = new QImage(group_module_out);
    } else if (iIconType == ICON_GROUP_SYNAPSE_IN) {
      image = new QImage(group_synapse_in);
    } else if (iIconType == ICON_PROCESS_LINK) {
      image = new QImage(process_link);
    }
    if (image != nullptr) {
      setRect(0, 0, image->width(), image->height());

      qsizeIcon.setWidth(image->width());
      qsizeIcon.setHeight(image->height());
    }
  }

  int type() const override { return nodegroupIconRTTI; }

  void setEnabled(bool b) {
    bEnabled = b;
    update();
  }

private:
  void paint(QPainter *painter, const QStyleOptionGraphicsItem * /*option*/,
             QWidget * /*widget*/) override {
    if (bEnabled) {
      if (image != nullptr) {
        painter->drawImage(boundingRect(), *image);
      }
    } else {
      if (imageDisabled != nullptr) {
        painter->drawImage(boundingRect(), *imageDisabled);
      }
    }
  }

  QImage *image;
  QImage *imageDisabled;
  bool bEnabled;
  QSize qsizeIcon;
};

#endif /* CLSCANVASNODEICON_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../.. && make -k "
//// End:
