#include <qevent.h>
#include <qframe.h>
#include <qnamespace.h>
#include <qpainter.h>
#include <qpen.h>
#include <qpoint.h>

#include "ClsPannerSceneView.h"

class QWidget;

//#define DEBUG_CLSPANNERSCENEVIEW

ClsPannerSceneView::ClsPannerSceneView(QWidget *_parent,
                                       QGraphicsView *_viewReference)
    : QGraphicsView(_parent), viewReference(_viewReference) {
#ifdef DEBUG_CLSPANNERSCENEVIEW
  cout << "ClsPannerSceneView::ClsPannerSceneView(QWidget* parent,const char* "
          "name)" << endl;
#endif
  setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
  setFrameStyle(QFrame::Panel | QFrame::Plain);
  setLineWidth(2);
  setDragMode(QGraphicsView::ScrollHandDrag);
}

void ClsPannerSceneView::mouseMoveEvent(QMouseEvent *event) {
#ifdef DEBUG_CLSPANNERSCENEVIEW
  cout << "ClsPannerSceneView::mouseMoveEvent ( QMouseEvent * event)" << endl;
#endif
  if (event->buttons() == Qt::LeftButton) {
    QPointF qp = mapToScene(event->pos());
    emit sigScrolledToSceneCenterPos(qp.x(), qp.y());
  }
}

void ClsPannerSceneView::drawForeground(QPainter *painter, const QRectF &rect) {
#ifdef DEBUG_CLSPANNERSCENEVIEW
  cout << "ClsPannerSceneView::drawForeground ( QPainter * painter, const "
          "QRectF & rect )" << endl;
#endif

  QPointF qp =
      viewReference->mapToScene(viewReference->sceneRect().topLeft().x(),
                                viewReference->sceneRect().topLeft().y());

  int iX = qp.x();
  int iY = qp.y();

  painter->setPen(QPen(Qt::black, 40));

  painter->drawRect(iX, iY, viewReference->width(), viewReference->height());
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
