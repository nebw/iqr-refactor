#ifndef CLSCANVASCONNECTIONSEGMENT_H
#define CLSCANVASCONNECTIONSEGMENT_H

#include <string>

#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsSceneContextMenuEvent>

#include <ClsFEDialogManager.h>
#include <ClsFESystemManager.h>
#include <ClsBlockDiagram.h>
#include "ClsDiagBaseConnection.h"
#include "ClsDiagConnection.h"

static const int connectionsegmentRTTI = 984382;
using namespace std;

class ClsDiagConnectionSegment : public QGraphicsLineItem {

public:
  ClsDiagConnectionSegment(ClsDiagBaseConnection *_parent)
      : QGraphicsLineItem(_parent),
        parent(_parent) {};

  string getSourceID() {
    return parent->getSourceID();
  };
  string getTargetID() {
    return parent->getTargetID();
  };
  string getParentID() {
    return parent->getConnectionID();
  };
  string getConnectionID() {
    return parent->getConnectionID();
  };

  void split(QPointF qpPos) {
    if (dynamic_cast<ClsDiagConnection *>(parent)) {
      dynamic_cast<ClsDiagConnection *>(parent)->split(this, qpPos);
    }
  }

  void setSelected(bool b) {
    markSegment(b);
    parent->setSelected(b);
  }

  void setPen(QPen pen) {
    pen.setCapStyle(Qt::RoundCap);
    qpenNormal = pen;
    qpenSelected = pen;
    qpenSelected.setWidth(pen.width() * 1.8);
    QGraphicsLineItem::setPen(pen);
  }

  void markSegment(bool b) {
    if (b) {
      setPen(qpenSelected);
    } else {
      setPen(qpenNormal);
    }
    QGraphicsItem::setSelected(b);
  }

  int type() const override { return connectionsegmentRTTI; }

  [[deprecated]]
  void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override {}

  void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override {
    ClsFEDialogManager::Instance()->createPropertyDialog(
        ClsFESystemManager::ITEM_CONNECTION, parent->getConnectionID(),
        event->screenPos());
  }

  virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) override {
    if (event->button() == Qt::LeftButton &&
        ClsBlockDiagram::Instance()->infoMode()) {
      ClsFEDialogManager::Instance()->createInfoTip(
          ClsFESystemManager::ITEM_CONNECTION, parent->getConnectionID(),
          event->screenPos());
    } else if (event->button() == Qt::RightButton) {
      ClsFEDialogManager::Instance()->createPopup(
          ClsFESystemManager::ITEM_CONNECTION, parent->getConnectionID(),
          event->screenPos());

    } else if (event->button() == Qt::LeftButton &&
               event->modifiers() == Qt::ControlModifier) {
      split(event->scenePos());
    }
  }

private:
  ClsDiagBaseConnection *parent;
  QPen qpenNormal;
  QPen qpenSelected;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
