#ifndef CLSCANVASNODEAP_H
#define CLSCANVASNODEAP_H

#include <qgraphicsitem.h>
#include <qgraphicsscene.h>
#include <qlist.h>
#include <qpoint.h>
#include <qrect.h>
#include <qstring.h>
#include <string>

#include "neuronManager.hpp"

class QGraphicsSceneDragDropEvent;
class QGraphicsSceneMouseEvent;

using namespace std;

static const int nodeAPRTTI = 984381;
static const int nodeAPPhantomRTTI = 984384;

class ClsDiagItemAP : public QGraphicsPixmapItem {
public:
  ClsDiagItemAP(QGraphicsItem *parent, string _strParentID, int _iOrientation,
                bool _bPhantom = false);

  ~ClsDiagItemAP() {
    QList<QGraphicsItem *> lst = childItems();
    QList<QGraphicsItem *>::iterator it;
    for (it = lst.begin(); it != lst.end(); ++it) {
      (*it)->setParentItem(nullptr);
    }
    if (scene()) {
      scene()->removeItem(this);
    }
  }

  int getOrientation() {
    return iOrientation;
  };
  int type() const override {
    if (bPhantom) {
      return nodeAPPhantomRTTI;
    }
    return nodeAPRTTI;
  }

  int getNumber();

  bool hasAPHandle(QString qstrID);

  void moveChildren();

  string getParentID() {
    return strParentID;
  };

  bool isFree();

  void mousePressEvent(QGraphicsSceneMouseEvent *) override;

  void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
  void dragLeaveEvent(QGraphicsSceneDragDropEvent *event) override;
  void dropEvent(QGraphicsSceneDragDropEvent *event) override;

  QPointF getCenter() {
    return scenePos() + boundingRect().center();
  };

private:
  string strParentID;
  int iOrientation;
  bool bPhantom;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
