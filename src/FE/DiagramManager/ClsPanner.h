#ifndef CLSQCANVASPANNER_H
#define CLSQCANVASPANNER_H

#include <qframe.h>
#include <qobjectdefs.h>

class ClsPannerSceneView;
class QGraphicsScene;
class QWidget;

using namespace std;

class ClsSceneView;

class ClsPanner : public QFrame {
  Q_OBJECT

public:
  ClsPanner(QWidget *_parent, ClsSceneView *_clsSceneView);
  void setScene(QGraphicsScene *clsScene);

signals:
  void sigScrolledToSceneCenterPost(int, int);

public
slots:
  void canvasResized();

private
slots:
  void scrolledToSceneCenterPos(int, int);
  void parentScrolledTo(int, int);

private:
  ClsPannerSceneView *clsPannerSceneView;
  ClsSceneView *clsSceneView;
  double dFactorX;
  double dFactorY;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
