#include "ClsScene.h"

#include <ClsBlockDiagram.h>
#include <qgraphicsitem.h>
#include <qgraphicssceneevent.h>
#include <qlist.h>
#include <qnamespace.h>
#include <algorithm>
#include <list>

#include "ClsDiagItemProcess.h"
#include "neuronManager.hpp"

class QObject;

ClsScene::ClsScene(QObject *parent, string _strID, int _iCanvasType)
    : QGraphicsScene(parent), strID(std::move(_strID)),
      iCanvasType(_iCanvasType) {};

ClsScene::~ClsScene() {

  /* let's remove all the left overs... */
  list<QGraphicsItem *> lstItemToDelete;
  QList<QGraphicsItem *> qCanvasItemList = items();

  QList<QGraphicsItem *>::iterator itq;
  for (itq = qCanvasItemList.begin(); itq != qCanvasItemList.end(); ++itq) {
    if ((*itq)->type() == nodeprocessRTTI) {
      lstItemToDelete.push_back((*itq));
    }
  }

  list<QGraphicsItem *>::iterator itDelete;
  for (itDelete = lstItemToDelete.begin(); itDelete != lstItemToDelete.end();
       ++itDelete) {
    delete (*itDelete);
  }
};

int ClsScene::getCanvasType() {
  return iCanvasType;
};
string ClsScene::getID() {
  return strID;
};

void ClsScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
  if (event->button() == Qt::RightButton) {
    ClsBlockDiagram::Instance()->mouseRightClickPane(strID, event->scenePos());
  } else if (event->button() == Qt::LeftButton) {
    ClsBlockDiagram::Instance()->mouseLeftClickPane(strID, event->scenePos());
  }
  QGraphicsScene::mousePressEvent(event);
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
