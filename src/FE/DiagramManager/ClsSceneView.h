#ifndef CLSQDIAGRAMCANVASVIEW_H
#define CLSQDIAGRAMCANVASVIEW_H

#include <qgraphicsview.h>
#include <qobjectdefs.h>
#include <map>
#include <string>

class QMouseEvent;
class QWidget;

using namespace std;

#define MAX_X_CANVAS 20000
#define MAX_Y_CANVAS 20000

class ClsSceneView : public QGraphicsView {
  Q_OBJECT

public:
  ClsSceneView(QWidget *parent);
  void clear();
  const map<string, int> getListSelectedItems();

signals:
  void sigGotFocus(int, string);
  void sigSceneScrolledToCenterPos(int, int);

private:
  void mousePressEvent(QMouseEvent *event) override;

  void scrollContentsBy(int dx, int dy) override;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
