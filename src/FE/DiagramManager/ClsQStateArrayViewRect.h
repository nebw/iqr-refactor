/****************************************************************************
 ** $Filename: ClsQStateArrayView.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jul  4 14:06:04 2003
 **
 *****************************************************************************/

#ifndef CLSQSTATEARRAYVIEW_H
#define CLSQSTATEARRAYVIEW_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <qpoint.h>
#include <iostream>
#include <list>
#include <utility>
#include <valarray>
#include <vector>

#include "ClsBaseQStateArrayView.h"

class ClsBaseTopology;
class QEvent;
class QPixmap;
class QWidget;

using namespace std;

class ClsQStateArrayViewRect : public ClsBaseQStateArrayView {
  Q_OBJECT
public:
  ClsQStateArrayViewRect(QWidget *parent = nullptr,
                         ClsBaseTopology *_clsBaseTopologyGroup = nullptr,
                         unsigned int _iSelectionMode = 0);

  void setValue(double fValue, int i, int j) override;
  void addValue(double fValue, int i, int j) override;
  void setValue(double fValue, list<pair<int, int> > lst) override;
  void setValue(vector<pair<int, double> > lstIndexDist) override;
  void addValue(vector<pair<int, double> > lstIndexDist) override;
  void setValue(vector<pair<int, int> > lstIndexDelay) override;
  void setValue(valarray<size_t> vaIndices, valarray<double> vaData) override;

  list<pair<int, int> > getSelected() override;

public
slots:
  void clear() override;

protected:
  void mouseHandleEllipse(const QPointF &pos) override;
  void drawCheckerBoard(QPixmap &qpm, int iOffsetX, int iOffsetY) override;
  bool event(QEvent *event) override;

private:
  int pixelCoord2groupCoord(int x) override;
  int groupCoord2pixelCoord(int i) override;
  int pos2index(int x, int y);

  int iLastToolTipIndex;
};

#endif /* CLSQSTATEARRAYVIEW_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
