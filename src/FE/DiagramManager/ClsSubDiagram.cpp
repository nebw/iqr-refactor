#include <qboxlayout.h>
#include <qcolor.h>
#include <qcursor.h>
#include <qdrawutil.h>
#include <qgraphicsscene.h>
#include <qgraphicsview.h>
#include <qicon.h>
#include <qmatrix.h>
#include <qnamespace.h>
#include <qpainter.h>
#include <qpalette.h>
#include <qpixmap.h>
#include <qscrollbar.h>
#include <qtabbar.h>
#include <qvariant.h>
#include <qwidget.h>
#include <cassert>

#include "ClsBlockDiagram.h"
#include "ClsFESystemManager.h"
#include "ClsPanner.h"
#include "ClsScene.h"
#include "ClsSceneView.h"
#include "ClsSubDiagram.h"
#include "LetterP_2_8x12.xpm"
#include "LetterS_2_8x12.xpm"
#include "cursors/new_connection_cursor.xpm"
#include "cursors/new_group_cursor.xpm"
#include "cursors/new_process_cursor.xpm"

class QPaintEvent;
class QResizeEvent;

//#define DEBUG_CLSSUBDIAGRAM

ClsSubDiagram::ClsSubDiagram(QFrame *parent,
                             ClsBlockDiagram *_clsBlockDiagramParent, int _iID,
                             int _iSpacerHeight)
    : QFrame(parent), clsBlockDiagramParent(_clsBlockDiagramParent),
      iSpacerHeight(_iSpacerHeight), iID(_iID) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::ClsSubDiagram(ClsBlockDiagram* "
          "clsBlockDiagramParent,const char* name)" << endl;
#endif

  setAttribute(Qt::WA_DeleteOnClose);

  setFrameStyle(QFrame::WinPanel | QFrame::Raised);
  //    setLineWidth (10);

  bFocus = false;
  iAddState = ADD_STATE_NULL;
  createCursors();

  clsSceneView = new ClsSceneView(this);
  clsSceneView->setCursor(Qt::ArrowCursor);
  clsSceneView->setTransformationAnchor(QGraphicsView::NoAnchor);
  clsSceneView->setResizeAnchor(QGraphicsView::NoAnchor);

  qtabbar = new QTabBar(this);
  connect(qtabbar, SIGNAL(/*selected(int)*/ currentChanged(int)),
          SLOT(slotTabChanged(int)));

  clsPanner = new ClsPanner(this, clsSceneView);

  qboxlayout = new QVBoxLayout(this);
  qboxlayout->setContentsMargins(0, 0, 0, 0);
  qboxlayout->setSpacing(0);

  /* TODO: it's getting better and better... not the qwgtSpacer needs to exist
   * to ensure the tabbar doesn't dissapear, but it needs not have any minimum
   * height specified... */
  qwgtSpacer = new QWidget(this);
  qwgtSpacer->show();
  qwgtSpacer->setMinimumHeight(iSpacerHeight);

  qboxlayout->addWidget(qwgtSpacer, 0, nullptr);

  qboxlayout->addWidget(qtabbar, 0, nullptr);
  qboxlayout->addWidget(clsSceneView, 10, nullptr);

  connect(clsSceneView, SIGNAL(sigGotFocus(int, string)), this,
          SLOT(slotGotFocus(int, string)));
  connect(clsPanner, SIGNAL(sigScrolledToSceneCenterPost(int, int)), this,
          SLOT(scrollSceneToCenterPos(int, int)));

  qtabbar->show();
  clsSceneView->show();
  clsPanner->show();
  this->show();
}

void ClsSubDiagram::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  QColor qcolorFrame;
  if (bFocus) {
    qcolorFrame = Qt::red;
  } else {
    qcolorFrame = palette().color(QPalette::Window);
  }
  qDrawPlainRect(&painter, frameRect(), qcolorFrame, lineWidth());
}

void ClsSubDiagram::scrollSceneToCenterPos(int iX, int iY) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::scrollSceneToCenterPos(int iX, int iX): " << iX
       << ", " << iY << endl;
#endif
  clsSceneView->centerOn(iX, iY);
};

string ClsSubDiagram::getNewConnectionProcessID() {
  return clsBlockDiagramParent->getNewConnectionProcessID();
};

void ClsSubDiagram::slotTabChanged(int ii) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::slotTabChanged(): " << ii << endl;
#endif

  if (qtabbar->currentIndex() != ii) {
    qtabbar->setCurrentIndex(ii);
  }

  QString qstr = qtabbar->tabData(ii).toString();

  ClsScene *clsSceneTemp = clsBlockDiagramParent->getCanvas(qstr);
  if (clsSceneTemp != nullptr) {
    clsSceneView->setScene(clsSceneTemp);
    clsPanner->setScene(clsSceneTemp);

    int iType = clsSceneTemp->getCanvasType();
    string strID = clsSceneTemp->getID();

    if (iAddState >= 0) {
      setCursor(iAddState, iType);
    }
    slotGotFocus(iType, strID);
  }
}

void ClsSubDiagram::resizeEvent(QResizeEvent *) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::resizeEvent ( QResizeEvent * )" << endl;
#endif

  int iOffSet = 16;

  QScrollBar *qsV = clsSceneView->verticalScrollBar();
  if (qsV != nullptr) {
    iOffSet = qsV->width() + frameWidth();
  }
  clsPanner->move(width() - clsPanner->width() - iOffSet,
                  height() - clsPanner->height() - iOffSet);
}

void ClsSubDiagram::addTab(const ClsFESystemManager::POPUP_ITEM iType,
                           string strName, string strID) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::addTab(int iType, string strName, string strID)"
       << endl;
#endif

  assert(iType == ClsFESystemManager::ITEM_SYSTEM ||
         iType == ClsFESystemManager::ITEM_PROCESS);

  int iT;
  if (iType == ClsFESystemManager::ITEM_SYSTEM) {
    iT = qtabbar->addTab(QIcon(QPixmap(LetterS_2)), strName.c_str());
  } else {
    iT = qtabbar->addTab(QIcon(QPixmap(LetterP_2)), strName.c_str());
  }

  qtabbar->setTabData(iT, QVariant(strID.c_str()));
  qtabbar->repaint();
};

void ClsSubDiagram::removeTab(string strID) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::removeTab( string strID): " << strID << endl;
#endif

  for (int ii = 0; ii < qtabbar->count(); ii++) {
    QString qstrTab = qtabbar->tabData(ii).toString();
    if (!strID.compare(qstrTab.toStdString())) {
      qtabbar->removeTab(ii);
    }
  }
  qtabbar->repaint();
};

void ClsSubDiagram::renameTab(string strID, string strName) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::renameTab( string strID, string strName)" << endl;
#endif

  for (int ii = 0; ii < qtabbar->count(); ii++) {
    QString qstrTab = qtabbar->tabData(ii).toString();
    if (!strID.compare(qstrTab.toStdString())) {
      qtabbar->setTabText(ii, strName.c_str());
    }
  }
  qtabbar->repaint();
};

void ClsSubDiagram::useSpacer(bool b) {
  if (b) {
    qwgtSpacer->setMinimumHeight(iSpacerHeight);
  } else {
    qwgtSpacer->setMinimumHeight(0);
  }
};

QGraphicsScene *ClsSubDiagram::scene() {
  return clsSceneView->scene();
};

void ClsSubDiagram::setScene(ClsScene *q) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::setScene(ClsScene *q)" << endl;
#endif

  clsSceneView->setScene(q);
  clsPanner->setScene(q);

  //TODO: check cursor?
};

void ClsSubDiagram::zoomIn() {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::zoomIn()" << endl;
#endif
  QMatrix m = clsSceneView->matrix();
  m.scale(1.1111, 1.1111);
  clsSceneView->setMatrix(m);
  clsPanner->canvasResized();
};

void ClsSubDiagram::zoomOut() {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::zoomOut()" << endl;
#endif
  QMatrix m = clsSceneView->matrix();
  m.scale(0.9, 0.9);
  clsSceneView->setMatrix(m);
  clsPanner->canvasResized();
};

void ClsSubDiagram::showProcessTab(string strProcessID) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::showProcessTab(string strProcessID)" << endl;
#endif

  QString qstr = strProcessID.c_str();

  ClsScene *clsSceneTemp = clsBlockDiagramParent->getCanvas(qstr);
  if (clsSceneTemp != nullptr) {
    clsSceneView->setScene(clsSceneTemp);

    clsPanner->setScene(clsSceneTemp);
  } else {
  }

  for (int ii = 0; ii < qtabbar->count(); ii++) {
    QString qstrTab = qtabbar->tabData(ii).toString();
    if (!strProcessID.compare(qstrTab.toStdString())) {
      if (qtabbar->currentIndex() != ii) {
        qtabbar->setCurrentIndex(ii);
      }
    }
  }

  int iType = clsSceneTemp->getCanvasType();
  string strID = clsSceneTemp->getID();
  emit canvasChanged(iType, strID);
}

void ClsSubDiagram::slotGotFocus(int iType, string strID) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::slotDiagramCanvasViewGotFocus()" << endl;
#endif

  ClsScene *q = dynamic_cast<ClsScene *>(clsSceneView->scene());

  if (q != nullptr) {
    int iType = q->getCanvasType();

    mark(true);
    emit sigGotFocus(iID, iType, q->getID());

    if (iType == ClsScene::CANVAS_SYSTEM) {
      emit sigDiagItemActivated(ClsFESystemManager::ITEM_SYSTEM, q->getID());
    } else if (iType == ClsScene::CANVAS_PROCESS) {
      emit sigDiagItemActivated(ClsFESystemManager::ITEM_PROCESS, q->getID());
    }
  }
  bFocus = true;
}

void ClsSubDiagram::mark(bool b) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "void ClsSubDiagram::mark(bool b)" << endl;
#endif
  bFocus = b;
  repaint();
}

void ClsSubDiagram::createCursors() {
  QPixmap qpmNG(new_group_cursor);
  QPixmap qpmNP(new_process_cursor);
  QPixmap qpmNC(new_connection_cursor);

  qcursorNewGroup = QCursor(qpmNG, 0, 0);
  qcursorNewProcess = QCursor(qpmNP, 0, 0);
  qcursorNewConnection = QCursor(qpmNC, 0, 0);
}

void ClsSubDiagram::setCursor(int _iAddState, int _iCanvasType) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::setCursor(int _iAddState, int _iCanvasType): "
       << _iAddState << ", " << _iCanvasType << endl;
#endif

  if (clsSceneView != nullptr) {
    if (_iAddState > 0) {
      if (_iCanvasType == ClsScene::CANVAS_SYSTEM) {
        if (_iAddState == ClsBlockDiagram::ADD_NODE) {
          clsSceneView->setCursor(qcursorNewProcess);
          clsSceneView->viewport()->setCursor(qcursorNewProcess);
        } else {
          clsSceneView->setCursor(Qt::ArrowCursor);
          clsSceneView->viewport()->setCursor(Qt::ArrowCursor);
        }
      } else if (_iCanvasType == ClsScene::CANVAS_PROCESS) {
        if (_iAddState == ClsBlockDiagram::ADD_NODE) {
          clsSceneView->setCursor(qcursorNewGroup);
          clsSceneView->viewport()->setCursor(qcursorNewGroup);
        } else {
          clsSceneView->setCursor(qcursorNewConnection);
          clsSceneView->viewport()->setCursor(qcursorNewConnection);
        }
      }
    } else {
      clsSceneView->setCursor(Qt::ArrowCursor);
      clsSceneView->viewport()->setCursor(Qt::ArrowCursor);
    }
  }
}

void ClsSubDiagram::setInfoMode(bool b) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::setInfoMode(bool b): " << b << endl;
#endif
  bInfoMode = b;

  if (b) {
    clsSceneView->setCursor(Qt::WhatsThisCursor);
    clsSceneView->viewport()->setCursor(Qt::WhatsThisCursor);
  } else {
    clsSceneView->setCursor(Qt::ArrowCursor);
    clsSceneView->viewport()->setCursor(Qt::ArrowCursor);
  }
}

void ClsSubDiagram::setAddState(int iState) {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::setAddState(): " << iState << ", ID: " << iID << endl;
#endif
  iAddState = iState;
  if (clsSceneView->scene() != nullptr) {
    int iCanvasType =
        dynamic_cast<ClsScene *>(clsSceneView->scene())->getCanvasType();
    if (!bInfoMode) {
      setCursor(iAddState, iCanvasType);
    }
  }
}

const map<string, int> ClsSubDiagram::getListSelectedItems() {
  return clsSceneView->getListSelectedItems();
};

void ClsSubDiagram::clear() {
#ifdef DEBUG_CLSSUBDIAGRAM
  cout << "ClsSubDiagram::clear()" << endl;
#endif
  clsSceneView->clear();
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
