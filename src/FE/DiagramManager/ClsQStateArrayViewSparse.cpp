/****************************************************************************
 ** $Filename: ClsQStateArrayViewSparse.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Tue Sep  9 17:18:57 2003
 **
 *****************************************************************************/

#include <ext/alloc_traits.h>
#include <qbrush.h>
#include <qdrawutil.h>
#include <qframe.h>
#include <qnamespace.h>
#include <qpainter.h>
#include <qpalette.h>
#include <qpixmap.h>

#include "ClsBaseTopology.h"
#include "ClsHyperLists.h"
#include "ClsQStateArrayViewSparse.h"
#include "ClsSubPopulations.h"
#include "ClsTopologySparse.h"
#include "parameterList.hpp"

class QWidget;

ClsQStateArrayViewSparse::ClsQStateArrayViewSparse(
    QWidget *parent, ClsBaseTopology *_clsBaseTopologyGroup, const char *name,
    unsigned int _iSelectionMode)
    : ClsBaseQStateArrayView(parent, _clsBaseTopologyGroup, _iSelectionMode) {

  iCellSize = 20;
  iNrCellsX = 10;
  iNrCellsY = 10;

  ParameterList parameterList = _clsBaseTopologyGroup->getListParameters();

  int iXMax = 0;
  int iYMax = 0;

  if (dynamic_cast<ClsTopologySparse *>(_clsBaseTopologyGroup)) {
    lstPoints =
        (dynamic_cast<ClsTopologySparse *>(_clsBaseTopologyGroup))->getList();
    list<pair<int, int> >::iterator it;
    for (it = lstPoints.begin(); it != lstPoints.end(); ++it) {
      int iX = (*it).first;
      int iY = (*it).second;
      iXMax = (iX > iXMax ? iX : iXMax);
      iYMax = (iY > iYMax ? iY : iYMax);
    }
  }

  iNrCellsX = iXMax;
  iNrCellsY = iYMax;

  iCellSize = iSizeMax / (iNrCellsX > iNrCellsY ? iNrCellsX : iNrCellsY);

  vCells.resize(iNrCellsX + 1);
  for (int ii = 0; ii < (iNrCellsX + 1); ii++) {
    vCells[ii].resize(iNrCellsY + 1);
  }

  for (int i = 0; i <= iNrCellsX; i++) {
    for (int j = 0; j <= iNrCellsY; j++) {
      vCells[i][j] = -99; //!!!!!!!!!!!!!
    }
  }

  list<pair<int, int> >::iterator it;
  for (it = lstPoints.begin(); it != lstPoints.end(); ++it) {
    int iX = (*it).first;
    int iY = (*it).second;
    vCells[iX][iY] = 0.;
  }

  this->setFrameStyle(QFrame::WinPanel | QFrame::Raised);

  setMinimumSize(iNrCellsX * iCellSize + 2 * BORDER,
                 iNrCellsY * iCellSize + 2 * BORDER);
  setMaximumSize(iNrCellsX * iCellSize + 2 * BORDER,
                 iNrCellsY * iCellSize + 2 * BORDER);

  clear();
  createNullPixmap();
}

void ClsQStateArrayViewSparse::clear() {
  for (int i = 0; i <= iNrCellsX; i++) {
    for (int j = 0; j <= iNrCellsY; j++) {
      vCells[i][j] = -99; //!!!!!!!!!!!!!
    }
  }

  list<pair<int, int> >::iterator it;
  for (it = lstPoints.begin(); it != lstPoints.end(); ++it) {
    int iX = (*it).first;
    int iY = (*it).second;
    vCells[iX][iY] = 0.;
  }
  this->render(&pmGridEmpty);
  pmGridValues = pmGridEmpty.copy();
}

list<pair<int, int> > ClsQStateArrayViewSparse::getSelected() {
  list<pair<int, int> > lst;
  for (int i = 1; i <= iNrCellsX; i++) {
    for (int j = 1; j <= iNrCellsY; j++) {
      double fValue = vCells[i][j];
      if (fValue != -99 && fValue != 0) {
        pair<int, int> pairTemp(i, j);
        lst.push_back(pairTemp);
      }
    }
  }
  return lst;
};

void ClsQStateArrayViewSparse::setValue(double fValue, int i, int j) {
  if (isValidCell(i, j)) {
    vCells[i][j] = fValue;
    QBrush qb(Qt::yellow);
    QPainter paintPM(&pmGridValues);
    qb.setColor(getColor(fValue));
    qDrawPlainRect(&paintPM, index2pos2(i) - BORDER, index2pos2(j) - BORDER,
                   iCellSize - 1, iCellSize - 1, Qt::black, 0, &qb);
    paintPM.end();
    this->render(&pmGridValues);
  }
}

void ClsQStateArrayViewSparse::addValue(double fValue, int i, int j) {
  if (isValidCell(i, j)) {
    vCells[i][j] += fValue;
    QBrush qb(Qt::yellow);
    QPainter paintPM(&pmGridValues);
    qb.setColor(getColor(fValue));
    qDrawPlainRect(&paintPM, index2pos2(i) - BORDER, index2pos2(j) - BORDER,
                   iCellSize - 1, iCellSize - 1, Qt::black, 0, &qb);
    paintPM.end();
  }
}

void ClsQStateArrayViewSparse::setValue(double fValue,
                                        list<pair<int, int> > lst) {
  list<pair<int, int> >::iterator it;
  for (it = lst.begin(); it != lst.end(); ++it) {
    int i, j;
    i = (*it).first;
    j = (*it).second;
    setValue(fValue, i, j);
  }
}

void
ClsQStateArrayViewSparse::setValue(vector<pair<int, double> > lstIndexDist) {
  vector<pair<int, double> >::iterator it;
  for (it = lstIndexDist.begin(); it != lstIndexDist.end(); ++it) {
    int iIndex = (*it).first;
    double fValue = (*it).second;
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    setValue(fValue, pPoint.first, pPoint.second);
  }
}

void
ClsQStateArrayViewSparse::addValue(vector<pair<int, double> > lstIndexDist) {
  vector<pair<int, double> >::iterator it;
  for (it = lstIndexDist.begin(); it != lstIndexDist.end(); ++it) {
    int iIndex = (*it).first;
    double fValue = (*it).second;
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    addValue(fValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewSparse::setValue(vector<pair<int, int> > lstIndexDelay) {
  vector<pair<int, int> >::iterator it;
  for (it = lstIndexDelay.begin(); it != lstIndexDelay.end(); ++it) {
    int iIndex = (*it).first;
    int iValue = (*it).second;
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    setValue(iValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewSparse::setValue(valarray<size_t> vaIndices,
                                        valarray<double> vaData) {
  for (unsigned int ii = 0; ii < vaIndices.size(); ii++) {
    int iIndex = vaIndices[ii];
    double fValue = vaData[ii];
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    setValue(fValue, pPoint.first, pPoint.second);
  }
}

void ClsQStateArrayViewSparse::setRegion(double fValue, int iXStart,
                                         int iYStart, int iWidth, int iHeight) {
  /* 1. find cells within region */
  list<tiPoint> lstPointsWithin;
  list<pair<int, int> >::iterator it;
  for (it = lstPoints.begin(); it != lstPoints.end(); ++it) {
    int iX = (*it).first;
    int iY = (*it).second;
    if (iX >= iXStart && iX < (iXStart + iWidth) && iY >= iYStart &&
        iY < (iYStart + iHeight)) {
      lstPointsWithin.push_back(*it);
    }
  }

  /* 2. find min/max x/y for these cells */
  int iHMax = 0, iVMax = 0;
  int iHMin = 99999, iVMin = 99999;

  list<tiPoint>::iterator it1;
  for (it1 = lstPointsWithin.begin(); it1 != lstPointsWithin.end(); ++it1) {
    int iX = (*it1).first;
    iHMax = (iX > iHMax ? iX : iHMax);
    iHMin = (iX < iHMin ? iX : iHMin);
    int iY = (*it1).second;
    iVMax = (iY > iVMax ? iY : iVMax);
    iVMin = (iY < iVMin ? iY : iVMin);
  }

  /* 3. reshape region */
  iXStart = iHMin;
  iYStart = iVMin;
  iWidth = iHMax - iHMin + 1;
  iHeight = iVMax - iVMin + 1;

  clsRegion.setXStart(iXStart);
  clsRegion.setYStart(iYStart);
  clsRegion.setWidth(iWidth);
  clsRegion.setHeight(iHeight);

  for (int ii = iXStart; ii < (iXStart + iWidth); ii++) {
    for (int jj = iYStart; jj < (iYStart + iHeight); jj++) {
      setValue(fValue, ii, jj);
    }
  }
}

void ClsQStateArrayViewSparse::setRegion(double fValue, ClsRegion _clsRegion) {
  int iXStart = _clsRegion.getXStart();
  int iYStart = _clsRegion.getYStart();
  int iWidth = _clsRegion.getWidth();
  int iHeight = _clsRegion.getHeight();

  setRegion(fValue, iXStart, iYStart, iWidth, iHeight);
}

void ClsQStateArrayViewSparse::drawCheckerBoard(QPixmap &qpm, int iOffsetX,
                                                int iOffsetY) {

  QPainter paint(&qpm);

  QBrush qb(Qt::yellow);
  for (int i = 1; i <= iNrCellsX; i++) {
    for (int j = 1; j <= iNrCellsY; j++) {
      double fValue = vCells[i][j];
      if (fValue == -99) {
        qb.setColor(palette().color(QPalette::Window));
      } else {
        qb.setColor(getColor(fValue));
      }
      qDrawPlainRect(&paint, index2pos2(i) + iOffsetX, index2pos2(j) + iOffsetY,
                     iCellSize - 1, iCellSize - 1, Qt::black, 0, &qb);
    }
  }
}

bool ClsQStateArrayViewSparse::isValidCell(int _iX, int _iY) {
  list<pair<int, int> >::iterator it;
  for (it = lstPoints.begin(); it != lstPoints.end(); ++it) {
    int iX = (*it).first;
    int iY = (*it).second;
    if (_iX == iX && _iY == iY) {
      return true;
    }
  }
  return false;
}

int ClsQStateArrayViewSparse::pos2index2(int x) {
  return (x - BORDER) / iCellSize + 1;
}

int ClsQStateArrayViewSparse::index2pos2(int i) {
  return (i - 1) * iCellSize + BORDER;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
