#include <qgraphicsitem.h>
#include <qstring.h>

#include "ClsDiagConnection.h"
#include "ClsDiagConnectionHandleEnd.h"
#include "ClsDiagConnectionHandleStart.h"
#include "ClsDiagConnectionSegment.h"
#include "ClsDiagHyperConnection.h"
#include "ClsDiagPhantomConnection.h"
#include "neuronManager.hpp"

//#define DEBUG_CLSDIAGPHANTOMCONNECTION

ClsDiagPhantomConnection::ClsDiagPhantomConnection(
    ClsDiagHyperConnection *_hyperParent, int _iConnectionType,
    int _iCanvasConnectionType)
    : ClsDiagBaseConnection(_hyperParent, _iConnectionType,
                            _iCanvasConnectionType) {
  clsDiagConnectionSegment = nullptr;
}

ClsDiagPhantomConnection::~ClsDiagPhantomConnection() {
#ifdef DEBUG_CLSDIAGPHANTOMCONNECTION
  cout << "ClsDiagPhantomConnection()::~ClsDiagPhantomConnection()" << endl;
#endif

  if (clsCanvasHandleStart != nullptr) {
    delete clsCanvasHandleStart;
    clsCanvasHandleStart = nullptr;
  }

  if (clsCanvasHandleEnd != nullptr) {
    delete clsCanvasHandleEnd;
    clsCanvasHandleEnd = nullptr;
  }
};

void ClsDiagPhantomConnection::addSegment(QPointF qp0, QPointF qp1,
                                          int iPosition, int iOrientation) {
#ifdef DEBUG_CLSDIAGPHANTOMCONNECTION
  cout << "ClsDiagPhantomConnection::addSegment(QPointF qp0, QPointF qp1, int "
          "iPosition, int iOrientation)" << endl;
#endif

  string strID = hyperParent->getID();

  QString qstrNum;
  clsDiagConnectionSegment = new ClsDiagConnectionSegment(this);
  clsDiagConnectionSegment->setPen(qpen);
  clsDiagConnectionSegment->setZValue(0);

  if (iPosition == ClsDiagConnection::SEG_SINGLE) {
    clsCanvasHandleStart =
        new ClsDiagConnectionHandleStart(this, strID, iOrientation);
    clsCanvasHandleStart->setConnectionType(iConnectionType);
    clsCanvasHandleStart->setZValue(10);
    clsCanvasHandleStart->show();

    clsCanvasHandleEnd =
        new ClsDiagConnectionHandleEnd(this, strID, iOrientation);
    clsCanvasHandleEnd->setConnectionType(iConnectionType);
    clsCanvasHandleEnd->setZValue(10);
    clsCanvasHandleEnd->show();
  }
}

void ClsDiagPhantomConnection::setConnectionType(int _iConnectionType) {
  ClsDiagBaseConnection::setConnectionType(_iConnectionType);
  if (clsDiagConnectionSegment != nullptr) {
    clsDiagConnectionSegment->setPen(qpen);
  }
}

void ClsDiagPhantomConnection::setSourceID(string _strSourceID) {
#ifdef DEBUG_CLSDIAGPHANTOMCONNECTION
  cout << "ClsDiagPhantomConnection::setSourceID(string _strSourceID): "
       << _strSourceID << endl;
#endif

  if (hyperParent != nullptr) {
    hyperParent->setSourceGroupID(_strSourceID);
  }
};

string ClsDiagPhantomConnection::getSourceID() {
  if (hyperParent != nullptr) {
    return hyperParent->getSourceGroupID();
  }
  return "";
};

void ClsDiagPhantomConnection::setTargetID(string _strTargetID) {
#ifdef DEBUG_CLSDIAGPHANTOMCONNECTION
  cout << "ClsDiagPhantomConnection::setTargetID(string _strTargetID): "
       << _strTargetID << endl;
#endif

  if (hyperParent != nullptr) {
    hyperParent->setTargetGroupID(
        _strTargetID); /* this is going to come back, but stop because of the
                          above check... */
  }
};

string ClsDiagPhantomConnection::getTargetID() {
  if (hyperParent != nullptr) {
    return hyperParent->getTargetGroupID();
  }
  return "";
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
