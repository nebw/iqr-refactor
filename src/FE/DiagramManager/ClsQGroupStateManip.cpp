#include <ClsFEGroup.h>
#include <ClsFESystemManager.h>
#include <ClsTopologyHex.h>
#include <ClsTopologyRect.h>
#include <ClsTopologySparse.h>
#include <ext/alloc_traits.h>
#include <qabstractbutton.h>
#include <qboxlayout.h>
#include <qbuttongroup.h>
#include <qcursor.h>
#include <qevent.h>
#include <qgridlayout.h>
#include <qgroupbox.h>
#include <qicon.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qmessagebox.h>
#include <qnamespace.h>
#include <qpalette.h>
#include <qpushbutton.h>
#include <qradiobutton.h>
#include <qspinbox.h>
#include <qsplitter.h>
#include <qstring.h>
#include <algorithm>

#include "ClsBaseQStateArrayView.h"
#include "ClsBaseTopology.h"
#include "ClsGroupManipPattern.h"
#include "ClsQGroupStateManip.h"
#include "ClsQSAList.h"
#include "ClsQStateArrayViewRect.h"
#include "cursors/eraser_cursor2.xpm"
#include "cursors/pencil_cursor2.xpm"
#include "eraser_12x12.xpm"
#include "neuronManager.hpp"
#include "pencil_12x12.xpm"

//#define DEBUG_CLSQGROUPSTATEMANIP

#define DEFAULTVALUE 1.0

ClsQGroupStateManip::ClsQGroupStateManip(const char *_name, string _strGroupID)
    : QFrame(nullptr), strGroupID(std::move(_strGroupID)) {

  setAttribute(Qt::WA_DeleteOnClose);
  bApplied = false;
  clsQStateArrayView = nullptr;
  iInterval = 1;
  iLoops = 1;
  iStepSize = 1;

  string strGroupName =
      ClsFESystemManager::Instance()->getGroupName(strGroupID).c_str();
  string strTitle = "State Manipulation Panel for \"" + strGroupName + "\"";
  this->setWindowTitle(strTitle.c_str());

  QPixmap qpmCursorEraser(eraser_cursor_xpm);
  qcursorEraser = new QCursor(qpmCursorEraser, 0, 0);

  QPixmap qpmCursorPencil(pencil_cursor_xpm);
  qcursorPencil = new QCursor(qpmCursorPencil, 0, 0);

  auto qsplitter = new QSplitter(this);
  auto qfmLeftPane = new QFrame(qsplitter);

  QBoxLayout *layoutMain = new QHBoxLayout(this);
  layoutMain->setSizeConstraint(QLayout::SetFixedSize);
  layoutMain->addWidget(qsplitter);

  QBoxLayout *layoutLeftPane =
      new QVBoxLayout(qfmLeftPane); //--, 5, -1, "mainL");

  qlblCaption = new QLabel(this);
  qlblCaption->setText(strGroupName.c_str());

  layoutLeftPane->addWidget(qlblCaption);

  qfmStateArray = new QFrame(qfmLeftPane);
  layoutLeftPane->addWidget(qfmStateArray, 0, Qt::AlignHCenter);

  auto qlayoutQfm = new QHBoxLayout(qfmStateArray);

  createStateArray(strGroupID);
  qlayoutQfm->addWidget(clsQStateArrayView);

  QPalette qfmStateArrayPal = qfmStateArray->palette();
  qfmStateArrayPal.setColor(QPalette::Window, Qt::red);
  qfmStateArray->setPalette(qfmStateArrayPal);

  qfmStateArray->show();
  clsQStateArrayView->show();
  clsQStateArrayView->setValue(DEFAULTVALUE);

  auto qlayoutGradient = new QHBoxLayout();
  layoutLeftPane->addLayout(qlayoutGradient);

  QString qstr;

  auto qlblMin = new QLabel(qfmLeftPane);
  qstr.setNum(fMinVal());

  qlblMin->setText(qstr);
  qlayoutGradient->addWidget(qlblMin, 0, Qt::AlignRight);

  qlblGradientPixmap = new QLabel(qfmLeftPane);
  ;
  qlayoutGradient->addWidget(qlblGradientPixmap, 1, Qt::AlignHCenter);

  qstr.setNum(fMaxVal());
  auto qlblMax = new QLabel(qfmLeftPane);
  qlblMax->setText(qstr);
  qlayoutGradient->addWidget(qlblMax);

  int iImgWidth = clsQStateArrayView->width() - qlblMin->minimumWidth() -
                  qlblMax->minimumWidth() - 30;
  int iImgHeight = 13;
  qlblGradientPixmap->setFixedSize(iImgWidth, iImgHeight);
  qlblGradientPixmap->setPixmap(
      clsQStateArrayView->getGradientPixmap(iImgWidth, iImgHeight));

  /* -------------------------------- */
  qgrpbxTools = new QGroupBox();

  auto lblValue = new QLabel();
  lblValue->setText("Value:");

  qdblspnbx = new QDoubleSpinBox(qgrpbxTools);
  qdblspnbx->setMinimum(fMinVal());
  qdblspnbx->setMaximum(fMaxVal());
  qdblspnbx->setDecimals(3);
  qdblspnbx->setSingleStep(0.01);
  qdblspnbx->setValue(DEFAULTVALUE);
  connect(qdblspnbx, SIGNAL(valueChanged(double)), this,
          SLOT(slotChangeValue(double)));

  QPushButton *qpbtnPen = new QPushButton(QIcon(QPixmap(pencil)), "");
  qpbtnPen->setCheckable(true);
  qpbtnPen->setFlat(true);
  qpbtnPen->setChecked(true);

  slotSelectTool(TOOL_PENCIL);

  QPushButton *qpbtnEraser = new QPushButton(QIcon(QPixmap(eraser)), "");
  qpbtnEraser->setCheckable(true);
  qpbtnEraser->setFlat(true);

  auto qlayoutTools = new QHBoxLayout;
  qlayoutTools->addWidget(lblValue);
  qlayoutTools->addWidget(qdblspnbx);
  qlayoutTools->addWidget(qpbtnPen);
  qlayoutTools->addWidget(qpbtnEraser);
  qgrpbxTools->setLayout(qlayoutTools);

  qbtngrpTools = new QButtonGroup();
  connect(qbtngrpTools, SIGNAL(buttonClicked(int)), this,
          SLOT(slotSelectTool(int)));
  qbtngrpTools->addButton(qpbtnPen, TOOL_PENCIL);
  qbtngrpTools->addButton(qpbtnEraser, TOOL_ERASER);
  layoutLeftPane->addWidget(qgrpbxTools);

  /* ------------------------------------ */

  auto layout2 = new QHBoxLayout();
  layoutLeftPane->addLayout(layout2);

  QPushButton *qpbtnClear = new QPushButton("Clear", qfmLeftPane);
  connect(qpbtnClear, SIGNAL(clicked()), this, SLOT(slotClear()));
  layout2->addWidget(qpbtnClear, Qt::AlignTop);

  QPushButton *qpbtnAdd = new QPushButton("Add", qfmLeftPane);
  connect(qpbtnAdd, SIGNAL(clicked()), this, SLOT(slotAdd()));
  layout2->addWidget(qpbtnAdd, Qt::AlignTop);

  QPushButton *qpbtnReplace = new QPushButton("Replace", qfmLeftPane);
  connect(qpbtnReplace, SIGNAL(clicked()), this, SLOT(slotReplace()));
  layout2->addWidget(qpbtnReplace, Qt::AlignTop);

  /* ------------------------------------ */
  QGroupBox *qgrpbxMode = new QGroupBox("Mode", qfmLeftPane);
  QRadioButton *qrbClamp = new QRadioButton("Clamp", qgrpbxMode);
  qrbClamp->setChecked(true);
  slotSetMode(ClsGroupManipPattern::MODE_CLAMP);

  QRadioButton *qrbAddPattern = new QRadioButton("Add", qgrpbxMode);
  QRadioButton *qrbMultiplyPattern = new QRadioButton("Multiply", qgrpbxMode);

  auto qlayoutMode = new QHBoxLayout;
  qlayoutMode->addWidget(qrbClamp, ClsGroupManipPattern::MODE_CLAMP);
  qlayoutMode->addWidget(qrbAddPattern, ClsGroupManipPattern::MODE_ADD);
  qlayoutMode->addWidget(qrbMultiplyPattern,
                         ClsGroupManipPattern::MODE_MULTIPLY);
  qgrpbxMode->setLayout(qlayoutMode);

  auto qbtngrpMode = new QButtonGroup();
  connect(qbtngrpMode, SIGNAL(buttonClicked(int)), SLOT(slotSetMode(int)));
  qbtngrpMode->addButton(qrbClamp, ClsGroupManipPattern::MODE_CLAMP);
  qbtngrpMode->addButton(qrbAddPattern, ClsGroupManipPattern::MODE_ADD);
  qbtngrpMode->addButton(qrbMultiplyPattern,
                         ClsGroupManipPattern::MODE_MULTIPLY);
  layoutLeftPane->addWidget(qgrpbxMode);
  /* ------------------------------------ */

  /* ------------------------------------ */
  QGroupBox *qgrpbxPlayBack = new QGroupBox("Play Back", qfmLeftPane);

  QRadioButton *qrbPersist = new QRadioButton("For ever");
  qrbPersist->setChecked(true);
  slotSetPlayback(ClsGroupManipPattern::PLAYBACK_LOOP);

  QRadioButton *qrbIterations = new QRadioButton("Times");

  auto qbtngrpPlayBack = new QButtonGroup();
  connect(qbtngrpPlayBack, SIGNAL(buttonClicked(int)),
          SLOT(slotSetPlayback(int)));
  qbtngrpPlayBack->addButton(qrbPersist, ClsGroupManipPattern::PLAYBACK_LOOP);
  qbtngrpPlayBack->addButton(qrbIterations,
                             ClsGroupManipPattern::PLAYBACK_ITERATIONS);

  qspnbxIterations = new QSpinBox();
  qspnbxIterations->setMinimum(1);
  qspnbxIterations->setMaximum(INT_MAX);
  qspnbxIterations->setMaximumWidth(50);
  connect(qspnbxIterations, SIGNAL(valueChanged(int)), this,
          SLOT(slotSetLoops(int)));

  auto qlblInterval = new QLabel();
  qlblInterval->setText("Interval");

  qspnbxInterval = new QSpinBox();
  qspnbxInterval->setMinimum(1);
  qspnbxInterval->setMaximum(INT_MAX);
  qspnbxInterval->setMaximumWidth(50);
  connect(qspnbxInterval, SIGNAL(valueChanged(int)), this,
          SLOT(slotSetInterval(int)));

  auto qlblStepSize = new QLabel();
  qlblStepSize->setText("StepSize");

  qspnbxStepSize = new QSpinBox();
  qspnbxStepSize->setMinimum(1);
  qspnbxStepSize->setMaximum(INT_MAX);
  qspnbxStepSize->setMaximumWidth(50);
  connect(qspnbxStepSize, SIGNAL(valueChanged(int)), this,
          SLOT(slotSetInterval(int)));

  auto qglayoutPlayBack = new QGridLayout();
  qglayoutPlayBack->addWidget(qrbPersist, 1, 1);
  qglayoutPlayBack->addWidget(qrbIterations, 1, 2);
  qglayoutPlayBack->addWidget(qspnbxIterations, 1, 3);

  qglayoutPlayBack->addWidget(qlblInterval, 2, 2);
  qglayoutPlayBack->addWidget(qspnbxInterval, 2, 3);
  qglayoutPlayBack->addWidget(qlblStepSize, 3, 2);
  qglayoutPlayBack->addWidget(qspnbxStepSize, 3, 3);
  qgrpbxPlayBack->setLayout(qglayoutPlayBack);

  layoutLeftPane->addWidget(qgrpbxPlayBack);
  /* ------------------------------------ */

  auto qlayoutCmdButtons = new QHBoxLayout();
  layoutLeftPane->addLayout(qlayoutCmdButtons);

  QPushButton *qpbtnApply = new QPushButton("Send", qfmLeftPane);
  connect(qpbtnApply, SIGNAL(clicked()), this, SLOT(slotApply()));

  qpbtnRevoke = new QPushButton("Revoke", qfmLeftPane);
  qpbtnRevoke->setEnabled(false);
  connect(qpbtnRevoke, SIGNAL(clicked()), this, SLOT(slotRevoke()));

  QPushButton *qpbtnClose = new QPushButton("Close", qfmLeftPane);
  connect(qpbtnClose, SIGNAL(clicked()), this, SLOT(close()));

  qlayoutCmdButtons->addWidget(qpbtnApply);
  qlayoutCmdButtons->addWidget(qpbtnRevoke);
  qlayoutCmdButtons->addWidget(qpbtnClose);

  qpbtnPen->setToolTip("Pencil");
  qpbtnEraser->setToolTip("Eraser");
  qpbtnClear->setToolTip("Clear");
  qpbtnAdd->setToolTip("Add");

  qrbClamp->setToolTip("Repace Value");

  qrbAddPattern->setToolTip("Add Values");
  qrbMultiplyPattern->setToolTip("Mutliply with Values");

  qrbPersist->setToolTip("Apply for ever");
  qspnbxIterations->setToolTip("Apply for selected timesteps");
  qspnbxInterval->setToolTip("Apply every X timestep");
  qspnbxStepSize->setToolTip("Apply step by X");

  clsQSAList = new ClsQSAList(qsplitter, this);
  connect(clsQSAList, SIGNAL(sigChangeMatrix(vector<vector<double> >)), this,
          SLOT(slotMatrixChanged(vector<vector<double> >)));

  clsQSAList->show();
  qsplitter->setStretchFactor(qsplitter->indexOf(clsQSAList), 0);
};

void ClsQGroupStateManip::createStateArray(string _strGroupID) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::createStateArray(string _strGroupID)" << endl;
#endif

  ClsBaseTopology *clsBaseTopology =
      ClsFESystemManager::Instance()->getFEGroup(_strGroupID)->getTopology();
  clsQStateArrayView = nullptr;
  if (dynamic_cast<ClsTopologyRect *>(clsBaseTopology)) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
    cout << "cast clsBaseTopologyGroup to ClsTopologyRect" << endl;
#endif
    clsQStateArrayView = new ClsQStateArrayViewRect(
        qfmStateArray, clsBaseTopology, ClsBaseQStateArrayView::LIST);
  } else if (dynamic_cast<ClsTopologyHex *>(clsBaseTopology)) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
    cout << "cast clsBaseTopologyGroup to ClsTopologyHex" << endl;
#endif
  } else if (dynamic_cast<ClsTopologySparse *>(clsBaseTopology)) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
    cout << "cast clsBaseTopologyGroup to ClsTopologySparse" << endl;
#endif
  }

  if (clsQStateArrayView != nullptr) {
    clsQStateArrayView->setColorMode(ClsBaseQStateArrayView::BLUE2RED);

    clsQStateArrayView->setMinValue(fMinVal());
    clsQStateArrayView->setMaxValue(fMaxVal());
    qfmStateArray->setMinimumSize(clsQStateArrayView->minimumSize());
  }
}

void ClsQGroupStateManip::slotCellClicked(int /* iX */, int /* iY */) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotCellClicked( int iX, int iY)" << endl;
#endif
}

void ClsQGroupStateManip::slotSelectTool(int iID) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotSelectTool(int)" << endl;
#endif
  if (iID == ClsQGroupStateManip::TOOL_PENCIL) {
    clsQStateArrayView->setValue(qdblspnbx->value());
    clsQStateArrayView->setCursor(*qcursorPencil);
  } else {
    clsQStateArrayView->setValue(0.);
    clsQStateArrayView->setCursor(*qcursorEraser);
  }
}

void ClsQGroupStateManip::slotClear() {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotClear()" << endl;
#endif
  clsQStateArrayView->clear();
}

void ClsQGroupStateManip::slotAdd() {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotAdd()" << endl;
#endif
  QPixmap qpm = clsQStateArrayView->getValuePixmap(
      clsQStateArrayView->width(), clsQStateArrayView->height());

  clsQSAList->insertSA(qpm, "", clsQStateArrayView->getMatrix());
}

void ClsQGroupStateManip::slotReplace() {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotReplace()" << endl;
#endif
  QPixmap qpm = clsQStateArrayView->getValuePixmap(
      clsQStateArrayView->width(), clsQStateArrayView->height());

  clsQSAList->insertSA(qpm, "", clsQStateArrayView->getMatrix(), true);
}

void ClsQGroupStateManip::slotGroupChanged() {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotGroupChanged()" << endl;
#endif
  qlblCaption->setText(
      ClsFESystemManager::Instance()->getGroupName(strGroupID).c_str());
  clsQStateArrayView->hide();
  createStateArray(strGroupID);
  clsQStateArrayView->show();

  if (bApplied) {
    ClsFESystemManager::Instance()
        ->getFEGroup(strGroupID)
        ->removeGroupManipPattern();
    slotApply();
  }
}

void ClsQGroupStateManip::slotRevoke() {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotRevoke()" << endl;
#endif
  ClsFESystemManager::Instance()
      ->getFEGroup(strGroupID)
      ->removeGroupManipPattern();

  bApplied = false;
  qpbtnRevoke->setEnabled(false);
}

void ClsQGroupStateManip::slotApply() {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotApply()" << endl;
#endif
  /* check for the group width/height.... */

  int iPatternCount = clsQSAList->getCount();

  if (iPatternCount < 1) {
    QMessageBox::information(this, "IQR", "Pattern stack is empty\n"
                                          "No patterns will be applied.");
  } else {
    ClsFESystemManager::Instance()
        ->getFEGroup(strGroupID)
        ->removeGroupManipPattern();

    ClsGroupManipPattern *clsGroupManipPattern = ClsFESystemManager::Instance()
                                                     ->getFEGroup(strGroupID)
                                                     ->getGroupManipPattern();
    clsGroupManipPattern->setMode(iMode);
    clsGroupManipPattern->setPlayBack(iPlayBack);
    clsGroupManipPattern->setInterval(iInterval);
    clsGroupManipPattern->setStepSize(iStepSize);
    clsGroupManipPattern->setLoops(iLoops);
    clsGroupManipPattern->setDepth(clsQSAList->getCount());

    ClsBaseTopology *clsBaseTopology =
        ClsFESystemManager::Instance()->getFEGroup(strGroupID)->getTopology();

    for (int ii = 0; ii < clsQSAList->getCount(); ii++) {
      vector<vector<double> > m = clsQSAList->getVectorAt(ii);
      clsGroupManipPattern->insertPattern(ii,
                                          matrix2vector(clsBaseTopology, m));
    }
  }
  qpbtnRevoke->setEnabled(true);
}

valarray<double>
ClsQGroupStateManip::matrix2vector(ClsBaseTopology *clsBaseTopology,
                                   vector<vector<double> > _m) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::matrix2vector(ClsBaseTopology* "
          "clsBaseTopology, vector <vector<double> > _m)" << endl;
#endif
  valarray<double> va(clsBaseTopology->Size());

  unsigned int iCellsX = clsBaseTopology->nrCellsHorizontal();
  unsigned int iCellsY = clsBaseTopology->nrCellsVertical();

  /* determin what is smaller: the group or the pattern */
  int iXBoundary = (iCellsX < (_m.size() - 1) ? iCellsX : (_m.size() - 1));
  int iYBoundary =
      (iCellsY < (_m[0].size() - 1) ? iCellsY : (_m[0].size() - 1));

#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "iCellsX, _m.size(): " << iCellsX << ", " << _m.size() << endl;
  cout << "iCellsY, _m[0].size(): " << iCellsY << ", " << _m[0].size() << endl;
  cout << "clsBaseTopology->Size(): " << clsBaseTopology->Size() << endl;
  cout << "iXBoundary: " << iXBoundary << endl;
  cout << "iYBoundary: " << iYBoundary << endl;
#endif

  for (int iY = 1; iY <= iYBoundary; iY++) {
    for (int iX = 1; iX <= iXBoundary; iX++) {
      int iIndex = clsBaseTopology->pos2index(iX, iY);
#ifdef DEBUG_CLSQGROUPSTATEMANIP
      cout << "iX: " << iX << ", iY: " << iY << ", iIndex: " << iIndex << endl;
      cout << "_m[iX][iY]; " << _m[iX][iY] << endl;
#endif
      va[iIndex] = _m[iX][iY];
    }
  }
  return va;
}

valarray<double>
ClsQGroupStateManip::matrix2vector(bool bSparse, int iSize,
                                   vector<vector<double> > _m) {
  valarray<double> va(iSize);
  int iCellCounter = 0;
  for (unsigned int ii = 1; ii < _m.size(); ii++) {
    vector<double> vaTemp = _m[ii];
    for (unsigned int i2 = 1; i2 < vaTemp.size(); i2++) {
      if (bSparse) {
        if (vaTemp[i2] > -99) {
          va[iCellCounter] = vaTemp[i2];
          iCellCounter++;
        }
      } else {
        va[iCellCounter] = vaTemp[i2];
        iCellCounter++;
      }
    }
  }
  return va;
}

void ClsQGroupStateManip::closeEvent(QCloseEvent *ce) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "iqrfe::ClsSubtypedAutoDialog::closeEvent( QCloseEvent* ce )" << endl;
#endif

  ClsFESystemManager::Instance()
      ->getFEGroup(strGroupID)
      ->removeGroupManipPattern();

  emit sigDiagramClosed(strGroupID);
  ce->accept();
}

void ClsQGroupStateManip::slotMatrixChanged(vector<vector<double> > v) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotMatrixChanged(vector <vector<double> > v)"
       << endl;
#endif
  clsQStateArrayView->setMatrix(v);
}

QPixmap ClsQGroupStateManip::getPixmap(vector<vector<double> > v) {
  clsQStateArrayView->setMatrix(v);
  QPixmap qpm = clsQStateArrayView->getValuePixmap(
      clsQStateArrayView->width(), clsQStateArrayView->height());

  return qpm;
}

void ClsQGroupStateManip::slotSetMode(int i) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotSetMode(int)" << endl;
#endif
  iMode = i;
}

void ClsQGroupStateManip::slotSetPlayback(int i) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotSetPlayback(int)" << endl;
#endif
  iPlayBack = i;
}

void ClsQGroupStateManip::slotSetInterval(int ii) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotSetInterval(int i)" << endl;
#endif
  iInterval = ii;
}

void ClsQGroupStateManip::slotSetStepSize(int ii) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotSetStepSize(int i)" << endl;
#endif
  iStepSize = ii;
}

void ClsQGroupStateManip::slotSetLoops(int ii) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotSetLoops(int i)" << endl;
#endif
  iLoops = ii;
}

void ClsQGroupStateManip::slotChangeValue(double dd) {
#ifdef DEBUG_CLSQGROUPSTATEMANIP
  cout << "ClsQGroupStateManip::slotChangeValue(double dd)" << endl;
#endif

  clsQStateArrayView->setValue(dd);
  clsQStateArrayView->setCursor(*qcursorPencil);
  if (qbtngrpTools->button(TOOL_PENCIL) != nullptr) {
    qbtngrpTools->button(TOOL_PENCIL)->setChecked(true);
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
