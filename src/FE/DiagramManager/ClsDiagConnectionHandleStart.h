#ifndef CLSCANVASHANDLESTART_H
#define CLSCANVASHANDLESTART_H

#include <qgraphicsitem.h>
#include <qpixmap.h>
#include <qrect.h>
#include <qvariant.h>
#include <string>

class QGraphicsSceneMouseEvent;

using namespace std;

static const int handlestartRTTI = 984380;

class ClsDiagBaseConnection;

class ClsDiagConnectionHandleStart : public QGraphicsPixmapItem {
public:
  ClsDiagConnectionHandleStart(ClsDiagBaseConnection *_parentConnection,
                               string _strID, int iOrientation);

  int type() const override;

  void setSourceID(string _strSourceID);
  string getID() {
    return strID;
  };

  string getConnectionID();
  int getCanvasConnectionType();

  void setConnectionType(int _iConnectionType);
  void setOrientation(int _iOrientation);

  string getAPID();
  int getAPNumber();

  void refresh();

  QVariant itemChange(GraphicsItemChange change,
                      const QVariant &value) override;

  void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
  void moveChildren();

private:
  QPixmap qpmExc, qpmInh, qpmMod;

  ClsDiagBaseConnection *parentConnection;

  string strID;
  int iOrientation;
  QRectF qrectParentBR;
  QRectF qrectMyBR;

  string strSourceID;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
