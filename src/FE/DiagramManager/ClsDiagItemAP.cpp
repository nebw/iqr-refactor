#include <APIcon_9x9.xpm>
#include <qgraphicssceneevent.h>
#include <qmimedata.h>
#include <qpixmap.h>
#include <algorithm>
#include <cassert>
#include <iostream>

#include "ClsBlockDiagram.h"
#include "ClsDiagBaseConnection.h"
#include "ClsDiagConnection.h"
#include "ClsDiagConnectionHandleEnd.h"
#include "ClsDiagConnectionHandleStart.h"
#include "ClsDiagHyperConnection.h"
#include "ClsDiagItem.h"
#include "ClsDiagItemAP.h"
#include "ClsDiagPhantomConnection.h"

//#define DEBUG_CLSCANVASNODEAP

ClsDiagItemAP::ClsDiagItemAP(QGraphicsItem *parent, string _strParentID,
                             int _iOrientation, bool _bPhantom)
    : QGraphicsPixmapItem(parent), strParentID(std::move(_strParentID)),
      iOrientation(_iOrientation), bPhantom(_bPhantom) {
  setPixmap(QPixmap(APIcon));
  setAcceptDrops(true);
  setOpacity(0.3);
};

bool ClsDiagItemAP::hasAPHandle(QString _qstrID) {
#ifdef DEBUG_CLSCANVASNODEAP
  cout << "ClsDiagItemAP::hasAPHandle(QString _qstrID)" << endl;
#endif

  QList<QGraphicsItem *> lst = childItems();
  QList<QGraphicsItem *>::iterator it;
  for (it = lst.begin(); it != lst.end(); ++it) {
    string strID = "";
    if ((*it)->type() == handlestartRTTI) {
      strID = dynamic_cast<ClsDiagConnectionHandleStart *>(*it)->getID();
    } else if ((*it)->type() == handleendRTTI) {
      strID = dynamic_cast<ClsDiagConnectionHandleEnd *>(*it)->getID();
    }
    if (!strID.compare(_qstrID.toStdString())) {
      return true;
    }
  }
  return false;
};

int ClsDiagItemAP::getNumber() {
#ifdef DEBUG_CLSCANVASNODEAP
  cout << "ClsDiagItemAP::getNumber()" << endl;
#endif
  if (dynamic_cast<ClsDiagItem *>(parentItem())) {
    return dynamic_cast<ClsDiagItem *>(parentItem())
        ->getAPHandleNumberByAddress(this);
  }
  return -1;
}

void ClsDiagItemAP::moveChildren() {
#ifdef DEBUG_CLSCANVASNODEAP
  cout << "ClsDiagItemAP::moveChildrenBy ( double dx, double dy)" << endl;
#endif

  QList<QGraphicsItem *> lst = childItems();
  QList<QGraphicsItem *>::iterator it;

  for (it = lst.begin(); it != lst.end(); ++it) {
    if ((*it)->type() == handlestartRTTI) {
      dynamic_cast<ClsDiagConnectionHandleStart *>(*it)->moveChildren();
    } else if ((*it)->type() == handleendRTTI) {
      dynamic_cast<ClsDiagConnectionHandleEnd *>(*it)->moveChildren();
    }
  }
};

void ClsDiagItemAP::mousePressEvent(QGraphicsSceneMouseEvent *) {
#ifdef DEBUG_CLSCANVASNODEAP
  cout << "ClsDiagItemAP::mousePressEvent ( QGraphicsSceneMouseEvent * )"
       << endl;
#endif
  ClsBlockDiagram::Instance()->mouseLeftClickAP(this);
}

void ClsDiagItemAP::dragEnterEvent(QGraphicsSceneDragDropEvent *event) {
#ifdef DEBUG_CLSCANVASNODEAP
  cout
      << "ClsDiagItemAP::dragEnterEvent ( QGraphicsSceneDragDropEvent * event )"
      << endl;
#endif
  setOpacity(1);
  if (event->mimeData()->hasFormat("text/plain")) {
    event->acceptProposedAction();
  }
}

void ClsDiagItemAP::dragLeaveEvent(QGraphicsSceneDragDropEvent *event) {
#ifdef DEBUG_CLSCANVASNODEAP
  cout
      << "ClsDiagItemAP::dragLeaveEvent ( QGraphicsSceneDragDropEvent * event )"
      << endl;
#endif
  setOpacity(0.3);
}

void ClsDiagItemAP::dropEvent(QGraphicsSceneDragDropEvent *event) {
#ifdef DEBUG_CLSCANVASNODEAP
  cout << "ClsDiagItemAP::dropEvent ( QGraphicsSceneDragDropEvent * event )"
       << endl;
#endif

  QString qstr = event->mimeData()->text();

  stringstream ss;
  ss.str(qstr.toStdString());

  string strType, strID, strConnType;
  ss >> strType;
  ss >> strID;
  ss >> strConnType;

  ClsDiagHyperConnection *hyperConn =
      ClsBlockDiagram::Instance()->getCanvasHyperConnection(strID);

  if (hyperConn != nullptr) {

    bool isIP = hyperConn->isIPConnection();
    bool willBeIP;

    assert(!strType.compare("HandleStart") || !strType.compare("HandleEnd"));

    if (!strType.compare("HandleStart")) {
      willBeIP =
          hyperConn->checkIfIP(strParentID, hyperConn->getTargetGroupID());
    } else {
      willBeIP =
          hyperConn->checkIfIP(hyperConn->getSourceGroupID(), strParentID);
    }

    if (isIP != willBeIP) {
    } else {
      ClsDiagBaseConnection *conn = nullptr;

      /* this is all we care about... */

      if (bPhantom) {
        if (!strConnType.compare(ClsDiagBaseConnection::connTypeLocal())) {
          //TODO: does this case exist?
          conn = hyperConn->getGrpGrpConnection();
        } else if (!strConnType.compare(
                        ClsDiagBaseConnection::connTypePhantomStart())) {
          conn = hyperConn->getPhantomStart();
        } else if (!strConnType.compare(
                        ClsDiagBaseConnection::connTypePhantomEnd())) {
          conn = hyperConn->getPhantomEnd();
        }
      }

      if (!bPhantom) {
        if (!strConnType.compare(ClsDiagBaseConnection::connTypeLocal())) {
          conn = hyperConn->getGrpGrpConnection();
        } else if (!strConnType.compare(
                        ClsDiagBaseConnection::connTypePhantomStart())) {
          //TODO: does this case exist?
          conn = hyperConn->getPhantomStart();
        } else if (!strConnType.compare(
                        ClsDiagBaseConnection::connTypePhantomEnd())) {
          //TODO: does this case exist?
          conn = hyperConn->getPhantomEnd();
        }
      }

      if (conn != nullptr) {
        if (!strType.compare("HandleStart")) {
          ClsDiagConnectionHandleStart *h = conn->getStartHandle();
          if (h != nullptr) {
            h->setParentItem(this);
          }
        } else if (!strType.compare("HandleEnd")) {
          ClsDiagConnectionHandleEnd *h = conn->getEndHandle();
          if (h != nullptr) {
            h->setParentItem(this);
          }
        }
      }
    }

    if (!strType.compare("HandleStart")) {
      hyperConn->setSourceGroupID(strParentID, getNumber());
    } else if (!strType.compare("HandleEnd")) {
      hyperConn->setTargetGroupID(strParentID, getNumber());
    }
  }
  setOpacity(0.3);
}

bool ClsDiagItemAP::isFree() {
  if (childItems().size() > 0) {
    return false;
  } else {
    return true;
  }
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
