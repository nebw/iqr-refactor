/****************************************************************************
 ** $Filename: ClsQStateArrayViewHex.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jul  4 19:14:28 2003
 **
 *****************************************************************************/

#ifndef CLSQSTATEARRAYVIEWHEX_H
#define CLSQSTATEARRAYVIEWHEX_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <iostream>
#include <list>
#include <utility>
#include <valarray>
#include <vector>

#include "ClsBaseQStateArrayView.h"

class ClsBaseTopology;
class ClsRegion;
class QPainter;
class QPixmap;
class QPoint;
class QWidget;

using namespace std;

class ClsQStateArrayViewHex : public ClsBaseQStateArrayView {
  Q_OBJECT
public:
  ClsQStateArrayViewHex(QWidget *parent = nullptr,
                        ClsBaseTopology *_clsBaseTopologyGroup = nullptr,
                        const char *name = nullptr,
                        unsigned int _iSelectionMode = 0);

  void setValue(double fValue, int i, int j) override;
  void addValue(double /* fValue */, int /* i */, int /* j */) override{};
  void setValue(double fValue, list<pair<int, int> > lst) override;
  void setValue(vector<pair<int, double> > /* lstIndexDist */) override{};
  void addValue(vector<pair<int, double> > /* lstIndexDist */) override{};
  void setValue(vector<pair<int, int> > /* lstIndexDelay */) override{};
  void setValue(valarray<size_t>, valarray<double>) override{};

  void setRegion(double fValue, int iXStart, int iYStart, int iWidth,
                 int iHeight) override;
  void setRegion(double fValue, ClsRegion _clsRegion) override;

  void markCell(int /* iColor */, int /* iX */, int /* iY */) override{};

  list<pair<int, int> > getSelected() override;

public
slots:
  void clear() override;

protected:
  void mouseHandleEllipse(const QPoint & /* pos */) {};

  void createNullPixmap() override;
  void drawCheckerBoard(QPixmap &qpm, int iOffsetX, int iOffsetY) override;

signals:
  void cellClicked(int, int);

private:
  int pos2index2(int x);
  int index2pos2(int i);

  void drawHexagon(QPainter &paint, int iXCenter, int iYCenter,
                   int iVertexlength, int iOrientation);
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:

#endif /* CLSQSTATEARRAYVIEWHEX_H */
