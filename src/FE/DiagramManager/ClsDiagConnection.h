#ifndef CLSDIAGCONNECTION_H
#define CLSDIAGCONNECTION_H

#include <qpoint.h>
#include <qstring.h>
#include <algorithm> //MARK
#include <iterator>
#include <string>
#include <vector>

#include "ClsDiagBaseConnection.h"

class ClsDiagConnectionHandle;
class ClsDiagHyperConnection;
class QGraphicsLineItem;

using namespace std;

class ClsDiagConnection : public ClsDiagBaseConnection {
public:
  ClsDiagConnection(ClsDiagHyperConnection *_hyperParent, int _iConnectionType,
                    int _iCanvasConnectionType);
  ~ClsDiagConnection();

  void setSourceID(string _strSourceID) override;
  string getSourceID() override;
  void setTargetID(string _strTargetID) override;
  string getTargetID() override;

  void setConnected(bool b) override;
  void setConnectionType(int _iConnectionType) override;
  void mark(bool b) override;

  void addSegment(QPointF qp0, QPointF qp1, int iPosition,
                  int iOrientatio) override;

  QGraphicsLineItem *getLine(QString qstrID);

  QGraphicsLineItem *getFirstSegment() override {
    if (vectorSegments.size() > 0) {
      return vectorSegments[0];
    } else {
      return nullptr;
    }
  };

  QGraphicsLineItem *getLastSegment() override {
    if (vectorSegments.size() > 0) {
      return vectorSegments[vectorSegments.size() - 1];
    } else {
      return nullptr;
    }
  };

  QGraphicsLineItem *getPrevSegment(ClsDiagConnectionHandle *h) {
    unsigned int iPos = findHandlePos(h);
    if (iPos < vectorSegments.size()) {
      return vectorSegments[iPos];
    } else {
      return nullptr;
    }
  };

  QGraphicsLineItem *getNextSegment(ClsDiagConnectionHandle *h) {
    unsigned int iPos = findHandlePos(h) + 1;
    if (iPos < vectorSegments.size()) {
      return vectorSegments[iPos];
    } else {
      return nullptr;
    }
  };

  int findSegmentPos(QGraphicsLineItem *h) {
    vector<QGraphicsLineItem *>::iterator pos;
    pos = find(vectorSegments.begin(), vectorSegments.end(), h);
    int iPos = distance(vectorSegments.begin(), pos);
    return iPos;
  }

  int findHandlePos(ClsDiagConnectionHandle *h) {
    vector<ClsDiagConnectionHandle *>::iterator pos;
    pos = find(vectorHandles.begin(), vectorHandles.end(), h);
    int iPos = distance(vectorHandles.begin(), pos);
    return iPos;
  }

  void split(QGraphicsLineItem *s, QPointF qpPos);
  void removeHandle(ClsDiagConnectionHandle *h);

  vector<ClsDiagConnectionHandle *> getHandles() { return vectorHandles; }

  vector<vector<int> > getPoints();

private:
  vector<QGraphicsLineItem *> vectorSegments;
  vector<ClsDiagConnectionHandle *> vectorHandles;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
