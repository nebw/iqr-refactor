//#include <q3popupmenu.h>

#include <qgraphicsitem.h>
#include <qgraphicsscene.h>
#include <qlist.h>
#include <qnamespace.h>
#include <qpoint.h>
#include <qrect.h>
#include <utility>

#include "ClsDiagItem.h"
#include "ClsDiagItemGroup.h"
#include "ClsDiagItemProcess.h"
#include "ClsFESystemManager.h"
#include "ClsScene.h"
#include "ClsSceneView.h"
#include "neuronManager.hpp"

class QMouseEvent;
class QWidget;

//#define DEBUG_CLSSCENEVIEW

#define GRID_SPACING 8

ClsSceneView::ClsSceneView(QWidget *_parent) : QGraphicsView(_parent) {
#ifdef DEBUG_CLSSCENEVIEW
  cout << "ClsSceneView::ClsSceneView(QWidget* parent,const char* name)"
       << endl;
#endif
  setLineWidth(1);
  setDragMode(QGraphicsView::RubberBandDrag);
  setRubberBandSelectionMode(Qt::ContainsItemShape);
}

void ClsSceneView::mousePressEvent(QMouseEvent *event) {
#ifdef DEBUG_CLSSCENEVIEW
  cout << "ClsSceneView::mousePressEvent ( QMouseEvent * event)" << endl;
#endif
  if (scene() != nullptr) {
    int iType = dynamic_cast<ClsScene *>(scene())->getCanvasType();
    string strID = dynamic_cast<ClsScene *>(scene())->getID();
    emit sigGotFocus(iType, strID);
  }
  QGraphicsView::mousePressEvent(event);
}

void ClsSceneView::clear() { /* DO WE NEED THIS?????*/
#ifdef DEBUG_CLSSCENEVIEW
  cout << "ClsSceneView::clear()" << endl;
#endif
  QList<QGraphicsItem *> list = scene()->items();
  QList<QGraphicsItem *>::Iterator it = list.begin();
  for (; it != list.end(); ++it) {
    if (*it)
      delete *it;
  }
}

const map<string, int> ClsSceneView::getListSelectedItems() {
#ifdef DEBUG_CLSSCENEVIEW
  cout << "ClsSceneView::getListSelectedItems()" << endl;
#endif
  map<string, int> mapSelections;
  if (scene() != nullptr) {
    QList<QGraphicsItem *> lst = scene()->selectedItems();
    QList<QGraphicsItem *>::iterator itSel;
    for (itSel = lst.begin(); itSel != lst.end(); ++itSel) {
      if (dynamic_cast<ClsDiagItemGroup *>((*itSel))) {
        string strID = dynamic_cast<ClsDiagItem *>((*itSel))->getID();
        pair<string, int> pairTemp(strID, ClsFESystemManager::ITEM_GROUP);
        mapSelections.insert(pairTemp);
      } else if (dynamic_cast<ClsDiagItemProcess *>((*itSel))) {
        string strID = dynamic_cast<ClsDiagItem *>((*itSel))->getID();
        pair<string, int> pairTemp(strID, ClsFESystemManager::ITEM_PROCESS);
        mapSelections.insert(pairTemp);
      }
    }
  }
  return mapSelections;
};

void ClsSceneView::scrollContentsBy(int dx, int dy) {
#ifdef DEBUG_CLSSCENEVIEW
  cout << "ClsSceneView::scrollContentsBy(int dx, int dy):";
#endif

  QPointF qp = mapToScene(sceneRect().topLeft().x(), sceneRect().topLeft().y());
  qp += QPointF(contentsRect().width() / 2, contentsRect().height() / 2);
  //     cout << "\tx: " << qp.x()  << ", y: " << qp.y() << endl;;

  emit(sigSceneScrolledToCenterPos(qp.x(), qp.y()));

  QGraphicsView::scrollContentsBy(dx, dy);
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
