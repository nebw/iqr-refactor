#include <algorithm>
#include <string>

using namespace std;

#include <ConnStartBlue.xpm>
#include <ConnStartGreen.xpm>
#include <ConnStartRed.xpm>
#include <qdrag.h>
#include <qgraphicsscene.h>
#include <qgraphicssceneevent.h>
#include <qline.h>
#include <qmimedata.h>
#include <qnamespace.h>
#include <qpoint.h>
#include <qstring.h>
#include <qwidget.h>

#include "ClsDiagBaseConnection.h"
#include "ClsDiagConnection.h"
#include "ClsDiagConnectionHandleStart.h"
#include "ClsDiagItemAP.h"
#include "ClsInfoConnection.h"
#include "ClsScene.h"

ClsDiagConnectionHandleStart::ClsDiagConnectionHandleStart(
    ClsDiagBaseConnection *_parentConnection, string _strID, int _iOrientation)
    : parentConnection(_parentConnection), strID(std::move(_strID)),
      iOrientation(_iOrientation) {

  setFlag(QGraphicsItem::ItemIgnoresParentOpacity, true);
  setFlag(QGraphicsItem::ItemIsMovable, true);
  setFlag(QGraphicsItem::ItemIsSelectable, false);
  setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
  setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);

  qpmExc = QPixmap(ConnStartRed);
  qpmMod = QPixmap(ConnStartGreen);
  qpmInh = QPixmap(ConnStartBlue);
  setPixmap(qpmExc);

  qrectMyBR = boundingRect();
  setOffset(-0.5 * QPointF(qrectMyBR.width(), 0));

  if (scene() != nullptr) {
    if (dynamic_cast<ClsScene *>(scene())->getCanvasType() ==
        ClsScene::CANVAS_PROCESS) {
      setCursor(Qt::OpenHandCursor);
    } else {
      setCursor(Qt::ArrowCursor);
    }
  }
};

void ClsDiagConnectionHandleStart::setOrientation(int _iOrientation) {
  iOrientation = _iOrientation;

  resetTransform();
  switch (iOrientation) {
  case ClsDiagConnection::WEST_EAST:
    setRotation(90);
    break;
  case ClsDiagConnection::EAST_WEST:
    setRotation(270);
    break;
  case ClsDiagConnection::NORTH_SOUTH:
    setRotation(180);
    break;
  case ClsDiagConnection::SOUTH_NORTH:
    setRotation(0);
    break;
  default:
    break;
  };
}

int ClsDiagConnectionHandleStart::type() const { return handlestartRTTI; }

string ClsDiagConnectionHandleStart::getConnectionID() {
  if (parentConnection) {
    return parentConnection->getConnectionID();
  }
  return nullptr;
};

int ClsDiagConnectionHandleStart::getCanvasConnectionType() {
  if (parentConnection) {
    return parentConnection->getCanvasConnectionType();
  }
  return -1;
};

void ClsDiagConnectionHandleStart::setConnectionType(int iConnectionType) {

  if (iConnectionType == iqrcommon::ClsInfoConnection::CONN_EXCITATORY) {
    setPixmap(qpmExc);
  } else if (iConnectionType == iqrcommon::ClsInfoConnection::CONN_INHIBITORY) {
    setPixmap(qpmInh);
  } else if (iConnectionType == iqrcommon::ClsInfoConnection::CONN_MODULATORY) {
    setPixmap(qpmMod);
  }
};

string ClsDiagConnectionHandleStart::getAPID() {
  if (dynamic_cast<ClsDiagItemAP *>(parentItem())) {
    return dynamic_cast<ClsDiagItemAP *>(parentItem())->getParentID();
  }
  return "";
}

int ClsDiagConnectionHandleStart::getAPNumber() {
  if (dynamic_cast<ClsDiagItemAP *>(parentItem())) {
    return dynamic_cast<ClsDiagItemAP *>(parentItem())->getNumber();
  }
  return -1;
}

void ClsDiagConnectionHandleStart::refresh() {
  qrectParentBR.setSize(parentItem()->boundingRect().size());

  qrectParentBR.setX(0);
  qrectParentBR.setY(0);

  setPos(qrectParentBR.center());
  if (dynamic_cast<ClsDiagItemAP *>(parentItem())) {
    iOrientation =
        dynamic_cast<ClsDiagItemAP *>(parentItem())->getOrientation();
    setOrientation(iOrientation);
  }
  QPointF qpHere = parentItem()->scenePos() + qrectParentBR.center();

  QGraphicsLineItem *qlinePost = parentConnection->getFirstSegment();
  if (qlinePost != nullptr) {
    if (iOrientation == ClsDiagConnection::WEST_EAST) {
      qlinePost->setLine(
          qpHere.x() - qrectMyBR.height(), qpHere.y() - 1,
          qlinePost->line().p2().x() + 2,
          qlinePost->line().p2().y()); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::EAST_WEST) {
      qlinePost->setLine(
          qpHere.x() + qrectMyBR.height(), qpHere.y(),
          qlinePost->line().p2().x() - 2,
          qlinePost->line().p2().y()); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::NORTH_SOUTH) {
      qlinePost->setLine(qpHere.x(), qpHere.y() - qrectMyBR.height(),
                         qlinePost->line().p2().x(),
                         qlinePost->line().p2().y() + 2);
    } else if (iOrientation == ClsDiagConnection::SOUTH_NORTH) {
      qlinePost->setLine(qpHere.x() - 1, qpHere.y() + qrectMyBR.height(),
                         qlinePost->line().p2().x(),
                         qlinePost->line().p2().y() - 2);
    }
  }
  qlinePost->setZValue(0);
  setZValue(10);
}

QVariant ClsDiagConnectionHandleStart::itemChange(GraphicsItemChange change,
                                                  const QVariant &value) {
  if (change == QGraphicsItem::ItemScenePositionHasChanged && scene()) {
    /* we need this for when the handle is move within a selection */
    moveChildren();
  } else if (change == QGraphicsItem::ItemParentHasChanged && scene()) {
    /* this is used when the handle is dropped onto a new group connector */
    if (parentItem() != nullptr) {
      refresh();
    } else {
      setSourceID("");
      QGraphicsLineItem *qlinePost = parentConnection->getFirstSegment();
      if (qlinePost != nullptr) {
        QPointF p = qlinePost->line().p1();
        setPos(p);
      }
    }

    if (scene() != nullptr) {
      if (dynamic_cast<ClsScene *>(scene())->getCanvasType() ==
          ClsScene::CANVAS_PROCESS) {
        setCursor(Qt::OpenHandCursor);
      } else {
        setCursor(Qt::ArrowCursor);
      }
    }
  }
  return QGraphicsItem::itemChange(change, value);
}

void ClsDiagConnectionHandleStart::setSourceID(string _strSourceID) {
  strSourceID = _strSourceID;
  parentConnection->setSourceID(_strSourceID);
}

void
ClsDiagConnectionHandleStart::mousePressEvent(QGraphicsSceneMouseEvent *event) {
  if (dynamic_cast<ClsScene *>(scene())->getCanvasType() ==
      ClsScene::CANVAS_PROCESS) {
    auto data = new QMimeData;
    QString qstr = QString("HandleStart ") + strID.c_str() + QString(" ") +
                   parentConnection->connTypeAsString().c_str();
    data->setText(qstr);

    auto drag = new QDrag(event->widget());
    drag->setMimeData(data);
    drag->exec();
  } else {
    event->ignore();
  }
}

void ClsDiagConnectionHandleStart::moveChildren() {
  QPointF qpHere = parentItem()->scenePos() + qrectParentBR.center();

  QGraphicsLineItem *qlinePost = parentConnection->getFirstSegment();
  if (qlinePost != nullptr) {
    if (iOrientation == ClsDiagConnection::WEST_EAST) {
      qlinePost->setLine(
          qpHere.x() - qrectMyBR.height(), qpHere.y() - 1,
          qlinePost->line().p2().x(),
          qlinePost->line().p2().y()); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::EAST_WEST) {
      qlinePost->setLine(
          qpHere.x() + qrectMyBR.height(), qpHere.y(),
          qlinePost->line().p2().x(),
          qlinePost->line().p2().y()); /* height because we are rotated! */
    } else if (iOrientation == ClsDiagConnection::NORTH_SOUTH) {
      qlinePost->setLine(qpHere.x(), qpHere.y() - qrectMyBR.height(),
                         qlinePost->line().p2().x(),
                         qlinePost->line().p2().y());
    } else if (iOrientation == ClsDiagConnection::SOUTH_NORTH) {
      qlinePost->setLine(qpHere.x() - 1, qpHere.y() + qrectMyBR.height(),
                         qlinePost->line().p2().x(),
                         qlinePost->line().p2().y());
    }
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make"
//// End:
