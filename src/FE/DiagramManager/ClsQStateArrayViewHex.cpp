/****************************************************************************
 ** $Filename: ClsQStateArrayViewHex.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Tue Sep  9 17:19:10 2003
 **
 *****************************************************************************/

#include <ext/alloc_traits.h>
#include <qbrush.h>
#include <qframe.h>
#include <qnamespace.h>
#include <qpainter.h>
#include <qpalette.h>
#include <qpixmap.h>
#include <qpolygon.h>
#include <qsize.h>
#include <string>

#include "ClsBaseTopology.h"
#include "ClsQStateArrayViewHex.h"
#include "ClsSubPopulations.h"
#include "iqrUtils.h"
#include "parameter.hpp"
#include "parameterList.hpp"
#include "tagLibrary.hpp"

class QWidget;

ClsQStateArrayViewHex::ClsQStateArrayViewHex(
    QWidget *parent, ClsBaseTopology *_clsBaseTopologyGroup, const char *_name,
    unsigned int _iSelectionMode)
    : ClsBaseQStateArrayView(parent, _clsBaseTopologyGroup, _iSelectionMode) {

  iCellSize = 20;
  iNrCellsX = 10;
  iNrCellsY = 10;

  QPalette palette;
  palette.setColor(this->backgroundRole(), Qt::yellow);
  this->setPalette(palette);

  ParameterList parameterList = _clsBaseTopologyGroup->getListParameters();

  while (parameterList.size()) {
    string strParamName = parameterList.front()->getName();
    string strParamValue = parameterList.front()->getValueAsString();

    if (!strParamName.compare(ClsTagLibrary::TopologyWidthTag())) {
      iNrCellsX = iqrUtils::string2int(strParamValue);

    } else if (!strParamName.compare(ClsTagLibrary::TopologyHeightTag())) {
      iNrCellsY = iqrUtils::string2int(strParamValue);
    }

    parameterList.pop_front();
  }

  iCellSize = iSizeMax / (iNrCellsX > iNrCellsY ? iNrCellsX : iNrCellsY);

  vCells.resize(iNrCellsX + 1);
  for (int ii = 0; ii < (iNrCellsX + 1); ii++) {
    vCells[ii].resize(iNrCellsY + 1);
  }

  iXPosStart = iYPosStart = 0;

  fValMin = 0;
  fValMax = 1.0;
  fScaleFactor = 255.0;
  this->setFrameStyle(QFrame::WinPanel | QFrame::Raised);

  setMinimumSize(iNrCellsX * iCellSize + 2 * BORDER,
                 iNrCellsY * iCellSize + 2 * BORDER);
  setMaximumSize(iNrCellsX * iCellSize + 2 * BORDER,
                 iNrCellsY * iCellSize + 2 * BORDER);

  clear();
  createNullPixmap();
}

void ClsQStateArrayViewHex::clear() {
  for (int i = 0; i <= iNrCellsX; i++) {
    for (int j = 0; j <= iNrCellsY; j++) {
      vCells[i][j] = 0.;
    }
  }

  pmGridValues = pmGridEmpty;
}

list<pair<int, int> > ClsQStateArrayViewHex::getSelected() {
  list<pair<int, int> > lst;
  for (int i = 1; i <= iNrCellsX; i++) {
    for (int j = 1; j <= iNrCellsY; j++) {
      double fValue = vCells[i][j];
      if (fValue > 0) {
        pair<int, int> pairTemp(i, j);
        lst.push_back(pairTemp);
      }
    }
  }
  return lst;
};

void ClsQStateArrayViewHex::setValue(double fValue, int i, int j) {
  if (i > 0 && j > 0 && i <= iNrCellsX && j <= iNrCellsY) {
    vCells[i][j] = fValue;

    QBrush qb(Qt::yellow);
    QPainter paint(this);
    qb.setColor(getColor(fValue));
  }
}

void ClsQStateArrayViewHex::setValue(double fValue, list<pair<int, int> > lst) {
  list<pair<int, int> >::iterator it;
  for (it = lst.begin(); it != lst.end(); ++it) {
    int i, j;
    i = (*it).first;
    j = (*it).second;
    setValue(fValue, i, j);
  }
}

void ClsQStateArrayViewHex::setRegion(double fValue, int iXStart, int iYStart,
                                      int iWidth, int iHeight) {
  clsRegion.setXStart(iXStart);
  clsRegion.setYStart(iYStart);
  clsRegion.setWidth(iWidth);
  clsRegion.setHeight(iHeight);
  clsRegion.print();

  for (int ii = iXStart; ii < (iXStart + iWidth); ii++) {
    for (int jj = iYStart; jj < (iYStart + iHeight); jj++) {
      setValue(fValue, ii, jj);
    }
  }
}

void ClsQStateArrayViewHex::setRegion(double fValue, ClsRegion _clsRegion) {
  clsRegion = _clsRegion;

  int iXStart = clsRegion.getXStart();
  int iYStart = clsRegion.getYStart();
  int iWidth = clsRegion.getWidth();
  int iHeight = clsRegion.getHeight();

  for (int ii = iXStart; ii < (iXStart + iWidth); ii++) {
    for (int jj = iYStart; jj < (iYStart + iHeight); jj++) {
      setValue(fValue, ii, jj);
    }
  }
}

void ClsQStateArrayViewHex::createNullPixmap() {
  QPixmap qpmTemp(size().width() - 2 * BORDER, size().height() - 2 * BORDER);
  drawCheckerBoard(pmGridEmpty, -BORDER, -BORDER);
  pmGridEmpty = qpmTemp;
}

void ClsQStateArrayViewHex::drawCheckerBoard(QPixmap &qpm, int iOffsetX,
                                             int iOffsetY) {

  QPainter paint(&qpm);
  QBrush qb(Qt::yellow);

  for (int i = 1; i <= iNrCellsX; i++) {
    int j = 1;
    double fValue = vCells[i][j];
    qb.setColor(getColor(fValue));
    paint.setBrush(qb);
    drawHexagon(paint, index2pos2(i) + iOffsetX, index2pos2(j) + iOffsetY,
                iCellSize, 1);
  }
}

void ClsQStateArrayViewHex::drawHexagon(QPainter &paint, int iXCenter,
                                        int iYCenter, int iVertexlength,
                                        int /* iOrientation */) {

  const double f = 1.7320508;

  iXCenter = iXCenter + (int)((double)iVertexlength / 4. * f);
  iYCenter = iYCenter + (int)((double)iVertexlength / 2.);

  QPolygonF pa;

  pa << QPointF(0, (int)(-iVertexlength / 2));
  pa << QPointF((int)(iVertexlength / 4 * f), (int)(-iVertexlength / 4));
  pa << QPointF((int)(iVertexlength / 4 * f), (int)(iVertexlength / 4));
  pa << QPointF(0, (int)(iVertexlength / 2));
  pa << QPointF((int)(-iVertexlength / 4 * f), (int)(iVertexlength / 4));
  pa << QPointF((int)(-iVertexlength / 4 * f), (int)(-iVertexlength / 4));

  pa.translate(iXCenter, iYCenter);

  paint.drawPolygon(pa, Qt::OddEvenFill);
}

int ClsQStateArrayViewHex::pos2index2(int _x) {
  return (_x - BORDER) / iCellSize + 1;
}

int ClsQStateArrayViewHex::index2pos2(int i) {
  return (i - 1) * iCellSize + BORDER;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
