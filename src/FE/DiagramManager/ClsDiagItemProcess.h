#ifndef CLSDIAGITEMPROCESS_H
#define CLSDIAGITEMPROCESS_H

#include <string>
#include <vector>

#include <QGraphicsScene>
#include <QFontMetrics>

#include "ClsDiagItemIcon.h"
#include "ClsDiagItem.h"

using namespace std;

static const int nodeprocessRTTI = 984375;

class ClsDiagItemProcess : public ClsDiagItem {
public:
  ClsDiagItemProcess(int _iType, int _x, int _y, int _width, int _height,
                     QGraphicsItem *_parent, string _strName, string _strID)
      : ClsDiagItem(_iType, _x, _y, _width, _height, _parent, _strName,
                    _strID) {

    prepareGeometryChange();

    clsDiagItemIconModule = nullptr;
    clsDiagItemIconLink = nullptr;

    qpenNotSelected.setColor(Qt::gray);
    qpenNotSelected.setWidth(3);
    qpenNotSelected.setBrush(Qt::Dense2Pattern);

    qpenSelected.setColor(Qt::red);
    qpenSelected.setWidth(2);

    setPen(qpenNotSelected);

    drawLabel();
    drawShadow();

    drawAPS();
  };

  int type() const override { return nodeprocessRTTI; }

  void setModuleIcon(moduleIcon mi) {
    if (mi.data != nullptr) {
      if (clsDiagItemIconModule != nullptr) {
        removeModuleIcon();
      }
      clsDiagItemIconModule = new ClsDiagItemIcon(this, mi);
      clsDiagItemIconModule->setPos(this->rect().right() - mi.x0,
                                    this->rect().bottom() - mi.y0);
      clsDiagItemIconModule->setZValue(1);
      clsDiagItemIconModule->show();
    }
  };

  void removeModuleIcon() {
    if (clsDiagItemIconModule != nullptr) {
      clsDiagItemIconModule->hide();
      delete clsDiagItemIconModule;
      clsDiagItemIconModule = nullptr;
    }
  }

  void setModuleEnabled(bool b) {
    clsDiagItemIconModule->setEnabled(b);
  }

  void setIsExternal(bool b) {
    if (b) {
      if (clsDiagItemIconLink == nullptr) {
        int iX = this->rect().left();
        int iY = this->rect().top();
        clsDiagItemIconLink =
            new ClsDiagItemIcon(this, ClsDiagItemIcon::ICON_PROCESS_LINK);
        clsDiagItemIconLink->setPos(
            iX - clsDiagItemIconLink->rect().width() * .5,
            iY - clsDiagItemIconLink->rect().height() * .5);
        clsDiagItemIconLink->setZValue(1);
        clsDiagItemIconLink->show();
      }
    } else {
      if (clsDiagItemIconLink != nullptr) {
        clsDiagItemIconLink->hide();
        delete clsDiagItemIconLink;
        clsDiagItemIconLink = nullptr;
      }
    }
  }

  void Hide() override {
    if (clsDiagItemIconModule != nullptr) {
      clsDiagItemIconModule->hide();
    }
    if (clsDiagItemIconLink != nullptr) {
      clsDiagItemIconLink->hide();
    }
    ClsDiagItem::Hide();
  }

private:
  ClsDiagItemIcon *clsDiagItemIconModule;
  ClsDiagItemIcon *clsDiagItemIconLink;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../.. && make -k "
//// End:
