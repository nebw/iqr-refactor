#include <ClsPanner.h>
#include <qgraphicsscene.h>
#include <qrect.h>
#include <qwidget.h>

#include "ClsPannerSceneView.h"
#include "ClsSceneView.h"

//#define DEBUG_CLSPANNER

ClsPanner::ClsPanner(QWidget *_parent, ClsSceneView *_clsSceneView)
    : QFrame(_parent), clsSceneView(_clsSceneView) {
  resize(100, 75);
  clsPannerSceneView = new ClsPannerSceneView(this, clsSceneView);
  clsPannerSceneView->resize(this->size());

  connect(clsPannerSceneView, SIGNAL(sigScrolledToSceneCenterPos(int, int)),
          this, SLOT(scrolledToSceneCenterPos(int, int)));
  connect(clsSceneView, SIGNAL(sigSceneScrolledToCenterPos(int, int)), this,
          SLOT(parentScrolledTo(int, int)));
};

void ClsPanner::setScene(QGraphicsScene *clsScene) {
#ifdef DEBUG_CLSPANNER
  cout << "ClsPanner::setScene(QCanvas *clsScene)" << endl;
#endif

  clsPannerSceneView->setScene(clsScene);
  dFactorX = (double)this->contentsRect().width() /
             (clsScene->width() > 0 ? (double)(clsScene->width()) : .1);
  dFactorY = (double)this->contentsRect().height() /
             (clsScene->height() > 0 ? (double)(clsScene->height()) : .1);

  clsPannerSceneView->scale(dFactorX, dFactorY);
};

void ClsPanner::canvasResized() {
#ifdef DEBUG_CLSPANNER
  cout << "ClsPanner::canvasResized ( )" << endl;
#endif

  QGraphicsScene *clsScene = clsPannerSceneView->scene();

  if (clsScene != nullptr) {
    dFactorX = (double)(this->contentsRect().width()) /
               (clsScene->width() > 0 ? (double)(clsScene->width()) : .1);
    dFactorY = (double)(this->contentsRect().height()) /
               (clsScene->height() > 0 ? (double)(clsScene->height()) : .1);
    clsPannerSceneView->scale(dFactorX, dFactorY);
  }
}

void ClsPanner::scrolledToSceneCenterPos(int iX, int iY) {
#ifdef DEBUG_CLSPANNER
  cout << "ClsPanner::scrolledToSceneCenterPos(int iX, int iY):" << iX << ", "
       << iY << endl;
#endif
  emit sigScrolledToSceneCenterPost(iX, iY);
}

void ClsPanner::parentScrolledTo(int sceneX, int sceneY) {
#ifdef DEBUG_CLSPANNER
  cout << "ClsPanner::parentScrolledTo: " << sceneX << ", " << sceneY << endl;
#endif
  clsPannerSceneView->viewport()->update();
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
