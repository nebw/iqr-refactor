#include <qcolor.h>
#include <qnamespace.h>
#include <string>

#include "ClsBaseConnection.h"
#include "ClsDiagBaseConnection.h"
#include "ClsDiagConnectionHandleEnd.h"
#include "ClsDiagConnectionHandleStart.h"
#include "ClsDiagHyperConnection.h"
#include "ClsFEConnection.h"
#include "neuronManager.hpp"

using namespace std;
using namespace iqrcommon;

//#define DEBUG_CLSDIAGBASECONNECTION

ClsDiagBaseConnection::ClsDiagBaseConnection(
    ClsDiagHyperConnection *_hyperParent, int _iConnectionType,
    int _iCanvasConnectionType)
    : QGraphicsLineItem(nullptr), hyperParent(_hyperParent),
      iConnectionType(_iConnectionType),
      iCanvasConnectionType(_iCanvasConnectionType) {

  clsCanvasHandleStart = nullptr;
  clsCanvasHandleEnd = nullptr;

  mapConnTypes[ClsDiagBaseConnection::LOCAL] = connTypeLocal();
  mapConnTypes[ClsDiagBaseConnection::IP] = connTypeIP();
  mapConnTypes[ClsDiagBaseConnection::PHANTOM_START] = connTypePhantomStart();
  mapConnTypes[ClsDiagBaseConnection::PHANTOM_END] = connTypePhantomEnd();

  if (iConnectionType == ClsFEConnection::CONN_EXCITATORY) {
    qbrush.setColor(QColor(240, 0, 0));
    qpen.setColor(QColor(240, 0, 0));
  } else if (iConnectionType == ClsFEConnection::CONN_INHIBITORY) {
    qbrush.setColor(QColor(0, 0, 240));
    qpen.setColor(QColor(0, 0, 240));
  } else if (iConnectionType == ClsFEConnection::CONN_MODULATORY) {
    qbrush.setColor(QColor(0, 240, 0));
    qpen.setColor(QColor(0, 240, 0));
  }

  qbrush.setStyle(Qt::SolidPattern);
  qpen.setWidth(3);
};

ClsDiagBaseConnection::~ClsDiagBaseConnection() {
#ifdef DEBUG_CLSDIAGBASECONNECTION
  cout << "ClsDiagBaseConnection()::~ClsDiagBaseConnection()" << endl;
#endif
  if (clsCanvasHandleStart != nullptr) {
    delete clsCanvasHandleStart;
    clsCanvasHandleStart = nullptr;
  }

  if (clsCanvasHandleEnd != nullptr) {
    delete clsCanvasHandleEnd;
    clsCanvasHandleEnd = nullptr;
  }
};

string ClsDiagBaseConnection::getConnectionID() {
  if (hyperParent != nullptr) {
    return hyperParent->getID();
  }
  // TODO: FIXME
  return "";
};

void ClsDiagBaseConnection::setConnected(bool b) {
#ifdef DEBUG_CLSDIAGBASECONNECTION
  cout << "ClsDiagBaseConnection::setConnected(bool b)" << endl;
#endif
  if (!b) { /* set pen to dashed */
    qpen.setStyle(Qt::DotLine);
  } else {
    qpen.setStyle(Qt::SolidLine);
  }
};

int ClsDiagBaseConnection::type() const { return connectionRTTI; }

void ClsDiagBaseConnection::setConnectionType(int _iConnectionType) {
#ifdef DEBUG_CLSDIAGBASECONNECTION
  cout << "ClsDiagBaseConnection::setConnectionType(int "
          "_iConnectionType)::iConnectionType:" << _iConnectionType << endl;
#endif

  iConnectionType = _iConnectionType;
  if (iConnectionType == ClsFEConnection::CONN_EXCITATORY) {
    qbrush.setColor(QColor(255, 0, 0));
    qpen.setColor(QColor(255, 0, 0));
  } else if (iConnectionType == ClsFEConnection::CONN_INHIBITORY) {
    qbrush.setColor(QColor(0, 0, 255));
    qpen.setColor(QColor(0, 0, 255));
  } else if (iConnectionType == ClsFEConnection::CONN_MODULATORY) {
    qbrush.setColor(QColor(0, 255, 0));
    qpen.setColor(QColor(0, 255, 0));
  }

  qbrush.setStyle(Qt::SolidPattern);
  qpen.setWidth(3);

  clsCanvasHandleStart->setConnectionType(iConnectionType);
  clsCanvasHandleEnd->setConnectionType(iConnectionType);
}

string ClsDiagBaseConnection::getStartHandleID() {
  if (clsCanvasHandleStart != nullptr) {
    return clsCanvasHandleStart->getID();
  }
  return "";
}

string ClsDiagBaseConnection::getEndHandleID() {
  if (clsCanvasHandleEnd != nullptr) {
    return clsCanvasHandleEnd->getID();
  }
  return "";
}

int ClsDiagBaseConnection::getStartHandleAPNumber() {
  if (clsCanvasHandleStart != nullptr) {
    return clsCanvasHandleStart->getAPNumber();
  }
  return -1;
}

int ClsDiagBaseConnection::getEndHandleAPNumber() {
  if (clsCanvasHandleEnd != nullptr) {
    return clsCanvasHandleEnd->getAPNumber();
  }
  return -1;
}

ClsDiagConnectionHandleStart *ClsDiagBaseConnection::getStartHandle() {
#ifdef DEBUG_CLSDIAGBASECONNECTION
  cout << "ClsDiagBaseConnection::getStartHandle()" << endl;
#endif
  return clsCanvasHandleStart;
};

ClsDiagConnectionHandleEnd *ClsDiagBaseConnection::getEndHandle() {
#ifdef DEBUG_CLSDIAGBASECONNECTION
  cout << "ClsDiagBaseConnection::getEndHandle()" << endl;
#endif
  return clsCanvasHandleEnd;
};

void ClsDiagBaseConnection::mark(bool b) {
#ifdef DEBUG_CLSDIAGBASECONNECTION
  cout << "**ClsDiagBaseConnection::setSelected(bool b)" << endl;
#endif
  if (hyperParent != nullptr) {
    hyperParent->mark(b);
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
