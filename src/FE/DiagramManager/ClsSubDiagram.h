#ifndef CLSQDIAGRAMVIEW_H
#define CLSQDIAGRAMVIEW_H

#include <qcursor.h>
#include <qframe.h>
#include <qobjectdefs.h>
#include <qstring.h>
#include <map>
#include <string>

#include "ClsFESystemManager.h"
#include "neuronManager.hpp"

class ClsPanner;
class ClsScene;
class ClsSceneView;
class QBoxLayout;
class QGraphicsScene;
class QGraphicsView;
class QPaintEvent;
class QResizeEvent;
class QTabBar;
class QWidget;

using namespace std;

#define ADD_STATE_NULL -99

class ClsBlockDiagram;

class ClsSubDiagram : public QFrame {
  Q_OBJECT

public:
  ClsSubDiagram(QFrame *parent, ClsBlockDiagram *clsBlockDiagramParent, int iID,
                int i);

  void addTab(const ClsFESystemManager::POPUP_ITEM iType, string strName,
              string strID);
  void removeTab(string strID);
  void renameTab(string strID, string strName);

  void setItemInfoMode(bool b); //?????????????

  void useSpacer(bool b);
  QGraphicsScene *scene();
  void setScene(ClsScene *q);

  string getNewConnectionProcessID();
  void showProcessTab(string strProcessID);

  const map<string, int> getListSelectedItems();
  void setAddState(int iState);

  void mark(bool);
  void setInfoMode(bool b);

public
slots:
  void slotTabChanged(int);
  void zoomIn();
  void zoomOut();
  void clear();

private
slots:
  void slotGotFocus(int, string);
  void resizeEvent(QResizeEvent *e) override;
  void scrollSceneToCenterPos(int, int);

signals:
  void status(const QString &);
  void canvasChanged(int, string);
  void sigGotFocus(int, int, string);
  void sigDiagItemActivated(int, string);

private:
  void paintEvent(QPaintEvent *event) override;

  void createCursors();
  void setCursor(int iAddState, int iCanvasType);

  QCursor qcursorNewGroup;
  QCursor qcursorNewProcess;
  QCursor qcursorNewConnection;

  ClsBlockDiagram *clsBlockDiagramParent;
  ClsSceneView *clsSceneView;

  QGraphicsView *qgraphicsviewMini;

  QBoxLayout *qboxlayout;
  QWidget *qwgtSpacer; /* this widget has no function whatsoever. It's only
                          purpose is to ensure the TabBar soesen't shrink to 0!
                          */
  int iSpacerHeight;

  QTabBar *qtabbar;

  ClsPanner *clsPanner;

  int iAddState;
  int iID;
  bool bFocus;
  bool bInfoMode;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
