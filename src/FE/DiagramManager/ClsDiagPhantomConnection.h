#ifndef CLSDIAGPHANTOMCONNECTION_H
#define CLSDIAGPHANTOMCONNECTION_H

#include <qpoint.h>
#include <string>

#include "ClsDiagBaseConnection.h"

class ClsDiagHyperConnection;
class QGraphicsLineItem;

using namespace std;

class ClsDiagPhantomConnection : public ClsDiagBaseConnection {
public:
  ClsDiagPhantomConnection(ClsDiagHyperConnection *_hyperParent,
                           int _iConnectionType, int _iCanvasConnectionType);
  ~ClsDiagPhantomConnection();

  void addSegment(QPointF qp0, QPointF qp1, int iPosition,
                  int iOrientatio) override;

  QGraphicsLineItem *getFirstSegment() override {
    return clsDiagConnectionSegment;
  };

  QGraphicsLineItem *getLastSegment() override {
    return clsDiagConnectionSegment;
  };

  void setSourceID(string _strSourceID) override;
  string getSourceID() override;
  void setTargetID(string _strTargetID) override;
  string getTargetID() override;

  void setConnectionType(int _iConnectionType) override;

private:
  QGraphicsLineItem *clsDiagConnectionSegment;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
