/****************************************************************************
 ** $Filename: ClsBaseQStateArrayView.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Thu Jul  3 22:41:58 2003
 **
 *****************************************************************************/
#include <qbrush.h>
#include <qevent.h>
#include <qnamespace.h>
#include <qpainter.h>
#include <qrect.h>
#include <qsize.h>
#include <stdlib.h>

#include "ClsBaseQStateArrayView.h"
#include "ClsBaseTopology.h"

class QWidget;

//#define DEBUG_CLSBASEQSTATEARRAYVIEW

ClsBaseQStateArrayView::ClsBaseQStateArrayView(
    QWidget *parent, ClsBaseTopology *_clsBaseTopologyGroup,
    unsigned int _iSelectionMode)
    : QFrame(parent), clsBaseTopologyGroup(_clsBaseTopologyGroup),
      iSelectionMode(_iSelectionMode) {

#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::ClsBaseQStateArrayView( QWidget *parent, "
          "ClsBaseTopology *_clsBaseTopologyGroup, unsigned int "
          "_iSelectionMode)" << endl;
#endif

  setAttribute(Qt::WA_DeleteOnClose);
  iColorMode = ClsBaseQStateArrayView::GRAY;
  fFixedValue = 0.5;
  fValMin = 0;
  fValMax = 1.0;
  fScaleFactor = 255.0;
  iXPosStart = iYPosStart = 0;
};

QColor ClsBaseQStateArrayView::getColor(double fValue) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::getColor(double fValue): " << fValue << endl;
#endif
  QColor qc;

  if (iColorMode == ClsBaseQStateArrayView::GRAY) {
    double fColorValue = (fValue - fValMin) * fScaleFactor;
    qc.setRgb((int)(fColorValue), (int)(fColorValue), (int)(fColorValue));
  } else if (iColorMode == ClsBaseQStateArrayView::BLUE2RED) {
    double fCenter = (fValMin + fValMax) / 2.;
    double fColorValue = fabs(fValue - fCenter) * fScaleFactor * 2.;
    if (fValue < fCenter) {
      qc.setRgb(0, 0, (int)(fColorValue));
    } else {
      qc.setRgb((int)(fColorValue), 0, 0);
    }
  } else {
    double fColorValue = (fValue - fValMin) * fScaleFactor;
    qc.setHsv((int)(fColorValue), 255, 255);
  }

  return qc;
}

const ClsRegion ClsBaseQStateArrayView::getRegion() {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::getRegion()" << endl;
#endif
  return clsRegion;
};

QPixmap ClsBaseQStateArrayView::getValuePixmap(int iImgWidth, int iImgHeight) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::getValuePixmap(int iImgWidth, int "
          "iImgHeight )" << endl;
#endif

  return pmGridValues.scaled(iImgWidth, iImgHeight);
}

QPixmap ClsBaseQStateArrayView::getGradientPixmap(int iImgWidth, int iImgHeight,
                                                  int _iColorMode) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::getGradientPixmap(int iImgWidth, int "
          "iImgHeight)" << endl;
#endif

  QPixmap pmGradient(iImgWidth, iImgHeight);
  QPainter p(&pmGradient);

  QLinearGradient shade(pmGradient.rect().topLeft(),
                        pmGradient.rect().bottomRight());

  if (_iColorMode == ClsBaseQStateArrayView::BLUE2RED) {
    shade.setColorAt(0, Qt::blue);
    shade.setColorAt(1, Qt::red);
  } else if (_iColorMode == ClsBaseQStateArrayView::GRAY) {
    shade.setColorAt(0, Qt::white);
    shade.setColorAt(1, Qt::black);
  }

  p.fillRect(pmGradient.rect(), shade);
  return pmGradient;
}

void ClsBaseQStateArrayView::setRegion(double fValue, int iXStart, int iYStart,
                                       int iWidth, int iHeight) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::setRegion(double fValue, int iXStart, int "
          "iYStart, int iWidth, int iHeight)" << endl;
#endif
  clsRegion.setXStart(iXStart);
  clsRegion.setYStart(iYStart);
  clsRegion.setWidth(iWidth);
  clsRegion.setHeight(iHeight);

  for (int ii = iXStart; ii < (iXStart + iWidth); ii++) {
    for (int jj = iYStart; jj < (iYStart + iHeight); jj++) {
      setValue(fValue, ii, jj);
    }
  }
}

void ClsBaseQStateArrayView::setRegion(double fValue, ClsRegion _clsRegion) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::setRegion(double fValue, ClsRegion "
          "_clsRegion)" << endl;
#endif
  clsRegion = _clsRegion;

  int iXStart = clsRegion.getXStart();
  int iYStart = clsRegion.getYStart();
  int iWidth = clsRegion.getWidth();
  int iHeight = clsRegion.getHeight();

  for (int ii = iXStart; ii < (iXStart + iWidth); ii++) {
    for (int jj = iYStart; jj < (iYStart + iHeight); jj++) {
      setValue(fValue, ii, jj);
    }
  }
}

void ClsBaseQStateArrayView::mouseHandle(const QPointF &_pos) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::mouseHandle( const QPointF &_pos )" << endl;
#endif
  int iX = pixelCoord2groupCoord(_pos.x());
  int iY = pixelCoord2groupCoord(_pos.y());

  if (iSelectionMode == ClsBaseQStateArrayView::SINGLE) {
    clear();
  }
  setValue(fFixedValue, iX, iY);
  emit cellClicked(iX, iY);
}

void ClsBaseQStateArrayView::mouseMoveEvent(QMouseEvent *e) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::mouseMoveEvent( QMouseEvent *e )" << endl;
#endif
  if (iSelectionMode == ClsBaseQStateArrayView::LIST ||
      iSelectionMode == ClsBaseQStateArrayView::SINGLE) {
    mouseHandle(e->pos());
  } else if (iSelectionMode == ClsBaseQStateArrayView::REGION) {
    mouseHandleRegion(e->pos());
  }
}

void ClsBaseQStateArrayView::mousePressEvent(QMouseEvent *e) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::mousePressEvent( QMouseEvent *e )" << endl;
#endif
  if (e->button() == Qt::LeftButton) {
    if (iSelectionMode == ClsBaseQStateArrayView::REGION ||
        iSelectionMode == ClsBaseQStateArrayView::SINGLE) {
      clear();
    }
    if ((iXPosStart * iYPosStart) == 0) {
      iXPosStart = e->pos().x();
      iYPosStart = e->pos().y();
    }
    if (iSelectionMode == ClsBaseQStateArrayView::LIST ||
        iSelectionMode == ClsBaseQStateArrayView::SINGLE) {
      mouseHandle(e->pos());
    } else if (iSelectionMode == ClsBaseQStateArrayView::REGION) {
      mouseHandleRegion(e->pos());
    } else if (iSelectionMode == 100) {
      mouseHandleEllipse(e->pos());
    }
  }
}

void ClsBaseQStateArrayView::mouseHandleRegion(const QPointF &_pos) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::mouseHandleRegion( const QPointF &_pos )"
       << endl;
#endif
  int iPosX = _pos.x();
  int iPosY = _pos.y();

  int iXMin = contentsRect().left() + 1;
  int iYMin = contentsRect().top() + 1;
  int iXMax = contentsRect().right() - 1;
  int iYMax = contentsRect().bottom() - 1;

  iPosX = (iPosX > iXMin ? iPosX : iXMin);
  iPosY = (iPosY > iYMin ? iPosY : iYMin);

  iPosX = (iPosX < iXMax ? iPosX : iXMax);
  iPosY = (iPosY < iYMax ? iPosY : iYMax);

  int iW = iPosX - iXPosStart;
  int iH = iPosY - iYPosStart;

  QBrush qb(Qt::yellow);
  QPainter paint(this);
  qb.setColor(getColor(fFixedValue));

  paint.drawRect(iXPosStart, iYPosStart, iW, iH); //**
}

void ClsBaseQStateArrayView::mouseReleaseEvent(QMouseEvent *e) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::mouseReleaseEvent ( QMouseEvent * e )"
       << endl;
#endif
  //--------------------
  int iXIndexStart = pixelCoord2groupCoord(iXPosStart);
  iXIndexStart = (iXIndexStart <= 0 ? 1 : iXIndexStart);
  iXIndexStart = (iXIndexStart > iNrCellsX ? iNrCellsX : iXIndexStart);

  int iXIndex = pixelCoord2groupCoord(e->pos().x());
  iXIndex = (iXIndex <= 0 ? 1 : iXIndex);
  iXIndex = (iXIndex > iNrCellsX ? iNrCellsX : iXIndex);

  int iX = (iXIndexStart < iXIndex ? iXIndexStart : iXIndex);
  int iW = abs(iXIndexStart - iXIndex) + 1;
  //--------------------

  //--------------------
  int iYIndexStart = pixelCoord2groupCoord(iYPosStart);
  iYIndexStart = (iYIndexStart <= 0 ? 1 : iYIndexStart);
  iYIndexStart = (iYIndexStart > iNrCellsY ? iNrCellsY : iYIndexStart);

  int iYIndex = pixelCoord2groupCoord(e->pos().y());
  iYIndex = (iYIndex <= 0 ? 1 : iYIndex);
  iYIndex = (iYIndex > iNrCellsY ? iNrCellsY : iYIndex);

  int iY = (iYIndexStart < iYIndex ? iYIndexStart : iYIndex);
  int iH = abs(iYIndexStart - iYIndex) + 1;
  //--------------------

  if (iSelectionMode == ClsBaseQStateArrayView::REGION) {
    clear();
    setRegion(fFixedValue, iX, iY, iW, iH);
  }

  iXPosStart = iYPosStart = 0; //**
}

void ClsBaseQStateArrayView::paintEvent(QPaintEvent *e) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::paintEvent( QPaintEvent *  )" << endl;
#endif
  QPainter paint(this);
  paint.drawPixmap(BORDER, BORDER, pmGridValues);
}

[[deprecated]]
void ClsBaseQStateArrayView::markCell(int iColor, int iX, int iY) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::markCell(int iColor, int iX, int iY)"
       << endl;
#endif
}

void ClsBaseQStateArrayView::markCell(int iColor, list<int> lstIndex) {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::markCell(int iColor, list<int> lstIndex)"
       << endl;
#endif

  list<int>::iterator it;
  for (it = lstIndex.begin(); it != lstIndex.end(); ++it) {
    int iIndex = (*it);
    pair<int, int> pPoint = clsBaseTopologyGroup->index2pos(iIndex);
    markCell(iColor, pPoint.first, pPoint.second);
  }
}

void ClsBaseQStateArrayView::createNullPixmap() {
#ifdef DEBUG_CLSBASEQSTATEARRAYVIEW
  cout << "ClsBaseQStateArrayView::createNullPixmap()" << endl;
#endif
  QPixmap pmTemp(size().width() - 2 * BORDER, size().height() - 2 * BORDER);
  drawCheckerBoard(pmTemp, -BORDER, -BORDER);
  pmGridEmpty = pmTemp;
  pmGridValues = pmTemp;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
