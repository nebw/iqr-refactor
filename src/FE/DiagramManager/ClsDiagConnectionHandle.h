#ifndef CLSCANVASHANDLE_H
#define CLSCANVASHANDLE_H

#include <qaction.h>
#include <qgraphicsitem.h>
#include <qgraphicssceneevent.h>
#include <qmenu.h>
#include <qobject.h>
#include <qobjectdefs.h>
#include <qpixmap.h>
#include <qvariant.h>

using namespace std;

static const int handleRTTI = 984378;

#define HANDLESIZE 8

class ClsDiagConnection;

class ClsDiagConnectionHandle : public QObject, public QGraphicsPixmapItem {
  Q_OBJECT

public:
  ClsDiagConnectionHandle(ClsDiagConnection *_parent, int x, int y);

  int type() const override;

  void setSelected(bool b);

  void refresh();

  void setConnectionType(int _iConnectionType);

  void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override {
    auto qmenuContext = new QMenu();
    QAction *qactDH = new QAction("Delete Handle", this);
    connect(qactDH, SIGNAL(triggered()), this, SLOT(remove()));
    qmenuContext->addAction(qactDH);
    qmenuContext->exec(event->screenPos());
  }

  QVariant itemChange(GraphicsItemChange change,
                      const QVariant &value) override;

  void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

private
slots:
  void remove();

private:
  QPixmap qpmCurrent;
  QPixmap qpmExc, qpmInh, qpmMod, qpmCurrentNS;
  QPixmap qpmExcSelected, qpmInhSelected, qpmModSelected, qpmCurrentSelected;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../.. && make -k "
//// End:
