#ifndef CLSSETTINGSREADER_H
#define CLSSETTINGSREADER_H

#include <qdom.h>
#include <list>
#include <string>

using namespace std;

class QXmlSimpleReader;

class ClsSettingsReader {

public:
  ClsSettingsReader();
  ~ClsSettingsReader();
  void parseBuffer(string strFileName);
  void terminateXMLPlatformUtils();

  string getEntity(string strEntityName);
  list<string> getListLastFiles();

private:
  enum SETTINGREADER_STATES {
    PARSER_INSTANTIATED,
    PARSER_BUFFER_PARSED
  };
  int iSysSettingReaderState;
  bool bXMLPlatformInitialized;

  QDomDocument ddocIqrSetting;
  QXmlSimpleReader *parser;
};

#endif

/// Local Variables:
/// mode: c++
/// End:
