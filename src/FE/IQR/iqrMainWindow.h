#ifndef IQRMAINWINDOW_H
#define IQRMAINWINDOW_H /*+ To stop multiple inclusions. +*/

#include <qglobal.h>
#include <qmainwindow.h>
#include <qnamespace.h>
#include <qobjectdefs.h>
#include <qstring.h>
#include <list>
#include <memory>
#include <string>

#include <ClsBaseDataBroker.h>
#include <ClsFESerializer.h>

class QAction;
class QActionGroup;
class QCloseEvent;
class QEvent;
class QLabel;
class QMenu;
class QMovie;
class QMutex;
class QProgressBar;
class QSplitter;
class QStatusBar;
class QTimer;
class QToolBar;
class QUdpSocket;
class QWidget;

#ifdef RC_TCP
#include "ipcserver.h"
#else
#endif
class ClsQSystemBrowser;

#define COUT cout << "[01;34m"
#define ENDL "[00m" << endl

static const qint16 ipcPort = 54923;

using namespace std;

class iqrMainWindow : public QMainWindow {
  Q_OBJECT
public:
  iqrMainWindow(QWidget *parent = nullptr, const char *name = nullptr,
                Qt::WindowFlags f = Qt::Window);

  void newSystem();
  void openSystem(string s);
  void customEvent(QEvent *e) override;
  void autoStartSimulation();
  void openConfig(string strConfigFileName);
  void openHarborConfig(string strHarborConfigFileName);
  void copyItemsToClipboard(int iType, list<string> lstIDs);
  void copyItemsToClipboard(list<string> lstGroupIDs,
                            list<string> lstConnectionIDs);
  void exportProcess(string strID);

signals:
  void sigSimulationRunning(bool);

public
slots:
  bool slotQuitIQR();

private
slots:
  void slotOpenSystem();
  void slotOpenOldFile(QAction *_qact);

  void slotRunControl();
  void slotPause();

  void slotSaveSystem();
  void slotSaveSystemAs();
  void slotAutoSaveSystem();
  void slotDebugSaveSystem();
  void slotExportSystemForAER();
  void slotImportProcess();
  void slotLinkProcess();
  void slotSaveToBackup();

  void slotSaveDiagram();
  void slotPrintDiagram();

  void slotNewSystem();
  bool slotCloseSystem();

  void slotSettings();
  bool buildSystem();
  void slotSystemBuildt();
  void slotShowLogWindow();
  void advanceProgress();
  void showLoadProgress(string s);
  void slotValidateSystem();

  void slotDiagViewActivated(int, string);

  /* dialogs */
  void slotSystemProperties();
  void slotHelpMenu(QAction *_qact);
  void slotOpenHarbor();
  void slotOpenDataSampler();
  void slotOpenDataBroadcaster();

  /* clipboard */

  void slotPaste();
  void slotClipboardChanged();

  void slotRemoteCommand(const QString &);

#ifndef RC_TCP
  void slotReadRCUDP();
#endif

  void slotShortCuts(QAction *_qact);

private:
  void createAnimation();
  void initLFO(list<string> &);
  void closeEvent(QCloseEvent *e) override;
  void changeState();

  int startSimulation();
  void stopSimulation();
  void pauseSimulation(bool b);

  void setupRCServer(int iPort);
  void startPdfHelp(int iType);

  void importORLinkProcess(bool);
  bool isValidSystem(bool bConfirm);

  enum ABOUT_MENU {
    HELP_MANUAL,
    HELP_USERDEFINED_TYPES
  };

  string strSystemFileName;
  QMutex *qmutexSysGUI;
  QMutex *qmutexSimulation;

  QTimer *qtimerAutoSave;
  int iActiveItemType;
  int iClipboardType;
  string strActiveItemID;

  /* core modules */
  std::unique_ptr<ClsFESerializer> clsFESerializer;
  std::unique_ptr<ClsBaseDataBroker> clsFECommHandler;
  ClsQSystemBrowser *clsQSystemBrowser;

  /* GUI related stuff */
  QMenu *qmenuFile;
  QMenu *qmenuEdit;
  QMenu *qmenuView;
  QMenu *qmenuDiagram;
  QMenu *qmenuData;
  QMenu *qmenuHelp;

  QToolBar *qtoolbarToolbar;
  QStatusBar *qstatusBar;

  QLabel *qlblStatus;

  QMovie *qmovRunAnimation;

  QProgressBar *progress;
  QLabel *lblStatusBar;
  QSplitter *qsplitter;
  QAction *qactionRunControl;
  QAction *qactionPause;

  /* ========== short cut actions  ==========  */
  QAction *qactCutItem;
  QAction *qactCopyItem;
  QAction *qactDeleteItem;
  QActionGroup *qactgrpShortCuts;

  /*  ========== menubar FILE  ==========  */
  QAction *qactOpenSystem;
  QAction *qactNewSystem;
  QAction *qactCloseSystem;
  QAction *qactSaveSystem;
  QAction *qactSaveSystemAs;
  QAction *qactSaveToBackup;
  QAction *qactExportSystemForAER;
  QAction *qactImportProcess;
  QAction *qactLinkProcess;
  QAction *qactSystemProperties;
  QAction *qactQuitIQR;

  /* ========== menubar EDIT  ==========  */
  QAction *qactPaste;
  QAction *qactSettings;
  QAction *qactValidateSystem;

  /*  ========== menubar VIEW  ==========  */
  QAction *qactViewLog;

  /*  ========== menubar DIAGRAM  ==========  */
  QAction *qactSaveDiagram;
  QAction *qactPrintDiagram;

  /* menubar  ========== HELP  ==========  */
  QAction *qactPDFManual;
  QAction *qactPDFUserDefTypes;
  QAction *qactAbout;
  QActionGroup *qactgrpHelp;

  /* menubar list of last files open */
  QActionGroup *qactgrpLFO;

  QAction *qaccelMainWindow;

#ifdef RC_TCP
  IpcServer *serverRC;
#else
  QUdpSocket *socketRC;
#endif

  bool bSimulationRunning;
  friend class ClsFEDialogManager;
  bool bSystemAlreadySaved;
};

#endif /* IQRMAINWINDOW_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
