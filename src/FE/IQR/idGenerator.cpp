/****************************************************************************
 ** $Filename: idGenerator.cpp
 ** $Header$
 **
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Tue Jan 16 22:14:33 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description: this class uses Linux/UNIX specific clock calls.  These
 **               must be replaced on other platforms.
 **
 **               MS GUID format:
 **                   CPU serial no.
 **                   Network adaptor MAC address
 **                   time
 **
 *****************************************************************************/

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "idGenerator.hpp"

#ifdef LINUX
static const char *pcOS = "L";
#else
static const char *pcOS = "O";
#endif

//#define DEBUG_CLSIDGENERATOR

#ifdef DEBUG_CLSIDGENERATOR
static const bool bDebugIDGenerator = true;
#else
static const bool bDebugIDGenerator = false;
#endif

ClsIDGenerator *ClsIDGenerator::_instanceIDGenerator = nullptr;

ClsIDGenerator *ClsIDGenerator::Instance() {
  if (_instanceIDGenerator == nullptr) {
    _instanceIDGenerator = new ClsIDGenerator();
  }
  return _instanceIDGenerator;
}

ClsIDGenerator::ClsIDGenerator() : iCount(0) {

  // Get current time.
  tPrevious = time(nullptr);

  // Store process ID as an integer.
  iPID = int(getpid());

  if (bDebugIDGenerator) {
    // qDebug can only be used once the QApplication is running.
    cerr << "ClsIDGenerator::ClsIDGenerator: created at " << ctime(&tPrevious)
         << " in process " << iPID << endl;
    cerr << "ClsIDGenerator::ClsIDGenerator: previous time set to "
         << int(tPrevious) << endl;
  }
}

string ClsIDGenerator::Next() {

  // Get current time (in seconds since 1 Jan 1970, this will loop
  // in 2034).
  tCurrent = time(nullptr);

  // Has the time changed since the last ID was generated?
  if (tCurrent == tPrevious) {

    // Time hasn't changed so increment the counter.  The counter
    // allows unique IDs to be created during the same second.
    // The limit is MAX_INT IDs during 1 second.
    iCount++;

  } else {

    // Time has changed, save for next check and
    tPrevious = tCurrent;
    iCount = 0;
  }

  ostringstream oss;

  // Generate ID.
  string strID;

#ifndef WIN32
  oss << pcOS << "-" << iPID << "-" << int(tCurrent) << "-" << random();
#else
  oss << pcOS << "-" << iPID << "-" << int(tCurrent) << "-" << rand();
#endif
  strID = oss.str();

  if (bDebugIDGenerator) {
    cout << "ClsIDGenrator::Next: new ID " << strID << " at "
         << ctime(&tCurrent) << endl;
  }

  return strID;
}
