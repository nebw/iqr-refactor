file(GLOB src "*.cpp" "*.h" "*.hpp")

# TODO: doesn't compile and appearently not in use -> remove?
list(REMOVE_ITEM src "${CMAKE_CURRENT_SOURCE_DIR}/ClsQPrcMonitor.cpp")
list(REMOVE_ITEM src "${CMAKE_CURRENT_SOURCE_DIR}/ClsQPrcMonitor.h")

add_library(RTInfo STATIC ${src})
