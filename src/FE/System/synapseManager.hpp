/****************************************************************************
 ** $Header$
 **
 ** $Author$
 **
 ** Created: Tue Sep  2 11:01:15 2003
 **
 ** $Date$
 **
 ** $Description$
 **
 ** $Log$
 **
 *****************************************************************************/

#ifndef SYNAPSEMANAGER_HPP
#define SYNAPSEMANAGER_HPP

#include <list>
#include <string>

#include "feTypeManager.hpp"

namespace iqrcommon {
class ClsSynapse;
} // namespace iqrcommon

using iqrcommon::ClsSynapse;
using namespace std;

namespace iqrfe {

void initializeSynapseManager(list<string> lstPaths);
typedef ClsTypeManager<ClsSynapse> SynapseManager;
}

#endif
