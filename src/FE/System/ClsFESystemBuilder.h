/****************************************************************************
 ** $Filename: ClsFESystemBuilder.h
 **
 ** $Author: Ulysses Bernardet
 **
 *****************************************************************************/

#ifndef CLSPRCSYSTEMBUILDER_H
#define CLSPRCSYSTEMBUILDER_H

#include <list>
#include <map>
#include <string>

#include "ClsInfoDiagramIcon.h"
#include "ClsInfoDiagramLine.h"
#include "ClsSysFileParser.h"

#define COUT cout << "[01;34m"
#define ENDL "[00m" << endl

using namespace std;
using namespace iqrcommon;

class ClsFESystemBuilder {

public:
  ClsFESystemBuilder();

  ~ClsFESystemBuilder(){};

  void readSystemFileFromDisk(string _strSystemFileName);
  string readFileFromDisk(string _strFileName);

  void importProcess(string, bool bEmitSignals, bool bLink);

  void parseSystemFile();
  void parseFragment(string strCont, bool bValidate, int iActiveItemType,
                     string strActiveItemID);

  void importFragment(string strCont, bool bValidate, bool bEmitSignals,
                      string _strFileName);

  void buildSystem();

  ClsInfoDiagramLine getDiagramLine(string _strID);
  ClsInfoDiagramIcon getDiagramIcon(string _strID);

private:
  string strSystemFileCont;
  ClsSysFileParser clsSysFileParser;
  friend class ClsFESystemManager;

  map<string, ClsInfoDiagramLine> mapDiagramLineInternal;
  map<string, ClsInfoDiagramIcon> mapDiagramIconInternal;
  map<string, ClsInfoDiagramLine> mapDiagramLineExternal;
  map<string, ClsInfoDiagramIcon> mapDiagramIconExternal;
  list<string> lstExternalPathsProcesses;
};

#endif
