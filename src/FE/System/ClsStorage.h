#ifndef CLSSTORAGE_H
#define CLSSTORAGE_H

#include <queue>

using namespace std;

template <class TParent, class TChild> class ClsStorage {

public:
  ClsStorage(){};

  ~ClsStorage() {
    while (queueChildren.size()) {
      queueChildren.pop();
    }
  };

  void addParent(TParent _tParent) { tParent = _tParent; }

  void addChild(TChild tChild) { queueChildren.push(tChild); }

  TParent getParent() { return tParent; };

  TChild getChild() {
    TChild tChild = queueChildren.front();
    queueChildren.pop();
    return tChild;
  };

private:
  TParent tParent;
  queue<TChild> queueChildren;
};

#endif
