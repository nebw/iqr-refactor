/****************************************************************************
 ** $Filename: ClsFEComputeEngine.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Thu Jul 10 00:45:22 2003
 **
 *****************************************************************************/

#ifndef CLSFECOMPUTEENGINE_H
#define CLSFECOMPUTEENGINE_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <qthread.h>
#include <atomic>
#include <map>
#include <string>

class QMutex;
class QWidget;

#if defined(QT_NO_THREAD)
#error Thread support not enabled.
#endif

using namespace std;

class ClsFEConnection;
class ClsFEDataSampler;
class ClsFEGroup;
class ClsFEProcess;

class ClsFEComputeEngine : public QThread {

public:
  static ClsFEComputeEngine *Instance();
  static void initializeComputeEngine(QWidget *_parent = nullptr,
                                      QMutex *_qmutexSysGUI = nullptr,
                                      QMutex *_qmutexSimulation = nullptr);

  void setDataSampler(ClsFEDataSampler *_clsFEDataSampler, int iInterval);

  void prepare(bool _bSyncPlots);
  void run() override;
  void stop();
  void pause(bool b);

signals:

private:
  ClsFEComputeEngine(QWidget *_parent, QMutex *_qmutexSysGUI,
                     QMutex *_qmutexSimulation);
  static ClsFEComputeEngine *_instanceComputeEngine;

  QWidget *parent;

  QMutex *qmutexSysGUI;
  QMutex *qmutexSimulation;

  atomic<int> iRoundCounter;
  atomic<bool> stopped;
  atomic<bool> paused;

  map<string, ClsFEGroup *>::iterator miGroup;
  map<string, ClsFEProcess *>::iterator miProcess;
  map<string, ClsFEConnection *>::iterator miConnection;

  ClsFEDataSampler *clsFEDataSampler;
  int iSamplerInterval;

  bool bSyncPlots;
};

#endif /* CLSFECOMPUTEENGINE_H */
