file(GLOB src "*.cpp" "*.h" "*.hpp")
file(GLOB src_arb "Connection/Arborization/*.cpp" 
                  "Connection/Arborization/*.h" 
                  "Connection/Arborization/*.hpp")
file(GLOB src_fun "Connection/Function/*.cpp" 
                  "Connection/Function/*.h" 
                  "Connection/Function/*.hpp")
file(GLOB src_con "Connection/Pattern/*.cpp" 
                  "Connection/Pattern/*.h" 
                  "Connection/Pattern/*.hpp")
file(GLOB src_top "Topology/*.cpp" 
                  "Topology/*.h" 
                  "Topology/*.hpp")

# TODO: whats this? remove?
list(REMOVE_ITEM src "${CMAKE_CURRENT_SOURCE_DIR}/comare.cpp")

add_library(System STATIC ${src} ${src_arb} ${src_fun} ${src_con} ${src_top})
