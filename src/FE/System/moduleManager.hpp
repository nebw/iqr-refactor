/****************************************************************************
 ** $Header$
 **
 ** $Author$
 **
 ** Created: Tue Sep  2 11:01:15 2003
 **
 ** $Date$
 **
 ** $Description$
 **
 ** $Log$
 **
 *****************************************************************************/

#ifndef MODULEMANAGER_HPP
#define MODULEMANAGER_HPP

#include <list>
#include <string>

#include "feTypeManager.hpp"

namespace iqrcommon {
class ClsModule;
} // namespace iqrcommon

using namespace std;
using iqrcommon::ClsModule;

namespace iqrfe {

void initializeModuleManager(list<string> lstPaths);
typedef ClsTypeManager<ClsModule> ModuleManager;
}

#endif
