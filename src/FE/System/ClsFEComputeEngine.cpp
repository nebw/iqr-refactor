#include <ClsFESystemManager.h>
#include <qapplication.h>
#include <qcoreevent.h>
#include <qmutex.h>
#include <qwidget.h>
#include <sys/time.h>
#include <chrono>
#include <iostream>
#include <utility>
#include <thread>

#include "ClsFEComputeEngine.h"
#include "ClsFEConnection.h"
#include "ClsFEDataManager.h"
#include "ClsFEDataSampler.h"
#include "ClsFEGroup.h"
#include "ClsFEProcess.h"
#include "ClsQCPSCustomEvent.h"

//#define DEBUG_CLSFECOMPUTEENGINE

double fCPSCurrent;

ClsFEComputeEngine *ClsFEComputeEngine::_instanceComputeEngine = nullptr;

void ClsFEComputeEngine::initializeComputeEngine(QWidget *_parent,
                                                 QMutex *_qmutexSysGUI,
                                                 QMutex *_qmutexSimulation) {
  _instanceComputeEngine =
      new ClsFEComputeEngine(_parent, _qmutexSysGUI, _qmutexSimulation);
}

ClsFEComputeEngine *ClsFEComputeEngine::Instance() {
  return _instanceComputeEngine;
}

ClsFEComputeEngine::ClsFEComputeEngine(QWidget *_parent, QMutex *_qmutexSysGUI,
                                       QMutex *_qmutexSimulation)
    : parent(_parent), qmutexSysGUI(_qmutexSysGUI),
      qmutexSimulation(_qmutexSimulation), clsFEDataSampler(nullptr),
      iSamplerInterval(1), iRoundCounter(0), stopped(false), paused(false),
      bSyncPlots(false) {}

void ClsFEComputeEngine::setDataSampler(ClsFEDataSampler *_clsFEDataSampler,
                                        int iInterval /*, int iCount*/) {
#ifdef DEBUG_CLSFECOMPUTEENGINE
  cout << "ClsFEComputeEngine::setDataSampler(ClsFEDataSampler* "
          "_clsFEDataSampler, int iInterval)" << endl;
#endif
  clsFEDataSampler = _clsFEDataSampler;
  iSamplerInterval = iInterval;
}

void ClsFEComputeEngine::prepare(bool _bSyncPlots) {
#ifdef DEBUG_CLSFECOMPUTEENGINE
  cout << "ClsFEComputeEngine::prepare( )" << endl;
#endif

  bSyncPlots = _bSyncPlots;

  /* loop here over process to start the (potentially) threaded modules */
  for (miProcess = ClsFESystemManager::Instance()->mapFEProcesses.begin();
       miProcess != ClsFESystemManager::Instance()->mapFEProcesses.end();
       ++miProcess) {
    miProcess->second->setMutex(qmutexSysGUI);
    miProcess->second->startModule();
  }
}

void ClsFEComputeEngine::run() {
#ifdef DEBUG_CLSFECOMPUTEENGINE
  cout << "ClsFEComputeEngine::run( ) " << endl;
#endif

  struct timeval sTimevalCurrent;
  struct timezone sTimezone;
  double fDeltaTime, fArrivalTime;
  long int iSleep;

  gettimeofday(&sTimevalCurrent, &sTimezone);
  double fLastUpdate =
      sTimevalCurrent.tv_sec + (double)sTimevalCurrent.tv_usec / 1000000.;
  double fStartTime = fLastUpdate;
  double fCurrentTime = fLastUpdate;
  double fCPS = ClsFESystemManager::Instance()->getCyclesPerSecond();
  static const int iSpeedMeasureInterval = 100;

  QMutexLocker simulationLock(qmutexSimulation);

  for (;;) {
    {
      QMutexLocker sysGUILock(qmutexSysGUI);
      // we're checking for changes of the CPS every 20th cycle: don't wait too
      // much time with this...
      if ((iRoundCounter % 20) == 0) {
        fCPS = ClsFESystemManager::Instance()->getCyclesPerSecond();
      }

#ifdef DEBUG_CLSFECOMPUTEENGINE
      cout << "updating groups:" << endl;
#endif
      for (miGroup = ClsFESystemManager::Instance()->mapFEGroups.begin();
           miGroup != ClsFESystemManager::Instance()->mapFEGroups.end();
           ++miGroup) {
        miGroup->second->update();
      }

#ifdef DEBUG_CLSFECOMPUTEENGINE
      cout << "updating processes:" << endl;
#endif
      for (miProcess = ClsFESystemManager::Instance()->mapFEProcesses.begin();
           miProcess != ClsFESystemManager::Instance()->mapFEProcesses.end();
           ++miProcess) {
        miProcess->second->update();
      }

#ifdef DEBUG_CLSFECOMPUTEENGINE
      cout << "updating  connections" << endl;
#endif
      for (miConnection =
               ClsFESystemManager::Instance()->mapFEConnections.begin();
           miConnection !=
               ClsFESystemManager::Instance()->mapFEConnections.end();
           ++miConnection) {
#ifdef DEBUG_CLSFECOMPUTEENGINE
        cout << "\t" << miConnection->second->getConnectionID() << endl;
#endif
        miConnection->second->update();
      }
    }

    if (clsFEDataSampler != nullptr && iSamplerInterval > 0) {
      if (iRoundCounter % iSamplerInterval == 0) {
        auto qceDS = new QEvent((QEvent::Type)ClsFEDataSampler::EVENT_SAVEDATA);
        QApplication::postEvent(clsFEDataSampler, qceDS);
      }
    }

    if (bSyncPlots) {
      auto qceDM = new QEvent((QEvent::Type)ClsFEDataManager::EVENT_UPDATE);
      QApplication::postEvent(ClsFEDataManager::Instance(), qceDM);
    }

    iRoundCounter++;
#ifdef DEBUG_CLSFECOMPUTEENGINE
    cout << "iRoundCounter: " << iRoundCounter << endl;
#endif

    if ((iRoundCounter % iSpeedMeasureInterval) == 0) {
      gettimeofday(&sTimevalCurrent, &sTimezone);
      fCurrentTime =
          sTimevalCurrent.tv_sec + (double)sTimevalCurrent.tv_usec / 1e6;
      fDeltaTime = fCurrentTime - fLastUpdate;
      fLastUpdate = fCurrentTime;

      if (fDeltaTime > 0.0000000000000001) {
        /* double 2008/08/03 */ fCPSCurrent =
            (double)iSpeedMeasureInterval / fDeltaTime;
        /* post the update speed to SystemGUI */
        auto qce = new ClsQCPSCustomEvent(fCPSCurrent);
        QApplication::postEvent(parent, qce);
      }
    }

    if (stopped) {
      break;
    }

    if (fCPS > 0) {
      /* jitter correction: */
      fArrivalTime = fStartTime + (double)iRoundCounter / fCPS;

      /* no jitter correction: */

      gettimeofday(&sTimevalCurrent, &sTimezone);
      fCurrentTime =
          sTimevalCurrent.tv_sec + (double)sTimevalCurrent.tv_usec / 1e6;
      iSleep = (int)((fArrivalTime - fCurrentTime) * 1e6);
      if (iSleep > 0) {
        usleep(iSleep);
      }
    }

    while (paused) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }

  /* loop here over process to stop the (potentially) threaded modules */
  for (miProcess = ClsFESystemManager::Instance()->mapFEProcesses.begin();
       miProcess != ClsFESystemManager::Instance()->mapFEProcesses.end();
       ++miProcess) {
    miProcess->second->stopModule();
  }

  cerr << "ClsFEComputeEngine:run() exit" << endl;
}

void ClsFEComputeEngine::stop() {
#ifdef DEBUG_CLSFECOMPUTEENGINE
  cout << "ClsFEComputeEngine::stop()" << endl;
#endif
  stopped = true;
  iRoundCounter = 0;
}

void ClsFEComputeEngine::pause(bool b) {
#ifdef DEBUG_CLSFECOMPUTEENGINE
  cout << "ClsFEComputeEngine::pause()" << endl;
#endif
  paused = b;
}
