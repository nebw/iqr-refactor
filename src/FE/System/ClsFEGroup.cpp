#include "ClsFEGroup.h"

#include <neuronManager.hpp>
#include <tagLibrary.hpp>
#include <valarray>

#include "ClsGroupManipPattern.h"
#include "neuron.hpp"
#include "parameter.hpp"
#include "stateArray.hpp"
#include "stateVariable.hpp"

ClsFEGroup::ClsFEGroup() { clsGroupManipPattern = nullptr; };

ClsFEGroup::ClsFEGroup(string _strGroupID, string _strGroupName,
                       string _strPrcID)
    : ClsBaseGroup(_strGroupID, _strGroupName, _strPrcID) {

  clsGroupManipPattern = nullptr;

  addStringParameter(ClsTagLibrary::NotesTag(), /* _strName */
                     "Notes",                   /* _strLabel */
                     "",                        /* _strValue */
                     true,                      /* _bEditable */
                     true,                      /* _bLong */
                     "",                        /* _strDescription */
                     "Notes" /* _strCategory */);

  addColorParameter(ClsTagLibrary::ColorTag(), /* _strName */
                    "Color",                   /* _strLabel */
                    "#FFFFFF",                 /* _strValue */
                    true,                      /* _bEditable */
                    "Color on Diagram", "Properties" /* _strCategory */);
}

string ClsFEGroup::getNotes() {
  string strNote =
      (getParameter(ClsTagLibrary::NotesTag()))->getValueAsString();
  return strNote;
}

void ClsFEGroup::setNotes(string _strNotes) {
  setParameter(ClsTagLibrary::NotesTag(), _strNotes);
}

void ClsFEGroup::setColor(string _str) {
  setParameter(ClsTagLibrary::ColorTag(), _str);
};
string ClsFEGroup::getColor() {
  return getParameter(ClsTagLibrary::ColorTag())->getValueAsString();
};

string ClsFEGroup::validate() {
  string strMessage = "";

  if (pNeuron == nullptr) {
    strMessage = getParameter(ClsTagLibrary::NameTag())->getValueAsString();
    strMessage.append("\n\tNo Neuron defined");
  }
  return strMessage;
}

ClsGroupManipPattern *ClsFEGroup::getGroupManipPattern() {
#ifdef DEBUG_CLSFEGROUP
  cout << "ClsFEGroup::getGroupManipPattern()" << endl;
#endif
  if (clsGroupManipPattern == nullptr) {
    clsGroupManipPattern = new ClsGroupManipPattern(getNumberOfNeurons());
  }
  return clsGroupManipPattern;
}

void ClsFEGroup::removeGroupManipPattern() {
#ifdef DEBUG_CLSFEGROUP
  cout << "ClsFEGroup::removeGroupManipPattern()" << endl;
#endif
  if (clsGroupManipPattern != nullptr) {
    delete clsGroupManipPattern;
    clsGroupManipPattern = nullptr;
  }
}

void ClsFEGroup::update() {
#ifdef DEBUG_CLSFEGROUP
  cerr << "ClsFEGroup::update()" << endl;
#endif

  ClsBaseGroup::update();

  if (clsGroupManipPattern != nullptr) {
    if (clsGroupManipPattern->bDoUpdate) {
      if (iRoundCounter % clsGroupManipPattern->getInterval() == 0) {
        if (clsGroupManipPattern->getMode() ==
            ClsGroupManipPattern::MODE_CLAMP) {
          pNeuron->getOutputState()->getStateArray()[0] =
              clsGroupManipPattern->getPatternBuffer()[0];
        } else {
          if (clsGroupManipPattern->getMode() ==
              ClsGroupManipPattern::MODE_ADD) {
            pNeuron->getOutputState()->getStateArray()[0] +=
                clsGroupManipPattern->getPatternBuffer()[0];
          } else if (clsGroupManipPattern->getMode() ==
                     ClsGroupManipPattern::MODE_MULTIPLY) {
            pNeuron->getOutputState()->getStateArray()[0] *=
                clsGroupManipPattern->getPatternBuffer()[0];
          }
        }
        clsGroupManipPattern->advance();
      }
    }
  }
}
