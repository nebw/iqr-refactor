/****************************************************************************
 ** $Filename: ClsPatterns.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Thu May 29 15:03:20 2003
 **
 **
 *****************************************************************************/

#ifndef CLSPATTERNS_H
#define CLSPATTERNS_H /*+ To stop multiple inclusions. +*/

#include <string>

#include "ClsBasePattern.h"

using namespace iqrcommon;
using namespace std;

class ClsPatternMapped : public ClsBasePattern {
public:
  ClsPatternMapped();

private:
};

class ClsPatternForeach : public ClsBasePattern {
public:
  ClsPatternForeach();
  string getTest();

private:
};

class ClsPatternTuples : public ClsBasePattern {
public:
  ClsPatternTuples();

private:
};

#endif /* CLSPATTERNS_H */
