/****************************************************************************
 ** $Filename: ClsPatterns.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Sat Jun 28 01:21:40 2003
 **
 *****************************************************************************/

#include "ClsPatterns.h"
#include "tagLibrary.hpp"
#include "ClsSubPopulations.h"

#include "optionsParameter.hpp"

ClsPatternMapped::ClsPatternMapped() {

  strType = ClsTagLibrary::ConnectionPatternMapped();

  addOptionsParameter(ClsTagLibrary::ConnectionTypeTag(), // string _strName
                      "mapping type", false,              // bool   _bReadOnly,
                      "mapping type",                     //_strDescription,
                      "Properties");
  ClsOptionsParameter *op = static_cast<ClsOptionsParameter *>(
      getParameter(ClsTagLibrary::ConnectionPatternMappedType()));

  op->addOption(ClsTagLibrary::ConnectionPatternMappedTypeAll());
  op->addOption(ClsTagLibrary::ConnectionPatternMappedTypeCenter());

  setParameter(ClsTagLibrary::ConnectionPatternMappedType(),
               ClsTagLibrary::ConnectionPatternMappedTypeCenter());

  /* defaults */
  clsSubPopulationSource = new ClsAll();
  clsSubPopulationTarget = new ClsAll();
};

ClsPatternForeach::ClsPatternForeach() {
  strType = ClsTagLibrary::ConnectionPatternForeach();

  /* defaults */
  clsSubPopulationSource = new ClsAll();
  clsSubPopulationTarget = new ClsAll();
};

string ClsPatternForeach::getTest() { return "WORKS"; };

ClsPatternTuples::ClsPatternTuples() {
  strType = ClsTagLibrary::ConnectionPatternTuples();
};
