/****************************************************************************
 ** $Filename: ClsBasePattern.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jun 27 04:30:41 2003
 **
 *****************************************************************************/

#include "ClsBasePattern.h"

#include <string>

#include "ClsSubPopulations.h"
#include "ClsSysFileNode.h"
#include "tagLibrary.hpp"

ClsBasePattern::ClsBasePattern() {
  clsSubPopulationSource = nullptr;
  clsSubPopulationTarget = nullptr;
  clsTuples = nullptr;
  bRuntimeEditable = false;
};

void
ClsBasePattern::setSourcePopulation(ClsBaseSubPopulation *clsSubPopulation) {
  if (dynamic_cast<ClsRegion *>(clsSubPopulation)) {
    clsSubPopulationSource = new ClsRegion();
    *((ClsRegion *)clsSubPopulationSource) =
        *(dynamic_cast<ClsRegion *>(clsSubPopulation));
    ;
  } else if (dynamic_cast<ClsList *>(clsSubPopulation)) {
    clsSubPopulationSource = new ClsList();
    *((ClsList *)clsSubPopulationSource) =
        *(dynamic_cast<ClsList *>(clsSubPopulation));
  } else if (dynamic_cast<ClsAll *>(clsSubPopulation)) {
    clsSubPopulationSource = new ClsAll();
    *((ClsAll *)clsSubPopulationSource) =
        *(dynamic_cast<ClsAll *>(clsSubPopulation));
  } else if (dynamic_cast<ClsTuples *>(clsSubPopulation)) {
    clsSubPopulationSource = new ClsTuples();
    *((ClsTuples *)clsSubPopulationSource) =
        *(dynamic_cast<ClsTuples *>(clsSubPopulation));
  } else {
    clsSubPopulationSource = new ClsBaseSubPopulation();
  }
}

void
ClsBasePattern::setTargetPopulation(ClsBaseSubPopulation *clsSubPopulation) {
  if (dynamic_cast<ClsRegion *>(clsSubPopulation)) {
    clsSubPopulationTarget = new ClsRegion();
    *((ClsRegion *)clsSubPopulationTarget) =
        *(dynamic_cast<ClsRegion *>(clsSubPopulation));
    ;
  } else if (dynamic_cast<ClsList *>(clsSubPopulation)) {
    clsSubPopulationTarget = new ClsList();
    *((ClsList *)clsSubPopulationTarget) =
        *(dynamic_cast<ClsList *>(clsSubPopulation));
  } else if (dynamic_cast<ClsAll *>(clsSubPopulation)) {
    clsSubPopulationTarget = new ClsAll();
    *((ClsAll *)clsSubPopulationTarget) =
        *(dynamic_cast<ClsAll *>(clsSubPopulation));
  } else if (dynamic_cast<ClsTuples *>(clsSubPopulation)) {
    clsSubPopulationTarget = new ClsTuples();
    *((ClsTuples *)clsSubPopulationTarget) =
        *(dynamic_cast<ClsTuples *>(clsSubPopulation));
  } else {
    clsSubPopulationTarget = new ClsBaseSubPopulation();
  }
}

ClsBaseSubPopulation *ClsBasePattern::getSourcePopulation() {
  return clsSubPopulationSource;
}

ClsBaseSubPopulation *ClsBasePattern::getTargetPopulation() {
  return clsSubPopulationTarget;
}

void ClsBasePattern::setTuples(ClsTuples *_clsTuples) {
  clsTuples = new ClsTuples();
  *clsTuples = *_clsTuples;
};

ClsTuples *ClsBasePattern::getTuples() { return clsTuples; };

void ClsBasePattern::setSourcePopulationParameter(
    ClsSysFileNode &_clsSysFileNodeSubPopulation) {
  string strSubPopulation = _clsSysFileNodeSubPopulation.getName();
  if (!strSubPopulation.compare(ClsTagLibrary::SelectorRegion())) {
    clsSubPopulationSource = new ClsRegion();
  } else if (!strSubPopulation.compare(ClsTagLibrary::SelectorList())) {
    clsSubPopulationSource = new ClsList();
  } else if (!strSubPopulation.compare(ClsTagLibrary::SelectorAll())) {
    clsSubPopulationSource = new ClsAll();
  } else {
    clsSubPopulationSource = nullptr;
  }

  if (clsSubPopulationSource != nullptr) {
    clsSubPopulationSource->setParameter(_clsSysFileNodeSubPopulation);
  }
};
void ClsBasePattern::setTargetPopulationParameter(
    ClsSysFileNode &_clsSysFileNodeSubPopulation) {
  string strSubPopulation = _clsSysFileNodeSubPopulation.getName();
  if (!strSubPopulation.compare(ClsTagLibrary::SelectorRegion())) {
    clsSubPopulationTarget = new ClsRegion();
  } else if (!strSubPopulation.compare(ClsTagLibrary::SelectorList())) {
    clsSubPopulationTarget = new ClsList();
  } else if (!strSubPopulation.compare(ClsTagLibrary::SelectorAll())) {
    clsSubPopulationTarget = new ClsAll();
  } else {
    clsSubPopulationTarget = nullptr;
  }

  if (clsSubPopulationTarget != nullptr) {
    clsSubPopulationTarget->setParameter(_clsSysFileNodeSubPopulation);
  }
};

void ClsBasePattern::setTupleParameter(
    ClsSysFileNode &_clsSysFileNodeSubPopulation) {

  if (clsTuples == nullptr) {
    clsTuples = new ClsTuples();
  }

  if (clsTuples != nullptr) {
    clsTuples->setParameter(_clsSysFileNodeSubPopulation);
  }
};
