/****************************************************************************
 ** $Filename: ClsArbRandom.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 16 13:02:45 2003
 **
 *****************************************************************************/

#ifndef CLSARBRANDOM_H
#define CLSARBRANDOM_H

#include <stdlib.h>
#include <iostream>

#include "ClsBaseArborization.h"
#include "intParameter.hpp"

using namespace iqrcommon;
using namespace std;

class ClsArbRandom : public ClsBaseArborization {

public:
  ClsArbRandom();
  int getCount() { return pclsArbCount->getValue(); }
  double DistMax() override {
    cerr << "NOT IMPLEMENTED " << __FILE__ << endl;
    exit(1);
    return 0;
  }

private:
  ClsIntParameter *pclsArbCount;
};

#endif // CLSARBRANDOM_H
