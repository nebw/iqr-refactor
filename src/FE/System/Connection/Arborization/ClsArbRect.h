/****************************************************************************
 ** $Filename: ClsArbRect.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 16 13:02:43 2003
 **
 *****************************************************************************/

#ifndef CLSARBRECT_H
#define CLSARBRECT_H

#include "ClsBaseArborization.h"
#include "intParameter.hpp"

using namespace iqrcommon;
using namespace std;

class ClsArbRect : public ClsBaseArborization {

public:
  ClsArbRect();
  int getWidth() { return pclsArborizationWidth->getValue(); };
  int getHeight() { return pclsArborizationHeight->getValue(); };

  double DistMax() override {
    return calculateDistance(1., 1., getWidth(), getHeight());
  };

private:
  ClsIntParameter *pclsArborizationWidth, *pclsArborizationHeight;
};

#endif // CLSARBRECT_H
