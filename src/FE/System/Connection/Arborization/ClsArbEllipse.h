/****************************************************************************
 ** $Filename: ClsArbEllipse.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 16 13:02:50 2003
 **
 *****************************************************************************/

#ifndef CLSARBELLIPSE_H
#define CLSARBELLIPSE_H

#include "ClsBaseArborization.h"
#include "intParameter.hpp"

using namespace iqrcommon;
using namespace std;

class ClsArbEllipse : public ClsBaseArborization {

public:
  ClsArbEllipse();
  int getWidth() { return pclsArborizationWidth->getValue(); };
  int getHeight() { return pclsArborizationHeight->getValue(); };

  double DistMax() override {
    return calculateDistance(1., 1., getWidth(), getHeight());
  };

private:
  ClsIntParameter *pclsArborizationWidth, *pclsArborizationHeight;
};

#endif // CLSARBELLIPSE_H
