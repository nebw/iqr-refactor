/****************************************************************************
 ** $Filename: ClsArbRectWindow.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 16 13:02:40 2003
 **
 *****************************************************************************/

#ifndef CLSARBRECTWINDOW_H
#define CLSARBRECTWINDOW_H

#include "ClsBaseArborization.h"
#include "intParameter.hpp"

using namespace iqrcommon;
using namespace std;

class ClsArbRectWindow : public ClsBaseArborization {

public:
  ClsArbRectWindow();
  int getInnerWidth() { return pclsArborizationInnerWidth->getValue(); }
  int getInnerHeight() { return pclsArborizationInnerHeight->getValue(); }
  int getOuterWidth() { return pclsArborizationOuterWidth->getValue(); }
  int getOuterHeight() { return pclsArborizationOuterHeight->getValue(); }

  double DistMax() override {
    return calculateDistance(1., 1., getOuterWidth(), getOuterHeight());
  };

private:
  ClsIntParameter *pclsArborizationInnerWidth, *pclsArborizationInnerHeight,
      *pclsArborizationOuterWidth, *pclsArborizationOuterHeight;
};

#endif // CLSARBRECTWINDOW_H
