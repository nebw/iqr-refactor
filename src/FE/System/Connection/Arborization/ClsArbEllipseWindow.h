/****************************************************************************
 ** $Filename: ClsArbEllipseWindow.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 16 13:02:48 2003
 **
 *****************************************************************************/

#ifndef CLSARBELLIPSEWINDOW_H
#define CLSARBELLIPSEWINDOW_H

#include "ClsBaseArborization.h"
#include "intParameter.hpp"

using namespace iqrcommon;
using namespace std;

class ClsArbEllipseWindow : public ClsBaseArborization {

public:
  ClsArbEllipseWindow();
  int getInnerWidth() { return pclsArborizationInnerWidth->getValue(); }
  int getInnerHeight() { return pclsArborizationInnerHeight->getValue(); }
  int getOuterWidth() { return pclsArborizationOuterWidth->getValue(); }
  int getOuterHeight() { return pclsArborizationOuterHeight->getValue(); }

  double DistMax() override {
    return calculateDistance(1., 1., getOuterWidth(), getOuterHeight());
  };

private:
  ClsIntParameter *pclsArborizationInnerWidth, *pclsArborizationInnerHeight,
      *pclsArborizationOuterWidth, *pclsArborizationOuterHeight;
};

#endif // CLSARBELLIPSEWINDOW_H
