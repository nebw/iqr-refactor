/****************************************************************************
 ** $Filename: ClsArbAll.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 16 13:03:11 2003
 **
 *****************************************************************************/

#include "ClsArbAll.h"

#include <string>

#include "tagLibrary.hpp"

ClsArbAll::ClsArbAll() { strType = ClsTagLibrary::ConnectionArborizationAll(); }
