#ifndef CLSFEGROUP_H
#define CLSFEGROUP_H

#include <string>

#include "ClsBaseGroup.h"

using namespace iqrcommon;
using namespace std;

class ClsGroupManipPattern;

class ClsFEGroup : public ClsBaseGroup {

public:
  ClsFEGroup();
  ClsFEGroup(string _strGroupID, string _strGroupName, string _strPrcID);

  ClsFEGroup *getFEGroup() { return this; }

  string getNotes(); // const;
  void setNotes(string _strNotes);

  void setColor(string _strColor);
  string getColor();

  string validate();
  void update() override;

  ClsGroupManipPattern *getGroupManipPattern();
  void removeGroupManipPattern();

private:
  ClsGroupManipPattern *clsGroupManipPattern;

  string strNotes;
};

#endif
