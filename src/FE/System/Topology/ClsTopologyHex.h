#ifndef TOPOLOGYHEX_H
#define TOPOLOGYHEX_H

#include <stdlib.h>
#include <iostream>
#include <list>

#include "ClsBaseTopology.h"
#include "ClsHyperLists.h"

class ClsBaseArborization;
class ClsBaseSubPopulation;
namespace iqrcommon {
class ClsIntParameter;
class ClsOptionsParameter;
} // namespace iqrcommon

class ClsTopologyHex : public ClsBaseTopology {

public:
  ClsTopologyHex();
  ~ClsTopologyHex() {}

  list<int>
  getCellsForSubPopulation(ClsBaseSubPopulation *clsBaseSubPopulation);
  list<tIndexDist>
  getCellsForArborization(tiPoint tPointCenter,
                          ClsBaseArborization *clsBaseArborization);
  int Size() override;
  double DistMax() override {
    cerr << "NOT IMPLEMENTED " << __FILE__ << endl;
    exit(1);
    return 0;
  }
  tiPoint index2pos(int /* iIndex */) override {
    tiPoint p;
    cerr << "NOT IMPLEMENTED" << endl;
    exit(1);
    return p;
  };
  int pos2index(int /* iX */, int /* iY */) override {
    cerr << "NOT IMPLEMENTED" << endl;
    exit(1);
    return 0;
  };
  static const char *pcName;

private:
  ClsIntParameter *pclsWidth, *pclsHeight, *pclsOffset;

  ClsOptionsParameter *pclsOrientation;
};

#endif
