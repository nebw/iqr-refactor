#ifndef TOPOLOGYRECT_H
#define TOPOLOGYRECT_H

#include <list>

#include "ClsBaseTopology.h"
#include "ClsHyperLists.h"

class ClsBaseArborization;
class ClsBaseSubPopulation;
namespace iqrcommon {
class ClsBoolParameter;
class ClsIntParameter;
} // namespace iqrcommon

using namespace iqrcommon;

class pointCompareRect {
public:
  pointCompareRect(int _iWidth);
  bool operator()(const tiPoint &p0, const tiPoint &p1);

private:
  int iWidth;
};

class ClsTopologyRect : public ClsBaseTopology {

public:
  ClsTopologyRect();
  ~ClsTopologyRect(){};

  list<tiPoint> getPointsForSubPopulation(
      ClsBaseSubPopulation *clsBaseSubPopulation) override;
  list<int> getIndicesForSubPopulation(
      ClsBaseSubPopulation *clsBaseSubPopulation) override;
  list<tIndexDist>
  getCellsForArborization(double fXCenter, double fYCenter,
                          ClsBaseArborization *clsBaseArborization) override;

  list<tiPoint> getPoints4Rect(double fXCenter, double fYCenter, double fWidth,
                               double fHeight) override;
  list<tiPoint> getPoints4Rect(int iXStart, int iYStart, int iWidth,
                               int iHeight);
  list<int> getPoints4Rect(int iGroupWidth, int iRectXStart, int iRectYStart,
                           int iRectWidth, int iRectHeight);
  list<tiPoint> getPoints4Ellipse(double fXCenter, double fYCenter,
                                  double fWidth, double fHeight) override;

  int Size() override;
  int nrCellsHorizontal() override;
  int nrCellsVertical() override;
  double DistMax() override;

  tiPoint index2pos(int iIndex) override;
  int pos2index(int iX, int iY) override;
  list<int> posList2indexList(list<tiPoint> lstIn);

  list<tiPoint> checkList(const list<tiPoint> &lstIn) override;
  static int pos2index(int _iWidth, int iX, int iY);

  static const char *pcName;

private:
  list<tiPoint> getAllPoints(int iWidth, int iHeight);

  double calculateDistance(tiPoint p0, tiPoint p1);
  double calculateDistance(double fAx, double fAy, double fBx, double fBy);
  bool isValidIndex(list<tiPoint> _lst, int iGroupWidth, int iIndex);
  ClsIntParameter *pclsWidth, *pclsHeight;
  ClsBoolParameter *clsBoolParameterVerticalCylinder;
  ClsBoolParameter *clsBoolParameterHorizontalCylinder;

  friend class pointCompareRect;
};

#endif
