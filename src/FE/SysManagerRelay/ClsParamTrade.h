#ifndef CLSPARAMTRADE_H
#define CLSPARAMTRADE_H

#include <string>
#include <iostream>

using namespace std;

struct ClsParamTrade {
  string Type;
  string Name;
  string ID;
  string Child;
  string Param;
};

#endif
