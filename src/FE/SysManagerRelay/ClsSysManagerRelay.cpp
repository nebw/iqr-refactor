#include <stddef.h>

#include "ClsSysManagerRelay.h"
#include "FE/SysManagerRelay/ClsParamTrade.h"
#include "neuronManager.hpp"

ClsSysManagerRelay *ClsSysManagerRelay::_instanceParamRelais = NULL;

void ClsSysManagerRelay::initializeParamRelais() {
  _instanceParamRelais = new ClsSysManagerRelay();
}

ClsSysManagerRelay *ClsSysManagerRelay::Instance() {
  if (_instanceParamRelais == NULL) {
    _instanceParamRelais = new ClsSysManagerRelay();
  }
  return _instanceParamRelais;
}

ClsSysManagerRelay::ClsSysManagerRelay() {}

list<ClsParamTrade> ClsSysManagerRelay::getParamTrades() {
  list<ClsParamTrade> lstParamTrades;

  return lstParamTrades;
}

int ClsSysManagerRelay::setParameterByItemID(string strType, string strItemID,
                                             string strParamID, double fValue) {
  return -1;
};
