/****************************************************************************
 ** $Filename: systemFileWriter.hpp
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Mon Sep 17 17:20:40 2001
 ** $Date: 2003/04/13 12:32:44 $
 **
 *****************************************************************************/

#ifndef CLSFESERIALIZER_HPP
#define CLSFESERIALIZER_HPP

#include <qdom.h>
#include <algorithm>
#include <list>
#include <string>

#include "parameterList.hpp"

class ClsFEConnection;
class ClsFEGroup;
class ClsFEProcess;
class ClsFESystem;

namespace iqrcommon {
class ClsItem;
}

using namespace iqrcommon;
using namespace std;

class ClsKey {
public:
  ClsKey(string _ID, string _origID, string _source, string _target,
         string _type)
      : ID(std::move(_ID)), origID(std::move(_origID)),
        source(std::move(_source)), target(std::move(_target)),
        type(std::move(_type)){};
  string ID;
  string origID;
  string source;
  string target;
  string type;
};

class ClsFESerializer {

public:
  ClsFESerializer();

  ~ClsFESerializer();

  bool SaveSystem(string _strFileName, bool bVerifySystem = false,
                  bool bVerifyFile = false);
  string getSystemAsString(bool bVerifySystem = false);

  string getProcessesAsXML(list<string> lstIDs);
  string getGroupsAsXML(list<string> lstIDs);
  string getConnectionsAsXML(list<string> lstIDs);
  string getGroupsWidthConnectionsAsXML(list<string> lstIDGroups,
                                        list<string> lstIDConnections);

  int serializeToAER(string strExportFilename);

private:
  void CreateDOMTree(bool _bIncludeXMLDeclNode = true);
  int SerializeDOMTree(string &strSystemCont);

  bool IsDOMTreeValid();
  bool IsFileValid(string _strFileName);

  void writeExternalProcesses();

  QDomElement *addFESystem(QDomDocument *ddocRoot, ClsFESystem *);
  QDomElement *addFEProcess(QDomDocument *ddocRoot, ClsFEProcess *);
  QDomElement *addFEGroup(QDomDocument *ddocRoot, ClsFEGroup *);
  QDomElement *addFEConnection(QDomDocument *ddocRoot, ClsFEConnection *);

  QDomElement *addParameterizedNode(QDomDocument *ddocRoot, string strNodeTag,
                                    string strNodeType,
                                    ParameterList parameterList);

  list<QDomElement *> getAERConnections(QDomDocument *ddocRoot);

  QDomDocument ddocSystem; // necessary for windows

  void addXRefHolders(QDomDocument *ddocRoot, QDomElement *delemParent,
                      ClsItem *itemParent);

  static const bool INCLUDE_XML_DECL_NODE = true;
  static const bool EXCLUDE_XML_DECL_NODE = false;

  bool bXMLPlatformInitialized;

  list<string> lstExternalProcesses;
};

#endif
