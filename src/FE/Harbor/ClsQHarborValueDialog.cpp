/****************************************************************************
** Form implementation generated from reading ui file 'ClsQHarborValueDialog.ui'
**
** Created: Thu Dec 8 11:39:44 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.4   edited Nov
*24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "ClsQHarborValueDialog.h"

#include <qboxlayout.h>
#include <qkeysequence.h>
#include <qlabel.h>
#include <qlayoutitem.h>
#include <qpushbutton.h>
#include <qsize.h>
#include <qsizepolicy.h>
#include <qstring.h>
#include <algorithm>
#include <iostream>

#include "doubleSpinBox.hpp"

class QWidget;

/*
 *  Constructs a ClsQHarborValueDialog as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  true to construct a modal dialog.
 */

ClsQHarborValueDialog::ClsQHarborValueDialog(QWidget *parent, int _iItemType,
                                             string _strItemID, int _iIndex,
                                             string _strParamName,
                                             string _strParamLabel,
                                             double _fMin, double _fMax,
                                             double _fValue)
    : QDialog(parent), iItemType(_iItemType), strItemID(std::move(_strItemID)),
      iIndex(_iIndex), strParamName(std::move(_strParamName)) {

  cout << "ClsQHarborValueDialog: iItemType: " << iItemType << endl;
  cout << "ClsQHarborValueDialog: strItemID: " << strItemID << endl;
  cout << "ClsQHarborValueDialog: strParamName: " << strParamName << endl;
  cout << "ClsQHarborValueDialog: fValue: " << _fValue << endl;

  setWindowTitle(tr("Harbor:change value-"));

  setSizeGripEnabled(false);
  setModal(true);
  ClsQHarborValueDialogLayout = new QVBoxLayout(this);

  layout5 = new QHBoxLayout();

  qlblParamName = new QLabel(this);
  qlblParamName->setText(_strParamLabel.c_str());
  layout5->addWidget(qlblParamName);
  spacer5 =
      new QSpacerItem(71, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  layout5->addItem(spacer5);

  clsDoubleSpinBox =
      new ClsDoubleSpinBox(_fMin,     // double _dMin,
                           _fMax,     // double _dMax,
                           5,         // int _iPrecision = DEFAULT_PRECISION,
                           .01,       // double _dStep = DEFAULT_LINE_STEP,
                           this /*,*/ // QWidget *_pqwgtParent = 0,
                           /*"clsDoubleSpinBox"*/); // const char *pcName = 0);
  clsDoubleSpinBox->setValue(_fValue);

  layout5->addWidget(clsDoubleSpinBox);
  ClsQHarborValueDialogLayout->addLayout(layout5);

  layout6 = new QHBoxLayout(/*ZZZ 0, 0, 6, "layout6"*/);

  buttonCancel = new QPushButton(this /*, "buttonCancel" */);
  buttonCancel->setAutoDefault(true);
  layout6->addWidget(buttonCancel);
  Horizontal_Spacing2 =
      new QSpacerItem(100, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  layout6->addItem(Horizontal_Spacing2);

  buttonOk = new QPushButton(this /*, "buttonOk" */);
  buttonOk->setAutoDefault(true);
  buttonOk->setDefault(true);
  layout6->addWidget(buttonOk);
  ClsQHarborValueDialogLayout->addLayout(layout6);
  languageChange();
  resize(QSize(212, 86).expandedTo(minimumSizeHint()));

  // signals and slots connections
  connect(buttonOk, SIGNAL(clicked()), this, SLOT(slotApply()));
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));

  setTabOrder(buttonOk, buttonCancel);
}

void ClsQHarborValueDialog::slotApply() {
  cout << "ClsQHarborValueDialog::slotApply()" << endl;
  emit sigChangeValue(iItemType, strItemID, iIndex, strParamName,
                      clsDoubleSpinBox->value());
  accept();
}

void ClsQHarborValueDialog::languageChange() {
  buttonCancel->setText(tr("&Cancel"));
  buttonCancel->setShortcut(QKeySequence(QString::null));
  buttonOk->setText(tr("&Apply"));
  buttonOk->setShortcut(QKeySequence(tr("Alt+A")));
}
