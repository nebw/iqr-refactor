#include <qstring.h>

#include "ClsFEParamRelais.h"
#include "ClsFESystemManager.h"
#include "ClsParamTrade.h"
#include "ClsQHarborImpl.h"

class QWidget;

ClsFEParamRelais *ClsFEParamRelais::_instanceParamRelais = nullptr;

void ClsFEParamRelais::initializeParamRelais(QWidget *_parent) {
  _instanceParamRelais = new ClsFEParamRelais(_parent);
}

ClsFEParamRelais *ClsFEParamRelais::Instance() {
  if (_instanceParamRelais == nullptr) {
    _instanceParamRelais = new ClsFEParamRelais();
  }
  return _instanceParamRelais;
}

ClsFEParamRelais::ClsFEParamRelais(QWidget *_parent) : parent(_parent) {
  clsQHarborImpl = nullptr;
}

void ClsFEParamRelais::showHarbor() {
  if (clsQHarborImpl == nullptr) {
    clsQHarborImpl = new ClsQHarborImpl(parent);
#ifndef _WINDOWS //TODO: cannot get this to work under minGW at the moment...
    connect(ClsFESystemManager::Instance(), SIGNAL(sigItemChanged(int, string)),
            clsQHarborImpl, SLOT(slotItemChanged(int, string)));
    connect(ClsFESystemManager::Instance(), SIGNAL(sigItemDeleted(int, string)),
            clsQHarborImpl, SLOT(slotItemDeleted(int, string)));
#endif
    clsQHarborImpl->show();
  } else {
    clsQHarborImpl->show();
  }
}

list<ClsParamTrade> ClsFEParamRelais::getParamTrades() {
  list<ClsParamTrade> lstParamTrades;

  if (clsQHarborImpl != nullptr) {
    return clsQHarborImpl->getParamTrades();
  }

  return lstParamTrades;
}

int ClsFEParamRelais::setParameterByItemID(string strType, string strItemID,
                                           string strParamID, double fValue) {

#ifndef _WINDOWS //TODO: cannot get this to work under minGW at the moment...
  QString qstrValue;
  qstrValue.setNum(fValue);
  if (!strType.compare("Group")) {
    return ClsFESystemManager::Instance()->setParameterByItemID(
        ClsFESystemManager::ITEM_NEURON, strItemID, strParamID,
        qstrValue.toStdString());
  } else if (!strType.compare("Connection")) {
    return ClsFESystemManager::Instance()->setParameterByItemID(
        ClsFESystemManager::ITEM_SYNAPSE, strItemID, strParamID,
        qstrValue.toStdString());
  }
#endif
  return -1;
};

void ClsFEParamRelais::cleanHarbor() {
  if (clsQHarborImpl != nullptr) {
    clsQHarborImpl->clearTable();
  }
};

[[deprecated]]
void ClsFEParamRelais::slotItemDeleted(int /*iType*/, string /*strID*/) {
};

[[deprecated]]
void ClsFEParamRelais::slotItemChanged(int /*iType*/, string /*strID*/) {
};

void ClsFEParamRelais::slotSimulationRunning(bool b) {
  if (clsQHarborImpl != nullptr) {
    clsQHarborImpl->setSimulationRunning(b);
  }
}

void ClsFEParamRelais::applyConfig(string str) {
  showHarbor();
  if (clsQHarborImpl != nullptr) {
    clsQHarborImpl->loadConfig(str);
  }
}
