#ifndef CLSQHARBORIMPL_H
#define CLSQHARBORIMPL_H

#include <qdialog.h>
#include <qfont.h>
#include <qnamespace.h>
#include <qobjectdefs.h>
#include <qstring.h>
#include <qtablewidget.h>
#include <qvariant.h>
//--#include "ClsQHarbor.h"
#include <ui_ClsQHarbor.h>
#include <iostream>
#include <list>
#include <map>
#include <string>

#include "neuronManager.hpp"

class QDragEnterEvent;
class QDropEvent;
class QWidget;
namespace iqrcommon {
class ClsItem;
} // namespace iqrcommon
struct ClsParamTrade;

using namespace std;
using namespace iqrcommon;
using namespace Ui;

#define COL_TYPE 0
#define COL_NAME 1
#define COL_ID 2
#define COL_CHILD 3
#define COL_PARAM_LABEL 4
#define COL_MIN 5
#define COL_MAX 6
#define COL_VALUE 7
#define COL_PARAM_NAME 8

class ClsQHarborImpl : public QDialog, public Ui::ClsQHarbor {
  Q_OBJECT

public:
  ClsQHarborImpl(QWidget *parent = nullptr, const char *name = nullptr,
                 bool modal = false, Qt::WindowFlags fl = nullptr);
  list<ClsParamTrade> getParamTrades();

  void clearTable();
  void setSimulationRunning(bool);
  void loadConfig(string strConfigName);
  void loadParamSet(string strParamSetName);

public
slots:
  void slotItemDeleted(int iType, string strID);
  void slotItemChanged(int iType, string strID);

private
slots:
  void slotDoItem(string, int);
  void slotDoItemCanceled(int);
  void slotDeleteRow();
  void slotSaveConfig();
  void slotLoadConfig();

  void slotSaveParamSet();
  void slotLoadParamSet();

  void slotRefresh();
  void slotChangeValue(int iItemType, string strItemID, int iIndex,
                       string strParamName, double fValue);

  void slotCellDoubleClicked(int row, int col);

private:
  void dragEnterEvent(QDragEnterEvent *event) override;
  void dropEvent(QDropEvent *event) override;

  void doDialog(int iItemType, string strItemID, int iIndex);
  void doValueDialog(int iItemType, string strItemID, int iIndex,
                     string strParamName, string strParamLabel, double fMin,
                     double fMax, double fValue);

  void fillMinMaxValueField(ClsItem *clsItem, string strParamName, int iRow);
  bool checkForItem(string _strItemID, string _strParamName);
  bool setTableItemValue(string _strItemID, string _strParamName,
                         string strValue);

  bool bSimulationRunning;
  ClsItem *clsItem;
  map<string, string> mapParams;

  void addHeaderItem(int iCol, string strLabel, bool bBold) {
    QTableWidgetItem *item = new QTableWidgetItem(strLabel.c_str());
    QFont font = item->font();
    font.setBold(bBold);
    item->setFont(font);
    tableWidget->setHorizontalHeaderItem(iCol, item);
  }

  void addItem(int iRow, int iCol, string strLabel) {
    QTableWidgetItem *newItem = new QTableWidgetItem(strLabel.c_str());
    tableWidget->setItem(iRow, iCol, newItem);
  }

  void changeItem(int iRow, int iCol, string strLabel) {
    QTableWidgetItem *newItem = tableWidget->item(iRow, iCol);
    if (newItem != nullptr) {
      newItem->setText(strLabel.c_str());
    } else {
      cerr << "ITEM NULL" << endl;
    }
  }

  string getItemValue(int iRow, int iCol) {
    QTableWidgetItem *newItem = tableWidget->item(iRow, iCol);
    if (newItem != nullptr) {
      return newItem->text().toStdString();
    }
    return "";
  }

  string getItemData(int iRow) {
    QTableWidgetItem *newItem = tableWidget->item(iRow, COL_PARAM_LABEL);
    if (newItem != nullptr) {
      return newItem->data(Qt::UserRole).toString().toStdString();
    }
    return "";
  }

  void setItemData(int iRow, string strValue) {
    QTableWidgetItem *newItem = tableWidget->item(iRow, COL_PARAM_LABEL);
    if (newItem != nullptr) {
      newItem->setData(Qt::UserRole, strValue.c_str());
    }
  }
};

#endif

//// Local Variables:
//// mode: c++
//// End:
