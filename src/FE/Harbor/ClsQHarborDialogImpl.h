#ifndef CLSQHARBORDIALOGIMPL_H
#define CLSQHARBORDIALOGIMPL_H

#include <qdialog.h>
#include <qnamespace.h>
#include <qobjectdefs.h>
#include <list>
#include <string>

#include "neuronManager.hpp"
#include "ui_ClsQHarborDialog.h"

class QWidget;

using namespace std;
using namespace Ui;

class ClsQHarborDialogImpl : public QDialog, public Ui::ClsQHarborDialog {
  Q_OBJECT

public:
  ClsQHarborDialogImpl(string strItemType, string _strID, string strItemName,
                       string strSubItemName, list<string> lstParams,
                       int _iIndex, QWidget *parent = nullptr,
                       const char *name = nullptr, bool modal = false,
                       Qt::WindowFlags fl = nullptr);

private
slots:
  void slotOK();
  void slotDoItemCanceled();

signals:
  void sigDoItem(string, int);
  void sigDoItemCanceled(int);

private:
  int iItemType;
  string strID;

  string strItemName;
  string strSubItemName;
  int iIndex;
};

#endif
