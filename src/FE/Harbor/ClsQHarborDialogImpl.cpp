#include <qcombobox.h>
#include <qgroupbox.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <iostream>
#include <map>
#include <string>

#include "ClsQHarborDialogImpl.h"

class QWidget;

//#include "moc_ClsQHarborDialogImpl.cxx"

ClsQHarborDialogImpl::ClsQHarborDialogImpl(
    string strItemType, string strID, string strItemName, string strSubItemName,
    list<string> lstParams, int _iIndex, QWidget *parent, const char * /*name*/,
    bool /*modal*/, Qt::WindowFlags /*f1*/)
    : QDialog(parent), iIndex(_iIndex) {

  setupUi(this);

  setAcceptDrops(false);
  connect(pbOK, SIGNAL(clicked()), SLOT(slotOK()));
  connect(pbCancel, SIGNAL(clicked()), SLOT(slotDoItemCanceled()));

  string strTitle = "Add " + strItemType;
  setWindowTitle(QString(strTitle.c_str()));
  lblID->setText(strID.c_str());
  lblName->setText(strItemName.c_str());
  gboxSubtype->setTitle(strSubItemName.c_str());

  for (auto &lstParam : lstParams) {
    qcomboParamList->insertItem(-1, QString((lstParam).c_str()));
  }
}

void ClsQHarborDialogImpl::slotOK() {
  string strParamLabel = qcomboParamList->currentText().toStdString();
  emit sigDoItem(strParamLabel, iIndex);
  close();
}

void ClsQHarborDialogImpl::slotDoItemCanceled() {
  emit sigDoItemCanceled(iIndex);
  close();
}
