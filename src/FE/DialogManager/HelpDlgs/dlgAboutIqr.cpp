#include <qfile.h>
#include <qiodevice.h>
#include <qlabel.h>
#include <qmovie.h>
#include <qstring.h>
#include <qtextedit.h>
#include <stdlib.h>
#include <iostream>
#include <string>

#include "dlgAboutIqr.h"

class QWidget;

dlgAboutIqr::dlgAboutIqr(QWidget *parent) : QDialog(parent) {
  setupUi(this);

#ifdef _WINDOWS
  char *cwd = getcwd(NULL, 0);
  string strAppResDir = cwd + string("/");
#else
  string strAppResDir;
  char *pcIQR_HOME = nullptr;
  pcIQR_HOME = getenv("IQR_HOME");
  if (pcIQR_HOME != nullptr) {
    strAppResDir = string(pcIQR_HOME) + "/";
  } else {
    strAppResDir = "/usr/share/iqr/";
  }
#endif

  QString qstrVersion = "";
  QString qstrFilename = string(strAppResDir + "iqr.version").c_str();

  QFile qfile(qstrFilename);
  if (qfile.exists()) {
    qfile.open(QIODevice::ReadOnly);
    char buf[1024];
    qfile.readLine(buf, sizeof(buf));
    qfile.close();
    qtxteditVersion->setText(buf);
  } else {
    cerr << "iqr.version not found" << endl;
  }

  QString qstrMovieFile = string(strAppResDir + "iqr-logo-lg.gif").c_str();
  qlblMovie->setMovie(new QMovie(qstrMovieFile));

  if (qlblMovie->movie() != nullptr) {
    qlblMovie->movie()->setPaused(false);
  }
}
