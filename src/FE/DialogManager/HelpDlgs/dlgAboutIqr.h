#ifndef DLGABOUTIQR_H
#define DLGABOUTIQR_H

#include <qdialog.h>
#include <qobjectdefs.h>

#include "ui_iqrAbout.h"

class QWidget;

using namespace std;

class dlgAboutIqr : public QDialog, private Ui::dlgAboutIqr {
  Q_OBJECT

public:
  dlgAboutIqr(QWidget *parent = nullptr);
};

#endif
