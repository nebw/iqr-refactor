#ifndef DLGRELEASENOTES_H
#define DLGRELEASENOTES_H

#include <qdialog.h>
#include <qobjectdefs.h>
#include <ui_iqrReleaseNotes.h>

class QWidget;

using namespace std;

class dlgReleaseNotes : public QDialog, public Ui::dlgReleaseNotes {
  Q_OBJECT

public:
  dlgReleaseNotes(QWidget *parent = nullptr);
};

#endif
