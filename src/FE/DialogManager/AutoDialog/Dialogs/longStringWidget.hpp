/****************************************************************************
 ** $Filename: longStringWidget.hpp
 ** $Header$
 **
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Thu Nov 15 13:21:37 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef LONGSTRINGWIDGET_HPP
#define LONGSTRINGWIDGET_HPP

#include <qobjectdefs.h>
#include <string>

#include "parameterWidget.hpp"

class QTextEdit;
class QWidget;

namespace iqrcommon {
class ClsStringParameter;
}

namespace iqrfe {

using iqrcommon::ClsStringParameter;

class ClsLongStringWidget : public iqrfe::ClsParameterWidget {
  Q_OBJECT

public:
  ClsLongStringWidget(ClsStringParameter &_parameter, QWidget *_pqwgtParent,
                      const char *_pcName = nullptr);
  ~ClsLongStringWidget();

  string getValue() const;
  string getValueAsString() const override;
  void setValue(string _strValue);
  void setValueFromString(string _strValue) override;

private
slots:
  void setValueChanged();

private:
  QTextEdit *pqmledValueWidget;
};
};

#endif
