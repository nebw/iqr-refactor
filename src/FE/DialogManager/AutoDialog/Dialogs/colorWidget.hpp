#ifndef COLORWIDGET_HPP
#define COLORWIDGET_HPP

#include <qobjectdefs.h>
#include <string>

#include "parameterWidget.hpp"

class QPushButton;
class QWidget;

namespace iqrcommon {
class ClsColorParameter;
}

namespace iqrfe {
using iqrcommon::ClsColorParameter;

class ClsColorWidget : public iqrfe::ClsParameterWidget {
  Q_OBJECT

public:
  ClsColorWidget(ClsColorParameter &_parameter, QWidget *_pqwgtParent,
                 const char *_pcName = nullptr);
  ~ClsColorWidget();

  string getValue() const;
  string getValueAsString() const override;
  void setValue(string _strValue);
  void setValueFromString(string _strValue) override;

private
slots:
  void setValueChanged();

private:
  QPushButton *qpb;
  QPushButton *qpbColor;

  void setColor(string str);
};
};

#endif
