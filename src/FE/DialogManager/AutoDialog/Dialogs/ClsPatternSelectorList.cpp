/****************************************************************************
 ** $Filename: ClsPatternSelectorList.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Sun Jun  1 00:17:17 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qboxlayout.h>
#include <qlabel.h>
#include <qpushbutton.h>
#include <qsizepolicy.h>
#include <qtextedit.h>
#include <string>

#include "ClsBaseAutoGroup.h"
#include "ClsBaseQStateArrayView.h"
#include "ClsBaseTopology.h"
#include "ClsPatternSelectorList.h"
#include "ClsQStateArrayViewRect.h"
#include "ClsSubPopulations.h"
#include "ClsTopologyHex.h"
#include "ClsTopologyRect.h"
#include "ClsTopologySparse.h"
#include "iqrUtils.h"
#include "tagLibrary.hpp"

using namespace iqrcommon;

ClsPatternSelectorList::ClsPatternSelectorList(
    ClsBaseAutoGroup *changeReceiver, ClsBaseTopology *_clsBaseTopologyGroup,
    const char *name)
    : ClsPatternBaseSelector(changeReceiver, name) {
  strType = ClsTagLibrary::SelectorList();

  clsQStateArrayView = nullptr;
  if (dynamic_cast<ClsTopologyRect *>(_clsBaseTopologyGroup)) {
    clsQStateArrayView = new ClsQStateArrayViewRect(
        this, _clsBaseTopologyGroup, ClsBaseQStateArrayView::LIST);
  } else if (dynamic_cast<ClsTopologyHex *>(_clsBaseTopologyGroup)) {
  } else if (dynamic_cast<ClsTopologySparse *>(_clsBaseTopologyGroup)) {
  }

  if (clsQStateArrayView != nullptr) {
    auto pqlayTop = new QVBoxLayout();
    this->setLayout(pqlayTop);
    pqlayTop->setContentsMargins(0, 0, 0, 0);

    pqlayTop->addWidget(clsQStateArrayView);

    auto pqlayButtons = new QHBoxLayout();
    pqlayTop->addLayout(pqlayButtons);
    pqlayButtons->addStretch(5);

    qbtnAdd = new QPushButton("Set");
    connect(qbtnAdd, SIGNAL(clicked()), this, SLOT(addEntry()));
    pqlayButtons->addWidget(qbtnAdd);

    qbtnClear = new QPushButton("Clear");
    connect(qbtnClear, SIGNAL(clicked()), this, SLOT(clear()));
    pqlayButtons->addWidget(qbtnClear);
    pqlayButtons->addStretch(5);

    /* --- */

    auto qlblStartHeight = new QLabel();
    qlblStartHeight->setText("Selection");
    pqlayTop->addWidget(qlblStartHeight);

    /* --- */

    qtexteditSelection = new QTextEdit();
    pqlayTop->addWidget(qtexteditSelection);
    qtexteditSelection->setSizePolicy(QSizePolicy::Ignored,
                                      QSizePolicy::Maximum);

    pqlayTop->addStretch(5);
  }
};

void ClsPatternSelectorList::addEntry() {
  if (clsQStateArrayView->getSelected().size() > 0) {
    lstSelection = clsQStateArrayView->getSelected();
    string str = "";
    list<pair<int, int> >::iterator it;
    for (it = lstSelection.begin(); it != lstSelection.end(); ++it) {
      str.append("(");
      str.append(iqrUtils::int2string((*it).first));
      str.append(",");
      str.append(iqrUtils::int2string((*it).second));
      str.append(")");
    }
    qtexteditSelection->setText(str.c_str());
  }
  changeReceiver->subItemChanged();
};

void ClsPatternSelectorList::clear() {
  clsQStateArrayView->clear();
  qtexteditSelection->setText("");
  lstSelection.clear();

  changeReceiver->subItemChanged();
};

void ClsPatternSelectorList::setSubPopulation(
    ClsBaseSubPopulation *_clsBaseSubPopulation) {
  if (_clsBaseSubPopulation != nullptr) {
    ClsList *_clsList = static_cast<ClsList *>(_clsBaseSubPopulation);
    lstSelection = _clsList->getData();

    string str = "";
    list<pair<int, int> >::iterator it;
    for (it = lstSelection.begin(); it != lstSelection.end(); ++it) {
      str.append("(");
      str.append(iqrUtils::int2string((*it).first));
      str.append(",");
      str.append(iqrUtils::int2string((*it).second));
      str.append(")");
    }
    qtexteditSelection->setText(str.c_str());

    clsQStateArrayView->setValue(.5, lstSelection);
  }
}

ClsBaseSubPopulation *ClsPatternSelectorList::getSubPopulation() {
  auto clsList = new ClsList();
  clsList->setData(lstSelection);

  return clsList;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k "
//// End:
