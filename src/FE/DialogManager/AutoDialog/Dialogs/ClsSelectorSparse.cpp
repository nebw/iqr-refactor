/****************************************************************************
 ** $Filename: ClsSelectorSparse.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Mon Jul  7 18:21:10 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qabstractitemmodel.h>
#include <qboxlayout.h>
#include <qchar.h>
#include <qitemselectionmodel.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <qstringlist.h>
#include <qtablewidget.h>
#include <qwidget.h>
#include <iostream>
#include <list>
#include <utility>

#include "ClsHyperLists.h"
#include "ClsSelectorSparse.h"
#include "ClsTopologySparse.h"
#include "dialogButtons.hpp"
#include "iqrUtils.h"
#include "memberWidget.h"

class QCloseEvent;

#ifdef DEBUG_CLSAUTODIALOG
static const bool bDebugAutoGroup = true;
#else
static const bool bDebugAutoGroup = false;
#endif

using namespace iqrcommon;

iqrfe::ClsSelectorSparse::ClsSelectorSparse(ClsItem &_item, bool _bModal,
                                            QWidget *_pqwgtParent,
                                            const char *_pcName,
                                            string strValue)
    : ClsBaseAutoGroup(_item, _pqwgtParent, _pcName) {

  cout << "ClsSelectorSparse::ClsSelectorSparse(...)" << endl;

  if (bDebugAutoGroup) {
    cout << "ClsSelectorSparse::ClsSelectorSparse" << endl;
  }

  string strTitle = _pcName;
  strTitle.append(" : ");
  strTitle.append(strValue);

  setTitle(strTitle.c_str());

  auto pqlayTop = new QVBoxLayout(this /*, 14, -1, "auto dialog layout"*/);

  qtablePoints = new QTableWidget(0, 2, this);

  QStringList qstrlstHeaders;
  qstrlstHeaders << "x";
  qstrlstHeaders << "y";
  qtablePoints->setHorizontalHeaderLabels(qstrlstHeaders);

  connect(qtablePoints, SIGNAL(valueChanged(int, int)), this,
          SLOT(tableChanged(int, int)));
  pqlayTop->addWidget(qtablePoints);

  clsTopologySparse = static_cast<ClsTopologySparse *>(&_item);

  auto newItem = new QTableWidgetItem();
  list<pair<int, int> > lst = clsTopologySparse->getList();
  ;
  list<pair<int, int> >::iterator it;
  int iRowCount = 0;
  for (it = lst.begin(); it != lst.end(); ++it) {
    qtablePoints->insertRow(iRowCount);
    int iX = (*it).first;
    int iY = (*it).second;

    newItem->setText(iqrUtils::int2string(iX).c_str());
    qtablePoints->setItem(iRowCount, 0, newItem);

    newItem->setText(iqrUtils::int2string(iY).c_str());
    qtablePoints->setItem(iRowCount, 1, newItem);

    iRowCount++;
  }

  auto pqlayTableControls = new QHBoxLayout(this /*, 1, "selectors"*/);
  pqlayTop->addLayout(pqlayTableControls);

  qbtnAddRow = new QPushButton("add row", this /*, "add"*/);
  connect(qbtnAddRow, SIGNAL(clicked()), this, SLOT(addRow()));
  pqlayTableControls->addWidget(qbtnAddRow);

  qbtnDeleteRow = new QPushButton("delete row(s)", this /*, "delete"*/);
  connect(qbtnDeleteRow, SIGNAL(clicked()), this, SLOT(deleteRow()));
  pqlayTableControls->addWidget(qbtnDeleteRow);

  if (_bModal) {
    pqwgtButtons = new ClsModalButtons(this /*, QCString(_pcName)+" buttons"*/);

    // OK in modal dialog applies changes and closes dialog.
    connect(pqwgtButtons, SIGNAL(ok()), this, SLOT(apply()));
    connect(pqwgtButtons, SIGNAL(ok()), this, SLOT(close()));

    // Cancel closes dialog without applying changes.
    connect(pqwgtButtons, SIGNAL(cancel()), this, SLOT(close()));
  } else {
    pqwgtButtons =
        new ClsModelessButtons(this /*, QCString(_pcName)+" buttons"*/);

    connect(this, SIGNAL(changed()), pqwgtButtons, SLOT(enableApply()));
    connect(pqwgtButtons, SIGNAL(apply()), this, SLOT(apply()));
    //TODO: How should the Undo button be handled?
    connect(pqwgtButtons, SIGNAL(close()), this, SLOT(close()));
  }

  pqlayTop->addWidget(pqwgtButtons, BUTTON_V_STRETCH);
}

iqrfe::ClsSelectorSparse::~ClsSelectorSparse() {
  if (bDebugAutoGroup) {
    cout << "ClsSelectorSparse::~ClsSelectorSparse" << endl;
  }
}

void iqrfe::ClsSelectorSparse::apply() {
  if (bDebugAutoGroup) {
    cout << "ClsSelectorSparse::apply" << endl;
  }

  list<pair<int, int> > lst;
  for (int ii = 0; ii < qtablePoints->rowCount(); ii++) {
    int iX = validateCell(ii, 0, false);
    int iY = validateCell(ii, 1, false);

    if (iX > 0 && iY > 0) {
      pair<int, int> pairPoint(iX, iY);
      lst.push_back(pairPoint);
    }
  }
  clsTopologySparse->setList(lst);
}

void iqrfe::ClsSelectorSparse::closeEvent(QCloseEvent *) {
  cout << "ClsSelectorSparse::closeEvent ( QCloseEvent * e )" << endl;
  emit autoGroupClosed(strName, "", ClsMemberWidget::HIDE);
}

void iqrfe::ClsSelectorSparse::tableChanged(int iR, int iC) {
  validateCell(iR, iC, true);
  emit changed();
}

void iqrfe::ClsSelectorSparse::addRow() {
  qtablePoints->insertRow(qtablePoints->rowCount());
}

void iqrfe::ClsSelectorSparse::deleteRow() {
  for (int ii = 0; ii < qtablePoints->rowCount(); ii++) {
    if (qtablePoints->selectionModel()->isRowSelected(
            ii, QModelIndex())) { // ZZZ THIS IS SICK
      qtablePoints->removeRow(ii);
    }
  }
}

int iqrfe::ClsSelectorSparse::validateCell(int iR, int iC, bool bAllowEmpty) {
  int iReturn = -1;

  QTableWidgetItem *qtwi = qtablePoints->itemAt(iR, iC);
  if (qtwi != nullptr) {
    QString qstr = qtwi->text().trimmed();
    if (!(qstr.length() <= 0)) {
      for (auto &elem : qstr) {
        QChar qc = elem;
        if (!qc.isDigit()) {
          QTableWidgetItem *newItem = new QTableWidgetItem("invalid entry");
          qtablePoints->setItem(iR, iC, newItem);
          return iReturn;
        }
      }
      try {
        iReturn = iqrUtils::string2int(qstr.toStdString());
      }
      catch (...) {
        QTableWidgetItem *newItem = new QTableWidgetItem("invalid entry");
        qtablePoints->setItem(iR, iC, newItem);
      }
    } else {
      if (!bAllowEmpty) {
        QTableWidgetItem *newItem = new QTableWidgetItem("invalid entry");
        qtablePoints->setItem(iR, iC, newItem);
      }
    }
    return iReturn;
  }
  return -1;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j4"
//// End:
