/****************************************************************************
 ** $Filename: xRefWidget.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Dec  5 18:58:12 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qboxlayout.h>
#include <qlabel.h>
#include <qsize.h>
#include <iostream>

#include "neuronManager.hpp"
#include "qstringConversions.hpp"
#include "xRef.hpp"
#include "xRefWidget.hpp"

#ifdef DEBUG_CLSXREFWIDGET
static const bool bDebugXRefWidget = true;
#else
static const bool bDebugXRefWidget = false;
#endif

using namespace std;
using namespace iqrcommon;

iqrfe::ClsXRefWidget::ClsXRefWidget(iqrcommon::ClsXRef *_xRef,
                                    list<string> lstXRefValues,
                                    string strTargetName, QWidget *_pqwgtParent,
                                    const char * /*_pcName*/)
    : QWidget(_pqwgtParent), xRef(_xRef), bValueChanged(false) {
  if (bDebugXRefWidget) {
    cout << "ClsXRefWidget::ClsXRefWidget: " << xRef->getName() << endl;
  }
  auto pqlayLayout = new QHBoxLayout(this);

  auto pqlabWidgetLabel = new QLabel(this);
  pqlabWidgetLabel->setText(string2QString(xRef->getLabel()));

  pqcmbValueWidget = new QComboBox(this);

  if (strTargetName.size() <= 0) {
    pqcmbValueWidget->addItem(QString(""));
  }

  list<string>::iterator it;
  for (it = lstXRefValues.begin(); it != lstXRefValues.end(); ++it) {
    pqcmbValueWidget->addItem(string2QString(*it));
  }

  int iIndex = pqcmbValueWidget->findText(strTargetName.c_str());
  if (iIndex > 0) {
    pqcmbValueWidget->setCurrentIndex(iIndex);
  }

  connect(pqcmbValueWidget, SIGNAL(textChanged(const QString &)), this,
          SLOT(setValueChanged()));
  connect(pqcmbValueWidget, SIGNAL(activated(int)), this,
          SLOT(setValueChanged()));

  pqlayLayout->addWidget(pqlabWidgetLabel);
  pqlayLayout->addWidget(pqcmbValueWidget);

  setFixedHeight(sizeHint().height());
}

iqrfe::ClsXRefWidget::~ClsXRefWidget() {
  if (bDebugXRefWidget) {
    cout << "ClsXRefWidget::~ClsXRefWidget" << endl;
  }
}

void iqrfe::ClsXRefWidget::setValueChanged() {
  if (bDebugXRefWidget) {
    cout << "ClsXRefWidget::setValueChanged()" << endl;
  }
  bValueChanged = true;
  emit changed();
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k"
//// End:
