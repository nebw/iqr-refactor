/****************************************************************************
 ** $Filename: ClsPatternSelectorAll.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jun  6 18:37:35 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qlabel.h>
#include <string>

#include "ClsPatternSelectorAll.h"
#include "ClsSubPopulations.h"
#include "tagLibrary.hpp"

namespace iqrfe {
class ClsBaseAutoGroup;
} // namespace iqrfe

using namespace iqrcommon;

ClsPatternSelectorAll::ClsPatternSelectorAll(ClsBaseAutoGroup *changeReceiver,
                                             const char *name)
    : ClsPatternBaseSelector(changeReceiver, name) {
  strType = ClsTagLibrary::SelectorAll();

  auto qlbl = new QLabel(this);
  qlbl->setText("   All");
};

void ClsPatternSelectorAll::setSubPopulation(
    ClsBaseSubPopulation * /*_clsBaseSubPopulation*/) {}

ClsBaseSubPopulation *ClsPatternSelectorAll::getSubPopulation() {
  auto clsAll = new ClsAll();
  return clsAll;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j2"
//// End:
