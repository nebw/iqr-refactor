/****************************************************************************
 ** $Filename: autoGroupPatternTuples.hpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jun  6 16:18:49 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSAUTOGROUPPATTERNTUPLES_H
#define CLSAUTOGROUPPATTERNTUPLES_H

#include <qobjectdefs.h>
#include <string>

#include "ClsBaseAutoGroup.h"
#include "parameterWidget.hpp"

class QCloseEvent;
class QWidget;

using namespace std;

// class ClsPatternBaseSelector;
class ClsPatternSelectorTuples;
class ClsPatternTuples;

namespace iqrcommon {
class ClsItem;
}

namespace iqrfe {

using iqrcommon::ClsItem;


class ClsAutoGroupPatternTuples : public ClsBaseAutoGroup {
  Q_OBJECT

public:
  ClsAutoGroupPatternTuples(ClsItem &_itemConnection, ClsItem &_item,
                            bool _bModal = true,
                            QWidget *_pqwgtParent = nullptr,
                            const char *_pcName = nullptr,
                            string strValue = nullptr);

  ~ClsAutoGroupPatternTuples();

private
slots:
  void setTuples();
  void apply() override;
  void closeEvent(QCloseEvent *e) override;

signals:

private:
  ClsItem &itemConnection;
  ClsPatternTuples *clsPatternTuples;

  ClsPatternSelectorTuples *clsPatternSelector;
  QWidget *pqwgtButtons;
};
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j8"
//// End:
