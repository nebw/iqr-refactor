/****************************************************************************
 ** $Filename: autoWidget.hpp
 ** $Header$
 **
 ** $Author: Mark Blanchard, Ulysses Bernardet
 **
 ** $CreateDate: Tue Feb  6 00:25:26 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSAUTOWIDGET_HPP
#define CLSAUTOWIDGET_HPP

#include <qobjectdefs.h>
#include <qwidget.h>
#include <list>

#include "parameterWidget.hpp"

namespace iqrfe {
class ClsXRefHolderWidget;
} // namespace iqrfe

namespace iqrcommon {
class ClsItem;
}

namespace iqrfe {

using iqrcommon::ClsItem;

class ClsAutoWidget : public QWidget {
  Q_OBJECT

public:
  ClsAutoWidget(ClsItem &_item, QWidget *_pqwgtParent = nullptr);

  const ParameterWidgetMap &getParameterWidgets() const {
    return mapParameterWidgets;
  }

  const std::list<ClsXRefHolderWidget *> &getXRefHolderWidgets() const {
    return lstXRefHolderWidget;
  }

signals:
  void changed();

protected:
private:
  ParameterWidgetMap mapParameterWidgets;
  std::list<ClsXRefHolderWidget *> lstXRefHolderWidget;
};
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k "
//// End:
