#ifndef PATHWIDGET_HPP
#define PATHWIDGET_HPP

#include <qobjectdefs.h>
#include <string>

#include "parameterWidget.hpp"

class QLineEdit;
class QPushButton;
class QWidget;

namespace iqrcommon {
class ClsPathParameter;
}

namespace iqrfe {
using iqrcommon::ClsPathParameter;

class ClsPathWidget : public iqrfe::ClsParameterWidget {
  Q_OBJECT

public:
  ClsPathWidget(ClsPathParameter &_parameter, QWidget *_pqwgtParent,
                const char *_pcName = nullptr);
  ~ClsPathWidget();

  string getValue() const;
  string getValueAsString() const override;
  void setValue(string _strValue);
  void setValueFromString(string _strValue) override;

private
slots:
  void setValueChanged();

private:
  QLineEdit *pqledValueWidget;
  QPushButton *qpb;
};
};

#endif
