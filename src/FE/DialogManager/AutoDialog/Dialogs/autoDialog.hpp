/****************************************************************************
 ** $Filename: autoDialog.hpp
 ** $Header$
 **
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Tue Feb  6 00:25:26 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSAUTODIALOG_HPP
#define CLSAUTODIALOG_HPP

#include <qobjectdefs.h>
#include <string>

#include "dialog.hpp"

class QWidget;

namespace iqrcommon {
class ClsItem;
}

namespace iqrfe {

using iqrcommon::ClsItem;

class ClsAutoWidget;

class ClsAutoDialog : public iqrfe::ClsDialog {
  Q_OBJECT

public:
  ClsAutoDialog(ClsItem &_item, int _iType, string _strID, bool _bModal = true,
                QWidget *_pqwgtParent = nullptr, const char *_pcName = nullptr);
  ~ClsAutoDialog();

signals:
  void sigApplied(int, string);

private
slots:
  void apply();

private:
  ClsItem &item;
  ClsAutoWidget *pAutoWidget;
};
};

#endif
