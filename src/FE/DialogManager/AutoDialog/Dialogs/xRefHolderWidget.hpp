#ifndef XREFHOLDERWIDGET_HPP
#define XREFHOLDERWIDGET_HPP

#include <qobjectdefs.h>
#include <qscrollarea.h>
#include <list>
#include <string>

class QWidget;
namespace iqrcommon {
class ClsItem;
class ClsXRefHolder;
} // namespace iqrcommon
//#include "xRefWidget.hpp"

using namespace std;
using namespace iqrcommon;

namespace iqrfe {
class ClsXRefWidget;

class ClsXRefHolderWidget : public QScrollArea
{
  Q_OBJECT

public:
  ClsXRefHolderWidget(ClsItem &_item, const ClsXRefHolder *xRefHolder,
                      QWidget *_pqwgtParent, const char *_pcName = nullptr);

  void apply();
signals:
  void changed();

private:
  list<ClsXRefWidget *> lstXRefWidgets;

  int iOwnerType;
  string strOwnerParentID;
  QWidget *qwgtContainer;
};
}

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k"
//// End:
