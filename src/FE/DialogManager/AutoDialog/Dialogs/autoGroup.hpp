/****************************************************************************
 ** $Filename: autoGroup.hpp
 ** $Header$
 **
 ** $Author: Mark Blanchard, Ulysses Bernardet
 **
 ** $CreateDate: Tue Feb  6 00:25:26 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSAUTOGROUP_HPP
#define CLSAUTOGROUP_HPP

#include <qobjectdefs.h>
#include <string>

#include "ClsBaseAutoGroup.h"
#include "parameterWidget.hpp"

class QCloseEvent;
class QWidget;

using namespace std;

namespace iqrcommon {
class ClsItem;
}

namespace iqrfe {

using iqrcommon::ClsItem;


class ClsAutoGroup : public ClsBaseAutoGroup {
  Q_OBJECT

public:
  ClsAutoGroup(ClsItem &_item, bool _bModal = true,
               QWidget *_pqwgtParent = nullptr, const char *_pcName = nullptr,
               string strValue = nullptr);
  ~ClsAutoGroup();

private
slots:
  void closeEvent(QCloseEvent *e) override;

signals:

private:
};
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j8"
//// End:
