/****************************************************************************
 ** $Filename: boolWidget.cpp
 ** $Header$
 **
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Thu Nov 15 13:17:00 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qboxlayout.h>
#include <qcheckbox.h>
#include <qevent.h>
#include <qlabel.h>
#include <qsize.h>
#include <qwidget.h>
#include <iostream>

#include "boolParameter.hpp"
#include "boolWidget.hpp"
#include "qstringConversions.hpp"
#include "stringConversions.hpp"

#ifdef DEBUG_CLSBOOLWIDGET
static const bool bDebugBoolWidget = true;
#else
static const bool bDebugBoolWidget = false;
#endif

using namespace std;
using namespace iqrcommon;

iqrfe::ClsBoolWidget::ClsBoolWidget(ClsBoolParameter &_parameter,
                                    QWidget *_pqwgtParent, const char *_pcName)
    : ClsParameterWidget(_parameter, false, _pqwgtParent, _pcName) {
  // Widget initialized directly from parent parameter.  It is
  // assumed that the data in the parent is valid.
  auto pqlayLayout = new QHBoxLayout(this);

  auto pqlabWidgetLabel = new QLabel(this);
  pqlabWidgetLabel->setText(string2QString(_parameter.getLabel()));

  pqchkValueWidget = new QCheckBox(this);
  pqchkValueWidget->setChecked(_parameter.getValue());

  connect(pqchkValueWidget, SIGNAL(toggled(bool)), this,
          SLOT(setValueChanged()));
  // Emit the Selected signal to enable/disable dependent widgets.
  connect(pqchkValueWidget, SIGNAL(toggled(bool)), this,
          SIGNAL(selected(bool)));

  connect(pqchkValueWidget, SIGNAL(toggled(bool)), this, SIGNAL(changed()));

  //TODO: The magic numbers must be replaced here.
  pqlayLayout->addWidget(pqlabWidgetLabel, 8);
  pqlayLayout->addWidget(pqchkValueWidget, 2);

  pqlabWidgetLabel->setToolTip(_parameter.getDescription().c_str());

  setFixedHeight(sizeHint().height());
}

iqrfe::ClsBoolWidget::~ClsBoolWidget() {
  if (bDebugBoolWidget) {
    cout << "ClsBoolWidget::~ClsBoolWidget" << endl;
  }
}

bool iqrfe::ClsBoolWidget::getValue() const {
  return pqchkValueWidget->isChecked();
}

string iqrfe::ClsBoolWidget::getValueAsString() const {
  return bool2string(pqchkValueWidget->isChecked());
}

void iqrfe::ClsBoolWidget::setValue(bool _bValue) {
  pqchkValueWidget->setChecked(_bValue);
}

void iqrfe::ClsBoolWidget::setValueFromString(string _strValue) {
  pqchkValueWidget->setChecked(string2bool(_strValue));
}

/**
 * Enable/disable this widget.
 *
 * PUBLIC SLOT
 *
 * This function overrides the default function in QWidget.  When a
 * bool widget is disabled, all dependent widgets should also be
 * disabled: this function uses the Selected SIGNAL to achieve this.
 * When the widget is enabled, the current state is emitted.
 *
 * @param _bEnable Enables the widget if true, disables it when false.
 */
void iqrfe::ClsBoolWidget::setEnabled(bool _bEnable) {
  QWidget::setEnabled(_bEnable);

  if (_bEnable) {
    // Emit the widget's current state in order to control
    // dependent widgets correctly.
    emit selected(pqchkValueWidget->isChecked());
  } else {
    // Disable all dependent widgets.  If this widget isn't
    // enabled, they must all be disabled.
    emit selected(false);
  }
}

/**
 * Indicate whether the value in the widget has changed by
 * changing the colors.
 *
 * PRIVATE SLOT
 *
 * This function is called automatically each time the state of the
 * value widget changes.  The value is compared with that in the
 * parent parameter: if the values are different, the colors are
 * changed; the colors revert to the default when the values are
 * equal.
 *
 * The color change is handled by the base class.
 */
void iqrfe::ClsBoolWidget::setValueChanged() {
  bValueChanged = true;
}

void iqrfe::ClsBoolWidget::showEvent(QShowEvent *_e) {
  if (!_e->spontaneous()) {
    if (isEnabled()) {
      // Emit the current state, so that dependent widgets are
      // enabled correctly.
      emit selected(pqchkValueWidget->isChecked());
    } else {
      /// The widget is disabled, disable all dependent widgets.
      emit selected(false);
    }
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j8"
//// End:
