/****************************************************************************
 ** $Filename: ClsSelectorSparse.h
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Mon Jul  7 18:21:03 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSSELECTORSPARSE_H
#define CLSSELECTORSPARSE_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <string>

#include "ClsBaseAutoGroup.h"
#include "parameterWidget.hpp"

class QCloseEvent;
class QPushButton;
class QTableWidget;
class QWidget;

using namespace std;

class ClsTopologySparse;

namespace iqrcommon {
class ClsItem;
}

namespace iqrfe {

using iqrcommon::ClsItem;

class ClsSelectorSparse : public ClsBaseAutoGroup {
  Q_OBJECT

public:
  ClsSelectorSparse(ClsItem &_item, bool _bModal = true,
                    QWidget *_pqwgtParent = nullptr,
                    const char *_pcName = nullptr, string strValue = "");
  ~ClsSelectorSparse();

private
slots:
  void apply() override;
  void tableChanged(int, int);

  void addRow();
  void deleteRow();
  int validateCell(int iR, int iC, bool bAllowEmpty);

  void closeEvent(QCloseEvent *e) override;

signals:

private:
  ClsTopologySparse *clsTopologySparse;

  QTableWidget *qtablePoints;
  QWidget *pqwgtButtons;

  QPushButton *qbtnAddRow;
  QPushButton *qbtnDeleteRow;
};
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j8"
//// End:

#endif /* CLSSELECTORSPARSE_H */
