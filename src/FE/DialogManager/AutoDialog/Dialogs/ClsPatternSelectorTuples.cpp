/****************************************************************************
 ** $Filename: ClsPatternSelectorTuples.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Sun Jun  1 00:17:17 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qboxlayout.h>
#include <qfiledialog.h>
#include <qgroupbox.h>
#include <qlist.h>
#include <qlistwidget.h>
#include <qpushbutton.h>
#include <qstring.h>
#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "ClsBaseAutoGroup.h"
#include "ClsBaseQStateArrayView.h"
#include "ClsBaseTopology.h"
#include "ClsListItems.h"
#include "ClsPatternSelectorTuples.h"
#include "ClsQStateArrayViewRect.h"
#include "ClsTopologyHex.h"
#include "ClsTopologyRect.h"
#include "ClsTopologySparse.h"
#include "ClsTuplesImporter.h"
#include "tagLibrary.hpp"

using namespace iqrcommon;

ClsPatternSelectorTuples::ClsPatternSelectorTuples(
    ClsBaseAutoGroup *changeReceiver, ClsBaseTopology *_clsBaseTopologySource,
    ClsBaseTopology *_clsBaseTopologyTarget, const char *name)
    : ClsPatternBaseSelector(changeReceiver, name) {
  strType = ClsTagLibrary::SelectorTuples();

  auto pqlayTop = new QVBoxLayout();
  this->setLayout(pqlayTop);
  pqlayTop->setContentsMargins(0, 0, 0, 0);

  auto qgroupboxSource = new QGroupBox();
  qgroupboxSource->setTitle("Source");
  clsQStateArrayViewSource = nullptr;
  if (dynamic_cast<ClsTopologyRect *>(_clsBaseTopologySource)) {
    clsQStateArrayViewSource = new ClsQStateArrayViewRect(
        nullptr, _clsBaseTopologySource, ClsBaseQStateArrayView::LIST);
  } else if (dynamic_cast<ClsTopologyHex *>(_clsBaseTopologySource)) {
  } else if (dynamic_cast<ClsTopologySparse *>(_clsBaseTopologySource)) {
  }

  auto qgroupboxTarget = new QGroupBox(this);
  qgroupboxTarget->setTitle("Target");
  clsQStateArrayViewTarget = nullptr;
  if (dynamic_cast<ClsTopologyRect *>(_clsBaseTopologyTarget)) {
    clsQStateArrayViewTarget = new ClsQStateArrayViewRect(
        nullptr, _clsBaseTopologyTarget, ClsBaseQStateArrayView::LIST);
  } else if (dynamic_cast<ClsTopologyHex *>(_clsBaseTopologyTarget)) {
  } else if (dynamic_cast<ClsTopologySparse *>(_clsBaseTopologyTarget)) {
  }

  if (clsQStateArrayViewSource != nullptr &&
      clsQStateArrayViewTarget != nullptr) {
    auto pqlayGrpbxSource = new QHBoxLayout();
    qgroupboxSource->setLayout(pqlayGrpbxSource);
    pqlayGrpbxSource->addWidget(clsQStateArrayViewSource);

    auto pqlayGrpbxTarget = new QHBoxLayout();
    qgroupboxTarget->setLayout(pqlayGrpbxTarget);
    pqlayGrpbxTarget->addWidget(clsQStateArrayViewTarget);

    auto pqlayStateArrays = new QHBoxLayout();
    pqlayTop->addLayout(pqlayStateArrays);
    pqlayStateArrays->addWidget(qgroupboxSource);
    pqlayStateArrays->addWidget(qgroupboxTarget);

    /* --- */

    auto pqlayButtons1 = new QHBoxLayout();
    pqlayTop->addLayout(pqlayButtons1);
    pqlayButtons1->addStretch(5);

    qbtnAdd = new QPushButton("Add");
    connect(qbtnAdd, SIGNAL(clicked()), this, SLOT(addEntry()));
    pqlayButtons1->addWidget(qbtnAdd);

    qbtnClear = new QPushButton("Clear");
    connect(qbtnClear, SIGNAL(clicked()), this, SLOT(clear()));
    pqlayButtons1->addWidget(qbtnClear);

    pqlayButtons1->addStretch(5);
    /* --- */

    qlstBox = new QListWidget();
    connect(qlstBox, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this,
            SLOT(showEntry(QListWidgetItem *)));
    pqlayTop->addWidget(qlstBox);

    /* --- */

    auto pqlayButtons2 = new QHBoxLayout();
    pqlayTop->addLayout(pqlayButtons2);
    pqlayButtons2->addStretch(5);

    qbtnImport = new QPushButton("Import");
    connect(qbtnImport, SIGNAL(clicked()), this, SLOT(import()));
    pqlayButtons2->addWidget(qbtnImport);

    qbtnRemove = new QPushButton("Remove");
    connect(qbtnRemove, SIGNAL(clicked()), this, SLOT(removeEntry()));
    pqlayButtons2->addWidget(qbtnRemove);

    qbtnShow = new QPushButton("Show");
    connect(qbtnShow, SIGNAL(clicked()), this, SLOT(showEntry()));
    pqlayButtons2->addWidget(qbtnShow);

    pqlayButtons2->addStretch(5);
  }
};

void ClsPatternSelectorTuples::addEntry() {
  list<pair<int, int> > lstSource = clsQStateArrayViewSource->getSelected();
  list<pair<int, int> > lstTarget = clsQStateArrayViewTarget->getSelected();

  if (lstSource.size() > 0 && lstTarget.size() > 0) {
    new ClsListItemTuples(qlstBox, lstSource, lstTarget);
  } else {
    cout << "LISTS ARE 0" << endl;
  }

  changeReceiver->subItemChanged();
};

void ClsPatternSelectorTuples::removeEntry() {
  int iCount = qlstBox->count();
  for (int ii = (iCount - 1); ii >= 0; ii--) {
    QListWidgetItem *item = qlstBox->item(ii);
    if (item != nullptr) {
      if (item->isSelected()) {
        qlstBox->takeItem(ii);
      }
    }
  }
  changeReceiver->subItemChanged();
};

void ClsPatternSelectorTuples::showEntry() {
  clear();
  QList<QListWidgetItem *> ql = qlstBox->selectedItems();
  QList<QListWidgetItem *>::iterator it;
  for (it = ql.begin(); it != ql.end(); ++it) {
    ClsListItemTuples *clsListItemList = dynamic_cast<ClsListItemTuples *>(*it);
    if (clsListItemList != nullptr) {
      clsQStateArrayViewSource->setValue(.5, clsListItemList->getListSource());
      clsQStateArrayViewTarget->setValue(.5, clsListItemList->getListTarget());
    }
  }
};

void ClsPatternSelectorTuples::showEntry(QListWidgetItem *item) {
  clear();
  ClsListItemTuples *clsListItemList = dynamic_cast<ClsListItemTuples *>(item);
  if (clsListItemList != nullptr) {
    clsQStateArrayViewSource->setValue(.5, clsListItemList->getListSource());
    clsQStateArrayViewTarget->setValue(.5, clsListItemList->getListTarget());
  }
}

void ClsPatternSelectorTuples::clear() {
  clsQStateArrayViewSource->clear();
  clsQStateArrayViewTarget->clear();
}

void ClsPatternSelectorTuples::setSubPopulation(ClsTuples *_clsTuples) {
  if (_clsTuples != nullptr) {
    list<pair<tListOfPairs, tListOfPairs> > lstTuples = _clsTuples->getData();
    list<pair<tListOfPairs, tListOfPairs> >::iterator it;

    for (it = lstTuples.begin(); it != lstTuples.end(); ++it) {
      new ClsListItemTuples(qlstBox, (*it).first, (*it).second);
    }
  }
}

ClsTuples *ClsPatternSelectorTuples::getSubPopulation() {
  auto clsTuples = new ClsTuples();

  for (int ii = 0; ii < qlstBox->count(); ii++) {
    ClsListItemTuples *clsListItemTuples =
        static_cast<ClsListItemTuples *>(qlstBox->item(ii));
    tListOfPairs lstSource = clsListItemTuples->getListSource();
    tListOfPairs lstTarget = clsListItemTuples->getListTarget();
    clsTuples->addData(lstSource, lstTarget);
  }

  return clsTuples;
}

void ClsPatternSelectorTuples::import() {
  QString qstrFileName = QFileDialog::getOpenFileName(this, "Open File"
                                                            "Choose a file",
                                                      "", "*");

  if (qstrFileName.length() > 0) {
    string strFileName = qstrFileName.toStdString();
    list<pair<tListOfPairs, tListOfPairs> > lstTuples =
        ClsTuplesImporter::parseFile(strFileName);
    list<pair<tListOfPairs, tListOfPairs> >::iterator it;

    for (it = lstTuples.begin(); it != lstTuples.end(); ++it) {
      new ClsListItemTuples(qlstBox, (*it).first, (*it).second);
    }
    changeReceiver->subItemChanged();
  }
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k "
//// End:
