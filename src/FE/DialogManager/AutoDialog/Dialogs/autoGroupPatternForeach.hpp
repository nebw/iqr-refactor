/****************************************************************************
 ** $Filename: autoGroupPatternForeach.hpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri May 30 19:10:29 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSAUTOGROUPPATTERNFOREACH_H
#define CLSAUTOGROUPPATTERNFOREACH_H

#include <qobjectdefs.h>
#include <qstring.h>
#include <map>
#include <string>

#include "ClsBaseAutoGroup.h"
#include "parameterWidget.hpp"

class QCloseEvent;
class QComboBox;
class QGroupBox;
class QStackedWidget;
class QVBoxLayout;
class QWidget;

using namespace std;

class ClsBaseTopology;
class ClsPatternForeach;

namespace iqrcommon {
class ClsItem;
}

namespace iqrfe {

using iqrcommon::ClsItem;


class ClsAutoGroupPatternForeach : public ClsBaseAutoGroup {
  Q_OBJECT

public:
  ClsAutoGroupPatternForeach(ClsItem &_itemConnection, ClsItem &_item,
                             bool _bModal = true,
                             QWidget *_pqwgtParent = nullptr,
                             const char *_pcName = nullptr,
                             string strValue = nullptr, bool bShowList = true);
  ~ClsAutoGroupPatternForeach();

private
slots:
  void apply() override;
  void closeEvent(QCloseEvent *e) override;
  void sourceTypeChanged(const QString &);
  void targetTypeChanged(const QString &);

private:
  ClsItem &itemConnection;

  ClsPatternForeach *clsPatternForeach;

  void setSourceSelector(string strType);
  void setTargetSelector(string strType);

  void setSelector(string strType, map<string, int> &_map, QStackedWidget *qws,
                   ClsBaseTopology *clsBaseTopology);

  QWidget *pqwgtButtons;

  QGroupBox *qgrpxSource;
  QGroupBox *qgrpxTarget;

  QComboBox *qcomboSourcePopulation;
  QComboBox *qcomboTargetPopulation;

  map<string, int> mapSelectorSourceStack;
  map<string, int> mapSelectorTargetStack;

  QStackedWidget *qwidgetstackSourceSelectors;
  QStackedWidget *qwidgetstackTargetSelectors;

  QVBoxLayout *qlayoutSource;
  QVBoxLayout *qlayoutTarget;
};
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j2"
//// End:
