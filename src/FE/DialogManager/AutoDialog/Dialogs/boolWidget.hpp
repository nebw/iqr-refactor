/****************************************************************************
 ** $Filename: boolWidget.hpp
 ** $Header$
 **
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Thu Nov 15 13:13:20 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef BOOLWIDGET_HPP
#define BOOLWIDGET_HPP

#include <qobjectdefs.h>
#include <string>

#include "parameterWidget.hpp"

class QCheckBox;
class QShowEvent;
class QWidget;

namespace iqrcommon {
class ClsBoolParameter;
}

namespace iqrfe {

using iqrcommon::ClsBoolParameter;

class ClsBoolWidget : public iqrfe::ClsParameterWidget {
  Q_OBJECT

public:
  ClsBoolWidget(ClsBoolParameter &_parameter, QWidget *_pqwgtParent,
                const char *_pcName = nullptr);
  ~ClsBoolWidget();

  bool getValue() const;
  string getValueAsString() const override;

  void setValue(bool _bValue);
  void setValueFromString(string _strValue) override;

signals:
  void selected(bool _bIsSelected);

public
slots:
  // Override QWidget::setEnabled so that the current state of
  // this widget can be emitted correctly.
  void setEnabled(bool _bEnable);

protected:
  void showEvent(QShowEvent *_e) override;

private
slots:
  void setValueChanged();

private:
  QCheckBox *pqchkValueWidget;
};
};

#endif
