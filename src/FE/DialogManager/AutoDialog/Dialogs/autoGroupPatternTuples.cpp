/****************************************************************************
 ** $Filename: autoGroupPatternTuples.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Jun  6 16:18:12 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <qboxlayout.h>
#include <qstring.h>
#include <qwidget.h>
#include <iostream>

#include "ClsFEConnection.h"
#include "ClsFEGroup.h"
#include "ClsFESystemManager.h"
#include "ClsPatternSelectorTuples.h"
#include "ClsPatterns.h"
#include "autoGroupPatternTuples.hpp"
#include "dialogButtons.hpp"
#include "memberWidget.h"

class ClsTuples;
class QCloseEvent;

//#define DEBUG_CLSAUTODIALOG

#ifdef DEBUG_CLSAUTODIALOG
static const bool bDebugAutoGroup = true;
#else
static const bool bDebugAutoGroup = false;
#endif

using namespace iqrcommon;

iqrfe::ClsAutoGroupPatternTuples::ClsAutoGroupPatternTuples(
    ClsItem &_itemConnection, ClsItem &_item, bool _bModal,
    QWidget *_pqwgtParent, const char *_pcName, string strValue)
    : ClsBaseAutoGroup(_item, _pqwgtParent, _pcName),
      itemConnection(_itemConnection) {
  if (bDebugAutoGroup) {
    cout << "ClsAutoGroupPatternTuples::ClsAutoGroupPatternTuples" << endl;
  }

  clsPatternTuples = static_cast<ClsPatternTuples *>(&_item);

  clsPatternSelector = nullptr;

  string strTitle = _pcName;
  strTitle.append(" : ");
  strTitle.append(strValue);

  setTitle(strTitle.c_str());

  // Create top-level layout manager for dialog.
  auto pqlayTop = new QVBoxLayout(this);

  QString qstrButtonName(tr("%1 %2").arg(_pcName).arg("buttons"));

  string strSourceGroupID =
      static_cast<ClsFEConnection *>(&itemConnection)->getConnectionSourceID();
  string strTargetGroupID =
      static_cast<ClsFEConnection *>(&itemConnection)->getConnectionTargetID();

  ClsBaseTopology *clsBaseTopologySource = ClsFESystemManager::Instance()
                                               ->getFEGroup(strSourceGroupID)
                                               ->getTopology();
  ClsBaseTopology *clsBaseTopologyTarget = ClsFESystemManager::Instance()
                                               ->getFEGroup(strTargetGroupID)
                                               ->getTopology();

  clsPatternSelector = new ClsPatternSelectorTuples(
      this, clsBaseTopologySource, clsBaseTopologyTarget, "source");
  pqlayTop->addWidget(clsPatternSelector);

  setTuples();

  if (_bModal) {
    pqwgtButtons = new ClsModalButtons(this);

    // OK in modal dialog applies changes and closes dialog.
    connect(pqwgtButtons, SIGNAL(ok()), this, SLOT(apply()));
    connect(pqwgtButtons, SIGNAL(ok()), this, SLOT(close()));

    // Cancel closes dialog without applying changes.
    connect(pqwgtButtons, SIGNAL(cancel()), this, SLOT(close()));
  } else {
    pqwgtButtons = new ClsModelessButtons(this);

    connect(this, SIGNAL(sigSubItemChanged()), pqwgtButtons,
            SLOT(enableApply()));

    connect(pqwgtButtons, SIGNAL(apply()), this, SLOT(apply()));

    //TODO: How should the Undo button be handled?
    connect(pqwgtButtons, SIGNAL(close()), this, SLOT(close()));
  }
  pqlayTop->addWidget(pqwgtButtons /*, BUTTON_V_STRETCH*/);
}

iqrfe::ClsAutoGroupPatternTuples::~ClsAutoGroupPatternTuples() {
  if (bDebugAutoGroup) {
    cout << "ClsAutoGroupPatternTuples::~ClsAutoGroupPatternTuples" << endl;
  }
}

void iqrfe::ClsAutoGroupPatternTuples::setTuples() {
  ClsTuples *clsTuples = clsPatternTuples->getTuples();
  if (clsTuples != nullptr) {
    clsPatternSelector->setSubPopulation(clsTuples);
  }
}

void iqrfe::ClsAutoGroupPatternTuples::apply() {
  if (bDebugAutoGroup) {
    cout << "ClsAutoGroupPatternTuples::apply" << endl;
  }

  clsPatternTuples->setTuples(clsPatternSelector->getSubPopulation());
  emit changed();
}

void iqrfe::ClsAutoGroupPatternTuples::closeEvent(QCloseEvent *) {
  emit autoGroupClosed(strName, "", ClsMemberWidget::HIDE);
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j2"
//// End:
