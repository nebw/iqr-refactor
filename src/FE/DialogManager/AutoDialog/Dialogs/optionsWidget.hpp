/****************************************************************************
 ** $Filename: optionsWidget.hpp
 ** $Header$
 **
 ** $Author: Mark Blanchard
 **
 ** $CreateDate: Thu Nov 15 13:40:21 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef OPTIONSWIDGET_HPP
#define OPTIONSWIDGET_HPP

// #include <qlabel.h>
// #include <qpushbutton.h>
// #include <qstringlist.h>

#include <qobjectdefs.h>
#include <string>

#include "parameterWidget.hpp"

class QComboBox;
class QWidget;

namespace iqrcommon {
class ClsOptionsParameter;
}

namespace iqrfe {

using iqrcommon::ClsOptionsParameter;

class ClsOptionsWidget : public iqrfe::ClsParameterWidget {
  Q_OBJECT

public:
  ClsOptionsWidget(ClsOptionsParameter &_parameter, QWidget *_pqwgtParent,
                   const char *_pcName = nullptr);
  ~ClsOptionsWidget();

  string getValue() const;
  string getValueAsString() const override;
  void setValue(int _iSelected);
  void setValueFromString(string _strValue) override;

private
slots:
  void setValueChanged();

private:
  QComboBox *pqcmbValueWidget;
};
};

#endif
