/****************************************************************************
 ** $Filename: xRefWidget.hpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Dec  5 18:58:29 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSXREFWIDGET_HPP
#define CLSXREFWIDGET_HPP

#include <qcombobox.h>
#include <qframe.h>
#include <qobjectdefs.h>
#include <qstring.h>
#include <qwidget.h>
#include <list>
#include <string>

namespace iqrcommon {
class ClsXRef;
} // namespace iqrcommon

using namespace std;

namespace iqrfe {

class ClsXRefWidget : public QWidget {
  Q_OBJECT

public:
  ClsXRefWidget(iqrcommon::ClsXRef *_xRef, list<string> lstXRefValues,
                string strTargetName, QWidget *_pqwgtParent,
                const char *_pcName = nullptr);
  ~ClsXRefWidget();

  bool valueHasChanged() const { return bValueChanged; }

  const string getValue() const {
    return pqcmbValueWidget->currentText().toStdString();
  };

private
slots:
  void setValueChanged();

signals:
  void changed();

private:
  QComboBox *pqcmbValueWidget;
  iqrcommon::ClsXRef *xRef;

  bool bValueChanged;
  bool bStretchable;

  // Constants which define the widgets' look & feel.
  static const int SMALL_ATOM_HEIGHT = 14;
  static const int DEFAULT_BORDER = 0;
  static const int DEFAULT_FRAME_STYLE = QFrame::NoFrame;

  static const int SMALL_HSPACE = 10;
  static const int SMALL_VSPACE = 8;

  friend class ClsXRefHolderWidget;
};
};

#endif
//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j8"
//// End:
