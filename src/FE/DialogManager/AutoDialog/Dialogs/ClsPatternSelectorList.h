/****************************************************************************
 ** $Filename: ClsPatternSelectorList.h
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Sat May 31 01:08:26 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSPATTERNSELECTORLIST_H
#define CLSPATTERNSELECTORLIST_H

#include <qobjectdefs.h>
#include <list>
#include <utility>

#include "ClsPatternBaseSelector.h"

class ClsBaseQStateArrayView;
class ClsBaseSubPopulation;
class ClsBaseTopology;
class QPushButton;
class QTextEdit;
namespace iqrfe {
class ClsBaseAutoGroup;
} // namespace iqrfe

using namespace std;

class ClsPatternSelectorList : public ClsPatternBaseSelector {
  Q_OBJECT
public:
  ClsPatternSelectorList(ClsBaseAutoGroup *changeReceiver,
                         ClsBaseTopology *_clsBaseTopologyGroup,
                         const char *name);

  void setSubPopulation(ClsBaseSubPopulation *_clsBaseSubPopulation) override;
  ClsBaseSubPopulation *getSubPopulation() override;

private:
  ClsBaseQStateArrayView *clsQStateArrayView;

  QPushButton *qbtnAdd;
  QPushButton *qbtnClear;

  QTextEdit *qtexteditSelection;

  list<pair<int, int> > lstSelection;

private
slots:
  void addEntry();
  void clear();
};

#endif /* CLSPATTERNSELECTORLIST_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k -j2"
//// End:
