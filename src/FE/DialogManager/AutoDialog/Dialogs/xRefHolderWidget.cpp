#include <qboxlayout.h>
#include <qwidget.h>
#include <iostream>

#include "ClsFESystemManager.h"
#include "item.hpp"
#include "neuronManager.hpp"
#include "xRef.hpp"
#include "xRefHolder.hpp"
#include "xRefHolderWidget.hpp"
#include "xRefList.hpp"
#include "xRefWidget.hpp"

#define NO_STRETCH 0
#define WIDGET_STRETCH 10
#define TAB_V_STRETCH 10

#define TAB_BORDER 5

#ifdef DEBUG_CLSXREFHOLDERWIDGET
static const bool bDebugXRefHolderWidget = true;
#else
static const bool bDebugXRefHolderWidget = false;
#endif

#include "module.hpp"
#include "synapse.hpp"

iqrfe::ClsXRefHolderWidget::ClsXRefHolderWidget(ClsItem &_item,
                                                const ClsXRefHolder *xRefHolder,
                                                QWidget *_pqwgtParent,
                                                const char * /*_pcName*/)
    : QScrollArea(_pqwgtParent) {

  iOwnerType = -1;
  strOwnerParentID = "";

  qwgtContainer = new QWidget();

  if (dynamic_cast<ClsSynapse *>(&_item)) {
    if (bDebugXRefHolderWidget) {
      cout << "got Synapse" << endl;
    }
    iOwnerType = ClsFESystemManager::ITEM_CONNECTION;
    strOwnerParentID = dynamic_cast<ClsSynapse *>(&_item)->getConnectionID();
  } else if (dynamic_cast<ClsModule *>(&_item)) {
    if (bDebugXRefHolderWidget) {
      cout << "got Module" << endl;
    }
    iOwnerType = ClsFESystemManager::ITEM_MODULE;
    strOwnerParentID = dynamic_cast<ClsModule *>(&_item)->getProcessID();
  }

  if (bDebugXRefHolderWidget) {
    cout << "strOwnerParentID: " << strOwnerParentID << endl;
  }
  if (strOwnerParentID.length() > 0) {
    list<string> lstXRefValues =
        ClsFESystemManager::Instance()->getXRefValueList(iOwnerType,
                                                         strOwnerParentID);
    if (bDebugXRefHolderWidget) {
      cout << "lstXRefValues.size(): " << lstXRefValues.size() << endl;
    }
    QVBoxLayout *pqlayTabLayout;
    pqlayTabLayout = new QVBoxLayout(qwgtContainer /*, TAB_BORDER*/);
    ClsXRefWidget *clsXRefWidget;
    XRefList xRefList = xRefHolder->getListXRefs();

    if (bDebugXRefHolderWidget) {
      cout << "xRefHolder->getName(): " << xRefHolder->getName() << endl;
      cout << "xRefList.size(): " << xRefList.size() << endl;
    }

    XRefList::iterator itXRef;
    for (itXRef = xRefList.begin(); itXRef != xRefList.end(); ++itXRef) {
      ClsXRef *pXRef = (*itXRef);
      string strXRefTargetName =
          ClsFESystemManager::Instance()->getXRefTargetNameByID(
              iOwnerType, strOwnerParentID, pXRef->getTarget());
      clsXRefWidget =
          new ClsXRefWidget(pXRef, lstXRefValues, strXRefTargetName, this);
      lstXRefWidgets.push_back(clsXRefWidget);
      connect(clsXRefWidget, SIGNAL(changed()), this, SIGNAL(changed()));
      pqlayTabLayout->addWidget(clsXRefWidget); //, WIDGET_STRETCH);
    }
    pqlayTabLayout->addStretch(TAB_V_STRETCH);
  }
  setWidget(qwgtContainer);
  setWidgetResizable(true);
}

void iqrfe::ClsXRefHolderWidget::apply() {
  if (bDebugXRefHolderWidget) {
    cout << "ClsXRefHolderWidget::apply()" << endl;
  }

  list<ClsXRefWidget *>::iterator it;
  for (it = lstXRefWidgets.begin(); it != lstXRefWidgets.end(); ++it) {
    if ((*it)->valueHasChanged()) {
      string strXRefTargetName = (*it)->getValue();
      string strXRefTargetID =
          ClsFESystemManager::Instance()->getXRefTargetIDByName(
              iOwnerType, strOwnerParentID, strXRefTargetName);
      (*it)->xRef->setTarget(strXRefTargetID);
    }
  }
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../ && make -k "
//// End:
