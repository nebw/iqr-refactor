#include <algorithm>

#include "dialog.hpp"

class QWidget;

iqrfe::ClsDialog::ClsDialog(QWidget *_pqwgtParent, const char *_pcName,
                            int _iType, string _strID, bool _bModal,
                            Qt::WidgetAttribute)
    : QDialog(_pqwgtParent, Qt::Dialog), iType(_iType),
      strID(std::move(_strID)) {
  setModal(_bModal);
  setWindowTitle(_pcName);
}

int iqrfe::ClsDialog::getType() { return iType; }

string iqrfe::ClsDialog::getID() { return strID; }
