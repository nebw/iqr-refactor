#ifndef CLSQINFOTIP_H
#define CLSQINFOTIP_H

#include <string>
#include <iostream>

#include <qlabel.h>
#include <qtooltip.h>

using namespace std;

#define TIPOFFSET 20

class ClsQInfoTip : public QLabel {

public:
  ClsQInfoTip(QPoint qpLocation, string strInfo)
      : QLabel(nullptr,
               Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint | Qt::Tool) {
    setPalette(QToolTip::palette());
    setMargin(4);
    setIndent(0);
    setFrameStyle(QFrame::Plain | QFrame::Box);
    setLineWidth(1);
    setAlignment(Qt::AlignLeft | Qt::AlignTop);
    setText(strInfo.c_str());
    adjustSize();
    move(qpLocation.x() - TIPOFFSET, qpLocation.y() - TIPOFFSET);
    show();
  }

private:
  void leaveEvent(QEvent *) override {
    hide();
    close();
  }
};

#endif

//// Local Variables:
//// mode: c++
//// End:
