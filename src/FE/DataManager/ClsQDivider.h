#ifndef CLSQDIVIDER_H
#define CLSQDIVIDER_H

#include <qobjectdefs.h>
#include <qpushbutton.h>

class QWidget;

class ClsQDivider : public QPushButton {
  Q_OBJECT

public:
  ClsQDivider(QWidget *parent);

private
slots:
  void slotMove();

signals:
  void clicked(bool bIsRight);

private:
  bool bIsRight;
};

#endif

//// Local Variables:
//// mode: c++
//// End:
