#include "wRasterPlot.h"

#include <qdom.h>
#include <qpainter.h>
#include <qvector.h>

#include "wGraphLegend.h"
#include "wRange.h"

class QWidget;

WRasterPlot::WRasterPlot(QWidget *parent, QWidget *legendParent,
                         WGraphXAxis::Position xAxisPos,
                         WGraphYAxis::Position yAxisPos, int border,
                         bool zoomButton)
    : WGraphPlot(parent, legendParent, xAxisPos, yAxisPos, border, zoomButton),
      yData(nullptr), cursorPos(0), wrapAround(true) {

  connect(this, SIGNAL(aboutToDraw()), SLOT(checkRange()));
}

void WRasterPlot::writeXML(QDomElement &elem) {
  WGraphPlot::writeXML(elem);
  elem.setAttribute("wrapAround", wrapAround ? "true" : "false");
}

void WRasterPlot::readXML(QDomElement &elem) {
  WGraphPlot::readXML(elem);
  wrapAround = elem.attribute("wrapAround", "true") == "true";
}

WGraphMatrix *WRasterPlot::addMatrix(const QString &name, QPen pen) {
  if (yData != nullptr)
    delete yData;
  yData = new WGraphMatrix(legend, name, pen, xData.count(), 1);
  connect(yData, SIGNAL(needRefresh()), this, SLOT(doRedraw()));
  return yData;
}

void WRasterPlot::removeTraces() {
  if (yData != nullptr) {
    disconnect(yData, SIGNAL(needRefresh()), this, SLOT(doRedraw()));
    delete yData;
    yData = nullptr;
  }
  if (legend)
    legend->toggleExclusive(false);
}

QString WRasterPlot::getVisibleTracesStr(int maxChars) const {
  QString str;

  if (yData != nullptr) {
    str = yData->getName();
    if (str.size() > maxChars) {
      str.truncate(maxChars - 3);
      str.append("...");
    }
  }

  return str;
}

void WRasterPlot::setTraceLength(unsigned int length) {
  if (yData != nullptr) {
    yData->resize(length);
  }
  WGraphPlot::setTraceLength(length);
  cursorPos %= length;
}

int WRasterPlot::incCursorPos() {
  if (xData.count() > 0) {
    cursorPos = (cursorPos + 1) % xData.count();
  } else {
    cursorPos = 0;
  }
  return cursorPos;
}

void WRasterPlot::setCurrentValues(std::valarray<double> values) {
  if (yData != nullptr) {
    if (yData->getData()[cursorPos].size() != values.size())
      yData->getData()[cursorPos].resize(values.size());
    yData->getData()[cursorPos] = values;
  }
}

void WRasterPlot::setCurrentValues(iqrcommon::StateArray *values) {
  if (yData != nullptr) {
    if (yData->getData()[cursorPos].size() != values->getWidth())
      yData->getData()[cursorPos].resize(values->getWidth());
    yData->getData()[cursorPos] = ((*values)[0]);
  }
}

void WRasterPlot::checkRange() {

  if (yData != nullptr) {
    WRange range;
    range.resetForRangeCheck();
    if (yData->isVisible()) {
      range |= yData->checkRange();
    }

    range.expand(0.1);
    if (!getYDataRange().comp(range, 0.05) && !range.isEmpty()) {
      setYDataRange(range);
    }
  }
}

void WRasterPlot::setYDataRange(WRange r) { WGraphPlot::setYDataRange(r); }

void WRasterPlot::setYDataWidth(unsigned int w) {
  if (yData != nullptr) {
    yData->resize(xData.count(), w ? w : 1);
  }
  if (getYDataRange().max() < w * 1.05) {
    WRange r(1.0, w);
    if (w == 1) {
      r.adjust(0.5);
      r.adjust(1.5);
    }
    r.expand(0.1);
    setYDataRange(r);
  }
}

void WRasterPlot::drawContent(QPainter &p, bool completeRedraw) {

  completeRedraw = true;
  p.eraseRect(p.window());

  if (yData != nullptr && yData->isVisible() && xData.count() > 0) {

    int pos = 0;
    float *x = xData.data();
    p.setPen(yData->getPen());

    if (wrapAround) {
      for (int i = 1; i < traceLength(); i++) {
        for (unsigned int n = 0; n < yData->getData()[i].size(); n++) {
          if (yData->getData()[i][n] > 0.0) {
            p.drawPoint(mapToViewCoords(x[i], n + 1));
          }
        }
      }
    } else {
      for (int i = 1; i < traceLength(); i++) {
        pos = (i + cursorPos + 1) % traceLength();
        for (unsigned int n = 0; n < yData->getData()[i].size(); n++) {
          if (yData->getData()[pos][n] > 0.0) {
            p.drawPoint(mapToViewCoords(x[i], n + 1));
          }
        }
      }
    }
  }
}
