#ifndef WRANGEEDIT_HPP
#define WRANGEEDIT_HPP

#include <qlineedit.h>
#include <qobjectdefs.h>
#include <qstring.h>
#include <qvalidator.h>
#include <qwidget.h>

#include "wRange.h"

class WDoubleValidator : public QDoubleValidator {
public:
  WDoubleValidator(QWidget *parent) : QDoubleValidator(parent) {}

  void fixup(QString &input) const override {
    if (input.toFloat() < bottom())
      input.setNum(bottom());
    if (input.toFloat() > top())
      input.setNum(top());
  }
};

class WDoubleEdit : public QLineEdit {
  Q_OBJECT
public:
  WDoubleEdit(QWidget *parent) : QLineEdit(parent) {
    setValidator(valid = new WDoubleValidator(this));
    setFixedWidth(60);
  }
  ~WDoubleEdit() {
    // QLineEdit does not take ownership of the validator
    delete (valid);
  }

  void setRange(float min, float max) { valid->setRange(min, max, 5); }
  void setRange(WRange r) { valid->setRange(r.min(), r.max(), 5); }

  virtual void setValue(float v) {
    setText(QString::number(v, 'g', 5));
    emit valueChanged(v);
  }

  float getValue() { return text().toFloat(); }

signals:

  void valueChanged(float);

protected:
  WDoubleValidator *valid;
};

class WRangeEdit : public QWidget /*, public QWidgetAction*/ {
  Q_OBJECT
public:
  WRangeEdit(QWidget *parent);

  void setEnvelope(WRange e) {
    envelope = e;
    setRange(getRange());
  }
  WRange getRange();

  void clear() {
    minValue->clear();
    maxValue->clear();
  }

signals:
  void rangeChanged(WRange);

public
slots:

  void catchReturnPressed();
  void setRange(WRange);

protected:
  WRange envelope;
  WDoubleEdit *minValue, *maxValue;
};

#endif // WRANGEEDIT_HPP
