#ifndef WBARPLOT_H
#define WBARPLOT_H

#include <qobjectdefs.h>

#include "wGraphAxis.h"
#include "wGraphPlot.h"

class QDomElement;
class QPainter;
class QWidget;
class WRange;

/*! The WBarPlot class inherits WGraphPlot and draws the traces as
  bars.*/
class WBarPlot : public WGraphPlot {
  Q_OBJECT

public:
  /*! Constructs a WBarPlot and passes all the arguments to WGraphPlot.*/
  WBarPlot(QWidget *, QWidget * = nullptr,
           WGraphXAxis::Position = WGraphXAxis::bottom,
           WGraphYAxis::Position = WGraphYAxis::left, int = 0,
           bool zoomButton = true);
  ~WBarPlot();

  virtual void writeXML(QDomElement &) override;
  virtual void readXML(QDomElement &) override;

  /*! Increases the current position of the cursor by one, or cycles
    back to zero, if it was bigger then the length of the traces set
    by setTraceLength(). The new cursor position is returned.*/
  int incCursorPos();
  /*! Returns the current position of the cursor.*/
  int getCursorPos() const {
    return cursorPos;
  };

  /*! Sets the origin of the bars within the plot to \em v. This has
    no effect, if the bar origin is not enabled. \sa
    setOrginiEnabled()*/
  void setBarQrigin(float v) {
    barOrigin = v;
    updateGraph();
  }
  /*! Enables the bar origin, if \em origin = true, disables it
    otherwise. Enabled means, that all bars will originate from the
    value set by setBarOrigin. So, if for example the origin was set
    to zero, bars will raise/descend from zero for positive/negative
    values respectively.*/
  void setOriginEnabled(bool origin) {
    originEnabled = origin;
    updateGraph();
  };
  /*! Sets the gap between bars to \em gap pixels. */
  void setBarGap(int gap) {
    barGap = gap;
    updateGraph();
  };

  void setXDataRange(WRange);

  /*! Sets the value of trace \em trace at the current cursor position
    to \em value*/
  void setCurrentValue(int trace, float value);

  bool isWrapAround() { return wrapAround; }

public
slots:

  void checkRange();
  void setWrapAround(bool b) { wrapAround = b; }

protected:
  virtual void drawContent(QPainter &, bool) override;

  float barOrigin;
  bool originEnabled;
  int barGap;
  int cursorPos;
  bool wrapAround;
};

#endif // WBARPLOT_H
