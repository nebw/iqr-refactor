#ifndef WCOLORBAR_HPP
#define WCOLORBAR_HPP

#include <qcolor.h>
#include <qnamespace.h>
#include <qobjectdefs.h>

#include "wGraphWidget.h"

class QMouseEvent;
class QPainter;
class QWidget;
class WRange;

class WColorBar : public WGraphWidget {
  Q_OBJECT
public:
  WColorBar(QWidget *);

  bool isAutoScale() { return yAxis()->isAutoScale(); }

public
slots:

  void setRangeSelectEnabled(bool b) {
    if (rangeSelectEnabled == b)
      return;
    rangeSelectEnabled = b;
    if (rangeSelectEnabled)
      setCursor(Qt::SplitHCursor);
    else
      unsetCursor();
  }

  void setColor(QColor c) {
    color = c;
    updateGraph();
  }
  void setAutoScale(bool a) { yAxis()->setAutoScale(a); }

signals:

  void rangeSelected(WRange);

protected:
  void mousePressEvent(QMouseEvent *) override;
  void mouseReleaseEvent(QMouseEvent *) override;
  void mouseMoveEvent(QMouseEvent *) override;

  virtual void drawContent(QPainter &, bool) override;
  virtual void drawContent_OLD(QPainter &, bool);

  bool rangeSelectEnabled, selected;

  int selecStartPos, selecEndPos;

  bool rangeSelecting;

  QColor color;
};

#endif // WCOLORBAR_HPP
