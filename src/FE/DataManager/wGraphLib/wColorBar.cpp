#include "wColorBar.h"

#include <QtGui/qpainter.h>
#include <math.h>
#include <qbrush.h>
#include <qevent.h>
#include <qflags.h>
#include <qpalette.h>
#include <qpoint.h>
#include <qrect.h>
#include <algorithm>

#include "wGraphAxis.h"
#include "wGraphFrame.h"
#include "wRange.h"

class QWidget;

using std::max;
using std::min;

WColorBar::WColorBar(QWidget *parent)
    : WGraphWidget(parent, WGraphXAxis::bottom, WGraphYAxis::right),
      rangeSelecting(false) {
  //    std::cout << "WColorBar(QWidget *parent)" << std::endl;

  showXAxis(false);
  setZoomEnabled(false);
  setAxisPanEnabled(false);

  setRangeSelectEnabled(true);
  yAxis()->setAutoScale(true);

  frame()->setFixedWidth(20);
}

void WColorBar::drawContent(QPainter &p, bool) {
  int hue, selectHue, s, v;

  color.getHsv(&hue, &s, &v);
  palette().highlight().color().getHsv(&selectHue, &s, &v);

  int selecBottom, selecTop;
  QColor qcolorBottom, qcolorTop;

  QPoint qpointStart = rect().topLeft();
  QPoint qpointStop = rect().bottomRight();

  if (rangeSelecting) { //TODO: this functionality is at the moment broken....
    selecBottom = max(selecStartPos, selecEndPos);
    selecTop = min(selecStartPos, selecEndPos);
    qcolorBottom.setHsv(selectHue, 128, 128);
    qcolorTop.setHsv(selectHue, 128, 256);
    qpointStart.setY(qpointStart.y() + selecTop);
    qpointStop.setY(qpointStop.y() - selecBottom);

  } else {
    qcolorBottom.setHsv(hue, 255, 0);
    qcolorTop.setHsv(hue, 255, 255);
  }

  QLinearGradient shade(rect().topLeft(), rect().bottomRight());
  shade.setColorAt(0, qcolorTop);
  shade.setColorAt(1, qcolorBottom);

  p.fillRect(QRect(qpointStart, qpointStop), shade);
}

void WColorBar::drawContent_OLD(QPainter &p, bool) {
  int hue, selectHue, s, v;

  color.getHsv(&hue, &s, &v);
  palette().highlight().color().getHsv(&selectHue, &s, &v);

  int left = p.window().left();
  int width = p.window().width();
  int top = p.window().top();

  int selecTop = -1, selecBottom = -1;

  if (rangeSelecting) {
    selecBottom = max(selecStartPos, selecEndPos);
    selecTop = min(selecStartPos, selecEndPos);
  }

  float step = float(p.window().height()) / 256;
  QColor paintColor;

  for (int i = 0; i < 256; i++) {
    float y = i * step;
    if (rint(y) > selecBottom || rint(y) < selecTop) {
      paintColor.setHsv(hue, 255, 255 - i);
    } else {
      paintColor.setHsv(selectHue, 128, int(255 - i * 0.5));
    }

    p.fillRect(left, top + int(rint(y)), width, int(rint(y + step) - rint(y)),
               QBrush(paintColor));
  }
}

void WColorBar::mousePressEvent(QMouseEvent *e) {
  if (rangeSelectEnabled && e->button() == Qt::LeftButton) {
    QPoint pos =
        frame()->mapFrom(this, e->pos()) - frame()->contentsRect().topLeft();

    if (frame()->underMouse()) {
      rangeSelecting = true;
      selecEndPos = selecStartPos = pos.y();
      updateGraph();
    }
  } else
    e->ignore();
}

void WColorBar::mouseReleaseEvent(QMouseEvent *e) {
  if (rangeSelectEnabled && e->button() == Qt::LeftButton) {
    emit rangeSelected(WRange(yAxis()->mapFromViewCoords(selecStartPos),
                              yAxis()->mapFromViewCoords(selecEndPos)));

    rangeSelecting = false;
    updateGraph();
  } else
    e->ignore();
}

void WColorBar::mouseMoveEvent(QMouseEvent *e) {
  if (rangeSelecting && (e->buttons() & Qt::LeftButton)) {
    QPoint pos =
        frame()->mapFrom(this, e->pos()) - frame()->contentsRect().topLeft();
    selecEndPos = pos.y();
    updateGraph();
  } else
    e->ignore();
}
