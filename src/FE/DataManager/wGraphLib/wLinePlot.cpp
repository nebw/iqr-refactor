#include "wLinePlot.h"

#include <qdom.h>
#include <qlist.h>
#include <qlogging.h>
#include <qpainter.h>
#include <qstring.h>

class QWidget;
class WGraphTrace;

WLinePlot::WLinePlot(QWidget *parent, QWidget *legendParent,
                     WGraphXAxis::Position xAxisPos,
                     WGraphYAxis::Position yAxisPos, int border,
                     bool zoomButton)
    : WGraphPlot(parent, legendParent, xAxisPos, yAxisPos, border, zoomButton),
      cursorPos(0), wrapAround(true) {

  connect(this, SIGNAL(aboutToDraw()), SLOT(checkRange()));
}

void WLinePlot::writeXML(QDomElement &elem) {
  WGraphPlot::writeXML(elem);
  elem.setAttribute("wrapAround", wrapAround ? "true" : "false");
}

void WLinePlot::readXML(QDomElement &elem) {
  WGraphPlot::readXML(elem);
  wrapAround = elem.attribute("wrapAround", "true") == "true";
}

int WLinePlot::incCursorPos() {
  if (xData.count() > 0) {
    cursorPos = (cursorPos + 1) % xData.count();
  } else {
    cursorPos = 0;
  }
  return cursorPos;
}

void WLinePlot::setTraceLength(unsigned int length) {
  WGraphPlot::setTraceLength(length);
  cursorPos %= length;
}

void WLinePlot::setCurrentValue(int trace, float value) {
  if ((trace >= 0) && (trace < countTraces()))
    traces.at(trace)->getData()[cursorPos] = value;
  else
    qWarning("WLinePlot::setCurrentValue: index out of range");
}

void WLinePlot::drawContent(QPainter &p, bool completeRedraw) {

  completeRedraw = true;
  QList<WGraphTrace *>::iterator it;

  p.eraseRect(p.window());

  if (!traces.isEmpty() && xData.count() > 0) {

    float *x = xData.data(), *y;

    for (it = traces.begin(); it != traces.end(); ++it) {
      if ((*it)->isVisible()) {
        y = (*it)->getData().data();
        p.setPen((*it)->getPen());
        QPainterPath path;

        if (wrapAround) {
          path.moveTo(mapToViewCoords(x[0], y[0]));
          for (int i = 1; i < traceLength(); i++) {
            path.lineTo(mapToViewCoords(x[i], y[i]));
          }
        } else {
          path.moveTo(
              mapToViewCoords(x[0], y[(cursorPos + 1) % traceLength()]));
          for (int i = 1; i < traceLength(); i++) {
            path.lineTo(
                mapToViewCoords(x[i], y[(i + cursorPos + 1) % traceLength()]));
          }
        }
        p.drawPath(path);
      }
    }
  }
}
