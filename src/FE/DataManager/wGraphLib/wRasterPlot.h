#ifndef WRASTERPLOT_HPP
#define WRASTERPLOT_HPP

#include <qobjectdefs.h>
#include <qpen.h>
#include <qstring.h>
#include <valarray>

#include "stateArray.hpp"
#include "wGraphAxis.h"
#include "wGraphMatrix.h"
#include "wGraphPlot.h"

class QDomElement;
class QPainter;
class QWidget;
class WGraphTrace;
class WRange;
template <class T> class QVector;

/*! The WRasterPlot class inherits WGraphPlot and draws the traces as
  lines. There are two different modes, one for drawing sequences, one
  for drawing functions.*/
class WRasterPlot : public WGraphPlot {
  Q_OBJECT
public:
  /*! Constructs a WRasterPlot and passes all the arguments to WGraphPlot.*/
  WRasterPlot(QWidget *parent, QWidget *legendParent,
              WGraphXAxis::Position xAxisPos = WGraphXAxis::bottom,
              WGraphYAxis::Position yAxisPos = WGraphYAxis::left,
              int border = 0, bool zoomButton = true);

  virtual void writeXML(QDomElement &) override;
  virtual void readXML(QDomElement &) override;

  /*! Replaces the data with a new matrix and sets name \em name and
    color \em color. The matrix is initialized to the length specified
    by setTraceLength().
    If a WGraphLegend was generated by passing a non-zero
    legendParent to WGraphPlot(), the trace's name is also inserted
    into the legend.*/
  WGraphTrace *addTrace(const QString &name, QPen pen) {
    return addMatrix(name, pen);
  }
  WGraphMatrix *addMatrix(const QString &name, QPen pen);
  /*! Removes the data matrix, frees the memory occupied, and also
    removes it from the legend*/
  void removeTraces();

  /*! Sets the length of the currently used matrix to \em length.
    \sa getTraceLength()*/
  void setTraceLength(unsigned int length);

  /*! Allways returns 0 or 1, depending if a matrix was added.*/
  int countTraces() const {
    return yData == nullptr ? 0 : 1;
  };
  /*! Returns the length of the traces in this WGraphPlot. \sa
    setTraceLength()*/
  int traceLength() const {
    return xData.count();
  };

  /*! Returns a comma seperated list of the currently visible
    traces. If there is more then one visible trace and the resulting
    string is longer then maxChars, "..." is returned. */
  QString getVisibleTracesStr(int maxChars) const;

  /*! Returns a reference to the data matrix*/
  QVector<std::valarray<double> > &traceData() {
    if (yData != nullptr) {
      return yData->getData();
    }
  };

  /*! Increases the current position of the cursor by one, or cycles
    back to zero, if it was bigger then the length of the traces set
    by setTraceLength(). The new cursor position is returned.*/
  int incCursorPos();
  /*! Returns the current position of the cursor.*/
  int getCursorPos() const {
    return cursorPos;
  };

  bool isWrapAround() { return wrapAround; }

  /*! Sets the value of trace \em trace at the current cursor position
    to \em value*/
  void setCurrentValues(std::valarray<double> values);
  void setCurrentValues(iqrcommon::StateArray *values);

public
slots:

  /*! Determines the range over which the data of the matrix
  extend, and sets the data range of the vertical axis
  accordingly. \sa WGraphWidget::setYDataRange(),
  WGraphMatrix::checkRange()*/
  void checkRange();
  void setWrapAround(bool b) { wrapAround = b; }
  void setYDataRange(WRange r);
  void setYDataWidth(unsigned int w);

protected:
  virtual void drawContent(QPainter &, bool) override;

  WGraphMatrix *yData;

  int cursorPos;
  bool wrapAround;
};

#endif // WRASTERPLOT_HPP
