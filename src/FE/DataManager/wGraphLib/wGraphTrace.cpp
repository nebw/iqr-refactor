#include "wGraphTrace.h"

#include <qcheckbox.h>
#include <qdom.h>
#include <qnamespace.h>
#include <qpalette.h>
#include <qpen.h>
#include <qradiobutton.h>
#include <algorithm>

#include "wGraphLegend.h"

WGraphTrace::WGraphTrace(WGraphLegend *l, QString n, QPen p, int length)
    : QObject(), name(std::move(n)), pen(p), legend(l) {

  resize(length);
  visible = true;

  pen.setCapStyle(Qt::RoundCap);
  pen.setJoinStyle(Qt::RoundJoin);

  if (legend) {
    legend->hide();

    checkButton = new QCheckBox(name, nullptr /*legend->checkGroup*/);
    legend->addCheckBox(checkButton);

    radioButton = new QRadioButton(name, nullptr /*legend->radioGroup*/);
    legend->addRadioButton(radioButton);

    QPalette p = checkButton->palette();
    p.setColor(QPalette::Normal, QPalette::Foreground, pen.color());
    p.setColor(QPalette::Inactive, QPalette::Foreground, pen.color());
    checkButton->setPalette(p);
    radioButton->setPalette(p);

    checkButton->setChecked(isVisible());

    /* TODO: this needs fixing
            if (legend->radioGroup->id(radioButton)==0) {
                radioButton->setChecked(true);
            }
    */

    connect(checkButton, SIGNAL(toggled(bool)), this, SLOT(setVisible(bool)));
    connect(radioButton, SIGNAL(toggled(bool)), this, SLOT(setVisible(bool)));

    connect(legend->exclButton, SIGNAL(toggled(bool)), this,
            SLOT(toggleExclusive(bool)));

    legend->show();

  } else {
    checkButton = nullptr;
    radioButton = nullptr;
  }
}

WGraphTrace::~WGraphTrace() {
  if (legend) {
    legend->hide();
    if (checkButton) {
      checkButton->close();
    }
    if (radioButton) {
      radioButton->close();
    }
    legend->layout->invalidate();
    legend->show();
  }
}

void WGraphTrace::toggleExclusive(bool on) {
  if (on) {
    setVisible(radioButton->isChecked());
  } else {
    setVisible(checkButton->isChecked());
  }
}

void WGraphTrace::setVisible(bool vis) {

  if (legend->isExclusive()) {
    radioButton->blockSignals(true);
    radioButton->setChecked(vis);
    radioButton->blockSignals(false);
  } else {
    checkButton->blockSignals(true);
    checkButton->setChecked(vis);
    checkButton->blockSignals(false);
  }

  visible = vis;

  emit needRefresh();
}

const WRange &WGraphTrace::checkRange() {
  dataRange.resetForRangeCheck();
  for (auto &elem : data) {
    dataRange.adjust(elem);
  }
  return dataRange;
}

void WGraphTrace::writeXML(QDomElement &elem) {
  QDomElement trace = elem.ownerDocument().createElement("trace");
  elem.appendChild(trace);
  trace.setAttribute("name", getName());
  trace.setAttribute("nonexclVisible",
                     checkButton->isChecked() ? "true" : "false");
  trace.setAttribute("exclVisible",
                     radioButton->isChecked() ? "true" : "false");
}

void WGraphTrace::readXML(QDomElement &elem) {
  QDomNodeList traceList = elem.elementsByTagName("trace");
  for (int i = 0; i < traceList.count(); i++) {
    QDomElement trace = traceList.item(i).toElement();
    if (trace.attribute("name") == getName()) {
      checkButton->setChecked(trace.attribute("nonexclVisible") == "true");
      radioButton->setChecked(trace.attribute("exclVisible") == "true");
      return;
    }
  }
}
