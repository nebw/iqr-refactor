#include "wGraphMatrix.h"

#include "wRange.h"

class WGraphLegend;

//#include "wGraphMatrix.moc"

WGraphMatrix::WGraphMatrix(WGraphLegend *l, const QString &n, QPen p,
                           unsigned int length, unsigned int width)
    : WGraphTrace(l, n, p, length) {
  pen.setWidth(3);
  resize(length, width);
}

const WRange &WGraphMatrix::checkRange() {
  dataRange.resetForRangeCheck();
  if (data[0].size() == 1) {
    dataRange.adjust(0.5);
    dataRange.adjust(1.5);
  } else {
    dataRange.adjust(1.0);
    dataRange.adjust(1.0);
    dataRange.adjust(data[0].size());
  }
  return dataRange;
}

void WGraphMatrix::resize(unsigned int length, unsigned int width = 0) {
  data.resize(length);
  for (unsigned int i = 0; i < length; i++) {
    data[i].resize(width, 0.0);
  }
}

void WGraphMatrix::clear() {
  for (auto &elem : data) {
    elem = 0;
  }
}
