#ifndef WGRAPHMATRIX_HPP
#define WGRAPHMATRIX_HPP

#include <qobjectdefs.h>
#include <qpen.h>
#include <qstring.h>
#include <qvector.h>
#include <stdlib.h>
#include <valarray>

#include "wGraphTrace.h"

class WGraphLegend;
class WRange;

/*! The WGraphTrace is the structure in which a trace added to a
  WGraphPlot is represented.*/
class WGraphMatrix : public WGraphTrace {
  Q_OBJECT
public:
  WGraphMatrix(WGraphLegend *, const QString &, QPen, unsigned int,
               unsigned int);

  /*! Resizes this matrix to \em length.*/
  void resize(int length) { resize(abs(length), 0); }
  void resize(unsigned int length, unsigned int width);
  /*! Clears the matrix.*/
  void clear();

  QVector<std::valarray<double> > &getData() {
    return data;
  };
  /*! Returns the range over which the data of this trace extends.*/
  const WRange &checkRange();

protected:
  QVector<std::valarray<double> > data;
};

#endif // WGRAPHMATRIX_HPP
