#include "wBarPlot.h"

#include <qdom.h>
#include <qlist.h>
#include <qlogging.h>
#include <qpainter.h>
#include <qpen.h>
#include <qpoint.h>
#include <qrect.h>
#include <qstring.h>
#include <qvector.h>

#include "wGraphTrace.h"
#include "wRange.h"

class QWidget;

WBarPlot::WBarPlot(QWidget *parent, QWidget *legendParent,
                   WGraphXAxis::Position xAxisPos,
                   WGraphYAxis::Position yAxisPos, int border, bool zoomButton)
    : WGraphPlot(parent, legendParent, xAxisPos, yAxisPos, border, zoomButton),
      barOrigin(0), originEnabled(true), barGap(1), cursorPos(0),
      wrapAround(true) {}

WBarPlot::~WBarPlot() {}

void WBarPlot::writeXML(QDomElement &elem) {
  WGraphWidget::writeXML(elem);
  elem.setAttribute("originEnabled", originEnabled ? "true" : "false");
  elem.setAttribute("barOrigin", barOrigin);
  elem.setAttribute("barGap", barGap);
  elem.setAttribute("wrapAround", wrapAround ? "true" : "false");
}

void WBarPlot::readXML(QDomElement &elem) {
  WGraphWidget::readXML(elem);
  originEnabled = elem.attribute("originEnabled", "true") == "true";
  barOrigin = elem.attribute("barOrigin", "0").toFloat();
  barGap = elem.attribute("barGap", "1").toInt();
  wrapAround = elem.attribute("wrapAround", "true") == "true";
}

int WBarPlot::incCursorPos() {
  if (xData.count() > 0) {
    cursorPos = (cursorPos + 1) % xData.count();
  } else {
    cursorPos = 0;
  }
  return cursorPos;
}

void WBarPlot::checkRange() {
  WRange range;
  range.resetForRangeCheck();

  QList<WGraphTrace *>::iterator it;
  for (it = traces.begin(); it != traces.end(); ++it) {
    if ((*it)->isVisible()) {
      range |= (*it)->checkRange();
    }
  }

  if (originEnabled) {
    range.adjust(barOrigin);
  }

  range.expand(0.1);
  blockSignals(true);
  if (!getYDataRange().comp(range, 0.05) && !range.isEmpty()) {
    setYDataRange(range);
  }
  blockSignals(false);
}

void WBarPlot::setXDataRange(WRange range) {

  for (int i = 0; i < xData.count(); i++) {
    xData[i] = range.unscaleValue(float(i) / (xData.count() - 1));
  }

  range.expand(0.5 / traceLength());
  WGraphWidget::setXDataRange(range);
}

void WBarPlot::setCurrentValue(int trace, float value) {
  if ((trace >= 0) && (trace < countTraces()))
    traces.at(trace)->getData()[cursorPos] = value;
  else
    qWarning("WBarPlot::setCurrentValue: index out of range");
}

void WBarPlot::drawContent(QPainter &p, bool) {

  QList<WGraphTrace *>::iterator it;
  QPoint p1, p2;
  QRect bar;

  int i, leftGap, rightGap;

  if (dataPointsPerPixel() > 0.25) {
    leftGap = rightGap = 0;
  } else {
    leftGap = rightGap = barGap / 2;
    leftGap += barGap % 2;
    rightGap++;
  }

  checkRange();

  if (!traces.isEmpty()) {

    p.eraseRect(p.window());

    for (it = traces.begin(); it != traces.end(); ++it) {
      WGraphTrace *trace = (*it);
      if (trace->isVisible()) {
        p.setPen(trace->getPen());

        float *x = xData.data();
        float *y = trace->getData().data();

        p1 = mapToViewCoords(
            x[0] - (x[1] - x[0]) / 2,
            y[wrapAround ? 0 : (cursorPos + 1) % traceLength()]);

        for (i = 0; i < traceLength(); i++) {

          p2 = mapToViewCoords(
              x[i] - (x[i] - x[i - 1]) / 2,
              y[wrapAround ? i : (i + cursorPos + 1) % traceLength()]);
          bar.setCoords(p1.x(), p1.y(), p2.x(), p1.y());

          if (originEnabled) {
            int o = mapToViewCoords(0, barOrigin).y();
            if (o < bar.top())
              bar.setTop(o);
            else
              bar.setBottom(o);
          } else
            bar.setBottom(p.window().bottom());

          bar.setRight(bar.right() - rightGap);
          bar.setLeft(bar.left() + leftGap);
          p.fillRect(bar, trace->getPen().color());

          p1 = p2;
        }

        p2 = mapToViewCoords(
            x[i] + (x[i] - x[i - 1]) / 2,
            y[wrapAround ? i - 1 : (i + cursorPos) % traceLength()]);

        bar.setCoords(p1.x(), p1.y(), p2.x(), p1.y());

        if (originEnabled) {
          int o = mapToViewCoords(0, barOrigin).y();
          if (o < bar.top())
            bar.setTop(o);
          else
            bar.setBottom(o);
        } else
          bar.setBottom(p.window().bottom());

        bar.setRight(bar.right() - rightGap - 1);
        bar.setLeft(bar.left() + leftGap);
        p.fillRect(bar, trace->getPen().color());
      }
    }
  }
}
