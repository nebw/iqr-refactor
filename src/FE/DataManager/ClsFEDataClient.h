/****************************************************************************
 ** $Filename: ClsFEDataClient.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Sep 14 17:02:32 2001
 ** $Date: 2001/11/16 12:30:21 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSFEDATACLIENT_H
#define CLSFEDATACLIENT_H /*+ To stop multiple inclusions. +*/

#include <map>
#include <string>

#include "ClsColorPicker.h"
#include "item.hpp"

using namespace iqrcommon;

class ClsDataSinkCopying;
class ClsFEDataManager;
class QMutex;
namespace iqrcommon {
class ClsItem;
} // namespace iqrcommon

using namespace std;

class ClsFEDataClient {

public:
  ClsFEDataClient(ClsFEDataManager *_clsFEDataManager, QMutex *qmutexSysGUI,
                  string _strDataClientID);
  virtual ~ClsFEDataClient() {};

  enum DATA_CLIENT_TYPES {
    CLIENT_SPACEPLOT = 0,
    CLIENT_TIMEPLOT,
    CLIENT_BARPLOT,
    CLIENT_CORRPLOT,
    CLIENT_DATASAMPLER,
    CLIENT_DATABROADCASTER,
    CLIENT_RASTERPLOT
  };

  void setClientType(int iType) {
    iDataClientType = iType;
  };
  int getClientType() {
    return iDataClientType;
  };
  string getClientID() {
    return strDataClientID;
  };

  int getDataSinkColor(string strID);

  void addDataSink(string strID, ClsItem *clsItem, string strParamname,
                   string strRange);
  virtual void removeDataSink(string strID);
  void setSinkMath(string strSinkID, int iFlag);

  virtual void init();
  virtual void DataSinkAdded(string strID, int iColor) {
    iColor = 0;
    strID = "";
  };
  virtual void DataSinkRemoved(string strID) {
    strID = "";
  };
  virtual void groupChanged(string strGroupID);
  virtual void connectionChanged(string _strConnectionID);

protected:
  ClsFEDataManager *clsFEDataManager;
  QMutex *qmutexSysGUI;
  string strDataClientID;
  map<string, ClsDataSinkCopying *> mapDataSinks;
  ClsColorPicker clsColorPicker;

private:
  int iDataClientType;
};

#endif /* CLSFEDATACLIENT_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
