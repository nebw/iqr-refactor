/****************************************************************************
 ** $Filename: ClsDataClientConfig.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Wed Oct  1 00:34:20 2003
 ** $Date: 2003/10/04 10:08:42 $
 **
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSDATACLIENTCONFIG_H
#define CLSDATACLIENTCONFIG_H /*+ To stop multiple inclusions. +*/

#include <string>
#include <list>
#include <map>
#include <iostream>

#include "ConfigTagLibrary.h"

using namespace std;

class ClsStateVariableDisplayConfig {
public:
  ClsStateVariableDisplayConfig(string _strID, string _strItemID)
      :
        strID(std::move(_strID)),
        strItemID(std::move(_strItemID)) {};
  ClsStateVariableDisplayConfig(string _strID,
                                string _strItemID, string _strSelectedIndices)
      :
        strID(std::move(_strID)),
        strItemID(std::move(_strItemID)),
        strSelectedIndices(std::move(_strSelectedIndices)) {};

  string getID() {
    return strID;
  };
  string getItemID() {
    return strItemID;
  };
  string getSelectedIndices() {
    return strSelectedIndices;
  };

  void addParameter(pair<string, string> p) {
    lstParameters.push_back(p);
  };
  list<pair<string, string> > getListParameters() {
    return lstParameters;
  };

private:
  string strID;
  string strItemID;
  string strSelectedIndices;
  list<pair<string, string> > lstParameters;
};

class ClsDataClientConfig {

public:
  ClsDataClientConfig(string _strID) : strID(std::move(_strID)) {};
  ClsDataClientConfig(string _strID, string _strType)
      : strID(std::move(_strID)), strType(std::move(_strType)) {};

  void setType(string _strType) {
    strType = _strType;
  };
  string getType() {
    return strType;
  };

  void setPosition(int iX, int iY) {
    pPosition.first = iX;
    pPosition.second = iY;
  };

  void setPosition(pair<int, int> _pPosition) {
    pPosition = _pPosition;
  };

  void setGeometry(int iW, int iH) {
    pGeometry.first = iW;
    pGeometry.second = iH;
  };

  void setGeometry(pair<int, int> _pGeometry) {
    pGeometry = _pGeometry;
  };

  void addStateVariableDisplayConfig(
      ClsStateVariableDisplayConfig _clsStateVariableDisplayConfig) {
    lstCVDConfigs.push_back(_clsStateVariableDisplayConfig);
  };

  list<ClsStateVariableDisplayConfig> getListStateVariableDisplayConfig() {
    return lstCVDConfigs;
  };

  void addParameter(pair<string, string> p) {
    lstParameters.push_back(p);
  };
  list<pair<string, string> > getListParameters() {
    return lstParameters;
  };

  pair<int, int> getPosition() {
    return pPosition;
  };
  pair<int, int> getGeometry() {
    return pGeometry;
  };

  string getID() {
    return strID;
  };

private:
  string strID;
  string strType;
  pair<int, int> pPosition;
  pair<int, int> pGeometry;
  list<ClsStateVariableDisplayConfig> lstCVDConfigs;
  list<pair<string, string> > lstParameters;
};

#endif /* CLSDATACLIENTCONFIG_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd /home/ulysses/Code/iqr421_Redesign/FE/IQR/ && make -k
///"
//// End:
