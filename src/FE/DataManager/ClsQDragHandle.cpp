/****************************************************************************
 ** $Filename: ClsQDragHandle.cpp
 ** $Header$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Tue Nov  6 15:54:51 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <pick_14x14.xpm>
#include <qevent.h>
#include <qnamespace.h>
#include <qpixmap.h>
#include <qwidget.h>

#include "ClsQDragHandle.h"

ClsQDragHandle::ClsQDragHandle(QWidget *parent) : QLabel(parent) {
  QWidget::setCursor(Qt::PointingHandCursor);
  this->setPixmap(QPixmap(pick));
};

void ClsQDragHandle::mouseMoveEvent(QMouseEvent *e) {
  if (e->buttons() == Qt::LeftButton) {
    emit drag(false);
  } else if (e->buttons() == Qt::LeftButton || Qt::ControlModifier) {
    emit drag(true);
  }
};
