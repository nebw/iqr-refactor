/****************************************************************************
 ** $Filename: ClsFEGroupPlot.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Oct 26 01:32:17 2001
 ** $Date: 2004/02/04 12:05:00 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <ClsDragDropDeEncoder.h>
#include <ClsFEDataSink.h>
#include <ClsFESystemManager.h>
#include <ClsTopologyRect.h>
#include <ClsTopologySparse.h>
#include <icon_groupplot_32x32.xpm>
#include <qboxlayout.h>
#include <qcolor.h>
#include <qdrag.h>
#include <qevent.h>
#include <qgridlayout.h>
#include <qicon.h>
#include <qmimedata.h>
#include <qmutex.h>
#include <qnamespace.h>
#include <qpixmap.h>
#include <qpoint.h>
#include <qsize.h>
#include <qstring.h>
#include <qvector.h>
#include <list>
#include <map>
#include <utility>

#include "ClsBaseTopology.h"
#include "ClsFEDataClient.h"
#include "ClsFEGroup.h"
#include "ClsFEGroupPlot.h"
#include "ClsHyperLists.h"
#include "ClsQBaseStateVariableDisplay.h"
#include "ClsQNeuronStateVariableDisplay.h"
#include "idGenerator.hpp"
#include "stateVariable.hpp"
#include "wColorBar.h"
#include "wSpacePlot.h"

class ClsFEDataManager;

//#define DEBUG_CLSFEGROUPPLOT

ClsFEGroupPlot::ClsFEGroupPlot(ClsFEDataManager *_clsFEDataManager,
                               QMutex *_qmutexSysGUI, string _strDataClientID,
                               ClsFEGroup *_clsFEGroup)
    : ClsFEDataClient(_clsFEDataManager, _qmutexSysGUI, _strDataClientID) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::ClsFEGroupPlot(...) [new]" << endl;
#endif
  bSparseTopology = false;
  setClientType(CLIENT_SPACEPLOT);
  setAcceptDrops(true);
  this->setWindowIcon(QPixmap(icon_groupplot));
  /* set up the WSpacePlot */
  wSpacePlot = new WSpacePlot(QSize(0, 0), false, this, this);
  wSpacePlot->setColor(QColor(0, 0, 0));
  wSpacePlot->showGrid(true);
  wSpacePlot->setMinimumWidth(200);

  (wSpacePlot->yAxis())->setDirection(-1);

  QBoxLayout *boxlayoutPlot = new QHBoxLayout();
  boxlayoutPlot->addWidget(wSpacePlot, 1);
  boxlayoutPlot->addWidget(wSpacePlot->getColorBar());
  boxlayoutPlot->setContentsMargins(5, 5, 5, 5);

  boxlayoutBase->addLayout(boxlayoutPlot, 0, 0);

  connect(wSpacePlot, SIGNAL(wouldDrag()), this, SLOT(startDragging()));
  /* ----------------------- */
  if (_clsFEGroup != nullptr) {

    string strOutputStateName =
        _clsFEGroup->getNeuronOutput()->getName();   // NEW
    list<string> lstSelectedStates;                  // NEW
    lstSelectedStates.push_back(strOutputStateName); // NEW

    addStateVariableDisplay(_clsFEGroup, lstSelectedStates, "");
  }

  QWidget::show();
};

string ClsFEGroupPlot::addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                               string /* strRange */) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::addStateVariableDisplay(ClsFEGroup* _clsFEGroup, "
          "string /* strRange */)" << endl;
#endif
  strGroupID = _clsFEGroup->getGroupID();

  string strDisplayID =
      _clsFEGroup->getGroupID() + ClsIDGenerator::Instance()->Next();

  strTitle = "Space Plot for \"" + _clsFEGroup->getGroupName() + "\"";
  setWindowTitle(strTitle.c_str());

  if (clsQBaseStateVariableDisplay != nullptr) {
    boxlayoutBase->removeWidget(clsQBaseStateVariableDisplay);
  }

  clsQBaseStateVariableDisplay =
      new ClsQNeuronStateVariableDisplay(this, strDisplayID, this, _clsFEGroup,
                                         "", false, false, true, false, false);

  connect(dynamic_cast<ClsQNeuronStateVariableDisplay *>(
              clsQBaseStateVariableDisplay),
          SIGNAL(addDisplay(string, list<string>, string)), this,
          SLOT(addStateVariableDisplay(string, list<string>, string)));

  iGroupWidth = _clsFEGroup->getNrCellsHorizontal();
  iGroupHeight = _clsFEGroup->getNrCellsVertical();
  getCellIndices(_clsFEGroup);
  wSpacePlot->setSize(QSize(iGroupWidth, iGroupHeight));

  boxlayoutBase->addWidget(clsQBaseStateVariableDisplay, 0, 2, Qt::AlignTop);
  return strDisplayID;
};

string ClsFEGroupPlot::addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                               list<string> lstSelectedStates,
                                               string /* strRange */) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::addStateVariableDisplay(ClsFEGroup* _clsFEGroup, "
          "list<string> lstSelectedStates, string strRange )" << endl;
#endif
  strGroupID = _clsFEGroup->getGroupID();
  string strDisplayID =
      _clsFEGroup->getGroupID() + ClsIDGenerator::Instance()->Next();

  strTitle = "Space Plot for \"" + _clsFEGroup->getGroupName() + "\"";
  setWindowTitle(strTitle.c_str());

  if (clsQBaseStateVariableDisplay != nullptr) {
    boxlayoutBase->removeWidget(clsQBaseStateVariableDisplay);
  }

  clsQBaseStateVariableDisplay =
      new ClsQNeuronStateVariableDisplay(this, strDisplayID, this, _clsFEGroup,
                                         "", false, false, true, false, false);
  dynamic_cast<ClsQNeuronStateVariableDisplay *>(clsQBaseStateVariableDisplay)
      ->setSelectedStates(lstSelectedStates);
  connect(dynamic_cast<ClsQNeuronStateVariableDisplay *>(
              clsQBaseStateVariableDisplay),
          SIGNAL(addDisplay(string, list<string>, string)), this,
          SLOT(addStateVariableDisplay(string, list<string>, string)));

  clsQBaseStateVariableDisplay->slotLiveData(true);

  iGroupWidth = _clsFEGroup->getNrCellsHorizontal();
  iGroupHeight = _clsFEGroup->getNrCellsVertical();
  getCellIndices(_clsFEGroup);

  wSpacePlot->setSize(QSize(iGroupWidth, iGroupHeight));

  boxlayoutBase->addWidget(clsQBaseStateVariableDisplay, 0, 2, Qt::AlignTop);
  return strDisplayID;
};

string ClsFEGroupPlot::addStateVariableDisplay(string _strGroupID,
                                               list<string> lstSelectedStates,
                                               string strRange) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::addStateVariableDisplay(string _strGroupID, "
          "list<string> lstSelectedStates, string strRange)" << endl;
#endif
  strGroupID = _strGroupID;
  strRange = "";
  if (ClsFESystemManager::Instance()->getFEGroup(_strGroupID) != nullptr) {
    return addStateVariableDisplay(
        ClsFESystemManager::Instance()->getFEGroup(_strGroupID),
        lstSelectedStates, "");
  }
  return "";
};

void ClsFEGroupPlot::plotData() {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::plotData()" << endl;
#endif

  if (mapDataSinks.size() > 0) {
    ClsDataSinkCopying *clsDataSink = mapDataSinks.begin()->second;

    clsDataSink->update();

    unsigned int iIndex = 0;

    if (bSparseTopology) {
      QVector<WSpacePlot::SpaceCell> qaCells = wSpacePlot->getCellData();
      for (iIndex = 0; iIndex < vectCellIndices.size(); iIndex++) {
        qaCells[vectCellIndices[iIndex]].setValue((*clsDataSink)[iIndex]);
      }
    } else {
      QVector<WSpacePlot::SpaceCell>::Iterator it;
      for (it = wSpacePlot->getCellData().begin();
           it != wSpacePlot->getCellData().end(); ++it) {
        it->setValue((*clsDataSink)[iIndex]);
        iIndex++;
      }
    }

    wSpacePlot->updateGraph();
  }
};

void ClsFEGroupPlot::slotLiveData(bool /*bToggled*/) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::slotLiveData(bool bToggled)" << endl;
#endif
};

void ClsFEGroupPlot::startDragging() {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::startDragging()" << endl;
#endif
  auto drag = new QDrag(this);
  auto mimeData = new QMimeData;

  string strParamList = dynamic_cast<ClsQNeuronStateVariableDisplay *>(
      clsQBaseStateVariableDisplay)->getSelectedStatesAsString();

  QVector<int> qvectorSelected = wSpacePlot->getSelecIndexList();
  string strRange = wSpacePlotSelected2Range(qvectorSelected);

  string str = ClsDragDropDeEncoder::encode(ClsFESystemManager::ITEM_GROUP,
                                     clsQBaseStateVariableDisplay->getItemID(),
                                     strParamList, strRange);

  mimeData->setData("text/iqr-plot", str.c_str());
  drag->setMimeData(mimeData);
  drag->exec();
};

string ClsFEGroupPlot::wSpacePlotSelected2Range(QVector<int> &qvectorSelected) {
  string strRange;

  if (dynamic_cast<ClsTopologySparse *>(ClsFESystemManager::Instance()
                                            ->getFEGroup(strGroupID)
                                            ->getTopology())) {
    ClsTopologySparse *clsTopologySparse = dynamic_cast<ClsTopologySparse *>(
        ClsFESystemManager::Instance()->getFEGroup(strGroupID)->getTopology());
    iGroupWidth = clsTopologySparse->nrCellsHorizontal();
    iGroupHeight = clsTopologySparse->nrCellsVertical();

    for (auto &elem : qvectorSelected) {
      int iy = int(elem / iGroupWidth) + 1;
      int ix = elem - int(elem / iGroupWidth) * iGroupWidth + 1;
      if (clsTopologySparse->isValidCell(ix, iy)) {
        strRange = iqrUtils::int2string(clsTopologySparse->pos2index(ix, iy)) +
                   ";" + strRange;
      }
    }
  } else {
    for (auto &elem : qvectorSelected) {
      strRange = iqrUtils::int2string(elem) + ";" + strRange;
    }
  }
  return strRange;
}

void ClsFEGroupPlot::dragEnterEvent(QDragEnterEvent *event) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::dragEnterEvent(QDragEnterEvent* event)" << endl;
#endif
  if (event->mimeData()->hasFormat("text/iqr-plot")) {
    event->acceptProposedAction();
  }
}

void ClsFEGroupPlot::dropEvent(QDropEvent *event) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::dropEvent(QDropEvent* event)" << endl;
#endif

  if (event->mimeData()->hasFormat("text/iqr-plot")) {
    QString text = event->mimeData()->data("text/iqr-plot");
    dropInfo di = ClsDragDropDeEncoder::decode(text.toStdString());
    unsigned int iItemType = di.ItemType;
    if (iItemType == ClsFESystemManager::ITEM_GROUP) {
      string strGroupID = di.ItemID;
      string strParams = di.Params;

      list<string> lst;
      string::size_type pos;
      while ((pos = strParams.find(";")) != string::npos) {
        lst.push_back(strParams.substr(0, pos));
        strParams.erase(0, pos + 1);
      }
      clsQBaseStateVariableDisplay->slotClose();
      addStateVariableDisplay(
          ClsFESystemManager::Instance()->getFEGroup(strGroupID), lst, "");
    }
  }
};

void ClsFEGroupPlot::init() { ClsFEDataClient::init(); }

void ClsFEGroupPlot::update() {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::update()" << endl;
#endif

  plotData();
};

void ClsFEGroupPlot::DataSinkAdded(string strID, int iColor) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::DataSinkAdded(string strID, int iColor)" << endl;
#endif
  strID = "";

  QColor qc;
  qc.setHsv(iColor * 2, 255, 210); // try to multiply by some factor, so the
                                   // differences become larger
  wSpacePlot->setColor(qc);
};

void ClsFEGroupPlot::closeEvent(QCloseEvent *ce) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::closeEvent( QCloseEvent* ce )" << endl;
#endif

  if (clsQBaseStateVariableDisplay != nullptr) {
    clsQBaseStateVariableDisplay->close();
    delete clsQBaseStateVariableDisplay;
    clsQBaseStateVariableDisplay = nullptr;
  }

  bSparseTopology = false;
  emit sigPlotClosed(strDataClientID);
  ce->accept();
}

void ClsFEGroupPlot::setConfig(ClsDataClientConfig clsDataClientConfig) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::setConfig(ClsDataClientConfig clsDataClientConfig)"
       << endl;
#endif

  list<ClsStateVariableDisplayConfig> lstSVDConfigs =
      clsDataClientConfig.getListStateVariableDisplayConfig();
  list<ClsStateVariableDisplayConfig>::iterator itSVD;
  for (itSVD = lstSVDConfigs.begin(); itSVD != lstSVDConfigs.end(); ++itSVD) {

    string _strGroupID = (*itSVD).getItemID();
    string strSelectedIndices = (*itSVD).getSelectedIndices();

    if (ClsFESystemManager::Instance()->getFEGroup(_strGroupID) != nullptr) {
      addStateVariableDisplay(
          ClsFESystemManager::Instance()->getFEGroup(_strGroupID),
          strSelectedIndices);
      if (clsQBaseStateVariableDisplay != nullptr) {
        clsQBaseStateVariableDisplay->setConfig((*itSVD));
      }
    }
  }
}

ClsDataClientConfig ClsFEGroupPlot::getConfig() {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::getConfig()" << endl;
#endif
  ClsDataClientConfig clsDataClientConfig(strDataClientID);
  clsDataClientConfig.setPosition(this->pos().x(), this->pos().y());
  clsDataClientConfig.setGeometry(this->size().width(), this->size().height());

  if (clsQBaseStateVariableDisplay != nullptr) {
    ClsStateVariableDisplayConfig clsStateVariableDisplayConfig =
        clsQBaseStateVariableDisplay->getConfig();
    clsDataClientConfig.addStateVariableDisplayConfig(
        clsStateVariableDisplayConfig);
  }
  return clsDataClientConfig;
}

void ClsFEGroupPlot::groupChanged(string _strGroupID) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::groupChanged(string strGroupID)" << endl;
#endif

  if (!strGroupID.compare(_strGroupID)) {
    ClsFEGroup *_clsFEGroup =
        ClsFESystemManager::Instance()->getFEGroup(_strGroupID);
    iGroupWidth = _clsFEGroup->getNrCellsHorizontal();
    iGroupHeight = _clsFEGroup->getNrCellsVertical();

    getCellIndices(_clsFEGroup);

    wSpacePlot->setSize(QSize(iGroupWidth, iGroupHeight));
    wSpacePlot->updateGraph();

    if (!_strGroupID.compare(clsQBaseStateVariableDisplay->getItemID())) {
      dynamic_cast<ClsQNeuronStateVariableDisplay *>(
          clsQBaseStateVariableDisplay)->groupChanged();
    }
    ClsFEDataClient::groupChanged(_strGroupID);
  }
};

void ClsFEGroupPlot::groupDeleted(string strID) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "ClsFEGroupPlot::groupDeleted(string strID)" << endl;
#endif

  if (!strGroupID.compare(strID)) {
    bSparseTopology = false;
    QWidget::close();
  }
}

void ClsFEGroupPlot::getCellIndices(ClsFEGroup *_clsFEGroup) {
#ifdef DEBUG_CLSFEGROUPPLOT
  cout << "getCellIndices(ClsFEGroup* _clsFEGroup)" << endl;
#endif

  ClsBaseTopology *clsBaseTopology = _clsFEGroup->getTopology();
  if (dynamic_cast<ClsTopologySparse *>(clsBaseTopology)) {
    vectCellIndices.resize(0);
    list<tiPoint> lstPoints =
        dynamic_cast<ClsTopologySparse *>(clsBaseTopology)->getList();
    list<tiPoint>::iterator it;
    for (it = lstPoints.begin(); it != lstPoints.end(); ++it) {
      vectCellIndices.push_back(
          ClsTopologyRect::pos2index(iGroupWidth, (*it).first, (*it).second));
    }
    bSparseTopology = true;
  } else {
    bSparseTopology = false;
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
