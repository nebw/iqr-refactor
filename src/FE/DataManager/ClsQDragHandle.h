#ifndef CLSQDRAGHANDLE_H
#define CLSQDRAGHANDLE_H

#include <qlabel.h>
#include <qobjectdefs.h>

class QMouseEvent;
class QWidget;

class ClsQDragHandle : public QLabel {
  Q_OBJECT
public:
  ClsQDragHandle(QWidget *parent);

signals:
  void drag(bool);

private
slots:

  void mouseMoveEvent(QMouseEvent *e) override;
};

#endif

//// Local Variables:
//// mode: c++
//// End:
