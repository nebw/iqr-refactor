add_subdirectory(wGraphLib)

file(GLOB src "*.cpp" "*.h" "*.hpp")

add_library(DataManager STATIC ${src})
target_link_libraries(DataManager wGraphLib)
