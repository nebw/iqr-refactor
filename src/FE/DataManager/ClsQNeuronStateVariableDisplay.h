/****************************************************************************
 ** $Filename: ClsQNeuronStateVariableDisplay.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Oct 26 01:32:27 2001
 ** $Date: 2002/02/10 13:21:50 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSQGROUPSTATEVARIABLEDISPLAY_H
#define CLSQGROUPSTATEVARIABLEDISPLAY_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <qstring.h>
#include <list>
#include <string>

#include "ClsDataClientConfig.h"
#include "ClsFEGroup.h"
#include "ClsQBaseStateVariableDisplay.h"

class ClsFEDataClient;
class QCheckBox;
class QCloseEvent;
class QDragEnterEvent;
class QDropEvent;
class QLabel;
class QPushButton;
class QWidget;

using namespace iqrcommon;
using namespace std;

class ClsQNeuronStateVariableDisplay : public ClsQBaseStateVariableDisplay {
  Q_OBJECT

public:
  ClsQNeuronStateVariableDisplay(ClsFEDataClient *clsFEDataClient, string strID,
                                 QWidget *parent, ClsFEGroup *_clsFEGroup,
                                 string strSelectedIndices, bool bCloseEnabled,
                                 bool bAverageEnabled, bool bExclusive,
                                 bool bAdditiveDisplay, bool bMiniGroupDisplay);

  ~ClsQNeuronStateVariableDisplay();

  list<string> getSelectedStates();
  string getSelectedStatesAsString();
  void setSelectedStates(list<string> lst);
  void setSelectedStates(string strStates);

  string getItemID() override {
    return getGroupID();
  };
  string getGroupID() {
    return clsFEGroup->getGroupID();
  };
  string getID() {
    return strID;
  };
  string getSelectedIndices() {
    return strSelectedIndices;
  };

  bool useAverage() {
    return bAverage;
  };

  void setConfig(ClsStateVariableDisplayConfig clsStateVariableDisplayConfig)
      override;
  ClsStateVariableDisplayConfig getConfig() override;

  void groupChanged();

signals:
  void sigClose(string);
  void addDisplay(string, list<string>, string);

public
slots:
  void slotClose() override;
  void slotLiveData(bool bToggled) override;

private
slots:
  void slotStateVariables(int ii) override;
  void slotAverage(bool bToggled);
  void startDragging(bool bCopy);

  void slotDropActionAdd();
  void slotDropActionReplace();

private:
  int iCurrentStateButton; // I know this is ugly, but qt doesn't provide any
                           // other mechanisms to find out which button as on
                           // before...
  void dragEnterEvent(QDragEnterEvent *event) override;
  void dropEvent(QDropEvent *event) override;
  void closeEvent(QCloseEvent *) override;

  QLabel *qLabelMiniGroup;

  ClsFEGroup *clsFEGroup;

  string strSelectedIndices;
  bool bExclusiveStates;

  bool bAdditiveDisplay;

  bool bAverage;
  QPushButton *qpbtnGroupInfo;
  QPushButton *qpbtnClose;
  QCheckBox *qchkbxAverage;
  QString qstrDropString;
};

#endif

//// Local Variables:
//// mode: c++
//// End:
