/****************************************************************************
 ** $Filename: ClsFEDataClient.cpp
 ** $Header$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Mon Nov 19 19:05:52 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <utility>

#include "ClsColorPicker.h"
#include "ClsFEDataClient.h"
#include "ClsFEDataManager.h"
#include "ClsFEDataSink.h"
#include "neuronManager.hpp"

class QMutex;
namespace iqrcommon {
class ClsItem;
} // namespace iqrcommon

ClsFEDataClient::ClsFEDataClient(ClsFEDataManager *_clsFEDataManager,
                                 QMutex *_qmutexSysGUI, string _strDataClientID)
    : clsFEDataManager(_clsFEDataManager), qmutexSysGUI(_qmutexSysGUI),
      strDataClientID(std::move(_strDataClientID)) {}

void ClsFEDataClient::addDataSink(string strID, ClsItem *clsItem,
                                  string strParamname, string strRange) {
  ClsDataSinkCopying *clsDataSinkTemp =
      clsFEDataManager->createDataSink(clsItem, strParamname, strRange);

  int iColor = clsColorPicker.getColor();
  clsDataSinkTemp->setColor(iColor);
  pair<string, ClsDataSinkCopying *> pairTemp(strID, clsDataSinkTemp);
  mapDataSinks.insert(pairTemp);
  DataSinkAdded(strID, iColor);
};

void ClsFEDataClient::removeDataSink(string strID) {
  if (mapDataSinks.find(strID) != mapDataSinks.end()) {
    clsColorPicker.putColor((mapDataSinks.find(strID)->second)->getColor());
    mapDataSinks.erase(strID);
  }

  DataSinkRemoved(strID);
};

int ClsFEDataClient::getDataSinkColor(string strID) {
  int iColor = -1;
  if (mapDataSinks.find(strID) != mapDataSinks.end()) {
    iColor = (mapDataSinks.find(strID)->second)->getColor();
  }
  return iColor;
};

void ClsFEDataClient::groupChanged(string _strGroupID) {
  map<string, ClsDataSinkCopying *>::iterator it;
  for (it = mapDataSinks.begin(); it != mapDataSinks.end(); ++it) {
    if (!_strGroupID.compare(it->second->getItemID())) {
      it->second->changeSize();
    }
  }
}

void ClsFEDataClient::init() {
  map<string, ClsDataSinkCopying *>::iterator it;
  for (it = mapDataSinks.begin(); it != mapDataSinks.end(); ++it) {
    it->second->changeSize();
  }
}

void ClsFEDataClient::setSinkMath(string strSinkID, int iFlag) {
  if (mapDataSinks.find(strSinkID) != mapDataSinks.end()) {
    mapDataSinks.find(strSinkID)->second->setMaths(iFlag);
  }
};

void ClsFEDataClient::connectionChanged(string _strConnectionID) {
  map<string, ClsDataSinkCopying *>::iterator it;
  for (it = mapDataSinks.begin(); it != mapDataSinks.end(); ++it) {
    if (!_strConnectionID.compare(it->second->getItemID())) {
      it->second->changeSize();
    }
  }
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
