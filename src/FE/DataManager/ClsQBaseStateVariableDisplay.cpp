#include "ClsQBaseStateVariableDisplay.h"

#include <qcheckbox.h>
#include <qcolor.h>
#include <qlabel.h>
#include <qnamespace.h>
#include <qpalette.h>
#include <algorithm>

class ClsFEDataClient;
class QWidget;

ClsQBaseStateVariableDisplay::ClsQBaseStateVariableDisplay(
    ClsFEDataClient *_clsFEDataClient, string _strID, QWidget *_parent)
    : QFrame(_parent, nullptr), parent(_parent),
      clsFEDataClient(_clsFEDataClient), strID(std::move(_strID)) {

  setAttribute(Qt::WA_DeleteOnClose);
  setAcceptDrops(true);
  this->setFrameStyle(Box | Raised);
  this->setLineWidth(1);

  lblCaption = new QLabel(this);
  QPalette palette;
  palette.setColor(lblCaption->backgroundRole(), QColor(255, 255, 205));
  lblCaption->setPalette(palette);
};

bool ClsQBaseStateVariableDisplay::isLive() {
  return qchkbxLiveData->isChecked();
};
