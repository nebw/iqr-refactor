#ifndef CLSFEDATABROADCASTER_H
#define CLSFEDATABROADCASTER_H

#include <ClsFEDataClient.h>
#include <ClsFEPlotFramework.h>
#include <qhostaddress.h>
#include <qobjectdefs.h>
#include <qwidget.h>
#include <list>
#include <map>
#include <string>

#include "ClsDataClientConfig.h"

class ClsFEDataManager;
class ClsFEGroup;
class ClsQNeuronStateVariableDisplay;
class QCloseEvent;
class QDragEnterEvent;
class QDropEvent;
class QFrame;
class QHBoxLayout;
class QLabel;
class QLineEdit;
class QMutex;
class QPushButton;
class QSpinBox;
class QTimer;
class QUdpSocket;

class ClsFEDataBroadcaster : public ClsFEPlotFramework, public ClsFEDataClient {
  Q_OBJECT

public:
  ClsFEDataBroadcaster(ClsFEDataManager *clsFEDataManager,
                       QMutex *_qmutexSysGUI, string _strDataClientID);
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup, string strRange);
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                 list<string> lstSelectedStates,
                                 string strRange);
  void init() override;

  void setConfig(ClsDataClientConfig clsDataClientConfig) override{};
  ClsDataClientConfig getConfig() override;
  void groupChanged(string strGroupID) override;
  void groupDeleted(string strID) override;
  void close() override {
    QWidget::close();
  };

public
slots:
  string addStateVariableDisplay(string, list<string>, string strRange);
  void removeStateVariableDisplay(string);
  void slotHideControls(bool bIsRight) override;

private
slots:
  void update() override;
  void broadcast(bool b);
  void toggleAllGroups(bool b);

private:
  void dragEnterEvent(QDragEnterEvent *event) override;
  void dropEvent(QDropEvent *event) override;
  void closeEvent(QCloseEvent *) override;

  void DataSinkAdded(string /*strID*/, int /*iColor*/) override{};
  void DataSinkRemoved(string /*strID*/) override{};

  void addAllGroups();
  void removeAllGroups();

  QFrame *qframeStateVariableDisplays;
  QHBoxLayout *boxlayoutStateFrames;
  map<string, ClsQNeuronStateVariableDisplay *> mapStateVariableDisplays;

  int iPort;
  QUdpSocket *qs;
  QHostAddress qa;
  QPushButton *qpbControl;
  QSpinBox *qspinPort;
  QSpinBox *qspinInterval;
  QLineEdit *qleIPReceiver;

  QTimer *qtimer;
  QPushButton *qpbAddAll;

  bool bAllGroups;
  QLabel *qlblAllGroups;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
