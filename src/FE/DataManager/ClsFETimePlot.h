#ifndef CLSFETIMEPLOT_H
#define CLSFETIMEPLOT_H

#include <ClsFEDataClient.h>
#include <ClsFEPlotFramework.h>
#include <qobjectdefs.h>
#include <qwidget.h>
#include <list>
#include <map>
#include <string>

#include "ClsDataClientConfig.h"

class ClsFEDataManager;
class ClsFEGroup;
class ClsQNeuronStateVariableDisplay;
class QCloseEvent;
class QDragEnterEvent;
class QDropEvent;
class QFrame;
class QHBoxLayout;
class QMutex;
class WLinePlot;

class ClsFETimePlot : public ClsFEPlotFramework, public ClsFEDataClient {
  Q_OBJECT

public:
  ClsFETimePlot(ClsFEDataManager *clsFEDataManager, QMutex *_qmutexSysGUI,
                string _strDataClientID);
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup, string strRange);
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                 list<string> lstSelectedStates,
                                 string strRange);
  void init() override;
  void update() override;
  void setConfig(ClsDataClientConfig clsDataClientConfig) override;
  ClsDataClientConfig getConfig() override;
  void groupChanged(string strGroupID) override;
  void groupDeleted(string strID) override;
  void close() override {
    QWidget::close();
  };

public
slots:
  void plotData();
  string addStateVariableDisplay(string, list<string>, string strRange);
  void removeStateVariableDisplay(string);
  void slotHideControls(bool bIsRight) override;

private
slots:

private:
  void dragEnterEvent(QDragEnterEvent *event) override;
  void dropEvent(QDropEvent *event) override;
  void closeEvent(QCloseEvent *) override;

  void DataSinkAdded(string strID, int iColor) override;
  void DataSinkRemoved(string strID) override;

  WLinePlot *wLinePlot;

  QFrame *qframeStateVariableDisplays;
  QHBoxLayout *boxlayoutStateFrames;
  map<string, ClsQNeuronStateVariableDisplay *> mapStateVariableDisplays;
  map<string, int> mapTraces;
  int iTraceLength;
};

#endif

//// Local Variables:
//// mode: c++
//// End:
