/****************************************************************************
 ** $Filename: ClsFEDataManager.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Sat Sep 15 21:00:43 2001
 ** $Date: 2004/02/02 16:48:03 $
 **
 ** $log:
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <ClsFEDataBroadcaster.h>
#include <ClsFEDataClient.h>
#include <ClsFEDataSampler.h>
#include <ClsFEDataSink.h>
#include <ClsFEGroupPlot.h>
#include <ClsFERasterPlot.h>
#include <ClsFESystemManager.h>
#include <ClsFETimePlot.h>
#include <iqrUtils.h>
#include <qfiledialog.h>
#include <qfileinfo.h>
#include <qmessagebox.h>
#include <qstring.h>
#include <qtimer.h>
#include <qwidget.h>
#include <iostream>
#include <list>
#include <utility>
#include <QMutexLocker>

#include "ClsDataClientConfig.h"
#include "ClsDataClientConfigReader.h"
#include "ClsDataClientConfigReaderException.h"
#include "ClsDataClientConfigWriter.h"
#include "ClsFEBasePlot.h"
#include "ClsFEConnection.h"
#include "ClsFEConnectionDiagram.h"
#include "ClsFEDataManager.h"
#include "ClsFEGroup.h"
#include "ClsFEPlotFramework.h"
#include "ClsQLogWindow.h"
#include "ConfigTagLibrary.h"
#include "diagramTypes.h"
#include "idGenerator.hpp"
#include "item.hpp"
#include "neuron.hpp"
#include "stateArray.hpp"
#include "stateVariable.hpp"
#include "synapse.hpp"

class QMutex;

//#define DEBUG_CLSFEDATAMANAGER

ClsFEDataManager *ClsFEDataManager::_instanceDataManager = nullptr;

void ClsFEDataManager::initializeDataManager(QWidget *_parent, const char *name,
                                             QMutex *_qmutexSysGUI) {
  _instanceDataManager = new ClsFEDataManager(_parent, name, _qmutexSysGUI);
}

ClsFEDataManager *ClsFEDataManager::Instance() { return _instanceDataManager; }

ClsFEDataManager::ClsFEDataManager(QWidget *_parent, const char *name,
                                   QMutex *_qmutexSysGUI)
    : QObject(_parent), parent(_parent), qmutexSysGUI(_qmutexSysGUI) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::ClsFEDataManager(...)" << endl;
#endif
  iDataClientCounter = 0;

#ifdef SINGLE_DATASAMPLER
  clsFEDataSampler = nullptr;
  strDataSamplerID = "";
#endif

  clsFEDataBroadcaster = nullptr;
  strDataBroadcasterID = "";

  bSyncPlots = false;

  qtimerPaceMaker = new QTimer(this);
  connect(qtimerPaceMaker, SIGNAL(timeout()), this, SLOT(updateClients()));
};

void ClsFEDataManager::DataClientCreate(int iClientType,
                                        string strSystemElementID,
                                        string strParamName, string strRange) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::DataClientCreate(int iClientType , string "
          "strSystemElementID, string strParamName, int iRange)" << endl;
#endif

  string strBaseID = ClsIDGenerator::Instance()->Next(); //.toStdString();

  string strClientID = "";
  strParamName = "";

  if (iClientType == ClsFEDataClient::CLIENT_SPACEPLOT) {
    ClsFEGroup *clsFEGroup = nullptr;
    if (strSystemElementID.size() > 0) {
      clsFEGroup =
          ClsFESystemManager::Instance()->getFEGroup(strSystemElementID);
    }

    if (clsFEGroup != nullptr) {
      string strValid = clsFEGroup->validate();
      if (strValid.size() > 0) {
        ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                          ClsQLogWindow::TYPE_ERROR, strValid);
      } else {
        strClientID = "GroupPlot:" + strBaseID + (string) ":" +
                      iqrUtils::int2string(iDataClientCounter);

        ClsFEBasePlot *clsFEBasePlot =
            new ClsFEGroupPlot(this, qmutexSysGUI, strClientID, clsFEGroup);
        pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
        mapPlots.insert(pairTemp);
        connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                SIGNAL(sigPlotClosed(string)), this,
                SLOT(slotPlotClosed(string)));
      }
    }
  } else if (iClientType == ClsFEDataClient::CLIENT_TIMEPLOT) {
    ClsFEGroup *clsFEGroup = nullptr;
    if (strSystemElementID.size() > 0) {
      clsFEGroup =
          ClsFESystemManager::Instance()->getFEGroup(strSystemElementID);
    }

    if (clsFEGroup != nullptr) {
      string strValid = clsFEGroup->validate();
      if (strValid.size() > 0) {
        ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                          ClsQLogWindow::TYPE_ERROR, strValid);
      } else {
        strClientID = "TimePlot:" + strBaseID + (string) ":" +
                      iqrUtils::int2string(iDataClientCounter);

        ClsFEBasePlot *clsFEBasePlot =
            new ClsFETimePlot(this, qmutexSysGUI, strClientID);

        if (strSystemElementID.size() > 0) {
          string strOutputStateName = clsFEGroup->getNeuronOutput()->getName();
          list<string> lstSelectedStates;
          lstSelectedStates.push_back(strOutputStateName);
          dynamic_cast<ClsFETimePlot *>(clsFEBasePlot)->addStateVariableDisplay(
              clsFEGroup, lstSelectedStates, strRange);
        }
        pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
        mapPlots.insert(pairTemp);
        connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                SIGNAL(sigPlotClosed(string)), this,
                SLOT(slotPlotClosed(string)));
      }
    }

  } else if (iClientType == ClsFEDataClient::CLIENT_RASTERPLOT) {
    ClsFEGroup *clsFEGroup = nullptr;
    if (strSystemElementID.size() > 0) {
      clsFEGroup =
          ClsFESystemManager::Instance()->getFEGroup(strSystemElementID);
    }

    if (clsFEGroup != nullptr) {
      string strValid = clsFEGroup->validate();
      if (strValid.size() > 0) {
        ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                          ClsQLogWindow::TYPE_ERROR, strValid);
      } else {
        strClientID = "RasterPlot:" + strBaseID + (string) ":" +
                      iqrUtils::int2string(iDataClientCounter);

        ClsFEBasePlot *clsFEBasePlot =
            new ClsFERasterPlot(this, qmutexSysGUI, strClientID);

        if (strSystemElementID.size() > 0) {
          string strOutputStateName = clsFEGroup->getNeuronOutput()->getName();
          list<string> lstSelectedStates;
          lstSelectedStates.push_back(strOutputStateName);
          dynamic_cast<ClsFERasterPlot *>(clsFEBasePlot)
              ->addStateVariableDisplay(clsFEGroup, lstSelectedStates,
                                        strRange);
        }
        pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
        mapPlots.insert(pairTemp);
        connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                SIGNAL(sigPlotClosed(string)), this,
                SLOT(slotPlotClosed(string)));
      }
    }

  } else if (iClientType == ClsFEDataClient::CLIENT_DATASAMPLER) {
    ClsFEGroup *clsFEGroup = nullptr;
    if (strSystemElementID.size() > 0) {
      clsFEGroup =
          ClsFESystemManager::Instance()->getFEGroup(strSystemElementID);
    }

    strClientID = "DataSampler:" + strBaseID + (string) ":" +
                  iqrUtils::int2string(iDataClientCounter);

#ifdef SINGLE_DATASAMPLER
    if (clsFEDataSampler != nullptr) {
      clsFEDataSampler->raise();
    } else {
      clsFEDataSampler = new ClsFEDataSampler(this, qmutexSysGUI, strClientID);
      strDataSamplerID = strClientID;
      connect(clsFEDataSampler, SIGNAL(sigPlotClosed(string)), this,
              SLOT(slotPlotClosed(string)));
    }
#else
    ClsFEDataSampler *clsFEDataSampler =
        new ClsFEDataSampler(this, strClientID);
    pair<string, ClsFEDataSampler *> pairTemp(strClientID, clsFEDataSampler);
    mapDataSamplers.insert(pairTemp);
    connect(dynamic_cast<ClsFEPlotFramework *>(clsFEDataSampler),
            SIGNAL(sigPlotClosed(string)), this, SLOT(slotPlotClosed(string)));
#endif

  } else if (iClientType == ClsFEDataClient::CLIENT_DATABROADCASTER) {
    strClientID = "DataBroadcaster:" + strBaseID + (string) ":" +
                  iqrUtils::int2string(iDataClientCounter);
    if (clsFEDataBroadcaster != nullptr) {
      clsFEDataBroadcaster->raise();
    } else {
      clsFEDataBroadcaster =
          new ClsFEDataBroadcaster(this, qmutexSysGUI, strClientID);
      strDataBroadcasterID = strClientID;
      connect(clsFEDataBroadcaster, SIGNAL(sigPlotClosed(string)), this,
              SLOT(slotPlotClosed(string)));
    }
  } else if (iClientType == diagramTypes::DIAGRAM_CONNECTION) {

    string strSourceID = ClsFESystemManager::Instance()->getConnectionSourceID(
        strSystemElementID);
    string strTargetID = ClsFESystemManager::Instance()->getConnectionTargetID(
        strSystemElementID);

    strClientID = "ConnectionDiagram:" + strBaseID + (string) ":" +
                  iqrUtils::int2string(iDataClientCounter);

    if (strSourceID.length() <= 0 && strTargetID.length() <= 0) {
      ClsQLogWindow::Instance()->report(
          ClsQLogWindow::TARGET_MESSAGE, ClsQLogWindow::TYPE_ERROR,
          "Cannot create diagram.\n"
          "Source and Target Groups do not exist.");
    } else if (strSourceID.length() <= 0) {
      ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                        ClsQLogWindow::TYPE_ERROR,
                                        "Cannot create diagram.\n"
                                        "Source Group does not exist.");

    } else if (strTargetID.length() <= 0) {
      ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                        ClsQLogWindow::TYPE_ERROR,
                                        "Cannot create diagram.\n"
                                        "Target Group does not exist.");
    } else {

      ClsFEConnection *clsFEConnection = nullptr;
      if (strSystemElementID.size() > 0) {
        clsFEConnection =
            ClsFESystemManager::Instance()->getFEConnection(strSystemElementID);
      }

      if (clsFEConnection != nullptr) {

        string strValid = clsFEConnection->validate();
        if (strValid.size() > 0) {
          ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                            ClsQLogWindow::TYPE_ERROR,
                                            strValid);
        } else {

          ClsFEBasePlot *clsFEBasePlot = new ClsFEConnectionDiagram(
              this, qmutexSysGUI, strClientID, strSystemElementID);
          dynamic_cast<ClsFEConnectionDiagram *>(clsFEBasePlot)
              ->createDiagram();
          dynamic_cast<ClsFEConnectionDiagram *>(clsFEBasePlot)
              ->addStateVariableDisplay(clsFEConnection);
          dynamic_cast<ClsFEConnectionDiagram *>(clsFEBasePlot)->show();
          pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
          mapPlots.insert(pairTemp);
          connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                  SIGNAL(sigPlotClosed(string)), this,
                  SLOT(slotPlotClosed(string)));
        }
      }
    }
  }

  iDataClientCounter++;
};

ClsDataSinkCopying *ClsFEDataManager::createDataSink(ClsItem *clsItem,
                                                     string strParamName,
                                                     string strRange) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::createDataSink( string strGroupID, string "
          "strParamName, string strRange )" << endl;
#endif

  /* send new data request to the ClsFECommHandler */
  //	  MESSAGE_SET_VALUE,
  //	  MESSAGE_REQ_PERM,
  //	  MESSAGE_REQ_VOL,
  //	  MESSAGE_CANCEL_REQ

  ClsDataSinkCopying *clsDataSink = nullptr;
  StateArray *pStateArray = nullptr;

  if (dynamic_cast<ClsFEGroup *>(clsItem)) {
    //	cout << "got group" << endl;
    ClsNeuron *pNeuron = dynamic_cast<ClsFEGroup *>(clsItem)->getNeuron();

    try {
      pStateArray = &(pNeuron->getState(strParamName)->getStateArray());
    }
    catch (...) {
      cerr << "Unknown state variable" << endl;
    }
    string strGroupID = dynamic_cast<ClsFEGroup *>(clsItem)->getGroupID();

    if (strRange.length() > 0) {
      clsDataSink = new ClsDataSinkCopying(strGroupID, strParamName,
                                           pStateArray, strRange);
    } else {
      clsDataSink =
          new ClsDataSinkCopying(strGroupID, strParamName, pStateArray);
    }

  } else if (dynamic_cast<ClsFEConnection *>(clsItem)) {
    string strConnectionID =
        dynamic_cast<ClsFEConnection *>(clsItem)->getConnectionID();
    ClsSynapse *pSynapse =
        dynamic_cast<ClsFEConnection *>(clsItem)->getSynapse();
    try {
      pStateArray = &(pSynapse->getState(strParamName)->getStateArray());
    }
    catch (...) {
      cerr << "Unknown state variable" << endl;
    }
    clsDataSink =
        new ClsDataSinkCopying(strConnectionID, strParamName, pStateArray);
  }

  /* not needed if no network
     ClsFEMessageOut clsFEMessageOut ( ClsProtocol::PROTO_CMD_REQUEST_PERMANENT,
     strGroupID, strParamName, clsDataSink );
     Mediator->addDataRequest(strPrcID, clsFEMessageOut); //KILL
  */

  return clsDataSink;
};

//////////////

void ClsFEDataManager::slotItemChanged(int iType, string strID) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::slotItemChanged()" << endl;
#endif

  if (iType == ClsFESystemManager::ITEM_GROUP) {
    map<string, ClsFEBasePlot *>::iterator it;
    for (it = mapPlots.begin(); it != mapPlots.end(); ++it) {
      it->second->groupChanged(strID);
    }

#ifdef SINGLE_DATASAMPLER
    if (clsFEDataSampler != nullptr) {
      clsFEDataSampler->groupChanged(strID);
    }
#endif
    if (clsFEDataBroadcaster != nullptr) {
      clsFEDataBroadcaster->groupChanged(strID);
    }

  } else if (iType == ClsFESystemManager::ITEM_CONNECTION) {
    map<string, ClsFEBasePlot *>::iterator it;
    for (it = mapPlots.begin(); it != mapPlots.end(); ++it) {
      it->second->connectionChanged(strID);
    }
  }
}

void ClsFEDataManager::slotItemDeleted(int iType, string strID) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::slotItemDeleted(int iType, string strID )" << endl;
#endif

  if (iType == ClsFESystemManager::ITEM_GROUP) {
    map<string, ClsFEBasePlot *>::iterator mapIteratorPlots;
    for (mapIteratorPlots = mapPlots.begin();
         mapIteratorPlots != mapPlots.end(); ++mapIteratorPlots) {
      mapIteratorPlots->second->groupDeleted(strID);
    }

#ifdef SINGLE_DATASAMPLER
    if (clsFEDataSampler != nullptr) {
      clsFEDataSampler->groupDeleted(strID);
    }
#endif
    if (clsFEDataBroadcaster != nullptr) {
      clsFEDataBroadcaster->groupDeleted(strID);
    }

  } else if (iType == ClsFESystemManager::ITEM_CONNECTION) {
    map<string, ClsFEBasePlot *>::iterator mapIteratorPlots;
    for (mapIteratorPlots = mapPlots.begin();
         mapIteratorPlots != mapPlots.end(); ++mapIteratorPlots) {
      mapIteratorPlots->second->connectionDeleted(strID);
    }
  }
}

[[deprecated]]
void ClsFEDataManager::cancelDataRequest(
    ClsDataSinkCopying * /* clsFEDataSink */, string /* strGroupID */,
    string /* strParamName */, string /* strRange*/) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::cancelDataRequest( ClsDataSinkCopying* "
          "clsFEDataSink, string strGroupID, string strParamName, string "
          "strRange)" << endl;
#endif
};

void ClsFEDataManager::customEvent(QEvent *e) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::customEvent(QEvent *e)" << endl;
#endif
  if (e->type() == EVENT_UPDATE) {
    updateClients();
  }
}

void ClsFEDataManager::updateClients() {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::updateClients()" << endl;
#endif
  QMutexLocker locker(qmutexSysGUI);

  if (!bPaused) {
    map<string, ClsFEBasePlot *>::iterator mapIteratorPlots;
    for (mapIteratorPlots = mapPlots.begin();
         mapIteratorPlots != mapPlots.end(); ++mapIteratorPlots) {
      mapIteratorPlots->second->update();
    }
  }
};

void ClsFEDataManager::start(bool _bSyncPlots) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::start()" << endl;
#endif

  bPaused = false;
  bSyncPlots = _bSyncPlots;

  map<string, ClsFEBasePlot *>::iterator mapIteratorPlots;
  for (mapIteratorPlots = mapPlots.begin(); mapIteratorPlots != mapPlots.end();
       ++mapIteratorPlots) {
    mapIteratorPlots->second->init();
  }

  startDataSampler();

  if (!bSyncPlots) {
    qtimerPaceMaker->setSingleShot(false);
    qtimerPaceMaker->start(100);
  }
};

void ClsFEDataManager::stop() {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::stop()" << endl;
#endif

  stopDataSampler();

  if (!bSyncPlots) {
    qtimerPaceMaker->stop();
  }
  bPaused = false;
};

void ClsFEDataManager::pause(bool b) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::pause(bool b)" << endl;
#endif

  bPaused = b;
};

void ClsFEDataManager::startDataSampler(bool bForce) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::startDataSampler()" << endl;
#endif

#ifdef SINGLE_DATASAMPLER
  if (clsFEDataSampler != nullptr) {
    if (clsFEDataSampler->isAutoStart() || bForce) {
      clsFEDataSampler->slotStartSampling();
    }
  }
#else
  map<string, ClsFEDataSampler *>::iterator it;
  for (it = mapDataSamplers.begin(); it != mapDataSamplers.end(); ++it) {
    if (it->second->isAutoStart() || bForce) {
      it->second->slotStartSampling();
    }
  }
#endif
}

void ClsFEDataManager::stopDataSampler(bool bForce) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::stopDataSampler()" << endl;
#endif
#ifdef SINGLE_DATASAMPLER
  if (clsFEDataSampler != nullptr) {
    if (clsFEDataSampler->isAutoStart() || bForce) {
      clsFEDataSampler->slotStopSampling();
    }
  }
#else
  map<string, ClsFEDataSampler *>::iterator it;
  for (it = mapDataSamplers.begin(); it != mapDataSamplers.end(); ++it) {
    if (it->second->isAutoStart() || bForce) {
      it->second->slotStopSampling();
    }
  }
#endif
}

void ClsFEDataManager::slotPlotClosed(string strID) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::slotPlotClosed(string strID)" << endl;
#endif

  if (!strID.compare(strDataSamplerID)) {
    clsFEDataSampler = nullptr;
  } else if (!strID.compare(strDataBroadcasterID)) {
    clsFEDataBroadcaster = nullptr;
  } else {
    map<string, ClsFEBasePlot *>::iterator it;
    it = mapPlots.find(strID);
    if (it != mapPlots.end()) {
      mapPlots.erase(it);
    }
  }
  //    cout << "mapPlots.size(): " << mapPlots.size() << endl;
}

void ClsFEDataManager::closeAllPlots() {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::closeAllPlots()" << endl;
#endif

#ifdef SINGLE_DATASAMPLER
  if (clsFEDataSampler != nullptr) {
    clsFEDataSampler->close();
  }
#else
  map<string, ClsFEDataSampler *>::iterator itDataSampler;
  for (itDataSampler = mapDataSamplers.begin();
       itDataSampler != mapDataSamplers.end(); itDataSampler++) {
    if (itDataSampler->second != NULL) {
      itDataSampler->second->close();
    }
  }

#endif

  if (clsFEDataBroadcaster != nullptr) {
    clsFEDataBroadcaster->close();
  }
}

void ClsFEDataManager::saveConfig() {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::saveConfig()" << endl;
#endif

  QString qstrFileName = "";
  bool bValidFile = false;

  while (!bValidFile) {
    qstrFileName = QFileDialog::getSaveFileName(
        nullptr, /*parent*/
        "Save File"
        "Choose a filename to save under", /*caption*/
        "",                                /* dir */
        "configuration (*.conf)"           /* filter */
        );

    /* append default extension here */
    QString qstrExtension(".conf");
    QString qstrCurrentTail = qstrFileName.right(qstrExtension.length());
    if (qstrCurrentTail.compare(qstrExtension)) {
      qstrFileName.append(qstrExtension);
    }
    /* ------------------------------ */

    QFileInfo qFileInfo(qstrFileName);
    QString qstrPath = qFileInfo.absolutePath();

    qFileInfo.setFile(qstrPath);
    if (!qFileInfo.isWritable()) {
      int iReturn = QMessageBox::critical(
          nullptr, "iqr", "No permission to write in this directory\n", "Retry",
          "Cancel", nullptr, 0, 1);
      if (iReturn == 1) {
        return;
      }
    } else {
      bValidFile = true;
    }
  }

  if (qstrFileName.length() <= 0) {
    return;
  }

  string strFileName = qstrFileName.toStdString();

  list<ClsDataClientConfig> lstConfigs;

  map<string, ClsFEBasePlot *>::iterator it;
  for (it = mapPlots.begin(); it != mapPlots.end(); ++it) {
    ClsDataClientConfig clsDataClientConfig = it->second->getConfig();

    string strType = "";
    if (dynamic_cast<ClsFEGroupPlot *>(it->second)) {
      strType = ConfigTagLibrary::PlotTypeGroup();
    } else if (dynamic_cast<ClsFETimePlot *>(it->second)) {
      strType = ConfigTagLibrary::PlotTypeTime();
    } else if (dynamic_cast<ClsFERasterPlot *>(it->second)) {
      strType = ConfigTagLibrary::PlotTypeRaster();
    } else if (dynamic_cast<ClsFEConnectionDiagram *>(it->second)) {
      strType = ConfigTagLibrary::PlotTypeConnection();
    } else {
      strType = "UnknownType";
    }
    clsDataClientConfig.setType(strType);
    lstConfigs.push_back(clsDataClientConfig);
  }

#ifdef SINGLE_DATASAMPLER
  if (clsFEDataSampler != nullptr) {
    ClsDataClientConfig clsDataClientConfigDataSampler =
        clsFEDataSampler->getConfig();
    clsDataClientConfigDataSampler.setType(ConfigTagLibrary::DataSampler());
    lstConfigs.push_back(clsDataClientConfigDataSampler);
  }
#endif

  ClsDataClientConfigWriter clsDataClientConfigWriter;
  if (!clsDataClientConfigWriter.saveConfig(strFileName, lstConfigs)) {
    string strError = "Error writing configuration\n";
    ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                      ClsQLogWindow::TYPE_WARNING, strError);
  }
};

void ClsFEDataManager::applyConfig(string strFileName) {
#ifdef DEBUG_CLSFEDATAMANAGER
  cout << "ClsFEDataManager::applyConfig()" << endl;
#endif

  bool bError = false;

  if (strFileName.size() <= 0) {
    QString qstrFileName =
        QFileDialog::getOpenFileName(nullptr, /*parent*/
                                     "open file dialog"
                                     "Choose a file",         /*caption*/
                                     "",                      /*dir*/
                                     "configuration (*.conf)" /*filter*/
                                     );

    if (qstrFileName.length() > 0) {
      strFileName = qstrFileName.toStdString();
    } else {
      return;
    }
  }

  if (strFileName.size() > 0) {
    ClsDataClientConfigReader clsDataClientConfigReader;

    list<ClsDataClientConfig> lstConfigs;
    try {
      lstConfigs = clsDataClientConfigReader.getDataClientConfig(strFileName);
    }
    catch (ClsDataClientConfigReaderException &e) {
      string strError =
          (string) "Error reading configuration\n" + e.getMessage();
      ClsQLogWindow::Instance()->report(ClsQLogWindow::TARGET_MESSAGE,
                                        ClsQLogWindow::TYPE_ERROR, strError);
    }

    if (!bError) {
      list<ClsDataClientConfig>::iterator itConfigs;
      for (itConfigs = lstConfigs.begin(); itConfigs != lstConfigs.end();
           ++itConfigs) {
        string strType = (*itConfigs).getType();
        int iClientType = -99;

        if (!strType.compare(ConfigTagLibrary::PlotTypeGroup())) {
          iClientType = ClsFEDataClient::CLIENT_SPACEPLOT;
        } else if (!strType.compare(ConfigTagLibrary::PlotTypeTime())) {
          iClientType = ClsFEDataClient::CLIENT_TIMEPLOT;
        } else if (!strType.compare(ConfigTagLibrary::PlotTypeRaster())) {
          iClientType = ClsFEDataClient::CLIENT_RASTERPLOT;
        } else if (!strType.compare(ConfigTagLibrary::PlotTypeConnection())) {
          iClientType = diagramTypes::DIAGRAM_CONNECTION;
        } else if (!strType.compare(ConfigTagLibrary::DataSampler())) {
          iClientType = ClsFEDataClient::CLIENT_DATASAMPLER;
        }

        string strClientID = (*itConfigs).getID();

        pair<int, int> pPosition = (*itConfigs).getPosition();
        pair<int, int> pGeometry = (*itConfigs).getGeometry();

        if (iClientType == ClsFEDataClient::CLIENT_SPACEPLOT) {
          ClsFEBasePlot *clsFEBasePlot =
              new ClsFEGroupPlot(this, qmutexSysGUI, strClientID);
          clsFEBasePlot->setConfig((*itConfigs));
          clsFEBasePlot->resize(pGeometry.first, pGeometry.second);
          clsFEBasePlot->move(pPosition.first, pPosition.second);

          pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
          mapPlots.insert(pairTemp);
          connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                  SIGNAL(sigPlotClosed(string)), this,
                  SLOT(slotPlotClosed(string)));
        } else if (iClientType == ClsFEDataClient::CLIENT_TIMEPLOT) {
          ClsFEBasePlot *clsFEBasePlot =
              new ClsFETimePlot(this, qmutexSysGUI, strClientID);
          clsFEBasePlot->setConfig((*itConfigs));
          clsFEBasePlot->resize(pGeometry.first, pGeometry.second);
          clsFEBasePlot->move(pPosition.first, pPosition.second);

          pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
          mapPlots.insert(pairTemp);
          connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                  SIGNAL(sigPlotClosed(string)), this,
                  SLOT(slotPlotClosed(string)));
        } else if (iClientType == ClsFEDataClient::CLIENT_RASTERPLOT) {
          ClsFEBasePlot *clsFEBasePlot =
              new ClsFERasterPlot(this, qmutexSysGUI, strClientID);
          clsFEBasePlot->setConfig((*itConfigs));
          clsFEBasePlot->resize(pGeometry.first, pGeometry.second);
          clsFEBasePlot->move(pPosition.first, pPosition.second);

          pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
          mapPlots.insert(pairTemp);
          connect(dynamic_cast<ClsFEPlotFramework *>(clsFEBasePlot),
                  SIGNAL(sigPlotClosed(string)), this,
                  SLOT(slotPlotClosed(string)));
        } else if (iClientType == diagramTypes::DIAGRAM_CONNECTION) {
          ClsFEBasePlot *clsFEBasePlot =
              new ClsFEConnectionDiagram(this, qmutexSysGUI, strClientID, "");
          clsFEBasePlot->setConfig((*itConfigs));
          clsFEBasePlot->resize(pGeometry.first, pGeometry.second);
          clsFEBasePlot->move(pPosition.first, pPosition.second);

          pair<string, ClsFEBasePlot *> pairTemp(strClientID, clsFEBasePlot);
          mapPlots.insert(pairTemp);
          connect(dynamic_cast<ClsFEConnectionDiagram *>(clsFEBasePlot),
                  SIGNAL(sigPlotClosed(string)), this,
                  SLOT(slotPlotClosed(string)));
        } else if (iClientType == ClsFEDataClient::CLIENT_DATASAMPLER) {
          if (clsFEDataSampler != nullptr) {
            clsFEDataSampler->raise();
          } else {
            clsFEDataSampler =
                new ClsFEDataSampler(this, qmutexSysGUI, strClientID);
            strDataSamplerID = strClientID;
            connect(clsFEDataSampler, SIGNAL(sigPlotClosed(string)), this,
                    SLOT(slotPlotClosed(string)));
          }
          clsFEDataSampler->setConfig((*itConfigs));
          clsFEDataSampler->resize(pGeometry.first, pGeometry.second);
          clsFEDataSampler->move(pPosition.first, pPosition.second);
        }
      }
    }
  }
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
