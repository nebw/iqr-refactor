/****************************************************************************
 ** $Filename: ClsFEDataManager.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Sat Sep 15 20:59:34 2001
 ** $Date: 2003/12/22 21:06:02 $
 **
 ** $log:
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSFEDATAMGR_H
#define CLSFEDATAMGR_H /*+ To stop multiple inclusions. +*/

#include <qcoreevent.h>
#include <qobject.h>
#include <qobjectdefs.h>
#include <map>
#include <string>

#include "neuronManager.hpp"

class ClsDataSinkCopying;
class ClsFEBasePlot;
class ClsFEDataBroadcaster;
class ClsFEDataSampler;
class QMutex;
class QTimer;
class QWidget;
namespace iqrcommon {
class ClsItem;
} // namespace iqrcommon

#define SINGLE_DATASAMPLER

using namespace std;
using namespace iqrcommon;

class ClsFEDataManager : public QObject {
  Q_OBJECT
public:
  static const QEvent::Type EVENT_UPDATE =
      static_cast<QEvent::Type>(QEvent::User + 1000);

  static ClsFEDataManager *Instance();
  static void initializeDataManager(QWidget *_parent, const char *name,
                                    QMutex *_qmutexSysGUI);

  void closeAllPlots();

  void customEvent(QEvent *) override;

public
slots:
  void updateClients();
  void DataClientCreate(int iClientType, string strSystemElementID,
                        string strParamName, string strRange);
  ClsDataSinkCopying *createDataSink(ClsItem *clsItem, string strParamName,
                                     string strRange);
  void cancelDataRequest(ClsDataSinkCopying *clsDataSink, string strGroupID,
                         string strParamName, string strRange); // TODO
  void start(bool _bSyncPlots);
  void stop();
  void pause(bool b);

  void startDataSampler(bool bForce = false);
  void stopDataSampler(bool bForce = false);

  void slotItemChanged(int iType, string strID);
  void slotItemDeleted(int iType, string strID);
  void saveConfig();
  void applyConfig(string strFilename = "");

private
slots:
  void slotPlotClosed(string strID);

private:
  ClsFEDataManager(QWidget *parent, const char *name, QMutex *_qmutexSysGUI);
  static ClsFEDataManager *_instanceDataManager;

  QTimer *qtimerPaceMaker;
  QWidget *parent;

  QMutex *qmutexSysGUI;
  int iDataClientCounter;
  bool bSyncPlots;

  map<string, ClsFEBasePlot *> mapPlots;

#ifdef SINGLE_DATASAMPLER
  ClsFEDataSampler *clsFEDataSampler;
  string strDataSamplerID;
#else
  map<string, ClsFEDataSampler *> mapDataSamplers;
#endif

  ClsFEDataBroadcaster *clsFEDataBroadcaster;
  string strDataBroadcasterID;

  friend class ClsFEDataSampler;

  bool bPaused;
};

#endif /* CLSFEDATAMGR_H */

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
