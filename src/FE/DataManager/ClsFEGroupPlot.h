/****************************************************************************
 ** $Filename: ClsFEGroupPlot.h
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Oct 26 01:32:22 2001
 ** $Date: 2003/10/04 09:50:48 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSFEGROUPPLOT_H
#define CLSFEGROUPPLOT_H

#include <ClsFEDataClient.h>
#include <ClsFEPlotFramework.h>
#include <qobjectdefs.h>
#include <qwidget.h>
#include <list>
#include <string>
#include <vector>

#include "ClsDataClientConfig.h"
#include "neuronManager.hpp"

class ClsFEDataManager;
class ClsFEGroup;
class QCloseEvent;
class QDragEnterEvent;
class QDropEvent;
class QMutex;
class WSpacePlot;
template <class T> class QVector;

class ClsFEGroupPlot : /* public ClsFEBasePlot, */ public ClsFEPlotFramework,
                       public ClsFEDataClient {
  Q_OBJECT

public:
  ClsFEGroupPlot(ClsFEDataManager *clsFEDataManager, QMutex *_qmutexSysGUI,
                 string strDataClientID, ClsFEGroup *_clsFEGroup = nullptr);

  void init() override;
  void update() override;
  void setConfig(ClsDataClientConfig clsDataClientConfig) override;
  void groupChanged(string strGroupID) override;
  void groupDeleted(string strID) override;
  void close() override {
    QWidget::close();
  };
  ClsDataClientConfig getConfig() override;

public
slots:
  void plotData();
  string addStateVariableDisplay(string, list<string>, string strRange);

private
slots:
  void slotLiveData(bool);
  void startDragging();

private:
  void dragEnterEvent(QDragEnterEvent *event) override;
  void dropEvent(QDropEvent *event) override;
  void closeEvent(QCloseEvent *) override;
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup, string strRange);
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                 list<string> lstSelectedStates,
                                 string strRange);
  void DataSinkAdded(string strID, int iColor) override;
  string wSpacePlotSelected2Range(QVector<int> &qvectorSelected);

  void getCellIndices(ClsFEGroup *_clsFEGroup);

  int iGroupWidth, iGroupHeight;

  /* sparse topology related */
  vector<int> vectCellIndices;
  bool bSparseTopology;
  /* ------ */

  string strGroupID;

  WSpacePlot *wSpacePlot;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd /home/ulysses/Code/iqr421_Redesign/FE/IQR/ && make -k
///"
//// End:
