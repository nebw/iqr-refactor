/****************************************************************************
 ** $Filename: ClsFEPlotFramework.cpp
 ** $Id$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Wed Nov  5 01:02:26 2003
 ** $Date$
 **
 ** $Log$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <ClsFEPlotFramework.h>
#include <ClsQDivider.h>
#include <ClsQNeuronStateVariableDisplay.h>
#include <qgridlayout.h>
#include <qlist.h>
#include <qstring.h>
#include <iostream>
#include <list>

#include "ClsQBaseStateVariableDisplay.h"

ClsFEPlotFramework::ClsFEPlotFramework() : QWidget(nullptr) {
  clsQBaseStateVariableDisplay = nullptr;

  boxlayoutBase = new QGridLayout(this); //, 1, 3, 0, 3, "boxlayoutBase");
  boxlayoutBase->setSpacing(1);
  boxlayoutBase->setContentsMargins(0, 0, 0, 0);

  /* the divider inbetween the left and right pane */
  clsQDivider = new ClsQDivider(this);
  boxlayoutBase->addWidget(clsQDivider, 0, 1);
  connect(clsQDivider, SIGNAL(clicked(bool)), SLOT(slotHideControls(bool)));
  /* ----------------------- */
};

void ClsFEPlotFramework::slotHideControls(bool bIsRight) {
  if (bIsRight && clsQBaseStateVariableDisplay != nullptr) {
    clsQBaseStateVariableDisplay->hide();
  } else {
    clsQBaseStateVariableDisplay->show();
  }
};

[[deprecated]]
void ClsFEPlotFramework::print() {
  cout << "ClsFEPlotFramework::print()" << endl;
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../.. && make -k "
//// End:
