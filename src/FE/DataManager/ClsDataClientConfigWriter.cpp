#include <qdom.h>
#include <qfile.h>
#include <qflags.h>
#include <qiodevice.h>
#include <qstring.h>
#include <qtextstream.h>
#include <iostream>
#include <utility>

#include "ClsDataClientConfig.h"
#include "ClsDataClientConfigWriter.h"
#include "ConfigTagLibrary.h"
#include "iqrUtils.h"

ClsDataClientConfigWriter::ClsDataClientConfigWriter() {
  bXMLPlatformInitialized = false;
};

bool
ClsDataClientConfigWriter::saveConfig(string strFileName,
                                      list<ClsDataClientConfig> lstConfigs) {
  if (!bXMLPlatformInitialized) {
    bXMLPlatformInitialized = true;
  }

  QDomImplementation impl;
  QDomDocumentType dtd = impl.createDocumentType(
      QString::fromStdString(ConfigTagLibrary::DataManagerConfiguration()),
      QString("-//INI/iqr421"), QString("iqrDataManagerConfiguration.dtd"));

  QDomDocument ddocConfig = impl.createDocument(
      nullptr,
      QString::fromStdString(ConfigTagLibrary::DataManagerConfiguration()),
      dtd);

  QDomElement delemConfig;
  delemConfig = ddocConfig.documentElement();

  list<ClsDataClientConfig>::iterator itConfigs;
  for (itConfigs = lstConfigs.begin(); itConfigs != lstConfigs.end();
       ++itConfigs) {
    string strID = (*itConfigs).getID();
    string strType = (*itConfigs).getType();
    pair<int, int> pPosition = (*itConfigs).getPosition();
    pair<int, int> pGeometry = (*itConfigs).getGeometry();

    QDomElement delemDataClient;
    delemDataClient = ddocConfig.createElement(
        QString::fromStdString(ConfigTagLibrary::DataClientTag()));
    delemDataClient.setAttribute(
        QString::fromStdString(ConfigTagLibrary::TypeTag()),
        QString::fromStdString(strType.c_str()));
    delemDataClient.setAttribute(
        QString::fromStdString(ConfigTagLibrary::IDTag()),
        QString::fromStdString(strID.c_str()));
    delemConfig.appendChild(delemDataClient);

    QDomElement delemPosition;
    delemPosition = ddocConfig.createElement(
        QString::fromStdString(ConfigTagLibrary::PositionTag()));
    delemPosition.setAttribute(
        QString::fromStdString(ConfigTagLibrary::PositionXTag()),
        QString::fromStdString(iqrUtils::int2string(pPosition.first).c_str()));
    delemPosition.setAttribute(
        QString::fromStdString(ConfigTagLibrary::PositionYTag()),
        QString::fromStdString(iqrUtils::int2string(pPosition.second).c_str()));
    delemDataClient.appendChild(delemPosition);

    QDomElement delemGeometry;
    delemGeometry = ddocConfig.createElement(
        QString::fromStdString(ConfigTagLibrary::Geometry()));
    delemGeometry.setAttribute(
        QString::fromStdString(ConfigTagLibrary::GeometryWidthTag()),
        QString::fromStdString(iqrUtils::int2string(pGeometry.first).c_str()));
    delemGeometry.setAttribute(
        QString::fromStdString(ConfigTagLibrary::GeometryHeightTag()),
        QString::fromStdString(iqrUtils::int2string(pGeometry.second).c_str()));
    delemDataClient.appendChild(delemGeometry);

    list<pair<string, string> > lstParameters =
        (*itConfigs).getListParameters();
    list<pair<string, string> >::iterator itLstParameters;
    for (itLstParameters = lstParameters.begin();
         itLstParameters != lstParameters.end(); ++itLstParameters) {
      string strParamName = (*itLstParameters).first;
      string strParamValue = (*itLstParameters).second;
      QDomElement delemParameter;
      delemParameter = ddocConfig.createElement(
          QString::fromStdString(strParamName.c_str()));
      delemDataClient.appendChild(delemParameter);
      QDomText dtxtParamValue = ddocConfig.createTextNode(
          QString::fromStdString(strParamValue.c_str()));
      delemParameter.appendChild(dtxtParamValue);
    }

    QDomElement delemSVD;
    delemSVD = ddocConfig.createElement(
        QString::fromStdString(ConfigTagLibrary::StateVariableDisplayTag()));
    delemDataClient.appendChild(delemSVD);

    list<ClsStateVariableDisplayConfig> lstSVDConfigs =
        (*itConfigs).getListStateVariableDisplayConfig();
    list<ClsStateVariableDisplayConfig>::iterator itSVD;
    for (itSVD = lstSVDConfigs.begin(); itSVD != lstSVDConfigs.end(); ++itSVD) {
      QDomElement delemStateVariable;
      delemStateVariable = ddocConfig.createElement(
          QString::fromStdString(ConfigTagLibrary::StateVariableDisplaysTag()));
      delemSVD.appendChild(delemStateVariable);
      delemStateVariable.setAttribute(
          QString::fromStdString(ConfigTagLibrary::IDTag()),
          QString::fromStdString((*itSVD).getID().c_str()));
      delemStateVariable.setAttribute(
          QString::fromStdString(ConfigTagLibrary::ItemIDTag()),
          QString::fromStdString((*itSVD).getItemID().c_str()));

      delemStateVariable.setAttribute(
          QString::fromStdString(ConfigTagLibrary::SelectedIndicesTag()),
          QString::fromStdString((*itSVD).getSelectedIndices().c_str()));

      list<pair<string, string> > lstParametersSVD =
          (*itSVD).getListParameters();
      list<pair<string, string> >::iterator itLstParametersSVD;
      for (itLstParametersSVD = lstParametersSVD.begin();
           itLstParametersSVD != lstParametersSVD.end(); ++itLstParametersSVD) {
        string strParamName = (*itLstParametersSVD).first;
        string strParamValue = (*itLstParametersSVD).second;
        QDomElement delemParameter;
        delemParameter = ddocConfig.createElement(
            QString::fromStdString(strParamName.c_str()));
        delemStateVariable.appendChild(delemParameter);
        QDomText dtxtParamValue = ddocConfig.createTextNode(
            QString::fromStdString(strParamValue.c_str()));
        delemParameter.appendChild(dtxtParamValue);
      }
    }
  }

  QFile qfileTarget(QString::fromStdString(strFileName.c_str()));

  if (!qfileTarget.open(QIODevice::WriteOnly | QIODevice::Text)) {
    cerr << "ClsDataClientConfigWriter::saveConfig: ERROR opening file: "
         << strFileName.c_str() << endl;
  }
  QTextStream qtxtstrOutput(&qfileTarget);
  delemConfig.save(qtxtstrOutput, 4);
  qfileTarget.close();

  return true;
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
