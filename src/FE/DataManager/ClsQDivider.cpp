#include <ClsQDivider.h>
#include <divide_left_6x40.xpm>
#include <divide_right_6x40.xpm>
#include <qpixmap.h>
#include <qsizepolicy.h>

class QWidget;

ClsQDivider::ClsQDivider(QWidget *parent) : QPushButton(parent) {
  bIsRight = false;
  setIcon(QPixmap(divide_right));
  setFixedWidth(10);

  setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);

  connect(this, SIGNAL(clicked()), SLOT(slotMove()));
};

void ClsQDivider::slotMove() {
  if (bIsRight) {
    bIsRight = false;
    setIcon(QPixmap(divide_right));
  } else {
    bIsRight = true;
    setIcon(QPixmap(divide_left));
  }
  emit clicked(bIsRight);
};

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
