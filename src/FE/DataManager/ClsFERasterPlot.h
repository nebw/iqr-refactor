#ifndef CLSFERASTERPLOT_H
#define CLSFERASTERPLOT_H

//$$$#include <ClsFEBasePlot.h>
#include <ClsFEDataClient.h>
#include <ClsFEPlotFramework.h>
#include <qobjectdefs.h>
#include <qwidget.h>
#include <list>
#include <string>

#include "ClsDataClientConfig.h"

class ClsFEDataManager;
class ClsFEGroup;
class QCloseEvent;
class QDragEnterEvent;
class QDropEvent;
class QFrame;
class QHBoxLayout;
class QMutex;
class QSplitter;
class QToolButton;
class QVBoxLayout;
class WBarPlot;
class WGraphAxisLabel;
class WRasterPlot;

class ClsFERasterPlot : public ClsFEPlotFramework, public ClsFEDataClient {
  Q_OBJECT

public:
  ClsFERasterPlot(ClsFEDataManager *clsFEDataManager, QMutex *_qmutexSysGUI,
                  string _strDataClientID);
  ~ClsFERasterPlot();
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup, string strRange);
  string addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                 list<string> lstSelectedStates,
                                 string strRange);
  void init() override;
  void update() override;
  void setConfig(ClsDataClientConfig clsDataClientConfig) override;
  ClsDataClientConfig getConfig() override;
  void groupChanged(string strGroupID) override;
  void groupDeleted(string strID) override;
  void close() override {
    QWidget::close();
  };

public
slots:
  void plotData();
  string addStateVariableDisplay(string, list<string>, string strRange);
  void slotHideControls(bool bIsRight) override;

protected
slots:
  void setZoomEnabled(bool zoom);

private
slots:

private:
  void dragEnterEvent(QDragEnterEvent *event) override;
  void dropEvent(QDropEvent *event) override;
  void closeEvent(QCloseEvent *) override;

  void DataSinkAdded(string strID, int iColor) override;
  void DataSinkRemoved(string strID) override;

  QSplitter *qSplitter;
  WBarPlot *wBarPlot;
  WRasterPlot *wRasterPlot;

  QToolButton *zoomButton;
  WGraphAxisLabel *xLabel;

  QFrame *qFramePlots, *qframeStateVariableDisplays;
  QVBoxLayout *boxlayoutPlotsFrame;
  QHBoxLayout *boxlayoutStateFrames;
  string strGroupID;
  int iTraceLength;
  int iRoundCounter;
  double fMovingAverage;
};

#endif

//// Local Variables:
//// mode: c++
//// End:
