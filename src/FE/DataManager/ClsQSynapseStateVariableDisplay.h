/****************************************************************************
 ** $Filename: ClsQSynapseStateVariableDisplay.h
 ** $Header:
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate:
 ** $Date:
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSQSYNAPSESTATEVARIABLEDISPLAY_H
#define CLSQSYNAPSESTATEVARIABLEDISPLAY_H /*+ To stop multiple inclusions. +*/

#include <qobjectdefs.h>
#include <qstring.h>
#include <list>
#include <string>

#include "ClsDataClientConfig.h"
#include "ClsFEConnection.h"
#include "ClsQBaseStateVariableDisplay.h"

class ClsFEDataClient;
class QCloseEvent;
class QGroupBox;
class QPushButton;
class QWidget;

using namespace iqrcommon;
using namespace std;

class ClsQSynapseStateVariableDisplay : public ClsQBaseStateVariableDisplay {
  Q_OBJECT

public:
  ClsQSynapseStateVariableDisplay(ClsFEDataClient *clsFEDataClient,
                                  string strID, QWidget *parent,
                                  ClsFEConnection *_clsFEConnection,
                                  string strSelectedIndices);

  ~ClsQSynapseStateVariableDisplay();

  enum {
    TYPE_DISTANCE,
    TYPE_DELAY,
    TYPE_ATTENUATION,
    TYPE_SYNAPSESTATE
  };

  list<string> getSelectedStates();
  string getSelectedStatesAsString();
  void setSelectedStates(list<string> lst);
  void setSelectedStates(string strStates);

  string getItemID() override {
    return getConnectionID();
  };
  string getConnectionID() {
    return clsFEConnection->getConnectionID();
  };
  string getID() {
    return strID;
  };
  string getSelectedIndices() {
    return strSelectedIndices;
  };

  void setConfig(ClsStateVariableDisplayConfig clsStateVariableDisplayConfig)
      override;
  ClsStateVariableDisplayConfig getConfig() override;

  void connectionChanged();

signals:
  void sigChangeType(int);
  void sigLiveData(bool);
  void sigClose(string);
  void addDisplay(string, list<string>, string);

public
slots:
  void slotClose() override;

private
slots:
  void slotLiveData(bool bToggled) override;
  void slotStateVariables(int ii) override;

  void slotSetType(int ii);

private:
  int iCurrentStateButton; // I know this is ugly, but qt doesn't provide any
                           // other mechanisms to find out which button as on
                           // before...
  void closeEvent(QCloseEvent *) override;

  ClsFEConnection *clsFEConnection;

  string strSelectedIndices;

  QGroupBox *qgrpbxDisplay;
  QPushButton *qpbtnConnectionInfo;
  QString qstrDropString;
};

#endif

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
