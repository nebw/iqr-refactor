/****************************************************************************
 ** $Filename: ClsFERasterPlot.cpp
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Fri Oct 26 02:13:54 2001
 ** $Date: 2004/02/05 10:57:33 $
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#include <ClsDragDropDeEncoder.h>
#include <ClsFESystemManager.h>
#include <ClsQDivider.h>
#include <qboxlayout.h>
#include <qcolor.h>
#include <qevent.h>
#include <qflags.h>
#include <qframe.h>
#include <qgridlayout.h>
#include <qicon.h>
#include <qmimedata.h>
#include <qmutex.h>
#include <qnamespace.h>
#include <qpixmap.h>
#include <qpoint.h>
#include <qsize.h>
#include <qsizepolicy.h>
#include <qsplitter.h>
#include <qstring.h>
#include <qtoolbutton.h>
#include <iostream>
#include <map>
#include <utility>

#include "ClsFEDataClient.h"
#include "ClsFEDataSink.h"
#include "ClsFEGroup.h"
#include "ClsFERasterPlot.h"
#include "ClsQBaseStateVariableDisplay.h"
#include "ClsQNeuronStateVariableDisplay.h"
#include "idGenerator.hpp"
#include "neuronManager.hpp"
#include "wBarPlot.h"
#include "wGraphAxis.h"
#include "wRange.h"
#include "wRasterPlot.h"

class ClsFEDataManager;

namespace {
#include <icon_rasterplot_32x32.xpm>
#include <zoom.xpm>
}

ClsFERasterPlot::ClsFERasterPlot(ClsFEDataManager *_clsFEDataManager,
                                 QMutex *_qmutexSysGUI, string _strDataClientID)
    : ClsFEDataClient(_clsFEDataManager, _qmutexSysGUI, _strDataClientID) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::ClsFERasterPlot(...)" << endl;
#endif
  strTitle = "Raster Plot";

  setWindowTitle(strTitle.c_str());

  setClientType(CLIENT_RASTERPLOT);
  iTraceLength = 1000;
  this->setWindowIcon(QPixmap(icon_rasterplot));
  setAcceptDrops(true);
  QColor qc;
  qc.setHsv(0, 255, 210);

  qSplitter = new QSplitter(Qt::Vertical, this);
  qSplitter->setSizePolicy(
      QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

  wRasterPlot = new WRasterPlot(this, nullptr, WGraphXAxis::bottom,
                                WGraphYAxis::left, 0, false);
  wRasterPlot->showGrid(true);
  wRasterPlot->setYLabel("neuron nr.");
  wRasterPlot->setZoomEnabled(false);
  wRasterPlot->setAutoDisableZoom(false);
  wRasterPlot->setWrapAround(false);
  wRasterPlot->setTraceLength(iTraceLength);
  wRasterPlot->setXDataRange(WRange(0., iTraceLength / 2));
  wRasterPlot->setXVisRange(
      WRange(iTraceLength / 2 - iTraceLength / 20, iTraceLength / 2));
  wRasterPlot->setMinimumWidth(200);
  wRasterPlot->addTrace(QString(""), qc);
  qSplitter->addWidget(wRasterPlot);

  wBarPlot = new WBarPlot(qSplitter, nullptr, WGraphXAxis::bottom,
                          WGraphYAxis::left, 0, false);
  wBarPlot->showGrid(true);
  wBarPlot->setYLabel("avg.");
  wBarPlot->setZoomEnabled(false);
  wBarPlot->setAutoDisableZoom(false);
  wBarPlot->setWrapAround(false);
  wBarPlot->setTraceLength(iTraceLength);
  wBarPlot->setBarGap(0);
  wBarPlot->setXDataRange(WRange(0., iTraceLength / 2));
  wBarPlot->setXVisRange(
      WRange(iTraceLength / 2 - iTraceLength / 20, iTraceLength / 2));
  wBarPlot->setMinimumSize(200, 50);
  wBarPlot->addTrace(QString(""), qc);
  qSplitter->addWidget(wBarPlot);

  boxlayoutBase->addWidget(qSplitter, 0, 0, 1, 2);

  zoomButton = new QToolButton(this);
  boxlayoutBase->addWidget(zoomButton, 1, 0, 1, 1,
                           Qt::AlignBottom | Qt::AlignLeft);

  zoomButton->setAutoRaise(true);
  zoomButton->setIcon(QPixmap(zoom));
  zoomButton->setSizePolicy(
      QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
  zoomButton->setCheckable(true);

  xLabel = new WGraphAxisLabel(this, WGraphAxisLabel::horizontal,
                               QString("time/cycles"));
  boxlayoutBase->addWidget(xLabel, 1, 1);

  boxlayoutBase->removeWidget(clsQDivider);
  boxlayoutBase->addWidget(clsQDivider, 0, 2, 2, 1);

  qframeStateVariableDisplays = new QFrame(this);
  boxlayoutStateFrames =
      new QHBoxLayout(qframeStateVariableDisplays /*zzz,0,0*/);
  boxlayoutBase->addWidget(qframeStateVariableDisplays, 0, 3, 2, 1);

  connect(zoomButton, SIGNAL(toggled(bool)), this, SLOT(setZoomEnabled(bool)));
  connect(zoomButton, SIGNAL(toggled(bool)), this, SLOT(setZoomEnabled(bool)));
  connect(wRasterPlot, SIGNAL(xVisRangeChanged(WRange)), wBarPlot,
          SLOT(setXVisRange(WRange)));
  connect(wBarPlot, SIGNAL(xVisRangeChanged(WRange)), wRasterPlot,
          SLOT(setXVisRange(WRange)));

  QWidget::show();
};

ClsFERasterPlot::~ClsFERasterPlot() {
  wRasterPlot->removeTraces();
  wBarPlot->removeTraces();
}

string ClsFERasterPlot::addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                                string /*strRange*/) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::addStateVariableDisplay(ClsFEGroup* _clsFEGroup, "
          "string /* strRange */)" << endl;
#endif

  strGroupID = _clsFEGroup->getGroupID();
  string strDisplayID =
      _clsFEGroup->getGroupID() + ClsIDGenerator::Instance()->Next();
  strTitle = "Raster Plot for \"" + _clsFEGroup->getGroupName() + "\"";
  setWindowTitle(strTitle.c_str());

  if (clsQBaseStateVariableDisplay != nullptr) {
    boxlayoutBase->removeWidget(clsQBaseStateVariableDisplay);
  }

  clsQBaseStateVariableDisplay =
      new ClsQNeuronStateVariableDisplay(this, strDisplayID, this, _clsFEGroup,
                                         "", false, false, true, false, false);
  connect(dynamic_cast<ClsQNeuronStateVariableDisplay *>(
              clsQBaseStateVariableDisplay),
          SIGNAL(addDisplay(string, list<string>, string)), this,
          SLOT(addStateVariableDisplay(string, list<string>, string)));
  boxlayoutStateFrames->addWidget(clsQBaseStateVariableDisplay, 0,
                                  Qt::AlignTop);

  wRasterPlot->setYDataWidth(_clsFEGroup->getNumberOfNeurons());

  return strDisplayID;
};

string ClsFERasterPlot::addStateVariableDisplay(ClsFEGroup *_clsFEGroup,
                                                list<string> lstSelectedStates,
                                                string /*strRange*/) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::addStateVariableDisplay(ClsFEGroup* _clsFEGroup, "
          "list<string> lstSelectedStates, string strRange )" << endl;
#endif

  strGroupID = _clsFEGroup->getGroupID();
  string strDisplayID =
      _clsFEGroup->getGroupID() + ClsIDGenerator::Instance()->Next();
  strTitle = "Raster Plot for \"" + _clsFEGroup->getGroupName() + "\"";
  setWindowTitle(strTitle.c_str());

  if (clsQBaseStateVariableDisplay != nullptr) {
    boxlayoutBase->removeWidget(clsQBaseStateVariableDisplay);
  }

  clsQBaseStateVariableDisplay =
      new ClsQNeuronStateVariableDisplay(this, strDisplayID, this, _clsFEGroup,
                                         "", false, false, true, false, false);
  dynamic_cast<ClsQNeuronStateVariableDisplay *>(clsQBaseStateVariableDisplay)
      ->setSelectedStates(lstSelectedStates);
  connect(dynamic_cast<ClsQNeuronStateVariableDisplay *>(
              clsQBaseStateVariableDisplay),
          SIGNAL(addDisplay(string, list<string>, string)), this,
          SLOT(addStateVariableDisplay(string, list<string>, string)));
  clsQBaseStateVariableDisplay->slotLiveData(true);
  boxlayoutStateFrames->addWidget(clsQBaseStateVariableDisplay, 0,
                                  Qt::AlignTop);

  wRasterPlot->setYDataWidth(_clsFEGroup->getNumberOfNeurons());

  return strDisplayID;
};

string ClsFERasterPlot::addStateVariableDisplay(string _strGroupID,
                                                list<string> lstSelectedStates,
                                                string strRange) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::addStateVariableDisplay(string _strGroupID, "
          "list<string> lstSelectedStates, string strRange)" << endl;
#endif
  strGroupID = _strGroupID;
  strRange = "";
  if (ClsFESystemManager::Instance()->getFEGroup(_strGroupID) != nullptr) {
    return addStateVariableDisplay(
        ClsFESystemManager::Instance()->getFEGroup(_strGroupID),
        lstSelectedStates, "");
  }
  return "";
};

void ClsFERasterPlot::plotData() {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::plotData()" << endl;
#endif

  const int movingAvgWidth = 10;
  float *data;

#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::mapDataSinks.size(): " << mapDataSinks.size()
       << endl;
#endif

  if (mapDataSinks.size() > 0) {
    ClsDataSinkCopying *clsDataSink = mapDataSinks.begin()->second;

    clsDataSink->update();
    double fAverage = clsDataSink->average();
    wRasterPlot->setCurrentValues(clsDataSink->getStateArray());
    fMovingAverage += fAverage;

    if (iRoundCounter >= movingAvgWidth && wBarPlot->traceData(0).size() > 0) {
      data = wBarPlot->traceData(0).data();
      if (wBarPlot->getCursorPos() - movingAvgWidth < 0) {
        for (int i = 0; i < wBarPlot->getCursorPos() - 1; i++) {
          data[i] = fMovingAverage / movingAvgWidth;
        }
        for (int i = wBarPlot->traceData(0).size() + wBarPlot->getCursorPos() -
                     movingAvgWidth;
             i < wBarPlot->traceData(0).size(); i++) {
          data[i] = fMovingAverage / movingAvgWidth;
        }
      } else {
        for (int i = 1; i < movingAvgWidth; i++) {
          data[wBarPlot->getCursorPos() - i] = fMovingAverage / movingAvgWidth;
        }
      }
      iRoundCounter = 0;
      fMovingAverage = 0.0;
    }
    wBarPlot->setCurrentValue(0, 0);
    iRoundCounter++;
  }
  wRasterPlot->updateGraph();
  wRasterPlot->incCursorPos();
  wBarPlot->updateGraph();
  wBarPlot->incCursorPos();
};

void ClsFERasterPlot::dragEnterEvent(QDragEnterEvent *event) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::dragEnterEvent(QDragEnterEvent* event)" << endl;
#endif
  if (event->mimeData()->hasFormat("text/iqr-plot")) {
    event->acceptProposedAction();
  }
}

void ClsFERasterPlot::dropEvent(QDropEvent *event) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::dropEvent(QDropEvent* event)" << endl;
#endif

  if (event->mimeData()->hasFormat("text/iqr-plot")) {
    QString text = event->mimeData()->data("text/iqr-plot");

    cout << "drop text: " << text.toStdString() << endl;
    dropInfo di = ClsDragDropDeEncoder::decode(text.toStdString());
    unsigned int iItemType = di.ItemType;
    if (iItemType == ClsFESystemManager::ITEM_GROUP) {
      string strGroupID = di.ItemID;
      string strParams = di.Params;
      string strRange = di.Range;

      list<string> lst;
      string::size_type pos;
      while ((pos = strParams.find(";")) != string::npos) {
        lst.push_back(strParams.substr(0, pos));
        strParams.erase(0, pos + 1);
      }
      clsQBaseStateVariableDisplay->slotClose();
      addStateVariableDisplay(
          ClsFESystemManager::Instance()->getFEGroup(strGroupID), lst,
          strRange);
    }
  }
};

void ClsFERasterPlot::init() {
  fMovingAverage = 0.0;
  iRoundCounter = 0;
  ClsFEDataClient::init();
}

void ClsFERasterPlot::update() {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::update()" << endl;
#endif

  plotData();
};

void ClsFERasterPlot::DataSinkAdded(string /* strID */, int /* iColor */) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::DataSinkAdded(string strID)" << endl;
#endif
};

void ClsFERasterPlot::DataSinkRemoved(string /* strID */) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::DataSinkRemoved(string strID)" << endl;
#endif
};

void ClsFERasterPlot::closeEvent(QCloseEvent *ce) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::closeEvent( QCloseEvent* ce )" << endl;
#endif
  if (clsQBaseStateVariableDisplay != nullptr) {
    clsQBaseStateVariableDisplay->close();
    delete clsQBaseStateVariableDisplay;
    clsQBaseStateVariableDisplay = nullptr;
  }

  emit sigPlotClosed(strDataClientID);
  ce->accept();
}

void ClsFERasterPlot::setConfig(ClsDataClientConfig clsDataClientConfig) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::setConfig(ClsDataClientConfig clsDataClientConfig)"
       << endl;
#endif
  list<ClsStateVariableDisplayConfig> lstSVDConfigs =
      clsDataClientConfig.getListStateVariableDisplayConfig();
  list<ClsStateVariableDisplayConfig>::iterator itSVD;

  for (itSVD = lstSVDConfigs.begin(); itSVD != lstSVDConfigs.end(); ++itSVD) {

    string _strGroupID = (*itSVD).getItemID();
    string strSelectedIndices = (*itSVD).getSelectedIndices();

    if (ClsFESystemManager::Instance()->getFEGroup(_strGroupID) != nullptr) {
      addStateVariableDisplay(
          ClsFESystemManager::Instance()->getFEGroup(_strGroupID),
          strSelectedIndices);
      if (clsQBaseStateVariableDisplay != nullptr) {
        clsQBaseStateVariableDisplay->setConfig((*itSVD));
      }
    }
  }
}

ClsDataClientConfig ClsFERasterPlot::getConfig() {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::getConfig()" << endl;
#endif
  ClsDataClientConfig clsDataClientConfig(strDataClientID);
  clsDataClientConfig.setPosition(this->pos().x(), this->pos().y());
  clsDataClientConfig.setGeometry(this->size().width(), this->size().height());

  if (clsQBaseStateVariableDisplay != nullptr) {
    ClsStateVariableDisplayConfig clsStateVariableDisplayConfig =
        clsQBaseStateVariableDisplay->getConfig();
    clsDataClientConfig.addStateVariableDisplayConfig(
        clsStateVariableDisplayConfig);
  }
  return clsDataClientConfig;
}

void ClsFERasterPlot::groupChanged(string _strGroupID) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::groupChanged(string strGroupID)" << endl;
#endif

  if (!strGroupID.compare(_strGroupID)) {
    ClsFEGroup *_clsFEGroup =
        ClsFESystemManager::Instance()->getFEGroup(_strGroupID);
    wRasterPlot->setYDataWidth(_clsFEGroup->getNumberOfNeurons());
    wRasterPlot->updateGraph();

    if (!_strGroupID.compare(clsQBaseStateVariableDisplay->getItemID())) {
      dynamic_cast<ClsQNeuronStateVariableDisplay *>(
          clsQBaseStateVariableDisplay)->groupChanged();
    }
    ClsFEDataClient::groupChanged(_strGroupID);
  }
};

void ClsFERasterPlot::groupDeleted(string strID) {
#ifdef DEBUG_CLSFERASTERPLOT
  cout << "ClsFERasterPlot::groupDeleted(string strID)" << endl;
#endif

  if (!strGroupID.compare(strID)) {
    QWidget::close();
  }
}

void ClsFERasterPlot::slotHideControls(bool bIsRight) {
  if (bIsRight && qframeStateVariableDisplays != nullptr) {
    qframeStateVariableDisplays->hide();
  } else {
    qframeStateVariableDisplays->show();
  }
};

void ClsFERasterPlot::setZoomEnabled(bool zoom = true) {
  wRasterPlot->setZoomEnabled(zoom);
  wBarPlot->setZoomEnabled(zoom);
}

//// Local Variables:
//// mode: c++
//// compile-command: "cd ../../ && make -k "
//// End:
