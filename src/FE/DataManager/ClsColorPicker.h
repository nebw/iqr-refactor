/****************************************************************************
 ** $Filename: ClsColorPicker.h
 ** $Header$
 **
 ** $Author: Ulysses Bernardet
 **
 ** $CreateDate: Wed Dec  5 12:48:30 2001
 ** $Date$
 **
 ** $Keywords:
 ** $Description:
 **
 *****************************************************************************/

#ifndef CLSCOLORPICKER_H
#define CLSCOLORPICKER_H /*+ To stop multiple inclusions. +*/

#include <stack>

using namespace std;

class ClsColorPicker {

public:
  ClsColorPicker() : iMin(0), iMax(360), iDiff(30) {};

  int getColor() {
    int iColor;
    if (stackColors.size() == 1) {
      iColor = stackColors.top() + iDiff;
      if (iColor > iMax)
        iColor = iMin;
      stackColors.top() = iColor;
    } else if (stackColors.size() == 0) {
      iColor = iMin;
      stackColors.push(iColor);
    } else if (stackColors.size() > 1) {
      iColor = stackColors.top();
      stackColors.pop();
    }
    return iColor;
  };

  void putColor(int _iColor) {
    stackColors.push(_iColor);
  };

  void reset() {
    while (!stackColors.empty()) {
      stackColors.pop();
    }
  };

private:
  int iMin, iMax, iDiff;
  stack<int> stackColors;
};

#endif /* CLSCOLORPICKER_H */
