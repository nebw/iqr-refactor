#include <iostream>
#include "remotectrlimpl.h"

#include <qpushbutton.h>
#include <qlineedit.h>
#include <qcolordialog.h>
#include <qimage.h>
//--#include <q3listbox.h>
#include <qradiobutton.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qfile.h>
  //--#include <q3textstream.h>
#include <qmessagebox.h>
#include <QFileDialog>
  
#include <QTextStream>


#include "CmdBuildImpl.h"


using namespace std;

RemoteCtrlImpl::RemoteCtrlImpl( ) {

    setupUi(this); 
    
    iPort = iDefaultPort;

    
    qsSocket = new QUdpSocket (NULL);

    connect(qsSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(errorOccured(QAbstractSocket::SocketError)));

    qstrHostname = "127.0.0.1";
    leHostname->setText(qstrHostname);
    spbPort->setValue(iPort);

    connect( sFile, SIGNAL(clicked()), SLOT(sendFile()) );
    connect( sText, SIGNAL(clicked()), SLOT(sendText()) );
    connect( sEntry, SIGNAL(clicked()), SLOT(sendEntry()) );
    connect( pbConnect, SIGNAL(clicked()), this, SLOT(testConnect()));


    connect(listBoxHistory, SIGNAL(itemDoubleClicked(QListWidgetItem*)), SLOT(slotRetrieve(QListWidgetItem*)));

    connect(cmdBuildImpl, SIGNAL(sigCommand(QString)), SLOT(slotCommand(QString)));

    listBoxHistory->clear();
    lblConnected->setText("");

}

void RemoteCtrlImpl::errorOccured(QAbstractSocket::SocketError){
    cout << "RemoteCtrlImpl::errorOccured( int ): " << qsSocket->errorString().toStdString() << endl;
    string strError = "Error occured: " + qsSocket->errorString().toStdString();
    lblConnected->setText(strError.c_str());
//--    lblConnected->setPaletteForegroundColor(Qt::red);
}



void RemoteCtrlImpl::sendPacket( QString qstrMessage ) {
    lblConnected->setText("");
    qhaAddress.setAddress (leHostname->text() );
    iPort = spbPort->value();    
    qsSocket->writeDatagram ( qstrMessage.toLatin1(), qstrMessage.length(), qhaAddress, iPort );
    cout << "host: " << leHostname->text().toStdString() << ", port: " << iPort << endl;
}

void RemoteCtrlImpl::sendFile() {
    QString qstrFilename = QFileDialog::getOpenFileName( this, QString::null, "All Files (*)");

     QFile f2( qstrFilename );
     if(!f2.open( QIODevice::ReadOnly | QIODevice::Text )){
	 cerr << "error opening file" << endl;
	 QMessageBox::warning( this, "Error", "Could not open file \"" + qstrFilename + "\"");
	 return;
     }

     QTextStream t( &f2 );

     while ( !t.atEnd() ) {
	 sendPacket(  t.readLine() );
     }
}

void RemoteCtrlImpl::sendText() {
    listBoxHistory->insertItem(0, textToSend->text());
    sendPacket( textToSend->text() );
    textToSend->clear();
}


void RemoteCtrlImpl::sendEntry() {
//    cout << "RemoteCtrlImpl::sendEntry()" << endl;
//    cout << "listBoxHistory->currentItem(): " << listBoxHistory->currentItem() << endl;
    if(listBoxHistory->currentItem() >= 0){
	sendPacket( listBoxHistory->currentItem()->text () );
    }
}


void RemoteCtrlImpl::slotRetrieve(QListWidgetItem* qlbi){
    textToSend->setText(qlbi->text());

}

void RemoteCtrlImpl::slotCommand(QString qstr){
    textToSend->setText(qstr);

}

void RemoteCtrlImpl::testConnect(){
    sendPacket("");
}


