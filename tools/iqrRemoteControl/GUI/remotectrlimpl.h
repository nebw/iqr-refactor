#ifndef REMOTECTRLIMPL_H
#define REMOTECTRLIMPL_H

#include <QUdpSocket>
#include <QHostAddress>


#include "ui_remotectrl.h"

using namespace Ui;


static const quint16 iDefaultPort = 54923;

class RemoteCtrlImpl : public QDialog, public Ui::RemoteCtrl {
    Q_OBJECT

public:
    RemoteCtrlImpl(  );

private slots:
    void sendFile();
    void sendText();
    void sendEntry();
    void errorOccured(QAbstractSocket::SocketError);
    void slotRetrieve(QListWidgetItem*);
    void slotCommand(QString);
    void testConnect();


private:
    void sendPacket( QString qstrMessage );

    QString qstrHostname;

    QUdpSocket* qsSocket;
    QHostAddress qhaAddress;

    unsigned int iPort;
};

#endif // REMOTECTRLIMPL_H


//// Local Variables:
//// mode: c++
//// End:
