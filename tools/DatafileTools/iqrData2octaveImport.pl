#!/usr/bin/perl

# the purpose of this perl script is to create a specific octave import script for the iqr
# data file passed as an argument.
#
# Author: Ulysses Bernardet
# Create Date: 2006/01/07

if(@ARGV!=1){
    $c = @ARGV;
    die "ERROR: Wrong number of arguments ($c).\nUsage: iqrData2octaveImport.pl <infile>\n\n";
}


$infile = $ARGV[0];



@parameters = `grep -e \"\\#\" $infile`;
$range = `grep -e cycle $infile`;
$range =~ s/cycle;//;

$nrcomment_lines = @parameters + 1;
$nrlines =  `wc -l < $infile`;
$nrlines =~ s/\n//;

#****

#we need to go through this because the IDs that are with the names do not contain the state!
@ranges = split(";", $range);

$lastIDState = "";
foreach $line(@ranges){
    $line =~ s/\n//;
    $_ = $line;
    m/(L-\d*-\d*-\d*)(_)(.*)(_)/;
    $state= $3;
    $ID = $1;
    $IDState = $ID . "_" . $state;
    if($IDState ne $lastIDState){
#	print $ID . "\t" . $IDState . "\n";
	push (@IDStates, $IDState);
	push (@NameStates, $state);
    }
    $lastIDState = $IDState;
}


$ii=-999;
$jj=0;
foreach $line(@parameters){
    $line =~ s/\n//;
    $line =~ s/\#\s//;
    $_ = $line;
#    ($a, $b) = split("=", $line);
    m/(.*)(=)(.*)/;
    $name = $1;
#    $ID = $3;
    $ID = $IDStates[$jj];
    $name =~ s/\"//g;
    $name =~ s/\s/_/g;
    $name =~ s/\//_/g;

#    print "$name \t\t\t $ID \t $IDStates[$jj]\n";
    $id2name{$ID} = $name . "_" . $NameStates[$jj];
    $id2start{$ID} = $ii;
    $id2width{$ID} = 0;
    $ii++;
    $jj++;
}


$colmax = 0;
$linecounter = 0;
foreach $line(@ranges){
    $line =~ s/\n//;
    $_ = $line;
    m/(L-\d*-\d*-\d*_.*)(_.*)/;
    $key = $1;
    if($id2start{$key}<0){
	$id2start{$key} = $linecounter +2;
    }

    $id2width{$key}++;
#    print $linecounter . "\t" . $line . "\t" . $id2start{$key} . "\t" . $id2width{$key} . "\n";
    $linecounter++;

    $colend = $id2start{$key} + $id2width{$key} -1;
    $colmax = ($colend > $colmax ? $colend : $colmax)

}

#****

$outfile = $infile;
$outfile =~ s/\.dat//g;
$outfile =~ s/\./_/g;
$dataname = $outfile;
$scriptname = $outfile . "_import";
$outfile = $outfile . "_import.m";

open (FH, ">$outfile");
print FH "function [data]=$scriptname()\n";
print FH "inputData=dlmread(\'$infile\', \';\', [$nrcomment_lines, 0, $nrlines, $colmax]);\n";
print FH "data.cycles=inputData(: ,1:1);\n";






@IDarray = keys(%id2name);
foreach $id(@IDarray){

    $name = $id2name{$id};
    $start = $id2start{$id};
    $width = $start + $id2width{$id} -1;

    print FH "data.$name=inputData(: ,$start:$width);\n";

}

print FH "data.nrrows = size(inputData,1);\n";
print FH "data.name = \'$dataname\';\n";


close(FH);



# loop script:
$outfile = "loopScript.m";
if(-e $outfile){
    print "\"$outfile\" found in directory; skipping creation\n";
} else {
    print "Creating \"$outfile\"\n";
    open (FH, ">$outfile");
    
    print FH " 
    files = dir(\'*.dat\');
    for ff=1:size(files,1)
	nn=regexp(files(ff).name, \'(\\w*)\', \'match\');
    basename = nn(1);
    script_filename = strcat(basename, \'_import.m\');
    cmd = [\'test -e \' char(script_filename)];
    if system(cmd)
	disp(\'import script does not exist: running converter:\')
	cmd = [\'iqrData2octaveImport.pl \' char(files(ff).name)]
	system(cmd)
	end
	function_name = char(strcat(basename, \'_import\'));
%        function_name=eval(function_name);
        eval(strcat(char(basename), '=', function_name));
    end
    clear cmd ff files nn function_name script_filename basename
    ";
    
    close(FH);
}

